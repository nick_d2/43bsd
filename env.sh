export ROOT=`pwd`

export SCRIPTS=$ROOT/scripts
export STAGE0=$ROOT/stage0
export STAGE1=$ROOT/stage1

export INCLUDE=$STAGE0/usr/include
export PATH=$SCRIPTS:$PATH
