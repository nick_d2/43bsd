#ifndef _SYS_MOUNT_H_
#define _SYS_MOUNT_H_

#include <sys/param.h>
/*#include <sys/types.h> sys/param.h*/

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mount.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Mount structure.
 * One allocated on every mount.
 * Used to find the super block.
 */
struct	mount {
	dev_t	m_dev;		/* device mounted */
	struct	buf *m_bufp;	/* pointer to superblock */
	struct	inode *m_inodp;	/* pointer to mounted on inode */
	struct	inode *m_qinod;	/* QUOTA: pointer to quota file */
};
#ifdef KERNEL
struct	mount mount[NMOUNT];
#endif

#ifndef KERNEL
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* sys/mount.c */
int mount __P((char *s, char *n, int f));

/* sys/umount.c */
int umount __P((char *s));
#endif

#endif
