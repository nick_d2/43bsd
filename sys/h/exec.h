#ifndef _SYS_EXEC_H_
#define _SYS_EXEC_H_

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)exec.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Header prepended to each a.out file.
 */
struct exec {
	long	a_magic;	/* magic number */
unsigned long	a_text;		/* size of text segment */
unsigned long	a_data;		/* size of initialized data */
unsigned long	a_bss;		/* size of uninitialized data */
unsigned long	a_syms;		/* size of symbol table */
unsigned long	a_entry;	/* entry point */
unsigned long	a_trsize;	/* size of text relocation */
unsigned long	a_drsize;	/* size of data relocation */
};

#define	OMAGIC	0407		/* old impure format */
#define	NMAGIC	0410		/* read-only text */
#define	ZMAGIC	0413		/* demand load format */

#ifndef KERNEL
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* gen/execvp.c */
int execlp __P((char *name, ...));
int execvp __P((char *name, char **argv));

/* gen/system.c */
int system __P((char *s));

/* sys/execl.c */
int execl __P((char *f, ...));

/* sys/execle.c */
int execle __P((char *f, ...));

/* sys/exect.c */
int exect __P((char *s, char *v[], char *e[]));

/* sys/execv.c */
int execv __P((char *s, char *v[]));

/* sys/execve.c */
int execve __P((char *s, char *v[], char *e[]));
#endif

#endif
