#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_

/*#include <sys/types.h> time.h*/

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)time.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structure returned by gettimeofday(2) system call,
 * and used in other calls.
 */
struct timeval {
	long	tv_sec;		/* seconds */
	long	tv_usec;	/* and microseconds */
};

struct timezone {
	int	tz_minuteswest;	/* minutes west of Greenwich */
	int	tz_dsttime;	/* type of dst correction */
};
#define	DST_NONE	0	/* not on dst */
#define	DST_USA		1	/* USA style dst */
#define	DST_AUST	2	/* Australian style dst */
#define	DST_WET		3	/* Western European dst */
#define	DST_MET		4	/* Middle European dst */
#define	DST_EET		5	/* Eastern European dst */
#define	DST_CAN		6	/* Canada */

/*
 * Operations on timevals.
 *
 * NB: timercmp does not work for >= or <=.
 */
#define	timerisset(tvp)		((tvp)->tv_sec || (tvp)->tv_usec)
#define	timercmp(tvp, uvp, cmp)	\
	((tvp)->tv_sec cmp (uvp)->tv_sec || \
	 (tvp)->tv_sec == (uvp)->tv_sec && (tvp)->tv_usec cmp (uvp)->tv_usec)
#define	timerclear(tvp)		(tvp)->tv_sec = (tvp)->tv_usec = 0

/*
 * Names of the interval timers, and structure
 * defining a timer setting.
 */
#define	ITIMER_REAL	0
#define	ITIMER_VIRTUAL	1
#define	ITIMER_PROF	2

struct	itimerval {
	struct	timeval it_interval;	/* timer interval */
	struct	timeval it_value;	/* current value */
};

#ifndef KERNEL
#include <time.h>
#endif

#ifndef KERNEL
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* compat-4.1/utime.c */
int utime __P((char *name, int otv[]));

/* gen/alarm.c */
int alarm __P((int secs));

/* gen/sleep.c */
void sleep __P((unsigned n));

/* gen/time.c */
long time __P((time_t *t));

/* gen/ualarm.c */
unsigned ualarm __P((register unsigned usecs, register unsigned reload));

/* gen/usleep.c */
void usleep __P((unsigned n));

/* sys/adjtime.c */
int adjtime __P((struct timeval *delta, struct timeval *odelta));

/* sys/getitimer.c */
int getitimer __P((int w, struct itimerval *v));

/* sys/gettimeofday.c */
int gettimeofday __P((struct timeval *t, struct timezone *z));

/* sys/setitimer.c */
int setitimer __P((int w, struct itimerval *v, struct itimerval *ov));

/* sys/settimeofday.c */
int settimeofday __P((struct timeval *t, struct timezone *z));

/* sys/utimes.c */
int utimes __P((char *f, struct timeval t[2]));
#endif

#endif
