#ifndef _SYS_SELECT_H_
#define _SYS_SELECT_H_

#include <strings.h>
#include <sys/param.h>
#include <sys/time.h>

/*#include <sys/param.h>*/

/* moved this from sys/types.h */
/*
 * Select uses bit masks of file descriptors in longs.
 * These macros manipulate such bit fields (the filesystem macros use chars).
 * FD_SETSIZE may be defined by the user, but the default here
 * should be >= NOFILE (param.h).
 */
#ifndef	FD_SETSIZE
#define	FD_SETSIZE	256
#endif

typedef long	fd_mask;
#define NFDBITS	(sizeof(fd_mask) * NBBY)	/* bits per mask */
/* this was duplicated in sys/param.h, get it from there */
/*#ifndef howmany*/
/*#define	howmany(x, y)	(((x)+((y)-1))/(y))*/
/*#endif*/

typedef	struct fd_set {
	fd_mask	fds_bits[howmany(FD_SETSIZE, NFDBITS)];
} fd_set;

#define	FD_SET(n, p)	((p)->fds_bits[(n)/NFDBITS] |= (1 << ((n) % NFDBITS)))
#define	FD_CLR(n, p)	((p)->fds_bits[(n)/NFDBITS] &= ~(1 << ((n) % NFDBITS)))
#define	FD_ISSET(n, p)	((p)->fds_bits[(n)/NFDBITS] & (1 << ((n) % NFDBITS)))
#define FD_ZERO(p)	bzero((char *)(p), sizeof(*(p)))

#ifndef KERNEL
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* sys/select.c */
int select __P((int n, fd_set *r, fd_set *w, fd_set *e, struct timeval *t));
#endif

#endif
