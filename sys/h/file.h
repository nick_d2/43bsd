#ifndef _SYS_FILE_H_
#define _SYS_FILE_H_

#include <sys/errno.h>
#include <sys/param.h>
/*#include <sys/types.h> sys/param.h*/
#ifdef __STDC__
#include <stdarg.h>
#endif

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)file.h	7.1 (Berkeley) 6/4/86
 */

#ifdef KERNEL
/*
 * Descriptor table entry.
 * One for each kernel object.
 */
struct	file {
	int	f_flag;		/* see below */
	short	f_type;		/* descriptor type */
	short	f_count;	/* reference count */
	short	f_msgcount;	/* references from message queue */
	struct	fileops {
		int	(*fo_rw)();
		int	(*fo_ioctl)();
		int	(*fo_select)();
		int	(*fo_close)();
	} *f_ops;
	caddr_t	f_data;		/* inode */
	off_t	f_offset;
};

struct	file *file, *fileNFILE;
int	nfile;
/*struct	file *getf();*/
/*struct	file *falloc();*/
#endif

/*
 * flags- also for fcntl call.
 */
#define	FOPEN		(-1)
#define	FREAD		00001		/* descriptor read/receive'able */
#define	FWRITE		00002		/* descriptor write/send'able */
#ifndef	F_DUPFD
#define	FNDELAY		00004		/* no delay */
#define	FAPPEND		00010		/* append on each write */
#endif
#define	FMARK		00020		/* mark during gc() */
#define	FDEFER		00040		/* defer for next gc pass */
#ifndef	F_DUPFD
#define	FASYNC		00100		/* signal pgrp when data ready */
#endif
#define	FSHLOCK		00200		/* shared lock present */
#define	FEXLOCK		00400		/* exclusive lock present */

/* bits to save after open */
#define	FMASK		00113
#define	FCNTLCANT	(FREAD|FWRITE|FMARK|FDEFER|FSHLOCK|FEXLOCK)

/* open only modes */
#define	FCREAT		01000		/* create if nonexistant */
#define	FTRUNC		02000		/* truncate to zero length */
#define	FEXCL		04000		/* error if already created */

#ifndef	F_DUPFD
/* fcntl(2) requests--from <fcntl.h> */
#define	F_DUPFD	0	/* Duplicate fildes */
#define	F_GETFD	1	/* Get fildes flags */
#define	F_SETFD	2	/* Set fildes flags */
#define	F_GETFL	3	/* Get file flags */
#define	F_SETFL	4	/* Set file flags */
#define	F_GETOWN 5	/* Get owner */
#define F_SETOWN 6	/* Set owner */
#endif

/*
 * User definitions.
 */

/*
 * Open call.
 */
#define	O_RDONLY	000		/* open for reading */
#define	O_WRONLY	001		/* open for writing */
#define	O_RDWR		002		/* open for read & write */
#define	O_NDELAY	FNDELAY		/* non-blocking open */
#define	O_APPEND	FAPPEND		/* append on each write */
#define	O_CREAT		FCREAT		/* open with file create */
#define	O_TRUNC		FTRUNC		/* open with truncation */
#define	O_EXCL		FEXCL		/* error on create if file exists */

/*
 * Flock call.
 */
#define	LOCK_SH		1	/* shared lock */
#define	LOCK_EX		2	/* exclusive lock */
#define	LOCK_NB		4	/* don't block when locking */
#define	LOCK_UN		8	/* unlock */

/*
 * Access call.
 */
#define	F_OK		0	/* does file exist */
#define	X_OK		1	/* is it executable by caller */
#define	W_OK		2	/* writable by caller */
#define	R_OK		4	/* readable by caller */

/*
 * Lseek call.
 */
#define	L_SET		0	/* absolute offset */
#define	L_INCR		1	/* relative to current offset */
#define	L_XTND		2	/* relative to end of file */

#ifdef KERNEL
#define	GETF(fp, fd) { \
	if ((unsigned)(fd) >= NOFILE || ((fp) = u.u_ofile[fd]) == NULL) { \
		u.u_error = EBADF; \
		return; \
	} \
}
#define	DTYPE_INODE	1	/* file */
#define	DTYPE_SOCKET	2	/* communications endpoint */
#endif

#ifndef KERNEL
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* sys/access.c */
int access __P((char *p, int m));

/* sys/close.c */
int close __P((int f));

/* sys/creat.c */
int creat __P((char *s, int m));

/* sys/dup.c */
int dup __P((int f));

/* sys/dup2.c */
int dup2 __P((int o, int n));

/* sys/fcntl.c */
int fcntl __P((int f, int c, int a));

/* sys/flock.c */
int flock __P((int f, int o));

/* sys/fsync.c */
int fsync __P((int f));

/* sys/ftruncate.c */
int ftruncate __P((int d, off_t l));

/* sys/getdtablesize.c */
int getdtablesize __P((void));

/* sys/link.c */
int link __P((char *a, char *b));

/* sys/lseek.c */
off_t lseek __P((int f, off_t o, int d));

/* sys/open.c */
int open __P((char *f, int m, ...));

/* sys/pipe.c */
int pipe __P((int f[2]));

/* sys/read.c */
int read __P((int f, void *b, int l));

/* sys/readlink.c */
int readlink __P((char *p, char *b, int s));

/* sys/rename.c */
int rename __P((char *f, char *t));

/* sys/symlink.c */
int symlink __P((char *t, char *f));

/* sys/sync.c */
void sync __P((void));

/* sys/truncate.c */
int truncate __P((char *p, off_t l));

/* sys/unlink.c */
int unlink __P((char *s));

/* sys/vopen.c */
int vopen __P((char *f, int m, va_list argp));

/* sys/write.c */
int write __P((int f, void *b, int l));
#endif

#endif
