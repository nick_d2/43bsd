#ifndef _C2_H_
#define _C2_H_

/*	c2.h	4.10	85/08/22	*/

/*
 * Header for object code improver
 */

#define	JBR	1
#define	CBR	2
#define	JMP	3
#define	LABEL	4
#define	DLABEL	5
#define	EROU	7
#define	JSW	9
#define	MOV	10
#define	CLR	11
#define	INC	12
#define	DEC	13
#define	TST	14
#define	PUSH	15
#define CVT 16
#define	CMP	17
#define	ADD	18
#define	SUB	19
#define	BIT	20
#define	BIC	21
#define	BIS	22
#define	XOR	23
#define	COM	24
#define	NEG	25
#define	MUL	26
#define	DIV	27
#define	ASH	28
#define EXTV	29
#define EXTZV	30
#define INSV	31
#define	CALLS	32
#define RET	33
#define	CASE	34
#define	SOB	35
#define	TEXT	36
#define	DATA	37
#define	BSS	38
#define	ALIGN	39
#define	END	40
#define MOVZ 41
#define WGEN 42
#define SOBGEQ 43
#define SOBGTR 44
#define AOBLEQ 45
#define AOBLSS 46
#define ACB 47
#define MOVA 48
#define PUSHA 49
#define LGEN 50
#define SET 51
#define MOVC3 52
#define RSB 53
#define JSB 54
#define MFPR 55
#define MTPR 56
#define PROBER 57
#define PROBEW 58
#define	LCOMM 59
#define	COMM 60

#define	JEQ	0
#define	JNE	1
#define	JLE	2
#define	JGE	3
#define	JLT	4
#define	JGT	5
/* rearranged for unsigned branches so that jxxu = jxx + 6 */
#define	JLOS	8
#define	JHIS	9
#define	JLO	10
#define	JHI	11

#define JBC 12
#define JBS 13
#define JLBC 14
#define JLBS 15
#define JBCC 16
#define JBSC 17
#define JBCS 18
#define JBSS 19

#define	JCC 20
#define	JCS 21
#define	JVC 22
#define	JVS 23

/*
 *	When the new opcodes were added, the relative
 *	ordering of the first 3 (those that are not float)
 *	had to be retained, so that other parts of the program
 *	were not broken.
 *
 *	In addition, the distance between OP3 and OP2 must be preserved.
 *	The order of definitions above OP2 must not be changed.
 *
 *	Note that these definitions DO NOT correspond to
 *	those definitions used in as, adb and sdb.
 */
#define	BYTE	1
#define	WORD	2
#define LONG	3
#define	FFLOAT	4
#define	DFLOAT	5
#define QUAD	6
#define OP2	7
#define OP3	8
#define OPB	9
#define OPX	10
#define	GFLOAT	11
#define	HFLOAT	12
#define OCTA	13

#define T(a,b) (a|((b)<<8))
#define U(a,b) (a|((b)<<4))

#define C2_ASIZE 128

extern struct optab {
	char	opstring[7];
	short	opcode;
} optab[];

struct node {
	union {
		struct {
			char op_op;
			char op_subop;
		} un_op;
		short	un_combop;
	} op_un;
	short	refc;
	struct	node	*forw;
	struct	node	*back;
	struct	node	*ref;
	char	*code;
	struct	optab	*pop;
	long	labno;
	short	seq;
};

#define op op_un.un_op.op_op
#define subop op_un.un_op.op_subop
#define combop op_un.un_combop

extern char	line[512];
extern struct	node	first;
extern char	*curlp;
extern int	nbrbr;
extern int	nsaddr;
extern int	redunm;
extern int	iaftbr;
extern int	njp1;
extern int	nrlab;
extern int	nxjump;
extern int	ncmot;
extern int	nrevbr;
extern int	loopiv;
extern int	nredunj;
extern int	nskip;
extern int	ncomj;
extern int	nsob;
extern int	nrtst;
extern int nbj;
extern int nfield;

extern int	nchange;
extern long	isn;
extern int	debug;
extern char	revbr[];
#define	NREG	12
extern char	*regs[NREG+5]; /* 0-11, 4 for operands, 1 for running off end */
extern char	conloc[C2_ASIZE];
extern char	conval[C2_ASIZE];
extern char	ccloc[C2_ASIZE];

#define	RT1	12
#define	RT2	13
#define RT3 14
#define RT4 15
#define	LABHS	127

/*char *copy();*/
/*long getnum();*/
/*struct node *codemove();*/
/*struct node *insertl();*/
/*struct node *nonlab();*/

#ifdef notdef
#define decref(p) \
	((p) && --(p)->refc <= 0 ? nrlab++, delnode(p) : 0)
#define delnode(p) \
	((p)->back->forw = (p)->forw, (p)->forw->back = (p)->back)
#endif

/*char *xalloc();*/
extern char *newa;
extern char *lasta;
extern char *lastr;
#define	XALIGN(n) \
		(((n)+(sizeof (char *) - 1)) & ~(sizeof (char *) - 1))
#define	alloc(n) \
		((struct node *) \
		 ((newa = lasta) + (n) > lastr ? \
			xalloc(n) : \
			(lasta += XALIGN(n), newa)))

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* c20.c */
char *xalloc __P((int n));
int main __P((int argc, char **argv));
int input __P((void));
struct optab *getline __P((void));
long getnum __P((register char *p));
int locuse __P((register char *p));
int locdef __P((register char *p));
void output __P((void));
char *copy __P((char *ap));
void opsetup __P((void));
struct optab *oplook __P((void));
void refcount __P((void));
void iterate __P((void));
void xjump __P((register struct node *p1));
struct node *insertl __P((register struct node *np));
struct node *codemove __P((struct node *ap));
void comjump __P((void));
void backjmp __P((struct node *ap1, struct node *ap2));

/* c21.c */
int redun3 __P((register struct node *p, int split));
void bmove __P((void));
void rmove __P((void));
char *byondrd __P((register struct node *p));
struct node *bflow __P((register struct node *p));
int ispow2 __P((register long n));
void bitopt __P((register struct node *p));
int isfield __P((register long n));
int bixprep __P((register struct node *p, int bix));
struct node *bicopt __P((register struct node *p));
int jumpsw __P((void));
void addsob __P((void));
int equop __P((register struct node *p1, struct node *p2));
void delnode __P((register struct node *p));
void decref __P((register struct node *p));
struct node *nonlab __P((struct node *ap));
void clearuse __P((void));
void clearreg __P((void));
void savereg __P((int ai, register char *s, int type));
void dest __P((register char *s, int type));
void splitrand __P((struct node *p));
int compat __P((int have, int want));
int equtype __P((int t1, int t2));
int findrand __P((char *as, int type));
int isreg __P((register char *s));
void check __P((void));
int source __P((char *ap));
void newcode __P((struct node *p));
void repladdr __P((struct node *p));
void redunbr __P((register struct node *p));
char *findcon __P((int i, int type));
int compare __P((int opc, char *acp1, char *acp2));
void setcon __P((register char *cv, register char *cl, int type));
int equstr __P((register char *p1, register char *p2));
void setcc __P((char *ap, int type));
int okio __P((register char *p));
int indexa __P((register char *p));
int natural __P((register char *p));
int isstatic __P((register char *cp));
int autoid __P((register char *p));

#endif
