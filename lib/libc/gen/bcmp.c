#include <strings.h>

/*
 * Copyright (c) 1987 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)bcmp.c	5.1 (Berkeley) 1/27/87";
#endif

/*
 * bcmp -- vax cmpc3 instruction
 */
int bcmp(_b1, _b2, length) void *_b1; void *_b2; register int length; {
#define b1 (*(char **)&_b1)
#define b2 (*(char **)&_b2)
	if (length == 0)
		return (0);
	do
		if (*b1++ != *b2++)
			break;
	while (--length);
	return(length);
#undef b1
#undef b2
}
