#include <gen.h>
#include <stdio.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)fakcu.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Null cleanup routine to resolve reference in exit() 
 * if not using stdio.
 */
void _cleanup() {
}
