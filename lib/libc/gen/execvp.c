#include <gen.h>
/*#include <strings.h> gen.h*/
/*#include <sys/errno.h> gen.h*/
/*#include <sys/exec.h> gen.h*/
/*#include <sys/time.h> gen.h*/
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)execvp.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 *	execlp(name, arg,...,0)	(like execl, but does path search)
 *	execvp(name, argv)	(like execv, but does path search)
 */
/*#include <errno.h>*/
/*#include <varargs.h>*/
#define	NULL	0

static	char shell[] =	"/bin/sh";
/*char	*execat(), *getenv();*/
extern	errno;

static char *execat __P((register char *s1, register char *s2, char *si));

#ifdef __STDC__
int execlp(char *name, ...)
#else
int execlp(name, va_alist) char *name; va_dcl
#endif
{
	va_list argp;
	int res;

	_va_start(argp, name);
	res = execvp(name, (char **)argp);
	va_end(argp);
	return res;
}

int execvp(name, argv) char *name; char **argv; {
	char *pathstr;
	register char *cp;
	char fname[128];
	char *newargs[256];
	int i;
	register unsigned etxtbsy = 1;
	register eacces = 0;

	if ((pathstr = getenv("PATH")) == NULL)
		pathstr = ":/bin:/usr/bin";
	cp = index(name, '/')? "": pathstr;

	do {
		cp = execat(cp, name, fname);
	retry:
		execv(fname, argv);
		switch(errno) {
		case ENOEXEC:
			newargs[0] = "sh";
			newargs[1] = fname;
			for (i=1; newargs[i+1]=argv[i]; i++) {
				if (i>=254) {
					errno = E2BIG;
					return(-1);
				}
			}
			execv(shell, newargs);
			return(-1);
		case ETXTBSY:
			if (++etxtbsy > 5)
				return(-1);
			sleep(etxtbsy);
			goto retry;
		case EACCES:
			eacces++;
			break;
		case ENOMEM:
		case E2BIG:
			return(-1);
		}
	} while (cp);
	if (eacces)
		errno = EACCES;
	return(-1);
}

static char *execat(s1, s2, si) register char *s1; register char *s2; char *si; {
	register char *s;

	s = si;
	while (*s1 && *s1 != ':')
		*s++ = *s1++;
	if (si != s)
		*s++ = '/';
	while (*s2)
		*s++ = *s2++;
	*s = '\0';
	return(*s1? ++s1: 0);
}
