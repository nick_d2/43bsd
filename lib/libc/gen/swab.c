#include <gen.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)swab.c	5.3 (Berkeley) 3/9/86";
#endif

/*
 * Swab bytes
 * Jeffrey Mogul, Stanford
 */

int swab(from, to, n) register char *from; register char *to; register int n; {
	register unsigned long temp;
	
	n >>= 1; n++;
#define	STEP	temp = *from++,*to++ = *from++,*to++ = temp
	/* round to multiple of 8 */
	while ((--n) & 07)
		STEP;
	n >>= 3;
	while (--n >= 0) {
		STEP; STEP; STEP; STEP;
		STEP; STEP; STEP; STEP;
	}
}
