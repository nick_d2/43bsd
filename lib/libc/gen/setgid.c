#include <sys/proc.h>

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)setgid.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Backwards compatible setgid.
 */
int setgid(gid) int gid; {

	return (setregid(gid, gid));
}
