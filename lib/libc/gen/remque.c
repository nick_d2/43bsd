#include <gen.h>

/*
 * Copyright (c) 1987 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)remque.c	5.1 (Berkeley) 1/27/87";
#endif

/*#include <gen.h>*/

/*
 * remque -- vax remque instruction
 *
 * NOTE: this implementation is non-atomic!!
 */

/* moved this to gen.h as it's shared by caller and by remque.c */
/*struct vaxque {*/		/* queue format expected by VAX queue instructions */
/*	struct vaxque	*vq_next;*/
/*	struct vaxque	*vq_prev;*/
/*};*/

int remque(e) register struct vaxque *e; {
	e->vq_prev->vq_next = e->vq_next;
	e->vq_next->vq_prev = e->vq_prev;
}
