#include <strings.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)strlen.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Returns the number of
 * non-NULL bytes in string argument.
 */

int strlen(s) register char *s; {
	register n;

	n = 0;
	while (*s++)
		n++;
	return(n);
}
