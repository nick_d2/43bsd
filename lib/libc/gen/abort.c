#include <gen.h>
/*#include <sys/proc.h> gen.h*/
/*#include <sys/signal.h> gen.h*/

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)abort.c	5.3 (Berkeley) 3/9/86";
#endif

/* C library -- abort */

/*#include "signal.h"*/

int abort() {
	sigblock(~0);
	signal(SIGILL, SIG_DFL);
	sigsetmask(~sigmask(SIGILL));
	kill(getpid(), SIGILL);
}
