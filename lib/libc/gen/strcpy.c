#include <strings.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)strcpy.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Copy string s2 to s1.  s1 must be large enough.
 * return s1
 */

char *strcpy(s1, s2) register char *s1; register char *s2; {
	register char *os1;

	os1 = s1;
	while (*s1++ = *s2++)
		;
	return(os1);
}
