#include <gen.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)getenv.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 *	getenv(name)
 *	returns ptr to value associated with name, if any, else NULL
 */
#define NULL	0
extern	char **environ;
/*char	*nvmatch();*/

static char *nvmatch __P((register char *s1, register char *s2));

char *getenv(name) register char *name; {
	register char **p = environ;
	register char *v;

	while (*p != NULL)
		if ((v = nvmatch(name, *p++)) != NULL)
			return(v);
	return(NULL);
}

/*
 *	s1 is either name, or name=value
 *	s2 is name=value
 *	if names match, return value of s2, else NULL
 *	used for environment searching: see getenv
 */

static char *nvmatch(s1, s2) register char *s1; register char *s2; {

	while (*s1 == *s2++)
		if (*s1++ == '=')
			return(s2);
	if (*s1 == '\0' && *(s2-1) == '=')
		return(s2);
	return(NULL);
}
