#include <gen.h>
/*#include <strings.h> gen.h*/

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)calloc.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Calloc - allocate and clear memory block
 */
void *calloc(num, size) register unsigned num; register unsigned size; {
	extern void *malloc();
	register void *p;

	size *= num;
	if (p = malloc(size))
		bzero(p, size);
	return (p);
}

int cfree(p) void *p; {
	free(p);
}
