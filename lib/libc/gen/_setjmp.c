#include <gen.h>
/*#include <setjmp.h> gen.h*/
#include <sys/file.h>

/*#include <setjmp.h>*/

int _setjmp(e) jmp_buf e; {
	write(2, "_setjmp()\n", 10);
	abort();
}
void _longjmp(e, v) jmp_buf e; int v; {
	write(2, "_longjmp()\n", 11);
	abort();
}
