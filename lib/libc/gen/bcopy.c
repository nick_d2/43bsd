#include <strings.h>
#include <sys/types.h>

#ifdef __STDC__
#include <stdint.h>
#else
typedef int intptr_t;
#endif

/*
 * Copyright (c) 1987 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)bcopy.c	5.1 (Berkeley) 1/27/87";
#endif

/*
 * bcopy -- vax movc3 instruction
 */
int bcopy(_src, _dst, length) void *_src; void *_dst; register int length; {
#define src (*(char **)&_src)
#define dst (*(char **)&_dst)
	if (length && src != dst)
		if (/*(u_int)*/dst < /*(u_int)*/src)
			if (((intptr_t)src | (intptr_t)dst | length) & 3)
				do	/* copy by bytes */
					*dst++ = *src++;
				while (--length);
			else {
				length >>= 2;
				do	/* copy by longs */
					*(*(long **)&dst)++ = *(*(long **)&src)++;
				while (--length);
			}
		else {			/* copy backwards */
			src += length;
			dst += length;
			if (((intptr_t)src | (intptr_t)dst | length) & 3)
				do	/* copy by bytes */
					*--dst = *--src;
				while (--length);
			else {
				length >>= 2;
				do	/* copy by shorts */
					*--(*(long **)&dst) = *--(*(long **)&src);
				while (--length);
			}
		}
	return(0);
#undef src
#undef dst
}
