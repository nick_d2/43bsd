#include <sys/signal.h>

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)signal.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Almost backwards compatible signal.
 */
/*#include <signal.h>*/

void (*signal(s, a)) __P((int sig)) int s; void (*a) __P((int sig)); {
	struct sigvec osv, sv;
	static int mask[NSIG];
	static int flags[NSIG];

	sv.sv_handler = (void (*) __P((int sig, int code, struct sigcontext *scp)))a;
	sv.sv_mask = mask[s];
	sv.sv_flags = flags[s];
	if (sigvec(s, &sv, &osv) < 0)
		return (BADSIG);
	if (sv.sv_mask != osv.sv_mask || sv.sv_flags != osv.sv_flags) {
		mask[s] = sv.sv_mask = osv.sv_mask;
		flags[s] = sv.sv_flags = osv.sv_flags;
		if (sigvec(s, &sv, 0) < 0)
			return (BADSIG);
	}
	return ((void (*) __P((int sig)))osv.sv_handler);
}
