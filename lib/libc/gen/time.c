#include <sys/time.h>
/*#include <sys/types.h> sys/time.h*/

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)time.c	5.3 (Berkeley) 3/9/86";
#endif

/*
 * Backwards compatible time call.
 */
/*#include <sys/types.h>*/
/*#include <sys/time.h>*/

long time(t) time_t *t; {
	struct timeval tt;

	if (gettimeofday(&tt, (struct timezone *)0) < 0)
		return (-1);
	if (t)
		*t = tt.tv_sec;
	return (tt.tv_sec);
}
