#include <grp.h>
#include <strings.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)getgrnam.c	5.2 (Berkeley) 3/9/86";
#endif

/*#include <grp.h>*/

struct group *getgrnam(name) register char *name; {
	register struct group *p;
	struct group *getgrent();

	setgrent();
	while( (p = getgrent()) && strcmp(p->gr_name,name) );
	endgrent();
	return(p);
}
