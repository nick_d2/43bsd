#include <gen.h>
#include <sys.h>

#ifdef __STDC__
#include <stdint.h>
#else
typedef int intptr_t;
#endif

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)valloc.c	5.2 (Berkeley) 3/9/86";
#endif

/*void	*malloc();*/

void *valloc(i) int i; {
	int valsiz = getpagesize();
	intptr_t j;
	char *cp = malloc(i + (valsiz-1));

	j = ((intptr_t)cp + (valsiz-1)) &~ (valsiz-1);
	return ((char *)j);
}
