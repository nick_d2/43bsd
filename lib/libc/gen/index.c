#include <strings.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)index.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Return the ptr in sp at which the character c appears;
 * NULL if not found
 */

#define	NULL	0

char *index(sp, c) register char *sp; int c; {
	do {
		if (*sp == c)
			return(sp);
	} while (*sp++);
	return(NULL);
}
