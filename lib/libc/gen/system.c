#include <sys/exec.h>
#include <sys/proc.h>
#include <sys/signal.h>
#include <sys/wait.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)system.c	5.2 (Berkeley) 3/9/86";
#endif

/*#include	<signal.h>*/

int system(s) char *s; {
	int status, pid, w;
	register void (*istat) __P((int sig)), (*qstat) __P((int sig));

	if ((pid = vfork()) == 0) {
		execl("/bin/sh", "sh", "-c", s, 0);
		_exit(127);
	}
	istat = signal(SIGINT, SIG_IGN);
	qstat = signal(SIGQUIT, SIG_IGN);
	while ((w = wait(&status)) != pid && w != -1)
		;
	if (w == -1)
		status = -1;
	signal(SIGINT, istat);
	signal(SIGQUIT, qstat);
	return(status);
}
