#include <gen.h>
/*#include <setjmp.h> gen.h*/
#include <sys/file.h>

/*#include <setjmp.h>*/

int setjmp(e) jmp_buf e; {
	write(2, "setjmp()\n", 9);
	abort();
}
void longjmp(e, v) jmp_buf e; int v; {
	write(2, "longjmp()\n", 10);
	abort();
}
