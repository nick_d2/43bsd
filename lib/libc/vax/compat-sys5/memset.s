/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#ifdef LIBC_SCCS
	.asciz	"@(#)memset.s	5.3 (Berkeley) 3/9/86"
#endif LIBC_SCCS

/* memset(base, fill, length) */

#include "DEFS.h"

ENTRY(memset, 0)
	movl	4(ap),r3
	jbr	2f
1:
	subl2	r0,12(ap)
	movc5	$0,(r3),8(ap),r0,(r3)
2:
	movzwl	$65535,r0
	cmpl	12(ap),r0
	jgtr	1b
	movc5	$0,(r3),8(ap),12(ap),(r3)
	ret
