/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#ifdef LIBC_SCCS
	.asciz	"@(#)memcpy.s	5.3 (Berkeley) 3/9/86"
#endif LIBC_SCCS

/* memcpy(to, from, size) */

#include "DEFS.h"

ENTRY(memcpy, R6)
	movl	4(ap),r3
	movl	8(ap),r1
	movl	12(ap),r6
	jbr	2f
1:
	subl2	r0,r6
	movc3	r0,(r1),(r3)
2:
	movzwl	$65535,r0
	cmpl	r6,r0
	jgtr	1b
	movc3	r6,(r1),(r3)
	ret
