/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#ifdef LIBC_SCCS
	.asciz	"@(#)memcmp.s	5.3 (Berkeley) 3/9/86"
#endif LIBC_SCCS

/* memcmp(s1, s2, n) */

#include "DEFS.h"

ENTRY(memcmp, 0)
	movl	4(ap),r1
	movl	8(ap),r3
	movl	12(ap),r4
1:
	movzwl	$65535,r0
	cmpl	r4,r0
	jleq	3f
	subl2	r0,r4
	cmpc3	r0,(r1),(r3)
	jeql	1b
2:
	subb3	(r1),(r3),r0	# r0 = *s1 - *s2
	cvtbl	r0,r0
	ret
3:
	cmpc3	r4,(r1),(r3)
	jneq	2b
	ret
