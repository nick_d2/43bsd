#!/bin/sh

#ROOT=`dirname $0`/..
ROOT=../..
ROOT=`cd $ROOT && pwd`
INCLUDE=$ROOT/cross/usr/include
SCRIPTS=$ROOT/scripts

#all_c=`echo *.c`
#if test "$all_c" = "*.c"
#then
#  all_c=
#fi
#all_c=`find . -type f -name '*.c' -print |sed -e 's:^\./::' |LC_ALL=C sort`
all_c=`find . -type f -name '*.c' -print |sed -e 's:^\./::' |grep -v '^vax/' |LC_ALL=C sort`

if test -z "$1" || test $1 -eq 0
then
  echo "===stage 0==="

  #rm -f *.[ch].allprotos
  #rm -f *.[ch].nocomm
  #rm -f *.[ch].oldprotos
  #rm -f *.[ch].protos
  #rm -f *.[ch].protos.nocomm
  #rm -f *.[ch].usedby
  #rm -f *.[ch].uses
  #rm -rf .xify
  rm -f `find . -name '*.[ch].allprotos' -print`
  rm -f `find . -name '*.[ch].nocomm' -print`
  rm -f `find . -name '*.[ch].oldprotos' -print`
  rm -f `find . -name '*.[ch].protos' -print`
  rm -f `find . -name '*.[ch].protos.nocomm' -print`
  rm -f `find . -name '*.[ch].usedby' -print`
  rm -f `find . -name '*.[ch].uses' -print`
  rm -rf `find . -name .xify -print`

  rm -f `find $INCLUDE/g -name '*.h.allprotos' -print`
  rm -f `find $INCLUDE/g -name '*.h.nocomm' -print`
  rm -f `find $INCLUDE/g -name '*.h.oldprotos' -print`
  rm -f `find $INCLUDE/g -name '*.h.protos' -print`
  rm -f `find $INCLUDE/g -name '*.h.protos.nocomm' -print`
  rm -f `find $INCLUDE/g -name '*.h.usedby' -print`
  rm -f `find $INCLUDE/g -name '*.h.uses' -print`
  rm -rf `find $INCLUDE/g -name .xify -print`

  rm -f a b conflicts.temp oldprotos.txt oldprotos.temp xx*

  if test "$1" = 000
  then
    #git checkout *.c *.h
    rm -rf $ROOT/include $ROOT/sys
    git checkout $ROOT/include $ROOT/sys $all_c
    rm -rf $INCLUDE/g
    mkdir -p $INCLUDE/g
    (cd $ROOT/include && $SCRIPTS/make.sh SHARED=copies install)
  fi
fi

#all_c=`echo *.c`
#if test "$all_c" = "*.c"
#then
#  all_c=
#fi
#all_c=`find . -type f -name '*.c' -print |sed -e 's:^\./::' |LC_ALL=C sort`
all_c=`find . -type f -name '*.c' -print |sed -e 's:^\./::' |grep -v '^vax/' |LC_ALL=C sort`

#all_h=`echo *.h`
#if test "$all_h" = "*.h"
#then
#  all_h=
#fi
#all_h=`find . -type f -name '*.h' -print |sed -e 's:^\./::' |LC_ALL=C sort`
all_h=`find $INCLUDE/g -type f -name '*.h' -print |LC_ALL=C sort`

#std_h=`find $INCLUDE/g -type f -name '*.h' -print |grep -v "^$ROOT/cross/usr/include/\(stand\|vaxif\|vaxmba\|vaxuba\)/" |LC_ALL=C sort`
std_h=

if test -z "$1" || test $1 -eq 1
then
  echo "===stage 1==="
 
  for i in $all_c $all_h
  do
    echo "i=$i"

    grep -H '^[A-Za-z_][0-9A-Za-z_]*[^0-9A-Za-z_][^;]*[A-Za-z_][0-9A-Za-z_]*[	 ]*([	 ]*)[	 ]*\(,[^;]*\)\?;' $i >>oldprotos.txt
    sed -e 's/^[A-Za-z_][0-9A-Za-z_]*[^0-9A-Za-z_][^;]*[A-Za-z_][0-9A-Za-z_]*[	 ]*([	 ]*)[	 ]*\(,[^;]*\)\?;/\/\*&\*\//' -i $i

    sed -e 's/^\(#[	 ]*\(else\|endif\)\)\([^0-9A-Za-z_].*\)\?/\1/' -i $i

    #rm -f xx*
    #csplit -b '%05d' -q $i '/^#[	 ]*ifndef[	 ]*lint$/' '{*}'
    #for j in xx*
    #do
    #  if test $j != xx00000 && sed -ne '2p' $j |grep -q 'char[	 ]*\(copyright[	 ]*\[[	 ]*\]\|\*[	 ]*copyright\)[	 ]*='
    #  then
    #    sed -e 's/^\(#[	 ]*if\)ndef\([	 ]*\)lint$/\1\2defined(DOCOPYRIGHT) \&\& !defined(lint)/' -i $j
    #  fi
    #  if test $j != xx00000 && sed -ne '2p' $j |grep -q 'char[	 ]*\(sccsid[	 ]*\[[	 ]*\]\|\*[	 ]*sccsid\)[	 ]*='
    #  then
    #    sed -e 's/^\(#[	 ]*if\)ndef\([	 ]*\)lint$/\1\2defined(DOSCCS) \&\& !defined(lint)/' -i $j
    #  else
    #    sed -e 's/^.*char[	 ]*\(sccsid[	 ]*\[[	 ]*\]\|\*[	 ]*sccsid\)[	 ]*=.*$/#if defined(DOSCCS) \&\& !defined(lint)\n&\n#endif/' -i $j
    #  fi
    #done
    #cat xx* >$i
 
    rm -f xx*
    csplit -b '%05d' -q $i '/^[	 ]*{[	 ]*$/' '{*}'
    for j in xx*
    do
      :
    done
    for k in xx*
    do
      if test $k != xx00000
      then
        sed -ne '2,$p' -i $k
      fi
      if test $k != $j
      then
        sed -e '$s/$/ {/' -i $k
      fi
    done
    cat xx* >$i
  done

  grep '\.h:' oldprotos.txt >oldprotos.temp

  sed -e 's/^[A-Za-z_].*__P.*;$/\/\*AAA&\*\//' -i $std_h
  mv $INCLUDE/g/varargs.h $ROOT/cross/usr/include/varargs.h.save
  echo "#define va_list char *" >$INCLUDE/g/varargs.h
  echo "#define va_list char *" >$INCLUDE/g/stdarg.h
  for i in $all_c
  do
    echo "i=$i"

    #group=
    group=$INCLUDE/g/`dirname $i`.h
    touch $group
    if test -f groups.txt
    then
      j=`sed -ne "s:^$i \\(.*\\):\\1:p" groups.txt`
      if test -n "$j"
      then
        j=$INCLUDE/g/$j
        echo $j >>$group.uses
        group=$j
        touch $group
      fi
    fi
    echo "group=$group"

    $ROOT/cproto-4.6/cproto -i$INCLUDE/g -Dvax -t -H -s $i |sed -ne '2,$p' >$i.allprotos
    if ! grep -qv '^int main __P((' $i.allprotos
    then
      echo -n >$i.allprotos
    fi

    grep "^static " <$i.allprotos >a
    if test -s a
    then
      mv a $i.protos
    fi
 
    grep -v "^static " <$i.allprotos >a
    if test -s a
    then
      if test -z "$group"
      then
        cp $i.allprotos $i.protos
      else
        (
          cat <<EOF

/* $i */
EOF
          cat a
        ) >>$group.protos
      fi

      protos="`sed -ne 's/^.*[^0-9A-Za-z_]\([A-Za-z_][0-9A-Za-z_]*\) __P((.*))\\( NORETURN\\)\\?;$/\1/p' a`"
      echo "protos=$protos"

      pattern=
      prefix=
      for j in $protos
      do
        pattern="$pattern$prefix$j"
        prefix='\|'
      done
      echo "pattern=$pattern"

      grep "[^0-9A-Za-z_]\\($pattern\\)[	 ]*(" oldprotos.temp >a
      if test -s a
      then
        mv a $i.oldprotos
        grep -v "^$group:" $i.oldprotos
      fi
    fi
  done
  rm $INCLUDE/g/stdarg.h
  mv $INCLUDE/g/varargs.h.save $ROOT/cross/usr/include/varargs.h
  sed -e 's/^\/\*AAA\(.*\)\*\/$/\1/' -i $std_h
fi

#all_h=`echo *.h`
#if test "$all_h" = "*.h"
#then
#  all_h=
#fi
#all_h=`find . -type f -name '*.h' -print |sed -e 's:^\./::' |LC_ALL=C sort`
all_h=`find $INCLUDE/g -type f -name '*.h' -print |LC_ALL=C sort`

if test -z "$1" || test $1 -eq 2
then
  echo "===stage 2==="
 
  all_c_nocomm=
  all_c_protos_nocomm=
  for i in $all_c
  do
    $SCRIPTS/nocomment <$i |$SCRIPTS/nostring >$i.nocomm
    all_c_nocomm="$all_c_nocomm $i.nocomm"
    if test -f $i.protos
    then
      $SCRIPTS/nocomment <$i.protos |$SCRIPTS/nostring >$i.protos.nocomm
      all_c_protos_nocomm="$all_c_protos_nocomm $i.protos.nocomm"
    fi
  done

  all_h_nocomm=
  all_h_protos_nocomm=
  for i in $all_h
  do
    $SCRIPTS/nocomment <$i |$SCRIPTS/nostring >$i.nocomm
    all_h_nocomm="$all_h_nocomm $i.nocomm"
    if test -f $i.protos
    then
      $SCRIPTS/nocomment <$i.protos |$SCRIPTS/nostring >$i.protos.nocomm
      all_h_protos_nocomm="$all_h_protos_nocomm $i.protos.nocomm"
    fi
  done

  for i in $std_h
  do
    $SCRIPTS/nocomment <$i |$SCRIPTS/nostring >$i.nocomm
  done

  if test -f conflicts.txt
  then
    sed -e 's/^/:/;s/ /::/g;s/$/:/' <conflicts.txt >conflicts.temp
  else
    touch conflicts.temp
  fi

  for i in $all_h $std_h
  do
    echo "i=$i"
    h=`echo $i |sed -e "s:^$INCLUDE/g/::"`
    echo "h=$h"

    macros="`sed -ne 's/^#[	 ]*define[	 ]\+\([A-Za-z_][0-9A-Za-z_]*\).*/\1/p' $i.nocomm |grep -v '^\(NULL\|__P\)$'`"
    echo "macros=$macros"
    typedefs="`sed -ne 's/^\(.*[^0-9A-Za-z_]\)\?typedef[^0-9A-Za-z_]\(.*[^0-9A-Za-z_]\)\?\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*\(\[[	 0-9]*\][	 ]*\)\?;.*/\3/p' $i.nocomm; sed -ne 's/^}[	 ]*\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*;.*/\1/p' $i.nocomm`"
    echo "typedefs=$typedefs"
    structs="`sed -ne 's/^\(.*[^0-9A-Za-z_]\)\?struct[	 ]\+\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*{.*/\2/p' $i.nocomm`"
    echo "structs=$structs"
    unions="`sed -ne 's/^\(.*[^0-9A-Za-z_]\)\?union[	 ]\+\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*{.*/\2/p' $i.nocomm`"
    echo "unions=$unions"
    if test -f $i.protos.nocomm
    then
      protos="`sed -ne 's/^.*[^0-9A-Za-z_]\([A-Za-z_][0-9A-Za-z_]*\) __P((.*))\\( NORETURN\\)\\?;$/\1/p' $i.nocomm $i.protos.nocomm`"
    else
      protos="`sed -ne 's/^.*[^0-9A-Za-z_]\([A-Za-z_][0-9A-Za-z_]*\) __P((.*))\\( NORETURN\\)\\?;$/\1/p' $i.nocomm`"
    fi
    echo "protos=$protos"

    rm -f $i.usedby

    prefix0=
    pattern0=

    pattern1=
    prefix1=
    for j in $macros
    do
      pattern1="$pattern1$prefix1$j"
      prefix1='\|'
    done
    for j in $typedefs
    do
      pattern1="$pattern1$prefix1$j"
      prefix1='\|'
    done
    for j in $structs
    do
      pattern1="$pattern1${prefix1}struct[	 ]\\+$j"
      prefix1='\|'
    done
    for j in $unions
    do
      pattern1="$pattern1${prefix1}union[	 ]\\+$j"
      prefix1='\|'
    done
    echo "pattern1=$pattern1"
    if test -n "$pattern1"
    then
      pattern0="$pattern0$prefix0\\(.*[^0-9A-Za-z_]\\)\\?\\($pattern1\\)\\([^0-9A-Za-z_].*\\)\\?"
      prefix0='\|'
    fi

    pattern1=
    prefix1=
    for j in $protos
    do
      pattern1="$pattern1$prefix1$j"
      prefix1='\|'
    done
    echo "pattern1=$pattern1"
    if test -n "$pattern1"
    then
      pattern0="$pattern0$prefix0\\(.*[^0-9A-Za-z_]\\)\\?\\($pattern1\\)[	 ]*(.*"
      prefix0='\|'
    fi

    echo "pattern0=$pattern0"

    if test -n "$pattern0" && test -n "$all_c_nocomm$all_c_protos_nocomm$all_h_protos_nocomm"
    then
      grep -H "^\\($pattern0\\)$" $all_c_nocomm $all_c_protos_nocomm $all_h_protos_nocomm |grep -v "^$i\.protos.nocomm:" >a
      if test -s a
      then
        cat a >>$i.usedby
        for j in `sed -e 's/\.protos\.nocomm:.*//; s/\.nocomm:.*//' <a |sort |uniq`
        do
          echo "j=$j"
          k=`echo $j |sed -e "s:^$INCLUDE/g/::"`
          echo "k=$k"
          if ! grep -q ":$h:.*:$k\\|:$k:.*:$h:" conflicts.temp
          then
            echo $i >>$j.uses
          fi
        done
      fi
    fi

    pattern0=
    prefix0=

    pattern1=
    prefix1=
    for j in $macros
    do
      pattern1="$pattern1$prefix1$j"
      prefix1='\|'
    done
    for j in $typedefs
    do
      pattern1="$pattern1$prefix1$j"
      prefix1='\|'
    done
    echo "pattern1=$pattern1"
    if test -n "$pattern1"
    then
      pattern0="$pattern0$prefix0\\(.*[^0-9A-Za-z_]\\)\\?\\($pattern1\\)\\([^0-9A-Za-z_].*\\)\\?"
      prefix0='\|'
    fi

    pattern1=
    prefix1=
    for j in $structs
    do
      pattern1="$pattern1${prefix1}struct[	 ]\\+$j"
      prefix1='\|'
    done
    for j in $unions
    do
      pattern1="$pattern1${prefix1}union[	 ]\\+$j"
      prefix1='\|'
    done
    echo "pattern1=$pattern1"
    if test -n "$pattern1"
    then
      pattern0="$pattern0$prefix0\\(.*[^0-9A-Za-z_]\\)\\?\\($pattern1\\)\\([^	 *0-9A-Za-z_].*\\|[	 ]\\+\\([^*].*\\)\\?\\)\\?"
      prefix0='\|'
    fi

    pattern1=
    prefix1=
    for j in $protos
    do
      pattern1="$pattern1$prefix1$j"
      prefix1='\|'
    done
    echo "pattern1=$pattern1"
    if test -n "$pattern1"
    then
      pattern0="$pattern0$prefix0\\(.*[^0-9A-Za-z_]\\)\\?\\($pattern1\\)[	 ]*(.*"
      prefix0='\|'
    fi

    echo "pattern0=$pattern0"

    if test -n "$pattern0" && test -n "$all_h_nocomm"
    then
      grep -H "^\\($pattern0\\)$" $all_h_nocomm |grep -v "^$i\.nocomm:" >a
      if test -s a
      then
        cat a >>$i.usedby
        for j in `sed -e 's/\.nocomm:.*//' <a |sort |uniq`
        do
          echo "j=$j"
          k=`echo $j |sed -e "s:^$INCLUDE/g/::"`
          echo "k=$k"
          if ! grep -q ":$h:.*:$k\\|:$k:.*:$h:" conflicts.temp
          then
            echo $i >>$j.uses
          fi
        done
      fi
    fi
  done
fi

if test -z "$1" || test $1 -eq 3
then
  echo "===stage 3==="

  for i in $all_h
  do
    echo "i=$i"
    rm -f xx*
    csplit -b '%05d' -q $i '/^#/' '{*}'
    if test -f xx00001 && ! $SCRIPTS/nocomment <xx00000 |grep -q '[^	 ]'
    then
      for j in xx*
      do
        :
      done
      echo "j=$j"
      define="`sed -ne 's/^#[	 ]*ifndef[	 ]\+\([A-Za-z_][0-9A-Za-z_]*\).*/\1/p' xx00001`"
      echo "define=$define"
      if test -n "$define" && grep -q '^#[	 ]*endif$' $j && ! grep -v "^#" $j |$SCRIPTS/nocomment |grep -q '[^	 ]'
      then
        sed -e "s/^#[	 ]*ifndef[	 ]\\+$define/\\/\\*&\\*\\//" -i xx00001
        sed -e "s/^\\(#[	 ]*define[	 ]\\+$define\\)[	 ]*$/\\/\\*\1\\*\\//" -i xx00002
        sed -e 's/^#[	 ]*endif$/\/\*&\*\//' -i $j
        cat xx* >a
        iflevel=0
        for j in `sed -ne 's/^#[	 ]*\(if\|ifdef\|ifndef\)[^0-9A-Za-z_].*/1/p; s/^#[	 ]*endif$/-1/p' a`
        do
          iflevel=`expr $iflevel + $j`
          if test $iflevel -lt 0
          then
            break
          fi
        done
        echo "iflevel=$iflevel"
        if test $iflevel -eq 0
        then
          mv a $i
        fi
      fi
    fi
  done

  for i in $all_c $all_h
  do
    echo "i=$i"
    rm -f xx*
    csplit -b '%05d' -q $i '/^#[	 ]*include[^0-9A-Za-z_]/' '{*}'
    for j in xx*
    do
      sed -e 's/^#[	 ]*include[	 ]*\(<[^>]*>\|"[^"]*"\)/\/\*&\*\//' -i $j
      if grep -v "^#[	 ]*include[^0-9A-Za-z_]" $j |sed -e '/^#[	 ]*if.*SCCS\|lint/,/^#[	 ]*endif/s/.*//' |$SCRIPTS/nocomment |grep -q '[^	 ]'
      then
        break
      fi
    done
    cat xx* >$i
  done

  for i in $all_c
  do
    echo "i=$i"

    if test -s $i.protos
    then
      pattern="`sed -ne '1{s/\*/\\\*/g; s/ __P((.*));/(/p}' $i.allprotos`"
      echo "pattern=$pattern"
      rm -f xx*
      csplit -q $i "/^$pattern/"
      (
        cat xx00
        if test `wc -l <$i.protos` -ge `wc -l <$i.allprotos`
        then
          cat <<EOF
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

EOF
        fi
        cat $i.protos
        if test -f xx01
        then
          echo
          cat xx01
        fi
      ) >$i
    fi

    rm -f a
    if test -s $i.uses
    then
      LC_ALL=C sort <$i.uses |uniq >a

      pattern=
      prefix=
      for j in `sed -e "s:^$INCLUDE/g/::" a`
      do
        pattern="$pattern$prefix$j"
        prefix='\|'
      done
      echo "pattern=$pattern"

      #sed -e "s:^#[	 ]*include[	 ]*\\(<\\($pattern\\)>\\|\"\\($pattern\\)\"\\):\\/\\*&\\*\\/:" -i $i
      sed -e "s:^#[	 ]*include[	 ]*<\\($pattern\\)>:\\/\\*&\\*\\/:" -i $i
    fi

    (
      if test -s a
      then
        sed -e "s:.*:#include \"&\":; s:\"$INCLUDE/g/\\(.*\\)\":<\\1>:; s:^#include <varargs.h>$:#ifdef __STDC__\\n#include <stdarg.h>\\n#define _va_start(argp, arg) va_start(argp, arg)\\n#else\\n#include <varargs.h>\\n#define _va_start(argp, arg) va_start(argp)\\n#endif:" a
        echo
      fi
      sed -e 's/\([^0-9A-Za-z_]\)va_start(argp);/\1_va_start(argp, fmt);/' $i
    ) |$SCRIPTS/newline >b
    mv b $i
  done

  for i in $all_h
  do
    echo "i=$i"
    h=`echo $i |sed -e "s:^$INCLUDE/g/::"`
    echo "h=$h"

    rm -f a
    if test -s $i.uses
    then
      LC_ALL=C sort <$i.uses |uniq >a

      pattern=
      prefix=
      for j in `sed -e "s:^$INCLUDE/g/::" a`
      do
        pattern="$pattern$prefix$j"
        prefix='\|'
      done
      echo "pattern=$pattern"

      #sed -e "s:^#[	 ]*include[	 ]*\\(<\\($pattern\\)>\\|\"\\($pattern\\)\"\\):\\/\\*&\\*\\/:" -i $i
      sed -e "s:^#[	 ]*include[	 ]*<\\($pattern\\)>:\\/\\*&\\*\\/:" -i $i
    fi

    name=_`echo $h |tr '.\-/abcdefghijklmnopqrstuvwxyz' '___ABCDEFGHIJKLMNOPQRSTUVWXYZ'`_
    echo "name=$name"
    (
      cat <<EOF
#ifndef $name
#define $name

EOF
      if test -s a
      then
        sed -e "s:.*:#include \"&\":; s:\"$INCLUDE/g/\\(.*\\)\":<\\1>:; s:^#include <varargs.h>$:#ifdef __STDC__\\n#include <stdarg.h>\\n#endif:" a
        echo
      fi
      cat $i
      if test -s $i.protos
      then
        echo
        if echo $h |grep -q '^\(net\|netimp\|netinet\|netns\|sys\|vax\|vaxif\|vaxmba\|vaxuba\)/'
        then
          echo "#ifndef KERNEL"
        fi
        cat <<EOF
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif
EOF
        cat $i.protos
        if echo $h |grep -q '^\(net\|netimp\|netinet\|netns\|sys\|vax\|vaxif\|vaxmba\|vaxuba\)/'
        then
          echo "#endif"
        fi
      fi
      cat <<EOF

#endif
EOF
    ) |$SCRIPTS/newline >b
    mv b $i
  done
fi

if test -z "$1" || test $1 -eq 4
then
  echo "===stage 4==="

  # checking header loops
  if test -n "$all_h"
  then
    #grep -H '^#include \(<[^>]*>\|"[^"]*"\)' $all_h |\
    #sed -e 's:^\(.*\)\:#include \(<\([^>]*\)>\|"\([^"]*\)"\):\1 \3\4:' |\
    #tsort >a
    grep -H '^#include <[^>]*>' $all_h |\
    sed -e "s:^$INCLUDE/::; s:^\\(.*\\)\\:#include <\\([^>]*\\)>:\1 \2:" |\
    tsort >a
  fi

  # removing useless extra header inclusions that are pulled in anyway
  touch $INCLUDE/g/stdarg.h
  rm -f a
  for i in $all_h $std_h
  do
    echo "i=$i"
    h=`echo $i |sed -e "s:^$INCLUDE/g/::"`
    echo "h=$h"

    pattern=
    prefix=
    cp $i temp.c
    for j in `cpp -nostdinc -I$INCLUDE/g -Dvax -DNCMD=1 -DNRSP=1 -M temp.c`
    do
      echo "j=$j"
      case $j in
      $INCLUDE/g/stdarg.h)
        # system header files are not supposed to define stdarg.h, even if
        # (like vfprintf etc) they have va_list paramters, it is silly and
        # we ignore this restriction, but clients shouldn't rely on this
        # (anyway, the above cpp command defines __STDC__ so we do not pick
        # up any dependency on varargs.h, hence let's keep it symmetrical)
        ;;
      *.h)
        k=`echo $j |sed -e "s:^$INCLUDE/g/::"`
        echo "k=$k"
        echo "$h $k" >>a
        pattern="$pattern$prefix$k"
        prefix='\|'
        ;;
      esac
    done
    echo "pattern=$pattern"
    if test -n "$pattern"
    then
      grep -H "^#include \\(<$h>\\|\"$h\"\\)$" $all_c $all_h >b
      if test -s b
      then
        #sed -e "s:^#include \\(<\\($pattern\\)>\\|\"\\($pattern\\)\"\\)$:/\\*& $h\\*/:" -i `sed -e 's/:.*//' b`
        sed -e "s:^#include <\\($pattern\\)>$:/\\*& $h\\*/:" -i `sed -e 's/:.*//' b`
      fi
    fi
  done
  rm -f $INCLUDE/g/stdarg.h temp.c
  tsort <a >b
fi

if test -z "$1" || test $1 -eq 5
then
  echo "===stage 5==="
  for i in . arpa protocols
  do
    echo "i=$i"
    for j in $ROOT/include/$i/*.h
    do
      echo "j=$j"
      cp $INCLUDE/g/$i/`basename $j` $ROOT/include/$i
    done
  done
  for i in compat-4.1 compat-sys5 gen inet net ns stdio sys
  do
    echo "i=$i"
    cp $INCLUDE/g/$i.h $ROOT/include
  done
  for i in net netimp netinet netns vax vaxif vaxmba vaxuba
  do
    echo "i=$i"
    for j in $ROOT/sys/$i/*.h
    do
      echo "j=$j"
      cp $INCLUDE/g/$i/`basename $j` $ROOT/sys/$i
    done
  done
  for i in $ROOT/sys/h/*.h
  do
    echo "i=$i"
    cp $INCLUDE/g/sys/`basename $i` $ROOT/sys/h
  done
fi
