#include <compat-4.1.h>
#include <sys/signal.h>

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)pause.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Backwards compatible pause.
 */
int pause() {

	sigpause(sigblock(0));
}
