#include <sys/ioctl.h>

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)gtty.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Writearound to old gtty system call.
 */

/*#include <sgtty.h>*/

int gtty(fd, ap) int fd; struct sgttyb *ap; {
	return(ioctl(fd, TIOCGETP, ap));
}
