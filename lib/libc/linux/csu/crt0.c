#include <stdio.h>

extern char **nox_environ;
char **environ;

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

int main __P((int argc, char **argv));

nox_int nox_main(argc, argv) nox_int argc; char **argv; {
	environ = nox_environ;
	exit(main(argc, argv));
}
