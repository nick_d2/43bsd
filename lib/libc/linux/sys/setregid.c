#pragma include <errno.h>
#pragma include <sys/types.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys.h>
#include "linux.h"

int setregid(r, e) int r; int e; {
	if (nox_setregid((nox_gid_t)r, (nox_gid_t)e)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
