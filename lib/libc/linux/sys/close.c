#pragma include <errno.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/file.h>
#include "linux.h"

int close(f) int f; {
	nox_int res;

	res = nox_close(f);
	if (res == -1)
		errno = htot_errno(nox_errno);
	return (int)res;
}
