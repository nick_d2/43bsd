#pragma include <unistd.h>

#include <sys/proc.h>

void _exit(s) int s; {
	nox__exit((nox_int)s);
}
