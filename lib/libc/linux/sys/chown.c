#pragma include <errno.h>
#pragma include <unistd.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/stat.h>
#include "linux.h"

int chown(s, u, g) char *s; int u; int g; {
	if (nox_chown(s, (nox_uid_t)u, (nox_gid_t)g)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
