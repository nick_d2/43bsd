#pragma include <errno.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/proc.h>
#include "linux.h"

int brk(a) void *a; {
	if (nox_brk(a)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
