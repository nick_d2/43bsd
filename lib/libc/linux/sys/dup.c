#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include "linux.h"

int dup(f) int f; {
	int res;

	res = nox_dup((nox_int)f);
	if (res == (nox_int)-1)
		errno = htot_errno(nox_errno);
	return (int)res;
}
