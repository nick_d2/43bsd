#pragma include <errno.h>
#pragma include <sys/stat.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/stat.h>
#include "linux.h"

int chmod(s, m) char *s; int m; {
	if (nox_chmod(s, ttoh_mode(m))) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
