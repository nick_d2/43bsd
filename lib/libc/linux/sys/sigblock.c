#pragma include <stdlib.h>
#pragma include <sys/signal.h>
#pragma include <sys/types.h>
#pragma include <unistd.h>

#include <sys/signal.h>
#include <sys/types.h>
#include "linux.h"

int sigblock(m) int m; {
	nox_sigset_t newset;
	nox_sigset_t oldset;

	ttoh_sigmask(m, &newset);
	if (nox_sigprocmask(nox_SIG_BLOCK, &newset, &oldset)) {
		nox_write((nox_int)2, "sigblock(): sigprocmask()\n", 26);
		nox_abort();
	}
	return htot_sigmask(&oldset);
}
