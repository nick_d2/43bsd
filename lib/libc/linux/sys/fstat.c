#pragma include <errno.h>
#pragma include <sys/stat.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/stat.h>
#include "linux.h"

int fstat(f, b) int f; struct stat *b; {
	struct nox_stat statbuf;

	if (nox_fstat((nox_int)f, &statbuf)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	htot_stat(&statbuf, b);
	return 0;
}
