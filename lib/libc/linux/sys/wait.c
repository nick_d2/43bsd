#pragma include <errno.h>
#pragma include <stdlib.h>
#pragma include <sys/types.h>
#pragma include <sys/wait.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "linux.h"

int wait(s) int *s; {
	nox_pid_t res;
	nox_int stat;

	res = nox_wait(&stat);
	if (res == (nox_pid_t)-1) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	if (s)
		*s = htot_wait(stat);
	return (int)res;
}
