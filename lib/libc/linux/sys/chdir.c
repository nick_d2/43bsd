#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/proc.h>
#include "linux.h"

int chdir(s) char *s; {
	if (nox_chdir(s)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
