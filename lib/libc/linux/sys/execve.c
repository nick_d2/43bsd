#pragma include <errno.h>
#pragma include <stdlib.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/exec.h>
#include "linux.h"

int execve(s, v, e) char *s; char *v[]; char *e[]; {
	if (nox_execve(s, v, e)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	nox_write((nox_int)2, "execve(): no return\n", 20);
	nox_abort();
}
