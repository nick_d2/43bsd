#pragma include <errno.h>
#pragma include <sys/resource.h>
#pragma include <sys/types.h>
#pragma include <sys/wait.h>

#include <sys/errno.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "linux.h"

int wait3(s, o, r) int *s; int o; struct rusage *r; {
	int opts;
	nox_pid_t res;
	nox_int stat;
	struct nox_rusage ru;

	opts = 0;
	if (o & WNOHANG)
		opts |= nox_WNOHANG;
	if (o & WUNTRACED)
		opts |= nox_WUNTRACED;
	res = nox_wait3(&stat, opts, &ru);
	if (res == (nox_pid_t)-1) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	if (s)
		*s = htot_wait(stat);
	if (r)
		htot_rusage(&ru, r);
	return (int)res;
}
