#pragma include <errno.h>
#pragma include <stdlib.h>
#pragma include <unistd.h>
#pragma include <sys/types.h>

#include <sys/errno.h>
#include <sys/file.h>
#include <sys/types.h>
#include "linux.h"

off_t lseek(f, o, d) int f; off_t o; int d; {
	nox_int whence;
	nox_off_t res;

	switch (d) {
	case L_SET:
		whence = nox_SEEK_SET;
		break;
	case L_INCR:
		whence = nox_SEEK_CUR;
		break;
	case L_XTND:
		whence = nox_SEEK_END;
		break;
	default:
		nox_write((nox_int)2, "lseek(): invalid whence\n", 24);
		nox_abort();
	}
	res = nox_lseek(f, (nox_off_t)o, whence);
	if (res == (nox_off_t)-1)
		errno = htot_errno(nox_errno);
	return (off_t)res;
}
