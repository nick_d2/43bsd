#pragma include <errno.h>
#pragma include <unistd.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/file.h>
#include <sys/types.h>
#include "linux.h"

int ftruncate(d, l) int d; off_t l; {
	if (nox_ftruncate((nox_int)d, (nox_off_t)l)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
