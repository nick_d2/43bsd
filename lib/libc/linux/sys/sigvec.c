#pragma include <errno.h>
#pragma include <signal.h>
#pragma include <stdlib.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include <sys/signal.h>
#include "linux.h"

int sigvec(c, f, m) int c; struct sigvec *f; struct sigvec *m; {
	struct nox_sigaction act, *act_p;
	struct nox_sigaction oldact;

	act_p = 0;
	if (f) {
		act.nox_sa_sigaction = (void (*)(nox_int, nox_siginfo_t *, void *))f->sv_handler; /* fix this later */
		ttoh_sigmask(f->sv_mask, &act.nox_sa_mask);
		act.nox_sa_flags = nox_SA_RESTART | nox_SA_SIGINFO;
		if (f->sv_flags & SV_ONSTACK)
			act.nox_sa_flags |= nox_SA_ONSTACK;
		if (f->sv_flags & SV_INTERRUPT) {
			nox_write((nox_int)2, "sigvec(): SV_INTERRUPT\n", 23);
			nox_abort();
		}
		act_p = &act;
	}
	if (c < 0 || c >= NSIG) {
		nox_write((nox_int)2, "sigvec(): out of range\n", 23);
		nox_abort();
	}
	if (nox_sigaction(ttoh_signo[c], act_p, &oldact)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	if (m) {
		m->sv_handler = (void (*)(int sig, int code, struct sigcontext *scp))oldact.nox_sa_sigaction; /* fix this later */
		m->sv_mask = htot_sigmask(&oldact.nox_sa_mask);
		m->sv_flags = 0;
		if (oldact.nox_sa_flags & nox_SA_ONSTACK)
			m->sv_flags |= SV_ONSTACK;
	}
	return 0;
}
