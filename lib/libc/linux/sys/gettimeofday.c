#pragma include <errno.h>
#pragma include <sys/time.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/time.h>
#include "linux.h"

int gettimeofday(t, z) struct timeval *t; struct timezone *z; {
	struct nox_timeval tv;
	struct nox_timezone tz;

	if (nox_gettimeofday(&tv, &tz)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	if (t) {
		t->tv_sec = (long)tv.nox_tv_sec;
		t->tv_usec = (long)tv.nox_tv_usec;
	}
	if (z) {
		z->tz_minuteswest = (int)tz.nox_tz_minuteswest;
		z->tz_dsttime = (int)tz.nox_tz_dsttime;
	}
	return 0;
}
