#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include "linux.h"

int symlink(t, f) char *t; char *f; {
	if (nox_symlink(t, f)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
