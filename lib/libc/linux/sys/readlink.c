#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include "linux.h"

int readlink(p, b, s) char *p; char *b; int s; {
	nox_ssize_t res;

	res = nox_readlink(p, b, (nox_size_t)s);
	if (res == (nox_ssize_t)-1)
		errno = htot_errno(nox_errno);
	return (int)res;
}
