#pragma include <errno.h>
#pragma include <sys/ioctl.h>
#pragma include <sys/types.h>
#pragma include <unistd.h>

#include <string.h>
#include <sys/errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include "linux.h"

int ioctl(d, r, p) int d; u_long r; void *p; {
	switch (r) {
	case TIOCGETP:
		if (!nox_isatty((nox_int)d)) {
			errno = ENOTTY;
			return -1;
		}
		/* just fake success for now */
		bzero(p, sizeof(struct sgttyb));
		return 0;
	case TIOCGWINSZ: {
		struct nox_winsize ws;
		if (nox_ioctl((nox_int)d, nox_TIOCGWINSZ, &ws)) {
			errno = htot_errno(nox_errno);
			return -1;
		}
		((struct winsize *)p)->ws_row = (unsigned short)ws.nox_ws_row;
		((struct winsize *)p)->ws_col = (unsigned short)ws.nox_ws_col;
		((struct winsize *)p)->ws_xpixel = (unsigned short)ws.nox_ws_xpixel;
		((struct winsize *)p)->ws_ypixel = (unsigned short)ws.nox_ws_ypixel;
		return 0;
	}
	default:
		errno = ENOTTY;
		return -1;
	}
}
