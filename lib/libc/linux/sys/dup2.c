#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include "linux.h"

int dup2(o, n) int o; int n; {
	int res;

	res = nox_dup2((nox_int)o, (nox_int)n);
	if (res == (nox_int)-1)
		errno = htot_errno(nox_errno);
	return (int)res;
}
