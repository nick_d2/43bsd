#pragma include <errno.h>
#pragma include <unistd.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/file.h>
#include <sys/types.h>
#include "linux.h"

int truncate(p, l) char *p; off_t l; {
	if (nox_truncate(p, (nox_off_t)l)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
