#pragma include <errno.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/proc.h>
#include "linux.h"

void *sbrk(i) int i; {
	void *res;

	res = nox_sbrk(i);
	if (res == (void *)-1)
		errno = htot_errno(nox_errno);
	return res;
}
