#pragma include <errno.h>
#pragma include <unistd.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/stat.h>
#include "linux.h"

int fchown(f, u, g) int f; int u; int g; {
	if (nox_fchown((nox_int)f, (nox_uid_t)u, (nox_gid_t)g)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
