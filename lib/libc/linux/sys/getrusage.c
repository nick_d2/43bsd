#pragma include <errno.h>
#pragma include <stdlib.h>
#pragma include <sys/resource.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/resource.h>
#include "linux.h"

int getrusage(res, rip) int res; struct rusage *rip; {
	nox_int who;
	struct nox_rusage ru;

	switch (res) {
	case RUSAGE_SELF:
		who = nox_RUSAGE_SELF;
		break;
	case RUSAGE_CHILDREN:
		who = nox_RUSAGE_CHILDREN;
		break;
	default:
		nox_write((nox_int)2, "getrusage(): invalid who\n", 25);
		nox_abort();
	}
	if (nox_getrusage(who, &ru)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	htot_rusage(&ru, rip);
	return 0;
}
