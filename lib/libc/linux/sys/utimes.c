#pragma include <errno.h>
#pragma include <sys/time.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/time.h>
#include "linux.h"

int utimes(f, t) char *f; struct timeval t[2]; {
	struct nox_timeval tv[2];

	tv[0].nox_tv_sec = (nox_time_t)t[0].tv_sec;
	tv[0].nox_tv_usec = (nox_suseconds_t)t[0].tv_usec;
	tv[1].nox_tv_sec = (nox_time_t)t[1].tv_sec;
	tv[1].nox_tv_usec = (nox_suseconds_t)t[1].tv_usec;
	if (nox_utimes(f, tv)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
