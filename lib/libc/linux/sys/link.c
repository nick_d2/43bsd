#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include "linux.h"

int link(a, b) char *a; char *b; {
	if (nox_link(a, b)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
