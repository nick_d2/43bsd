#pragma include <errno.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/file.h>
#include "linux.h"

int read(f, b, l) int f; void *b; int l; {
	nox_size_t res;
	
	res = nox_read((nox_int)f, b, (nox_size_t)l);
	if (res == (nox_size_t)-1)
		errno = htot_errno(nox_errno);
	return (int)res;
}
