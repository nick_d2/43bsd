#pragma include <errno.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/proc.h>
#include <sys/types.h>
#include "linux.h"

int fork() {
	nox_pid_t res;
	
	res = nox_fork();
	if (res == (nox_pid_t)-1)
		errno = htot_errno(nox_errno);
	return (int)res;
}
