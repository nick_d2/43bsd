#pragma include <dirent.h>
#pragma include <errno.h>
#pragma include <stdlib.h>
#pragma include <string.h>
#pragma include <sys/wait.h>
#pragma include <unistd.h>

#include <gen.h>
#include <string.h>
#include <sys/dir.h>
#include <sys/errno.h>
#include <sys/wait.h>
#include "linux.h"

nox_int ttoh_errno[] = {
	0,
	nox_EPERM,
	nox_ENOENT,
	nox_ESRCH,
	nox_EINTR,
	nox_EIO,
	nox_ENXIO,
	nox_E2BIG,
	nox_ENOEXEC,
	nox_EBADF,
	nox_ECHILD,
	nox_EAGAIN,
	nox_ENOMEM,
	nox_EACCES,
	nox_EFAULT,
	nox_ENOTBLK,
	nox_EBUSY,
	nox_EEXIST,
	nox_EXDEV,
	nox_ENODEV,
	nox_ENOTDIR,
	nox_EISDIR,
	nox_EINVAL,
	nox_ENFILE,
	nox_EMFILE,
	nox_ENOTTY,
	nox_ETXTBSY,
	nox_EFBIG,
	nox_ENOSPC,
	nox_ESPIPE,
	nox_EROFS,
	nox_EMLINK,
	nox_EPIPE,
	nox_EDOM,
	nox_ERANGE,
	nox_EWOULDBLOCK,
	nox_EINPROGRESS,
	nox_EALREADY,
	nox_ENOTSOCK,
	nox_EDESTADDRREQ,
	nox_EMSGSIZE,
	nox_EPROTOTYPE,
	nox_ENOPROTOOPT,
	nox_EPROTONOSUPPORT,
	nox_ESOCKTNOSUPPORT,
	nox_EOPNOTSUPP,
	nox_EPFNOSUPPORT,
	nox_EAFNOSUPPORT,
	nox_EADDRINUSE,
	nox_EADDRNOTAVAIL,
	nox_ENETDOWN,
	nox_ENETUNREACH,
	nox_ENETRESET,
	nox_ECONNABORTED,
	nox_ECONNRESET,
	nox_ENOBUFS,
	nox_EISCONN,
	nox_ENOTCONN,
	nox_ESHUTDOWN,
	nox_ETOOMANYREFS,
	nox_ETIMEDOUT,
	nox_ECONNREFUSED,
	nox_ELOOP,
	nox_ENAMETOOLONG,
	nox_EHOSTDOWN,
	nox_EHOSTUNREACH,
	nox_ENOTEMPTY,
	0, /*nox_EPROCLIM,*/
	nox_EUSERS,
	nox_EDQUOT
};

nox_int ttoh_signo[] = {
	0,
	nox_SIGHUP,
	nox_SIGINT,
	nox_SIGQUIT,
	nox_SIGILL,
	nox_SIGTRAP,
	nox_SIGIOT,
	0, /*nox_SIGEMT,*/
	nox_SIGFPE,
	nox_SIGKILL,
	nox_SIGBUS,
	nox_SIGSEGV,
	nox_SIGSYS,
	nox_SIGPIPE,
	nox_SIGALRM,
	nox_SIGTERM,
	nox_SIGURG,
	nox_SIGSTOP,
	nox_SIGTSTP,
	nox_SIGCONT,
	nox_SIGCHLD,
	nox_SIGTTIN,
	nox_SIGTTOU,
	nox_SIGIO,
	nox_SIGXCPU,
	nox_SIGXFSZ,
	nox_SIGVTALRM,
	nox_SIGPROF,
	nox_SIGWINCH,
	0,
	nox_SIGUSR1,
	nox_SIGUSR2
};

int htot_errno(n) nox_int n; {
	for (int i = 0; i < sizeof(ttoh_errno) / sizeof(nox_int); ++i)
		if (n == ttoh_errno[i])
			return i;
	nox_write((nox_int)2, "htot_errno()\n", (nox_size_t)13);
	nox_abort();
}

int htot_signo(n) nox_int n; {
	for (int i = 0; i < sizeof(ttoh_signo) / sizeof(nox_int); ++i)
		if (n == ttoh_signo[i])
			return i;
	nox_write((nox_int)2, "htot_signo()\n", (nox_size_t)13);
	nox_abort();
}

void ttoh_sigmask(m, res) int m; nox_sigset_t *res; {
	nox_sigemptyset(res);
	for (int i = 1; i < sizeof(ttoh_signo) / sizeof(nox_int); ++i)
		if ((m & sigmask(i)) && ttoh_signo[i])
			nox_sigaddset(res, ttoh_signo[i]);
}

int htot_sigmask(set) nox_sigset_t *set; {
	int res = 0;
	for (int i = 1; i < sizeof(ttoh_signo) / sizeof(nox_int); ++i)
		if (ttoh_signo[i] && nox_sigismember(set, ttoh_signo[i]))
			res |= sigmask(i);
	return res;
}

nox_mode_t ttoh_mode(m) unsigned m; {
	nox_mode_t res = m & 0777;
	if (m & S_ISVTX)
		res |= nox_S_ISVTX;
	if (m & S_ISGID)
		res |= nox_S_ISGID;
	if (m & S_ISUID)
		res |= nox_S_ISUID;
	switch (m & S_IFMT) {
	case /*S_IFIFO*/ 0:
		res |= nox_S_IFIFO;
		break;
	case S_IFCHR:
		res |= nox_S_IFCHR;
		break;
	case S_IFDIR:
		res |= nox_S_IFDIR;
		break;
	case S_IFBLK:
		res |= nox_S_IFBLK;
		break;
	case S_IFREG:
		res |= nox_S_IFREG;
		break;
	case S_IFLNK:
		res |= nox_S_IFLNK;
		break;
	case S_IFSOCK:
		res |= nox_S_IFSOCK;
		break;
	default:
		nox_write((nox_int)2, "ttoh_mode()\n", 12);
		nox_abort();
	}
	return res;
}

unsigned htot_mode(m) nox_mode_t m; {
	unsigned res = m & 0777;
	if (m & nox_S_ISVTX)
		res |= S_ISVTX;
	if (m & nox_S_ISGID)
		res |= S_ISGID;
	if (m & nox_S_ISUID)
		res |= S_ISUID;
	switch (m & nox_S_IFMT) {
	case nox_S_IFIFO:
		/*res |= S_IFIFO; apparently 4.3bsd kernel returns 0, check*/
		break;
	case nox_S_IFCHR:
		res |= S_IFCHR;
		break;
	case nox_S_IFDIR:
		res |= S_IFDIR;
		break;
	case nox_S_IFBLK:
		res |= S_IFBLK;
		break;
	case nox_S_IFREG:
		res |= S_IFREG;
		break;
	case nox_S_IFLNK:
		res |= S_IFLNK;
		break;
	case nox_S_IFSOCK:
		res |= S_IFSOCK;
		break;
	default:
		nox_write((nox_int)2, "htot_mode()\n", 12);
		nox_abort();
	}
	return res;
}

void htot_stat(stat, res) struct nox_stat *stat; struct stat *res; {
	bzero(res, sizeof(struct stat));
	res->st_dev = (dev_t)stat->nox_st_dev;
	res->st_ino = (ino_t)stat->nox_st_ino;
	res->st_mode = htot_mode(stat->nox_st_mode);
	res->st_nlink = (short)stat->nox_st_nlink;
	res->st_uid = (uid_t)stat->nox_st_uid;
	res->st_gid = (gid_t)stat->nox_st_gid;
	res->st_rdev = (dev_t)stat->nox_st_rdev;
	res->st_size = (off_t)stat->nox_st_size;
	res->st_atime = (time_t)stat->nox_st_atime;
	res->st_mtime = (time_t)stat->nox_st_mtime;
	res->st_ctime = (time_t)stat->nox_st_ctime;
	res->st_blksize = (long)stat->nox_st_blksize;
	res->st_blocks = (long)stat->nox_st_blocks;
}
	
void htot_rusage(ru, res) struct nox_rusage *ru; struct rusage *res; {
	res->ru_utime.tv_sec = (time_t)ru->nox_ru_utime.nox_tv_sec;
	res->ru_utime.tv_usec = (time_t)ru->nox_ru_utime.nox_tv_usec;
	res->ru_stime.tv_sec = (time_t)ru->nox_ru_stime.nox_tv_sec;
	res->ru_stime.tv_usec = (time_t)ru->nox_ru_stime.nox_tv_usec;
	res->ru_maxrss = (long)ru->nox_ru_maxrss;
	res->ru_ixrss = (long)ru->nox_ru_ixrss;
	res->ru_idrss = (long)ru->nox_ru_idrss;
	res->ru_isrss = (long)ru->nox_ru_isrss;
	res->ru_minflt = (long)ru->nox_ru_minflt;
	res->ru_majflt = (long)ru->nox_ru_majflt;
	res->ru_nswap = (long)ru->nox_ru_nswap;
	res->ru_inblock = (long)ru->nox_ru_inblock;
	res->ru_oublock = (long)ru->nox_ru_oublock;
	res->ru_msgsnd = (long)ru->nox_ru_msgsnd;
	res->ru_msgrcv = (long)ru->nox_ru_msgrcv;
	res->ru_nsignals = (long)ru->nox_ru_nsignals;
	res->ru_nvcsw = (long)ru->nox_ru_nvcsw;
	res->ru_nivcsw = (long)ru->nox_ru_nivcsw;
}

int htot_wait(stat) int stat; {
	union wait w;

	if (nox_WIFEXITED(stat)) {
		w.w_termsig = 0;
		w.w_coredump = 0;
		w.w_retcode = nox_WEXITSTATUS(stat);
	}
	else if (nox_WIFSIGNALED(stat)) {
		w.w_termsig = htot_signo(nox_WTERMSIG(stat));
		w.w_coredump = nox_WCOREDUMP(stat);
		w.w_retcode = 0;
	}
	else if (nox_WIFSTOPPED(stat)) {
		w.w_stopval = WSTOPPED;
		w.w_stopsig = htot_signo(nox_WSTOPSIG(stat));
	}
	else {
		nox_write((nox_int)2, "htot_wait()\n", 12);
		nox_abort();
	}
	return w.w_status;
}

static void htot_direct(dirent, res) struct nox_dirent *dirent; struct direct *res; {
	res->d_ino = (ino_t)dirent->nox_d_ino;
	if (res->d_ino == 0) {
		nox_write((nox_int)2, "htot_direct(): null inode\n", (nox_size_t)27);
		nox_abort();
	}
	res->d_namlen = strlen(dirent->nox_d_name);
	if (res->d_namlen > MAXNAMLEN) {
		nox_write((nox_int)2, "htot_direct(): name too long\n", (nox_size_t)29);
		nox_abort();
	}
	strncpy(res->d_name, dirent->nox_d_name, (res->d_namlen + 4) & ~3);
	res->d_reclen = DIRSIZ(res);
}

nox_int htot_dir(fd) nox_int fd; {
	char tempfn[15];
	nox_int tempfd;
	nox_DIR *dp;
	struct nox_dirent *d;
	char buf[DIRBLKSIZ + sizeof(struct direct)], *p, *q;

	strcpy(tempfn, "/tmp/dirXXXXXX");
	tempfd = nox_mkstemp(tempfn);
	if (tempfd == -1) {
		errno = htot_errno(nox_errno);
		return (nox_int)-1;
	}
	if (nox_unlink(tempfn) || (dp = nox_fdopendir(fd)) == 0) {
		errno = htot_errno(nox_errno);
		nox_close(fd);
		return (nox_int)-1;
	}
	if ((d = nox_readdir(dp)) != 0) {
		htot_direct(d, (struct direct *)buf);
		p = buf;
		while ((d = nox_readdir(dp)) != 0) {
			q = p + ((struct direct *)p)->d_reclen;
			htot_direct(d, q);
			if (q + ((struct direct *)q)->d_reclen > buf + DIRBLKSIZ) {
 /*nox_memset(q, 0xaa, buf + DIRBLKSIZ - q);*/
				((struct direct *)p)->d_reclen = buf + DIRBLKSIZ - p;
				nox_errno = nox_ENOSPC;
				if (nox_write(tempfd, buf, (nox_size_t)DIRBLKSIZ) != (nox_ssize_t)DIRBLKSIZ)
					goto nogood;
 /*htot_direct(d, q);*/
				nox_memcpy(buf, q, ((struct direct *)q)->d_reclen);
				q = buf;
			}
			p = q;
		}
 /*q = p + ((struct direct *)p)->d_reclen;*/
 /*nox_memset(q, 0xaa, buf + DIRBLKSIZ - q);*/
		((struct direct *)p)->d_reclen = buf + DIRBLKSIZ - p;
		nox_errno = nox_ENOSPC;
		if (nox_write(tempfd, buf, (nox_size_t)DIRBLKSIZ) != (nox_ssize_t)DIRBLKSIZ)
			goto nogood;
	}
	if (nox_lseek(tempfd, (nox_off_t)0, nox_SEEK_SET) == (nox_off_t)-1) {
	nogood:
		errno = htot_errno(nox_errno);
		nox_closedir(dp);
		nox_close(tempfd);
		return (nox_int)-1;
	}
	nox_closedir(dp);
	if (nox_dup2(tempfd, fd) == (nox_int)-1) {
		errno = htot_errno(nox_errno);
		nox_close(tempfd);
		return (nox_int)-1;
	}
	if (nox_close(tempfd)) {
		errno = htot_errno(nox_errno);
		return (nox_int)-1;
	}
	return 0;
}

void *nox___libc_malloc(size) nox_size_t size; {
	return malloc((unsigned)size);
}

void *nox___libc_realloc(p, size) void *p; nox_size_t size; {
	return realloc(p, (unsigned)size);
}

void nox___libc_free(p) void *p; {
	free(p);
}

void *nox___libc_calloc(i, j) nox_size_t i; nox_size_t j; {
	return calloc((unsigned)i, (unsigned)j);
}

void nox___libc_cfree(void *p) __attribute__ ((nox_alias ("__libc_free")));

void *nox___malloc(nox_size_t size) __attribute__ ((nox_alias ("__libc_malloc")));
void *nox___realloc(void *p, nox_size_t size) __attribute__ ((nox_alias ("__libc_realloc")));
void nox___free(void *p) __attribute__ ((nox_alias ("__libc_free")));
void *nox___calloc(nox_size_t i, nox_size_t j) __attribute__ ((nox_alias ("__libc_calloc")));
void nox___cfree(void *p) __attribute__ ((nox_alias ("__libc_free")));

void *nox_malloc(nox_size_t size) __attribute__ ((nox_alias ("__libc_malloc")));
void *nox_realloc(void *p, nox_size_t size) __attribute__ ((nox_alias ("__libc_realloc")));
void nox_free(void *p) __attribute__ ((nox_alias ("__libc_free")));
void *nox_calloc(nox_size_t i, nox_size_t j) __attribute__ ((nox_alias ("__libc_calloc")));
void nox_cfree(void *p) __attribute__ ((nox_alias ("__libc_free")));
