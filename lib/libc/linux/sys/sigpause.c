#pragma include <errno.h>
#pragma include <stdlib.h>
#pragma include <sys/signal.h>
#pragma include <sys/types.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/signal.h>
#include <sys/types.h>
#include "linux.h"

int sigpause(m) int m; {
	nox_sigset_t set;

	ttoh_sigmask(m, &set);
	if (nox_sigsuspend(&set)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	nox_write((nox_int)2, "sigpause(): sigsuspend()\n", 25);
	nox_abort();
}
