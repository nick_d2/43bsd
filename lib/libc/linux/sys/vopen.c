#pragma include <errno.h>
#pragma include <fcntl.h>
#pragma include <stdlib.h>
#pragma include <sys/stat.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/file.h>
#ifdef __STDC__
#include <stdarg.h>
#else
#include <varargs.h>
#endif
#include "linux.h"

int vopen(f, m, argp) char *f; int m; va_list argp; {
	nox_int flags;
	nox_mode_t mode;
	nox_int res;
	struct nox_stat stat;

	switch (m & 3) {
	case O_RDONLY:
		flags = nox_O_RDONLY;
		break;
	case O_WRONLY:
		flags = nox_O_WRONLY;
		break;
	case O_RDWR:
		flags = nox_O_RDWR;
		break;
	default:
		nox_write((nox_int)2, "vopen(): invalid flags\n", 23);
		nox_abort();
	}
	if (m & O_NDELAY)
		flags |= nox_O_NDELAY;
	if (m & O_APPEND)
		flags |= nox_O_APPEND;
	mode = 0;
	if (m & O_CREAT) {
		flags |= nox_O_CREAT;
		mode = (nox_mode_t)va_arg(argp, int);
	}
	if (m & O_TRUNC)
		flags |= nox_O_TRUNC;
	if (m & O_EXCL)
		flags |= nox_O_EXCL;
	res = nox_open(f, flags, mode);
	if (res == (nox_int)-1) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	if (nox_fstat(res, &stat)) {
		errno = htot_errno(nox_errno);
		nox_close(res);
		return -1;
	}
	if (nox_S_ISDIR(stat.nox_st_mode) && htot_dir(res))
		return -1;
	return (int)res;
}
