#pragma include <errno.h>
#pragma include <sys/stat.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/stat.h>
#include "linux.h"

int fchmod(f, m) int f; int m; {
	if (nox_fchmod((nox_int)f, ttoh_mode(m))) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
