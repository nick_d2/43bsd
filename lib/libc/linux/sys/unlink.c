#pragma include <errno.h>
#pragma include <unistd.h>

#include <sys/errno.h>
#include <sys/file.h>
#include "linux.h"

int unlink(s) char *s; {
	if (nox_unlink(s)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
