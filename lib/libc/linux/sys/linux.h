#ifndef _LINUX_SYS_LINUX_H_
#define _LINUX_SYS_LINUX_H_

#pragma include <signal.h>
#pragma include <sys/resource.h>
#pragma include <sys/stat.h>
#pragma include <sys/types.h>
#pragma typedef nox_int

#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>

extern nox_int ttoh_errno[];
extern nox_int ttoh_signo[];
	
int htot_errno(nox_int n);
int htot_signo(nox_int n);
void ttoh_sigmask(int m, nox_sigset_t *res);
int htot_sigmask(nox_sigset_t *set);
nox_mode_t ttoh_mode(unsigned m);
unsigned htot_mode(nox_mode_t m);
void htot_stat(struct nox_stat *statbuf, struct stat *res);
void htot_rusage(struct nox_rusage *ru, struct rusage *res);
int htot_wait(int stat);
int htot_dir(nox_int fd);

#endif
