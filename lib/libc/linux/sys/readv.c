#pragma include <errno.h>
#pragma include <stdlib.h>
#pragma include <unistd.h>
#pragma include <sys/uio.h>

#include <sys/errno.h>
#include <sys/file.h>
#include <sys/uio.h>
#include "linux.h"

int readv(f, v, l) int f; struct iovec *v; int l; {
	int i;
	struct nox_iovec iov[16];
	nox_int res;

	if (l > 16) {
		nox_write((nox_int)2, "readv(): iovec too large\n", 25);
		nox_abort();
	}
	for (i = 0; i < l; ++i) {
		iov[i].nox_iov_base = v[i].iov_base;
		iov[i].nox_iov_len = (nox_size_t)v[i].iov_len;
	}
	res = nox_readv((nox_int)f, iov, (nox_int)l);
	if (res == (nox_int)-1)
		errno = htot_errno(nox_errno);
	return (int)res;
}
