#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/proc.h>
#include "linux.h"

int chroot(s) char *s; {
	if (nox_chroot(s)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
