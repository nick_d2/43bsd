#pragma include <errno.h>
#pragma include <sys/types.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys.h>
#include "linux.h"

int setreuid(r, e) int r; int e; {
	if (nox_setreuid((nox_uid_t)r, (nox_uid_t)e)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
