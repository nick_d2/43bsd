#pragma include <errno.h>
#pragma include <sys/types.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys.h>
#include "linux.h"

int setpgrp(g, pg) int g; int pg; {
	if (nox_setpgid((nox_pid_t)g, (nox_pid_t)pg)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
