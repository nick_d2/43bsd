#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include "linux.h"

int pipe(f) int f[2]; {
	nox_int fds[2];

	if (nox_pipe(fds)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	f[0] = (int)fds[0];
	f[1] = (int)fds[1];
	return 0;
}
