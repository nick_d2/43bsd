#pragma include <errno.h>
#pragma include <sys/types.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys.h>
#include "linux.h"

int getpgrp(p) int p; {
	nox_pid_t res;

	res = nox_getpgid((nox_pid_t)p);
	if (res == (nox_pid_t)-1) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return res;
}
