#pragma include <errno.h>
#pragma include <sys/time.h>
#pragma include <sys/types.h>

#include <errno.h>
#include <sys/time.h>
#include "linux.h"

int settimeofday(t, z) struct timeval *t; struct timezone *z; {
	struct nox_timeval tv, *tv_p;
	struct nox_timezone tz, *tz_p;

	tv_p = 0;
	if (t) {
		tv.nox_tv_sec = (nox_time_t)t->tv_sec;
		tv.nox_tv_usec = (nox_suseconds_t)t->tv_usec;
		tv_p = &tv;
	}
	tz_p = 0;
	if (z) {
		tz.nox_tz_minuteswest = (nox_int)z->tz_minuteswest;
		tz.nox_tz_dsttime = (nox_int)z->tz_dsttime;
		tz_p = &tz;
	}
	if (nox_settimeofday(tv_p, tz_p)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
