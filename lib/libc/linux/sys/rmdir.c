#pragma include <errno.h>
#pragma include <unistd.h>

#include <errno.h>
#include <sys/file.h>
#include "linux.h"

int rmdir(p) char *p; {
	if (nox_rmdir(p)) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
