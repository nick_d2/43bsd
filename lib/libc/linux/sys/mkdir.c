#pragma include <errno.h>
#pragma include <sys/stat.h>

#include <errno.h>
#include <sys/stat.h>
#include "linux.h"

int mkdir(p, m) char *p; int m; {
	if (nox_mkdir(p, ttoh_mode(m))) {
		errno = htot_errno(nox_errno);
		return -1;
	}
	return 0;
}
