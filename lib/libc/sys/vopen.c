#include <sys/file.h>
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif

/*#include <sys/file.h>*/
/*#include <varargs.h>*/

int vopen(f, m, argp) char *f; int m; va_list argp; {
	return open(f, m, m & O_CREAT ? va_arg(argp, int) : 0);
}
