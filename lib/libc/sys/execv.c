#include <sys/exec.h>

extern char **environ;

int execv(s, v) char *s; char *v[]; {
	return execve(s, v, environ);
}
