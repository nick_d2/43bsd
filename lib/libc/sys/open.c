#include <sys/file.h>
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif

/*#include <varargs.h>*/

#ifdef __STDC__
int open(char *f, int m, ...)
#else
int open(f, m, va_alist) char *f; int m; va_dcl
#endif
{
	va_list argp;
	int res;

	_va_start(argp, m);
	res = vopen(f, m, argp);
	va_end(argp);
	return res;
}
