#include <sys/exec.h>
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif

#ifdef __x86_64__
#include <gen.h>
#include <sys/file.h>
#endif

#ifdef __STDC__
int execle(char *f, ...)
#else
int execle(f, va_alist) char *f; va_dcl
#endif
{
	va_list argp;
#ifdef __x86_64__
	int i;
	char *argv[32];

	_va_start(argp, f);
	for (i = 0; (argv[i] = va_arg(argp, char *)) != 0; ++i)
		if (i >= 32) {
			write(2, "execle(): too many arguments\n", 29);
			abort();
		}
#else
	char **argv;

	_va_start(argp, f);
	argv = (char **)argp;
	while (va_arg(argp, char *))
		;
#endif
	return execve(f, argv, va_arg(argp, char **));
}
