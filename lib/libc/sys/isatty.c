#include <sys/ioctl.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)isatty.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Returns 1 iff file is a tty
 */

/*#include <sgtty.h>*/

int isatty(f) int f; {
	struct sgttyb ttyb;

	if (ioctl(f, TIOCGETP, &ttyb) < 0)
		return(0);
	return(1);
}
