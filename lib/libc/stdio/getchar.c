#include <stdio.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)getchar.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * A subroutine version of the macro getchar.
 */
/*#include <stdio.h>*/

#undef getchar

int getchar() {
	return(getc(stdin));
}
