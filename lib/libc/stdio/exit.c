#include <gen.h>
#include <stdio.h>
/*#include <sys/proc.h> gen.h*/

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)exit.c	5.2 (Berkeley) 3/9/86";
#endif

int exit(code) int code; {

	_cleanup();
	_exit(code);
}
