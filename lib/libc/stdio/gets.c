#include <stdio.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)gets.c	5.2 (Berkeley) 3/9/86";
#endif

/*#include	<stdio.h>*/

char *gets(s) char *s; {
	register c;
	register char *cs;

	cs = s;
	while ((c = getchar()) != '\n' && c != EOF)
		*cs++ = c;
	if (c == EOF && cs==s)
		return(NULL);
	*cs++ = '\0';
	return(s);
}
