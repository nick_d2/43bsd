#include <stdio.h>
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)scanf.c	5.2 (Berkeley) 3/9/86";
#endif

/*#include <stdio.h>*/
/*#include <varargs.h>*/

#ifdef __STDC__
int scanf(char *fmt, ...)
#else
int scanf(fmt, va_alist) char *fmt; va_dcl
#endif
{
	va_list argp;
	int len;

	_va_start(argp, fmt);
	len = _doscan(stdin, fmt, argp);
	va_end(argp);
	return len;
}

#ifdef __STDC__
int fscanf(FILE *iop, char *fmt, ...)
#else
int fscanf(iop, fmt, va_alist) FILE *iop; char *fmt; va_dcl
#endif
{
	va_list argp;
	int len;

	_va_start(argp, fmt);
	len = _doscan(iop, fmt, argp);
	va_end(argp);
	return len;
}

#ifdef __STDC__
int sscanf(register char *str, char *fmt, ...)
#else
int sscanf(str, fmt, va_alist) register char *str; char *fmt; va_dcl
#endif
{
	FILE _strbuf;
	va_list argp;
	int len;

	_va_start(argp, fmt);
	_strbuf._flag = _IOREAD|_IOSTRG;
	_strbuf._ptr = _strbuf._base = str;
	_strbuf._cnt = 0;
	while (*str++)
		_strbuf._cnt++;
	_strbuf._bufsiz = _strbuf._cnt;
	len = _doscan(&_strbuf, fmt, argp);
	va_end(argp);
	return len;
}
