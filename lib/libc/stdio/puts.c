#include <stdio.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)puts.c	5.2 (Berkeley) 3/9/86";
#endif

/*#include	<stdio.h>*/

int puts(s) register char *s; {
	register c;

	while (c = *s++)
		putchar(c);
	return(putchar('\n'));
}
