#include <stdio.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)fputc.c	5.2 (Berkeley) 3/9/86";
#endif

/*#include <stdio.h>*/

int fputc(c, fp) int c; register FILE *fp; {
	return(putc(c, fp));
}
