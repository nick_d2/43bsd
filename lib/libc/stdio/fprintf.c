#include <stdio.h>
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)fprintf.c	5.4 (Berkeley) 6/27/88";
#endif

/*#include <stdio.h>*/
/*#include <varargs.h>*/

#ifdef __STDC__
int fprintf(register FILE *iop, char *fmt, ...)
#else
int fprintf(iop, fmt, va_alist) register FILE *iop; char *fmt; va_dcl
#endif
{
	va_list argp;
	int len;
	char localbuf[BUFSIZ];

	_va_start(argp, fmt);
	if (iop->_flag & _IONBF) {
		iop->_flag &= ~_IONBF;
		iop->_ptr = iop->_base = localbuf;
		iop->_bufsiz = BUFSIZ;
		len = _doprnt(fmt, argp, iop);
		fflush(iop);
		iop->_flag |= _IONBF;
		iop->_base = NULL;
		iop->_bufsiz = NULL;
		iop->_cnt = 0;
	} else
		len = _doprnt(fmt, argp, iop);
	va_end(argp);
	return(ferror(iop) ? EOF : len);
}
