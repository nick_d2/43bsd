#include <stdio.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)putchar.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * A subroutine version of the macro putchar
 */
/*#include <stdio.h>*/

#undef putchar

int putchar(c) register c; {
	putc(c, stdout);
}
