#include <stdio.h>

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)clrerr.c	5.2 (Berkeley) 3/9/86";
#endif

/*#include <stdio.h>*/
#undef	clearerr

int clearerr(iop) register FILE *iop; {
	iop->_flag &= ~(_IOERR|_IOEOF);
}
