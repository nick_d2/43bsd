/*#include <arpa/nameser.h> resolv.h*/
#include <netdb.h>
#include <resolv.h>

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)sethostent.c	6.3 (Berkeley) 4/10/86";
#endif

/*#include <sys/types.h>*/
/*#include <arpa/nameser.h>*/
/*#include <netinet/in.h>*/
/*#include <resolv.h>*/

int sethostent(stayopen) int stayopen; {
	if (stayopen)
		_res.options |= RES_STAYOPEN | RES_USEVC;
}

int endhostent() {
	_res.options &= ~(RES_STAYOPEN | RES_USEVC);
	_res_close();
}

int sethostfile(name) char *name; {
#ifdef lint
name = name;
#endif
}
