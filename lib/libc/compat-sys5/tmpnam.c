#include <compat-sys5.h>
#include <stdio.h>
#include <sys/proc.h>

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)tmpnam.c	4.3 (Berkeley) 3/9/86";
#endif

char *tmpnam(s) char *s; {
	static seed;

	sprintf(s, "temp.%d.%d", getpid(), seed++);
	return(s);
}
