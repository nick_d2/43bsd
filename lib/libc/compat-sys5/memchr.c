#include <memory.h>

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)memchr.c	5.2 (Berkeley) 86/03/09";
#endif

void *memchr(_s, c, n) void *_s; register c; register n; {
#define s (*(char **)&_s)
	while (--n >= 0)
		if (*s++ == c)
			return (--s);
	return (0);
#undef s
}
