#include <memory.h>

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)memcpy.c	5.2 (Berkeley) 86/03/09";
#endif

void *memcpy(_t, _f, n) void *_t; void *_f; register n; {
#define t (*(char **)&_t)
#define f (*(char **)&_f)
	register char *p = t;

	while (--n >= 0)
		*t++ = *f++;

	return (p);
#undef t
#undef f
}
