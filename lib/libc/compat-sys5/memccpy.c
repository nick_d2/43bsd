#include <memory.h>

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)memccpy.c	5.2 (Berkeley) 86/03/09";
#endif

char *memccpy(_t, _f, c, n) void *_t; void *_f; register c; register n; {
#define t (*(char **)&_t)
#define f (*(char **)&_f)
	while (--n >= 0)
		if ((*t++ = *f++) == c)
			return (t);
	return (0);
#undef t
#undef f
}
