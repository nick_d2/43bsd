#include <memory.h>

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)memcmp.c	5.2 (Berkeley) 86/03/09";
#endif

int memcmp(_s1, _s2, n) void *_s1; void *_s2; register n; {
#define s1 (*(char **)&_s1)
#define s2 (*(char **)&_s2)
	while (--n >= 0)
		if (*s1++ != *s2++)
			return (*--s1 - *--s2);
	return (0);
#undef s1
#undef s2
}
