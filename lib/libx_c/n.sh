#!/bin/sh

rm -rf .xify *.c *.h *.a *.o *.[ch].nocomm *.[ch].protos *.[ch].protos.nocomm *.temp.c *.[ch].usedby *.[ch].uses arpa compat-4.1 compat-sys5 csu gen inet linux machine net netimp netinet netns ns pascal protocols stdio sys vax vaxif vaxmba vaxuba xx*

find ../libc \( -name '*.[cs]' -o -name Makefile \) -print |\
sed -e 's:^\.\./libc/::' |\
while read i
do
  mkdir --parents `dirname $i`
  cp ../libc/$i $i
done

#find vax -name '*.c' -print |\
#sed -e 's:^vax/\(.*\)\.c$:\1:' |\
#while read i
#do
#  echo "i=$i"
#  if grep -q '^}' vax/$i.c
#  then
#    if test -f $i.c
#    then
#      rm vax/$i.c
#    else
#      mkdir -p `dirname $i`
#      mv vax/$i.c $i.c
#    fi
#  else
#    mv vax/$i.c vax/$i.s
#  fi
#done
#
#cp ../../usr.bin/lint/llib-lc .
#patch llib-lc <llib-lc.patch
#sed -e 's/[\t ]\+/ /g;s/^ /void /;s/\* /*/g;s/( /(/g;s/ )/)/g' <llib-lc >llib-lc.temp
#
#rm -f a
#
#find vax -name '*.s' -print |\
#sed -e 's:^vax/\(.*\)\.s$:\1:' |\
#while read i
#do
#  echo "i=$i"
#  if test -f $i.c
#  then
#    rm vax/$i.s
#  elif test -f ../libc4.3tahoe/$i.c
#  then
#    rm vax/$i.s
#    mkdir -p `dirname $i`
#    cp ../libc4.3tahoe/$i.c $i.c
#  else
#    entries="`sed -ne 's/^\(ASENTRY\|ENTRY\|PSEUDO\|SYSCALL\)(\([^),]*\)\(,[^)]*\)\?)$/\2/p' vax/$i.s`"
#    echo "entries=$entries"
#    if test -n "$entries"
#    then
#      for j in $entries
#      do
#        grep "[^0-9A-Za-z_]$j[	 ]*(" llib-lc.temp |\
#        sed -e "s/{.*}/{\\n\\twrite(2, \"$j()\\\\n\", `expr length $j + 3`);\\n\\tabort();\\n}/"
#      done >b
#      if test -s b
#      then
#        rm vax/$i.s
#        mkdir -p `dirname $i`
#        mv b $i.c
#        echo $i.c >>a
#      fi
#    fi
#  fi
#done
#
#LC_ALL=C sort <a >generated.txt
#
#find vax -name Makefile -print |\
#while read i
#do
#  rm $i
#done
#
#(
#  find csu -type f -print
#  find vax -type f -print
#) >unported.txt
rm -r csu vax

find ../../include -name '*.h' -print |\
sed -e 's:^\.\./\.\./include/::' |\
while read i
do
  mkdir -p `dirname $i`
  cp ../../include/$i $i
done

#echo mp.h >>unported.txt
#rm mp.h

find ../../sys/h -name '*.h' -print |\
sed -e 's:^\.\./\.\./sys/h/::' |\
while read i
do
  mkdir -p sys/`dirname $i`
  cp ../../sys/h/$i sys
done

for i in net netimp netinet netns vax vaxif vaxmba vaxuba
do
  find ../../sys/$i -name '*.h' -print |\
  sed -e "s:^\.\./\.\./sys/$i/::" |\
  while read j
  do
    mkdir -p $i/`dirname $j`
    cp ../../sys/$i/$j $i/$j
  done
done
