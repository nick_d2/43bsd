#!/bin/sh
#
##sed -e 's/^/:/;s/ /::/g;s/$/:/' <conflicts.txt >conflicts.temp
#
#find . -name '*.[ch]' -print |\
#sed -e 's:^\./::' |\
#while read i
#do
#  echo "i=$i"
#  rm -f xx*
#  csplit -b '%05d' -q $i '/^[	 ]*{[	 ]*$/' '{*}'
#  for j in xx*
#  do
#    :
#  done
#  for k in xx*
#  do
#    if test $k != xx00000
#    then
#      sed -ne '2,$p' -i $k
#    fi
#    if test $k != $j
#    then
#      sed -e '$s/$/ {/' -i $k
#    fi
#  done
#  cat xx* |sed -e 's/^\(#[	 ]*\(else\|endif\)\)\([^0-9A-Za-z_].*\)\?/\1/' |../../xify/nocomment |../../xify/nostring >$i.nocomm
#done
#
#while read i
#do
#  (echo; cat $i) >a
#  mv a $i
#done <generated.txt
#
#find . -name '*.h' -print |\
#sed -e 's:^\./::' |\
#LC_ALL=C sort -r |\
#while read i
#do
#  echo "i=$i"
#  macros="`sed -ne 's/^#[	 ]*define[	 ]\+\([A-Za-z_][0-9A-Za-z_]*\).*/\1/p' $i.nocomm |grep -v '^NULL$'`"
#  echo "macros=$macros"
#  typedefs="`sed -ne 's/^\(.*[^0-9A-Za-z_]\)\?typedef[^0-9A-Za-z_]\(.*[^0-9A-Za-z_]\)\?\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*\(\[[	 0-9]*\][	 ]*\)\?;.*/\3/p' $i.nocomm; sed -ne 's/^}[	 ]*\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*;.*/\1/p' $i.nocomm`"
#  echo "typedefs=$typedefs"
#  structs="`sed -ne 's/^\(.*[^0-9A-Za-z_]\)\?struct[	 ]\+\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*{.*/\2/p' $i.nocomm`"
#  echo "structs=$structs"
#  unions="`sed -ne 's/^\(.*[^0-9A-Za-z_]\)\?union[	 ]\+\([A-Za-z_][0-9A-Za-z_]*\)[	 ]*{.*/\2/p' $i.nocomm`"
#  echo "unions=$unions"
#
#  rm -f $i.usedby
#
#  pattern=
#  prefix=
#  for j in $macros
#  do
#    pattern="$pattern$prefix$j"
#    prefix='\|'
#  done
#  for j in $typedefs
#  do
#    pattern="$pattern$prefix$j"
#    prefix='\|'
#  done
#  for j in $structs
#  do
#    pattern="$pattern${prefix}struct[	 ]\\+$j"
#    prefix='\|'
#  done
#  for j in $unions
#  do
#    pattern="$pattern${prefix}union[	 ]\\+$j"
#    prefix='\|'
#  done
#  echo "pattern=$pattern"
#
#  if test -n "$pattern"
#  then
#    grep -H "^\\(.*[^0-9A-Za-z_]\\)\\?\\($pattern\\)\\([^0-9A-Za-z_].*\\)\\?$" `cat generated.txt |sed -e 's/$/\.nocomm/'` >a
#    if test -s a
#    then
#      cat a >>$i.usedby
#      for j in `sed -e 's/\.nocomm:.*//' <a |sort |uniq`
#      do
#        echo "j=$j"
#        #if ! grep -q ":$i:.*:$j\\|:$j:.*:$i:" conflicts.temp && ! grep -q "^#include <$i>\$" $j
#        #then
#          sed -e "1i#include <$i>" -i $j
#        #fi
#      done
#    fi
#  fi
#done
#
#while read i
#do
#  ../../xify/newline <$i >a
#  mv a $i
#done <generated.txt
#
#rm -rf ../libx_c.pre
#cp -r ../libx_c ../libx_c.pre
#patch --strip 1 <pre.patch
