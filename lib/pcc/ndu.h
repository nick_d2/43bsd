#ifndef _NDU_H_
#define _NDU_H_

#include "config.h"
#include "macdefs.h"

/*	ndu.h	4.1	85/03/19	*/

/*
 * This file defines the basic tree node data structure for the PCC.
 */

typedef	union ndu NODE;
typedef	unsigned int TWORD;
#define NIL	(NODE *)0

union ndu {
	struct {		/* interior node */
		int	op;
		int	rall;
		TWORD	type;
		int	su;
#ifndef FLEXNAMES
		char	name[NCHNAM];
#else
		char	*name;
		int	stalign;
#endif
		NODE	*left;
		NODE	*right;
	} in;
	struct {		/* terminal node */
		int	op;
		int	rall;
		TWORD	type;
		int	su;
#ifndef FLEXNAMES
		char	name[NCHNAM];
#else
		char	*name;
		int	stalign;
#endif
#ifdef __x86_64__
 /* this is very non-ideal, code such as the following in pftn.c,
  *	p->tn.rval = p->in.left->tn.rval;
  *	p->in.type = p->in.left->in.type;
  * uses the different node types interchangeably, just align them for now
  */
 int pad0;
#endif
		CONSZ	lval;
#ifdef __x86_64__
 int pad1;
#endif
		int	rval;
#ifdef __x86_64__
 int pad2;
#endif
	} tn;
	struct {		/* branch node */
		int	op;
		int	rall;
		TWORD	type;
		int	su;
		int	label;		/* for use with branching */
	} bn;
	struct {		/* structure node */
		int	op;
		int	rall;
		TWORD	type;
		int	su;
		int	stsize;		/* sizes of structure objects */
		int	stalign;	/* alignment of structure objects */
	} stn;
	struct {		/* front node */
		int	op;
		int	cdim;
		TWORD	type;
		int	csiz;
	} fn;
	/*
	 * This structure is used when a double precision
	 * floating point constant is being computed
	 */
	struct {			/* DCON node */
		int	op;
		TWORD	type;
		int	cdim;
		int	csiz;
		double	dval;
	} dpn;
	/*
	 * This structure is used when a single precision
	 * floating point constant is being computed
	 */
	struct {			/* FCON node */
		int	op;
		TWORD	type;
		int	cdim;
		int	csiz;
		float	fval;
	} fpn;
};

#endif
