s/@/ATSIGN/g
s/^/@/
s/$/@/
s/[^A-Za-z0-9_]\+/@&@/g

# stab.c
s/@private@/@static@/g
s/@and@/@\&\&@/g
s/@or@/@||@/g
s/@not@/@!@/g
s/@div@/@\/@/g
s/@mod@/@%@/g
s/@nil@/@0@/g
s/@Boolean@/@bool@/g

s/@//g
s/ATSIGN/@/g
