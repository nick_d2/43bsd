#ifndef _PASS1_H_
#define _PASS1_H_

/*#include "config.h" manifest.h*/
/*#include "macdefs.h" manifest.h*/
#include "manifest.h"
/*#include "ndu.h" manifest.h*/

/*	pass1.h	4.2	85/08/22	*/

/*#ifndef _PASS1_*/
/*#define	_PASS1_*/

/*#include "macdefs.h"*/
/*#include "manifest.h"*/
/*#include "ndu.h"*/

/*
 * Symbol table definition.
 *
 * Colliding entries are moved down with a standard
 * probe (no quadratic rehash here) and moved back when
 * entries are cleared.
 */
struct	symtab {
#ifndef FLEXNAMES
	char	sname[NCHNAM];
#else
	char	*sname;
#endif
	struct	symtab *snext;	/* link to other symbols in the same scope */
	TWORD	stype;		/* type word */
	char	sclass;		/* storage class */
	char	slevel;		/* scope level */
	char	sflags;		/* flags, see below */
	int	offset;		/* offset or value */
	short	dimoff;		/* offset into the dimension table */
	short	sizoff;		/* offset into the size table */
	int	suse;		/* line number of last use of the variable */
};

/*
 * Storage classes
 */
#define SNULL		0		/* initial value */
#define AUTO		1		/* automatic (on stack) */
#define EXTERN		2		/* external reference */
#define STATIC		3		/* static scope */
#define REGISTER	4		/* register requested */
#define EXTDEF		5		/* external definition */
#define LABEL		6		/* label definition */
#define ULABEL		7		/* undefined label reference */
#define MOS		8		/* member of structure */
#define PARAM		9		/* parameter */
#define STNAME		10		/* structure name */
#define MOU		11		/* member of union */
#define UNAME		12		/* union name */
#define TYPEDEF		13		/* typedef name */
#define FORTRAN		14		/* fortran function */
#define ENAME		15		/* enumeration name */
#define MOE		16		/* member of enumeration */
#define UFORTRAN 	17		/* undefined fortran reference */
#define USTATIC		18		/* undefined static reference */

/* field size is ORed in */
#define FIELD		0100
#define FLDSIZ		077
#ifndef BUG1
/*extern	char *scnames();*/
#endif

/*
 * Symbol table flags
 */
#define SMOS		01		/* member of structure */
#define SHIDDEN		02		/* hidden in current scope */
#define SHIDES		04		/* hides symbol in outer scope */
#define SSET		010		/* symbol assigned to */
#define SREF		020		/* symbol referenced */
#define SNONUNIQ	040		/* non-unique structure member */
#define STAG		0100		/* structure tag name */

/*
 * Location counters
 */
#define PROG		0		/* program segment */
#define DATA		1		/* data segment */
#define ADATA		2		/* array data segment */
#define STRNG		3		/* string data segment */
#define ISTRNG		4		/* initialized string segment */
#define STAB		5		/* symbol table segment */

#ifndef FIXDEF
#define FIXDEF(p)
#endif
#ifndef FIXARG
#define FIXARG(p)
#endif
#ifndef FIXSTRUCT
#define FIXSTRUCT(a,b)
#endif

	/* alignment of initialized quantities */
#ifndef AL_INIT
#define	AL_INIT ALINT
#endif

/*
 * External definitions
 */
struct sw {		/* switch table */
	CONSZ	sval;	/* case value */
	int	slab;	/* associated label */
};
extern	struct sw swtab[];
extern	struct sw *swp;
extern	int swx;

extern	int ftnno;
extern	int blevel;
extern	int instruct, stwart;

extern	int lineno, nerrors;

extern	CONSZ lastcon;
extern	float fcon;
extern	double dcon;

extern	char ftitle[];
extern	char ititle[];
extern	struct symtab stab[];
extern	int curftn;
extern	int curclass;
extern	int curdim;
extern	int dimtab[];
extern	int paramstk[];
extern	int paramno;
extern	int autooff, argoff, strucoff;
extern	int regvar;
extern	int minrvar;
extern	int brkflag;
typedef union {
	int intval;
	NODE * nodep;
	} YYSTYPE;
extern	YYSTYPE yylval;
extern	char yytext[];

extern	int strflg;

extern	OFFSZ inoff;

extern	int reached;

/* tunnel to buildtree for name id's */
extern	int idname;

extern	NODE node[];
extern	NODE *lastfree;

extern	int cflag, hflag, pflag;

/* various labels */
extern	int brklab;
extern	int contlab;
extern	int flostat;
extern	int retlab;
extern	int retstat;
extern	int asavbc[], *psavbc;

/* declarations of various functions */
extern	NODE
	*buildtree(),
	*bdty(),
	*mkty(),
	*rstruct(),
	*dclstruct(),
	*getstr(),
	*tymerge(),
	*stref(),
	*offcon(),
	*bcon(),
	*bpsize(),
	*convert(),
	*pconvert(),
	*oconvert(),
	*ptmatch(),
	*tymatch(),
	*makety(),
	*block(),
	*doszof(),
	*talloc(),
	*optim(),
	*fixargs(),
	*clocal();
OFFSZ	tsize(),
	psize();
/*TWORD	types();*/
/*double	atof();*/
/*char	*exname(), *exdcon();*/

#define checkst(x)

#ifndef CHARCAST
/* to make character constants into character connstants */
/* this is a macro to defend against cross-compilers, etc. */
#define CHARCAST(x) (char)(x)
#endif

/*
 * Flags used in structures/unions
 */
#define SEENAME		01
#define INSTRUCT	02
#define INUNION		04
#define FUNNYNAME	010
#define TAGNAME		020

/*
 * Flags used in the (elementary) flow analysis ...
 */
#define FBRK		02
#define FCONT		04
#define FDEF		010
#define FLOOP		020

/*
 * Flags used for return status
 */
#define RETVAL		1
#define NRETVAL		2

#define NONAME		040000		/* marks constant w/o name field */
#define NOOFFSET	(-10201)	/* mark an offset which is undefined */

/* NOPREF must be defined for use in first pass tree machine */
/* for some reason this is only used in pass1 if it's the one-pass compiler */
/* see pass2.h for the complete set of bits that can be on in the same word */
#define NOPREF	020000		/* no preference for register assignment */

/*#endif*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* cgram.c */
NODE *mkty __P((unsigned t, int d, int s));
NODE *bdty __P((int op, NODE *p, int v));
void dstash __P((int n));
void savebc __P((void));
void resetbc __P((int mask));
void addcase __P((NODE *p));
void adddef __P((void));
void swstart __P((void));
void swend __P((void));
int yyparse __P((void));

/* code.c */
void branch __P((int n));
void defalign __P((int n));
int locctr __P((int l));
void deflab __P((int n));
int getlab __P((void));
void efcode __P((void));
void bfcode __P((int a[], int n));
void bccode __P((void));
void ejobcode __P((int flag));
void aobeg __P((void));
void aocode __P((struct symtab *p));
void aoend __P((void));
void defnam __P((register struct symtab *p));
void bycode __P((int t, int i));
void zecode __P((int n));
int fldal __P((unsigned t));
void fldty __P((struct symtab *p));
void where __P((int c));
char *toreg __P((TWORD type));
int main __P((int argc, char *argv[]));
void genswitch __P((register struct sw *p, int n));
void makeheap __P((register struct sw *p, int m, int n));
int select __P((int m));
void walkheap __P((int start, int limit));

/* local.c */
NODE *cast __P((register NODE *p, TWORD t));
NODE *clocal __P((NODE *p));
int andable __P((NODE *p));
void cendarg __P((void));
int cisreg __P((TWORD t));
NODE *offcon __P((OFFSZ off, TWORD t, int d, int s));
void incode __P((register NODE *p, int sz));
void fincode __P((double d, int sz));
void cinit __P((NODE *p, int sz));
void vfdzero __P((int n));
char *exname __P((char *p));
int ctype __P((TWORD type));
int noinit __P((void));
void commdec __P((int id));
int isitlong __P((int cb, int ce));
int isitfloat __P((char *s));
void ecode __P((NODE *p));
int tlen __P((NODE *p));

/* optim.c */
NODE *fortarg __P((NODE *p));
NODE *optim __P((register NODE *p));
int ispow2 __P((CONSZ c));

/* pftn.c */
void defid __P((register NODE *q, register int class));
void psave __P((int i));
void ftnend __P((void));
void dclargs __P((void));
NODE *rstruct __P((int idn, int soru));
void moedef __P((int idn));
int bstruct __P((int idn, int soru));
NODE *dclstruct __P((int oparam));
void yyerror __P((char *s));
void yyaccpt __P((void));
void ftnarg __P((int idn));
int talign __P((register unsigned ty, register s));
OFFSZ tsize __P((TWORD ty, int d, int s));
void inforce __P((OFFSZ n));
void vfdalign __P((int n));
void beginit __P((int curid));
void instk __P((int id, TWORD t, int d, int s, OFFSZ off));
NODE *getstr __P((void));
void putbyte __P((int v));
void endinit __P((void));
void doinit __P((register NODE *p));
void gotscal __P((void));
void ilbrace __P((void));
void irbrace __P((void));
int upoff __P((int size, register alignment, register *poff));
int oalloc __P((register struct symtab *p, register *poff));
int falloc __P((register struct symtab *p, int w, int new, NODE *pty));
void nidcl __P((NODE *p));
TWORD types __P((TWORD t1, TWORD t2, TWORD t3));
NODE *tymerge __P((NODE *typ, NODE *idp));
void tyreduce __P((register NODE *p));
void fixtype __P((register NODE *p, int class));
int uclass __P((register class));
int fixclass __P((int class, TWORD type));
struct symtab *mknonuniq __P((int *idindex));
int lookup __P((char *name, int s));
int XXXcheckst __P((int lev));
struct symtab *relook __P((register struct symtab *p));
void clearst __P((register int lev));
int hide __P((register struct symtab *p));
void unhide __P((register struct symtab *p));

/* scan.c */
int mainp1 __P((int argc, char *argv[]));
void lxenter __P((register char *s, int m));
void lxmore __P((register c, register m));
void lxinit __P((void));
void lxstr __P((int ct));
void lxcom __P((void));
int yylex __P((void));
int lxres __P((void));
void lxtitle __P((void));
char *savestr __P((register char *cp));
char *hash __P((char *s));

/* stab.c */
void fixarg __P((struct symtab *p));
void outstab __P((struct symtab *sym));
void outstruct __P((int szindex, int paramindex));
void pstab __P((char *name, int type));
void pstabdot __P((int type, int value));
void psline __P((void));
void plcstab __P((int level));
void prcstab __P((int level));
void pfstab __P((char *sname));

/* trees.c */
void printact __P((NODE *t, int acts));
NODE *buildtree __P((int o, register NODE *l, register NODE *r));
int fpe __P((void));
NODE *fixargs __P((register NODE *p));
int chkstr __P((int i, int j, TWORD type));
int conval __P((register NODE *p, int o, register NODE *q));
void chkpun __P((register NODE *p));
NODE *stref __P((register NODE *p));
int notlval __P((register NODE *p));
NODE *bcon __P((int i));
NODE *bpsize __P((register NODE *p));
OFFSZ psize __P((NODE *p));
NODE *convert __P((register NODE *p, int f));
void econvert __P((register NODE *p));
NODE *pconvert __P((register NODE *p));
NODE *oconvert __P((register NODE *p));
NODE *ptmatch __P((register NODE *p));
NODE *tymatch __P((register NODE *p));
NODE *makety __P((register NODE *p, TWORD t, int d, int s));
NODE *block __P((int o, register NODE *l, register NODE *r, TWORD t, int d, int s));
int icons __P((register NODE *p));
int opact __P((NODE *p));
int moditype __P((TWORD ty));
NODE *doszof __P((register NODE *p));
void eprint __P((register NODE *p, int down, int *a, int *b));
void prtdcon __P((register NODE *p));
void ecomp __P((register NODE *p));
int prtree __P((register NODE *p));
void p2tree __P((register NODE *p));

/* xdefs.c */
char *scnames __P((register c));

#endif
