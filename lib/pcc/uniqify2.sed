s/@/ATSIGN/g
s/^/@/
s/$/@/
s/[^A-Za-z0-9_]\+/@&@/g

# onepass.h
s/@crslab@/@crs2lab@/g
s/@where@/@where2@/g
s/@xdebug@/@x2debug@/g
s/@tdebug@/@t2debug@/g
s/@deflab@/@def2lab@/g
s/@edebug@/@e2debug@/g
s/@eprint@/@e2print@/g
s/@getlab@/@get2lab@/g
s/@filename@/@ftitle@/g

s/@//g
s/ATSIGN/@/g
