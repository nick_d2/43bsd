#ifndef _CPP_H_
#define _CPP_H_

#ifdef __STDC__
#include <stdarg.h>
#endif

struct symtab {
	char	*name;
	char	*value;
};

extern char fastab[];
extern char *outp,*inp,*newp;
extern int flslvl;
extern int passcom;

/*char *skipbl();*/ 
/*struct symtab *lookup(), *slookup();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* cpp.c */
void sayline __P((int where));
void dump __P((void));
char *refill __P((register char *p));
char *cotoken __P((register char *p));
char *skipbl __P((register char *p));
char *unfill __P((register char *p));
char *doincl __P((register char *p));
int equfrm __P((register char *a, register char *p1, register char *p2));
char *dodef __P((char *p));
char *control __P((register char *p));
char *savestring __P((register char *start, register char *finish));
struct symtab *stsym __P((register char *s));
struct symtab *ppsym __P((char *s));
void vpperror __P((char *s, va_list argp));
void pperror __P((char *s, ...));
void yyerror __P((char *s, ...));
void ppwarn __P((char *s, ...));
struct symtab *lookup __P((char *namep, int enterf));
struct symtab *slookup __P((register char *p1, register char *p2, int enterf));
char *subst __P((register char *p, struct symtab *sp));
char *trmdir __P((register char *s));
char *copy __P((register char *s));
char *strdex __P((char *s, int c));
int yywrap __P((void));
int main __P((int argc, char *argv[]));

/* cpy.c */
int yylex __P((void));
int tobinary __P((char *st, int b));
int yyparse __P((void));

#endif
