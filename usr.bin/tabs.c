#include <gen.h>
#include <stdio.h>
/*#include <strings.h> gen.h*/
#include <sys/ioctl.h>
/*#include <sys/ttydev.h> sys/ioctl.h*/
#include <vax/mtpr.h>

#if defined(DOSCCS) && !defined(lint)
static char *sccsid = "@(#)tabs.c	4.1 (Berkeley) 10/1/80";
#endif
/*#include <stdio.h>*/
/*#include <sgtty.h>*/

#define SP	' '
#define TB	'\t'
#define NL	'\n'

# define ESC 033
# define RHM 060
# define SI 017
# define DEL 0177
# define SET '1'
# define CLR '2'
# define MGN '9'
# define CR '\r'
# define BS '\b'

struct sysnod {
	char	*sysnam;
	int	sysval;
};

#define	DASI300 1
#define	DASI300S 2
#define DASI450 3
#define TN300 4
#define TTY37 5
#define HP	6
struct sysnod tty[] = {
	{"dasi300", DASI300},
	{"300", DASI300},
	{"dasi300s", DASI300S},
	{"300s", DASI300S},
	{"dasi450", DASI450},
	{"450", DASI450},
	{"37", TTY37},
	{"tty37", TTY37},
	{"tn300", TN300},
	{"terminet", TN300},
	{"tn", TN300},
	{"hp",	HP},
	{0, 0},
};
int	margset = 1;

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

int syslook __P((char *w));
void main __P((int argc, char **argv));
void clear __P((int n));
void delay __P((int n));
void tabs __P((int n));
void margin __P((int n));
void escape __P((int c));
void bs __P((int n));
void nl __P((void));
void dasi450 __P((void));
void tty37 __P((void));
void dasi300 __P((void));
void tn300 __P((void));
void hp2645 __P((void));
void misc __P((void));

int syslook(w) char *w; {
	register struct sysnod *sp;

	for (sp = tty; sp->sysnam!=NULL; sp++)
		if (strcmp(sp->sysnam, w)==0)
			return(sp->sysval);
	return(0);
}

void main(argc, argv) int argc; char **argv; {
	struct sgttyb tb;
	int type;
	char *getenv();

	type=0;
	if (argc>=2 && strcmp(argv[1],"-n")==0) {
		margset--; argc--; argv++;
	}
	if (argc>=2) {
		type=syslook(argv[1]);
	} else {
		type=syslook(getenv("TERM"));
	}

	switch(type) {

		case DASI300:	dasi300(); break;

		case DASI300S:	dasi300(); break;

		case DASI450:	dasi450(); break;

		case TN300:	tn300(); break;

		case TTY37:	tty37(); break;

		case HP:	hp2645(); break;

		default:
				gtty (0, &tb);
				if ( (tb.sg_flags & (LCASE|CRMOD)) == CRMOD) {
					/* test for CR map on, upper case off, i.e. terminet but not 33 */
					if ((tb.sg_ispeed) == B300) /* test for 300 baud */
						misc();
				}
				else if ((tb.sg_flags & (CRMOD|LCASE)) == 0 && (tb.sg_ispeed ) == B150) {
					/* apparent model 37 */
					tty37();
				}
	}
}

void clear(n) int n; {
	escape(CLR); 
	delay(n);
	putchar(CR); nl();
}

void delay(n) int n; {
	while (n--) putchar(DEL);
}

void tabs(n) int n; {
	int i,j;

	if(margset) n--;

	for( i=0; i<n; ++i ){
		for( j=0; j<8; ++j ) {
			putchar(SP);
		}
		escape(SET);
	}
}

void margin(n) int n; {
	int i;

	if(margset) {
		for( i=0; i<n; ++i) putchar(SP);
	}
}

void escape(c) int c; {
	putchar(ESC); putchar(c);
}

void bs(n) int n; {
	while (n--) putchar(BS);
}

void nl() {
	putchar(NL);
}

/* ======== terminal types ======== */

void dasi450() {
	struct sgttyb t;
	gtty(0,&t);
	t.sg_flags &= ~ALLDELAY;
	stty(0,&t);
	clear(8); bs(16); margin(8); escape(MGN); nl(); tabs(16);
	escape(RHM); nl();
}

void tty37() {
	putchar(SI); clear(40); bs(8); tabs(9); nl();
}

void dasi300() {
	clear(8); tabs(15); nl();
}

void tn300() {
	struct sgttyb t;
	gtty(0,&t);
	t.sg_flags &= ~ALLDELAY;
	t.sg_flags |= CR1|BS1;
	stty(0,&t);
	clear(8); margin(8); escape(SET); tabs(14); nl();
}

void hp2645() {
	escape('3'); /*clr*/
	putchar(CR);
	tabs(10);
	nl();
}

void misc() {
	tabs(14); nl();
}
