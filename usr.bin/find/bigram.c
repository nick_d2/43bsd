#include <stdio.h>
#include <strings.h>

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)bigram.c	4.2	(Berkeley)	7/21/83";
#endif

/*
 *  bigram < text > bigrams
 * 
 * List bigrams for 'updatedb' script.
 * Use 'code' to encode a file using this output.
 */

/*#include <stdio.h>*/

#define MAXPATH	1024		/* maximum pathname length */

char path[MAXPATH];
char oldpath[MAXPATH] = " ";	

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

void main __P((void));
int prefix_length __P((char *s1, char *s2));

void main() {
  	register int count, j;

     	while ( gets ( path ) != NULL ) {

		count = prefix_length ( oldpath, path );
		/*
		   output post-residue bigrams only
		*/
		for ( j = count; path[j] != NULL; j += 2 ) {
			if ( path[j + 1] == NULL ) 
				break;
			putchar ( path[j] );
			putchar ( path[j + 1] );
			putchar ( '\n' );
		}
		strcpy ( oldpath, path );
   	}
}

int prefix_length(s1, s2)	/* return length of longest common prefix */ char *s1; char *s2;		/* ... of strings s1 and s2 */ {
	register char *start;

    	for ( start = s1; *s1 == *s2; s1++, s2++ )	
		if ( *s1 == NULL )		
	    		break;
    	return ( s1 - start );
}
