#ifndef _LDEFS_H_
#define _LDEFS_H_

#include <stdio.h>

#ifdef __STDC__
#include <stdint.h>
#else
typedef int intptr_t;
#endif

/*	ldefs.c	4.1	83/08/11	*/

/*# include <stdio.h>*/
# define PP 1
# ifdef unix

/*# define CWIDTH 7*/
/*# define CMASK 0177*/
# define ASCII 1
# endif

# ifdef gcos
/*# define CWIDTH 9*/
/*# define CMASK 0777*/
# define ASCII 1
# endif

# ifdef ibm
/*# define CWIDTH 8*/
/*# define CMASK 0377*/
# define EBCDIC 1
# endif

# ifdef ASCII
# define NCH 128
# endif

# ifdef EBCDIC
# define NCH 256
# endif

# define TOKENSIZE 1000
# define DEFSIZE 40
# define DEFCHAR 1000
# define STARTCHAR 100
# define STARTSIZE 256
# define CCLSIZE 1000
# ifdef SMALL
# define TREESIZE 600
# define NTRANS 1500
# define NSTATES 300
# define MAXPOS 1500
# define NOUTPUT 1500
# endif

# ifndef SMALL
# define TREESIZE 1000
# define NSTATES 500
# define MAXPOS 2500
# define NTRANS 2000
# define NOUTPUT 3000
# endif
# define NACTIONS 100
# define ALITTLEEXTRA 30

# define RCCL NCH+90
# define RNCCL NCH+91
# define RSTR NCH+92
# define RSCON NCH+93
# define RNEWE NCH+94
# define FINAL NCH+95
# define RNULLS NCH+96
# define RCAT NCH+97
# define STAR NCH+98
# define PLUS NCH+99
# define QUEST NCH+100
# define DIV NCH+101
# define BAR NCH+102
# define CARAT NCH+103
# define S1FINAL NCH+104
# define S2FINAL NCH+105

# define DEFSECTION 1
# define RULESECTION 2
# define ENDSECTION 5
# define TRUE 1
# define FALSE 0

# define PC 1
# define PS 1

# ifdef DEBUG
# define LINESIZE 110
extern int yydebug;
extern int debug;		/* 1 = on */
extern int charc;
# endif

# ifndef DEBUG
# define freturn(s) s
# endif

extern int sargc;
extern char **sargv;
extern char buf[520];
extern int ratfor;		/* 1 = ratfor, 0 = C */
extern int yyline;		/* line number of file */
extern int sect;
extern int eof;
extern int lgatflg;
extern int divflg;
extern int funcflag;
extern int pflag;
extern int casecount;
extern int chset;	/* 1 = char set modified */
extern FILE *fin, *fout, *fother, *errorf;
extern int fptr;
extern char *ratname, *cname;
extern int prev;	/* previous input character */
extern int pres;	/* present input character */
extern int peek;	/* next input character */
extern int *name;
extern intptr_t *left;
extern intptr_t *right;
extern int *parent;
extern char *nullstr;
extern int tptr;
extern char pushc[TOKENSIZE];
extern char *pushptr;
extern char slist[STARTSIZE];
extern char *slptr;
extern char **def, **subs, *dchar;
extern char **sname, *schar;
extern char *ccl;
extern char *ccptr;
extern char *dp, *sp;
extern int dptr, sptr;
extern char *bptr;		/* store input position */
extern char *tmpstat;
extern int count;
extern int **foll;
extern int *nxtpos;
extern int *positions;
extern int *gotof;
extern int *nexts;
extern char *nchar;
extern int **state;
extern int *sfall;		/* fallback state num */
extern char *cpackflg;		/* true if state has been character packed */
extern int *atable, aptr;
extern int nptr;
extern char symbol[NCH];
extern char cindex[NCH];
extern int xstate;
extern int stnum;
extern int ctable[];
extern int ZCH;
extern int ccount;
extern char match[NCH];
extern char extra[NACTIONS];
extern char *pcptr, *pchar;
extern int pchlen;
extern int nstates, maxpos;
extern int yytop;
extern int report;
extern int ntrans, treesize, outsize;
extern long rcount;
extern int optim;
extern int *verify, *advance, *stoff;
extern int scon;
extern char *psave;
/*extern char *calloc(), *myalloc();*/
/*extern int buserr(), segviol();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

#ifndef NORETURN
#ifdef __GNUC__
#define NORETURN __attribute__ ((__noreturn__))
#else
#define NORETURN
#endif
#endif

/* header.c */
void phead1 __P((void));
void chd1 __P((void));
void rhd1 __P((void));
void phead2 __P((void));
void chd2 __P((void));
void ptail __P((void));
void ctail __P((void));
void rtail __P((void));
void statistics __P((void));

/* lmain.c */
int main __P((int argc, char **argv));
void get1core __P((void));
void free1core __P((void));
void get2core __P((void));
void free2core __P((void));
void get3core __P((void));
int free3core __P((void));
char *myalloc __P((int a, int b));
int buserr __P((void));
int segviol __P((void));
void yyerror __P((char *s));

/* sub1.c */
char *getl __P((char *p));
int space __P((int ch));
int digit __P((int c));
void error __P((char *s, ...)) NORETURN;
void warning __P((char *s, ...));
int _index __P((int a, char *s));
int alpha __P((int c));
int printable __P((int c));
void lgate __P((void));
void scopy __P((char *s, char *t));
int siconv __P((char *t));
int slength __P((char *s));
int scomp __P((char *x, char *y));
int ctrans __P((char **ss));
void cclinter __P((int sw));
int usescape __P((int c));
int lookup __P((char *s, char **t));
int cpyact __P((void));
int gch __P((void));
int mn2 __P((int a, int d, int c));
int mn1 __P((int a, intptr_t d));
int mn0 __P((int a));
void munput __P((int t, intptr_t p));
int dupl __P((int n));
int allprint __P((int c));
int strpt __P((char *s));
int sect1dump __P((void));
int sect2dump __P((void));
int treedump __P((void));

/* sub2.c */
void cfoll __P((int v));
int pfoll __P((void));
void add __P((int **array, int n));
void follow __P((int v));
void first __P((int v));
void cgoto __P((void));
void nextstate __P((int s, int c));
int notin __P((int n));
void packtrans __P((int st, char *tch, int *tst, int cnt, int tryit));
int pstate __P((int s));
int member __P((int d, char *t));
int stprt __P((int i));
void acompute __P((int s));
int pccl __P((void));
void mkmatch __P((void));
void layout __P((void));
void rprint __P((int *a, char *s, int n));
void shiftr __P((int *a, int n));
void upone __P((int *a, int n));
void bprint __P((char *a, char *s, int n));
void padd __P((int **array, int n));

/* y.tab.c */
int yylex __P((void));
int XXXfreturn __P((int i));
int yyparse __P((void));

#endif
