#ifndef _COMPAT_4_1_H_
#define _COMPAT_4_1_H_

#include <sys/ioctl.h>
#include <sys/resource.h>
/*#include <sys/time.h> sys/resource.h*/
#include <sys/timeb.h>
#include <sys/times.h>
#include <sys/vlimit.h>
#include <sys/vtimes.h>

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* compat-4.1/getpw.c */
int getpw __P((int uid, char buf[]));

/* compat-4.1/pause.c */
int pause __P((void));

/* compat-4.1/rand.c */
int srand __P((unsigned x));
int rand __P((void));

/* compat-4.1/tell.c */
long tell __P((int f));

#endif
