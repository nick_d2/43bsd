#ifndef _NETDB_H_
#define _NETDB_H_

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)netdb.h	5.7 (Berkeley) 5/12/86
 */

/*
 * Structures returned by network
 * data base library.  All addresses
 * are supplied in host order, and
 * returned in network order (suitable
 * for use in system calls).
 */
struct	hostent {
	char	*h_name;	/* official name of host */
	char	**h_aliases;	/* alias list */
	int	h_addrtype;	/* host address type */
	int	h_length;	/* length of address */
	char	**h_addr_list;	/* list of addresses from name server */
#define	h_addr	h_addr_list[0]	/* address, for backward compatiblity */
};

/*
 * Assumption here is that a network number
 * fits in 32 bits -- probably a poor one.
 */
struct	netent {
	char		*n_name;	/* official name of net */
	char		**n_aliases;	/* alias list */
	int		n_addrtype;	/* net address type */
	unsigned long	n_net;		/* network # */
};

struct	servent {
	char	*s_name;	/* official service name */
	char	**s_aliases;	/* alias list */
	int	s_port;		/* port # */
	char	*s_proto;	/* protocol to use */
};

struct	protoent {
	char	*p_name;	/* official protocol name */
	char	**p_aliases;	/* alias list */
	int	p_proto;	/* protocol # */
};

/*struct hostent	*gethostbyname(), *gethostbyaddr(), *gethostent();*/
/*struct netent	*getnetbyname(), *getnetbyaddr(), *getnetent();*/
/*struct servent	*getservbyname(), *getservbyport(), *getservent();*/
/*struct protoent	*getprotobyname(), *getprotobynumber(), *getprotoent();*/

/*
 * Error return codes from gethostbyname() and gethostbyaddr()
 */

extern  int h_errno;	

#define	HOST_NOT_FOUND	1 /* Authoritive Answer Host not found */
#define	TRY_AGAIN	2 /* Non-Authoritive Host not found, or SERVERFAIL */
#define	NO_RECOVERY	3 /* Non recoverable errors, FORMERR, REFUSED, NOTIMP */
#define NO_ADDRESS	4 /* Valid host name, no address, look for MX record */

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* net/getnetbyaddr.c */
struct netent *getnetbyaddr __P((register int net, register int type));

/* net/getnetbyname.c */
struct netent *getnetbyname __P((register char *name));

/* net/getnetent.c */
int setnetent __P((int f));
int endnetent __P((void));
struct netent *getnetent __P((void));

/* net/getproto.c */
struct protoent *getprotobynumber __P((register int proto));

/* net/getprotoent.c */
int setprotoent __P((int f));
int endprotoent __P((void));
struct protoent *getprotoent __P((void));

/* net/getprotoname.c */
struct protoent *getprotobyname __P((register char *name));

/* net/getservbyname.c */
struct servent *getservbyname __P((char *name, char *proto));

/* net/getservbyport.c */
struct servent *getservbyport __P((int port, char *proto));

/* net/getservent.c */
int setservent __P((int f));
int endservent __P((void));
struct servent *getservent __P((void));

/* net/hosttable/gethostent.c */
int sethostent __P((int f));
int endhostent __P((void));
struct hostent *gethostent __P((void));
int sethostfile __P((char *file));

/* net/hosttable/gethostnamadr.c */
struct hostent *gethostbyname __P((register char *nam));
struct hostent *gethostbyaddr __P((char *addr, register int length, register int type));

/* net/named/gethostnamadr.c */
struct hostent *gethostbyname __P((char *name));
struct hostent *gethostbyaddr __P((char *addr, int len, int type));
int _sethtent __P((int f));
int _endhtent __P((void));
struct hostent *_gethtent __P((void));
struct hostent *_gethtbyname __P((char *name));
struct hostent *_gethtbyaddr __P((char *addr, int len, int type));

/* net/named/sethostent.c */
int sethostent __P((int stayopen));
int endhostent __P((void));
int sethostfile __P((char *name));

#endif
