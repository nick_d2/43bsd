#ifndef _STDIO_H_
#define _STDIO_H_

#include <sys/types.h>
#ifdef __STDC__
#include <stdarg.h>
#endif

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)stdio.h	5.3 (Berkeley) 3/15/86
 */

/*# ifndef FILE*/
#define	BUFSIZ	1024
extern	struct	_iobuf {
	int	_cnt;
	char	*_ptr;		/* should be unsigned char */
	char	*_base;		/* ditto */
	int	_bufsiz;
	short	_flag;
	char	_file;		/* should be short */
} _iob[];

#define	_IOREAD	01
#define	_IOWRT	02
#define	_IONBF	04
#define	_IOMYBUF	010
#define	_IOEOF	020
#define	_IOERR	040
#define	_IOSTRG	0100
#define	_IOLBF	0200
#define	_IORW	0400
#define	NULL	0
#define	FILE	struct _iobuf
#define	EOF	(-1)

#define	stdin	(&_iob[0])
#define	stdout	(&_iob[1])
#define	stderr	(&_iob[2])
#ifndef lint
#define	getc(p)		(--(p)->_cnt>=0? (int)(*(unsigned char *)(p)->_ptr++):_filbuf(p))
#endif
#define	getchar()	getc(stdin)
#ifndef lint
#define putc(x, p)	(--(p)->_cnt >= 0 ?\
	(int)(*(unsigned char *)(p)->_ptr++ = (x)) :\
	(((p)->_flag & _IOLBF) && -(p)->_cnt < (p)->_bufsiz ?\
		((*(p)->_ptr = (x)) != '\n' ?\
			(int)(*(unsigned char *)(p)->_ptr++) :\
			_flsbuf(*(unsigned char *)(p)->_ptr, p)) :\
		_flsbuf((unsigned char)(x), p)))
#endif
#define	putchar(x)	putc(x,stdout)
#define	feof(p)		(((p)->_flag&_IOEOF)!=0)
#define	ferror(p)	(((p)->_flag&_IOERR)!=0)
#define	fileno(p)	((p)->_file)
#define	clearerr(p)	((p)->_flag &= ~(_IOERR|_IOEOF))

/*FILE	*fopen();*/
/*FILE	*fdopen();*/
/*FILE	*freopen();*/
/*FILE	*popen();*/
/*long	ftell();*/
/*char	*fgets();*/
/*char	*gets();*/
#if 0 /*def vax*/
/*char	*sprintf();*/		/* too painful to do right */
#endif
/*# endif*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

#ifndef NORETURN
#ifdef __GNUC__
#define NORETURN __attribute__ ((__noreturn__))
#else
#define NORETURN
#endif
#endif

/* stdio/clrerr.c */
/*int clearerr __P((register FILE *iop));*/

/* stdio/doprnt.c */
int _doprnt __P((char *fmt, va_list argp, register FILE *fp));

/* stdio/doscan.c */
int _doscan __P((FILE *iop, register char *fmt, register va_list argp));

/* stdio/exit.c */
int exit __P((int code)) NORETURN;

/* stdio/fdopen.c */
FILE *fdopen __P((int fd, register char *mode));

/* stdio/fgetc.c */
int fgetc __P((FILE *fp));

/* stdio/fgets.c */
char *fgets __P((char *s, int n, register FILE *iop));

/* stdio/filbuf.c */
int _filbuf __P((register FILE *iop));

/* stdio/findiop.c */
FILE *_findiop __P((void));
int _f_morefiles __P((void));
void f_prealloc __P((void));
void _fwalk __P((register int (*function) __P((FILE *iop))));
void _cleanup __P((void));

/* stdio/flsbuf.c */
int _flsbuf __P((int c, register FILE *iop));
int fflush __P((register FILE *iop));
int fclose __P((register FILE *iop));

/* stdio/fopen.c */
FILE *fopen __P((char *file, register char *mode));

/* stdio/fprintf.c */
int fprintf __P((register FILE *iop, char *fmt, ...));

/* stdio/fputc.c */
int fputc __P((int c, register FILE *fp));

/* stdio/fputs.c */
int fputs __P((register char *s, register FILE *iop));

/* stdio/fread.c */
int fread __P((void *_ptr, unsigned size, unsigned count, register FILE *iop));

/* stdio/freopen.c */
FILE *freopen __P((char *file, register char *mode, register FILE *iop));

/* stdio/fseek.c */
int fseek __P((register FILE *iop, long offset, int ptrname));

/* stdio/ftell.c */
long ftell __P((register FILE *iop));

/* stdio/fwrite.c */
int fwrite __P((void *_ptr, unsigned size, unsigned count, register FILE *iop));

/* stdio/getchar.c */
/*int getchar __P((void));*/

/* stdio/gets.c */
char *gets __P((char *s));

/* stdio/getw.c */
int getw __P((register FILE *iop));

/* stdio/popen.c */
FILE *popen __P((char *cmd, char *mode));
int pclose __P((FILE *ptr));

/* stdio/printf.c */
int printf __P((char *fmt, ...));

/* stdio/putchar.c */
/*int putchar __P((register c));*/

/* stdio/puts.c */
int puts __P((register char *s));

/* stdio/putw.c */
int putw __P((int w, register FILE *iop));

/* stdio/rew.c */
int rewind __P((register FILE *iop));

/* stdio/scanf.c */
int scanf __P((char *fmt, ...));
int fscanf __P((FILE *iop, char *fmt, ...));
int sscanf __P((register char *str, char *fmt, ...));

/* stdio/setbuf.c */
int setbuf __P((register FILE *iop, char *buf));

/* stdio/setbuffer.c */
int setbuffer __P((register FILE *iop, char *buf, int size));
int setlinebuf __P((register FILE *iop));

/* stdio/sprintf.c */
int sprintf __P((char *str, char *fmt, ...));

/* stdio/strout.c */
int _strout __P((register count, register char *string, int adjust, register FILE *file, int fillch));

/* stdio/ungetc.c */
int ungetc __P((int c, register FILE *iop));

/* stdio/vfprintf.c */
int vfprintf __P((FILE *iop, char *fmt, va_list argp));

/* stdio/vprintf.c */
int vprintf __P((char *fmt, va_list argp));

/* stdio/vsprintf.c */
int vsprintf __P((char *str, char *fmt, va_list argp));

#endif
