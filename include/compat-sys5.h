#ifndef _COMPAT_SYS5_H_
#define _COMPAT_SYS5_H_

#include <memory.h>
#include <string.h>

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* compat-sys5/getopt.c */
int getopt __P((int nargc, char **nargv, char *ostr));

/* compat-sys5/tmpnam.c */
char *tmpnam __P((char *s));

#endif
