#ifndef _STRINGS_H_
#define _STRINGS_H_

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)strings.h	5.1 (Berkeley) 5/30/85
 */

/*
 * External function definitions
 * for routines described in string(3).
 */
/*char	*strcat();*/
/*char	*strncat();*/
/*int	strcmp();*/
/*int	strncmp();*/
/*char	*strcpy();*/
/*char	*strncpy();*/
/*int	strlen();*/
/*char	*index();*/
/*char	*rindex();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* gen/bcmp.c */
int bcmp __P((void *_b1, void *_b2, register int length));

/* gen/bcopy.c */
int bcopy __P((void *_src, void *_dst, register int length));

/* gen/bzero.c */
int bzero __P((void *_b, register int length));

/* gen/index.c */
char *index __P((register char *sp, int c));

/* gen/rindex.c */
char *rindex __P((register char *sp, int c));

/* gen/strcat.c */
char *strcat __P((register char *s1, register char *s2));

/* gen/strcmp.c */
int strcmp __P((register char *s1, register char *s2));

/* gen/strcpy.c */
char *strcpy __P((register char *s1, register char *s2));

/* gen/strlen.c */
int strlen __P((register char *s));

/* gen/strncat.c */
char *strncat __P((register char *s1, register char *s2, register n));

/* gen/strncmp.c */
int strncmp __P((register char *s1, register char *s2, register n));

/* gen/strncpy.c */
char *strncpy __P((register char *s1, register char *s2, int n));

#endif
