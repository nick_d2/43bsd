#ifndef _STRING_H_
#define _STRING_H_

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)string.h	5.1 (Berkeley) 85/08/05
 */

#include <memory.h>
#include <strings.h>

/*
 * these next few are obsolete trash
 */

/*extern char *strcpyn();*/
/*extern char *strcatn();*/
/*extern int strcmpn();*/

/*
 * and the rest are Sys5 functions supported just so
 * Sys5 progs will compile easily.
 */

/*extern char *strchr();*/
/*extern char *strrchr();*/
/*extern char *strpbrk();*/
/*extern int strspn();*/
/*extern int strcspn();*/
/*extern char *strtok();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* compat-sys5/strcatn.c */
char *strcatn __P((register char *s1, register char *s2, register n));

/* compat-sys5/strchr.c */
char *strchr __P((register char *sp, int c));

/* compat-sys5/strcmpn.c */
int strcmpn __P((register char *s1, register char *s2, register n));

/* compat-sys5/strcpyn.c */
char *strcpyn __P((register char *s1, register char *s2, int n));

/* compat-sys5/strcspn.c */
int strcspn __P((register char *s, register char *set));

/* compat-sys5/strpbrk.c */
char *strpbrk __P((register char *s, register char *brk));

/* compat-sys5/strrchr.c */
char *strrchr __P((register char *sp, int c));

/* compat-sys5/strspn.c */
int strspn __P((register char *s, register char *set));

/* compat-sys5/strtok.c */
char *strtok __P((register char *s, register char *sep));

#endif
