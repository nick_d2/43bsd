#ifndef _TIME_H_
#define _TIME_H_

#include <sys/types.h>

/*	time.h	1.1	85/03/13	*/

/*
 * Structure returned by gmtime and localtime calls (see ctime(3)).
 */
struct tm {
	int	tm_sec;
	int	tm_min;
	int	tm_hour;
	int	tm_mday;
	int	tm_mon;
	int	tm_year;
	int	tm_wday;
	int	tm_yday;
	int	tm_isdst;
};

/*extern	struct tm *gmtime(), *localtime();*/
/*extern	char *asctime(), *ctime();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* gen/ctime.c */
char *ctime __P((time_t *t));
struct tm *localtime __P((time_t *tim));
struct tm *gmtime __P((time_t *tim));
char *asctime __P((struct tm *t));
int dysize __P((int y));

#endif
