#ifndef _GRP_H_
#define _GRP_H_

/*	grp.h	4.1	83/05/03	*/

struct	group { /* see getgrent(3) */
	char	*gr_name;
	char	*gr_passwd;
	int	gr_gid;
	char	**gr_mem;
};

/*struct group *getgrent(), *getgrgid(), *getgrnam();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* gen/getgrent.c */
int setgrent __P((void));
int endgrent __P((void));
struct group *getgrent __P((void));

/* gen/getgrgid.c */
struct group *getgrgid __P((register gid));

/* gen/getgrnam.c */
struct group *getgrnam __P((register char *name));

/* gen/initgroups.c */
int initgroups __P((char *uname, int agroup));

#endif
