#ifndef _GEN_H_
#define _GEN_H_

#include <a.out.h>
#include <ctype.h>
#include <disktab.h>
#include <fstab.h>
#include <grp.h>
#include <math.h>
#include <ndbm.h>
#include <pwd.h>
#include <setjmp.h>
#include <strings.h>
#include <sys/dir.h>
#include <sys/errno.h>
/*#include <sys/exec.h> a.out.h*/
#include <sys/proc.h>
#include <sys/signal.h>
#include <sys/syslog.h>
/*#include <sys/time.h> sys/proc.h*/
/*#include <time.h> sys/proc.h*/
#include <ttyent.h>
#include <utmp.h>

/* formerly duplicated in gen/insque.c and gen/remque.c */
struct vaxque {		/* queue format expected by VAX queue instructions */
	struct vaxque	*vq_next;
	struct vaxque	*vq_prev;
};

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

#ifndef NORETURN
#ifdef __GNUC__
#define NORETURN __attribute__ ((__noreturn__))
#else
#define NORETURN
#endif
#endif

/* gen/abort.c */
int abort __P((void)) NORETURN;

/* gen/abs.c */
int abs __P((int i));

/* gen/atoi.c */
int atoi __P((register char *p));

/* gen/atol.c */
long atol __P((register char *p));

/* gen/calloc.c */
void *calloc __P((register unsigned num, register unsigned size));
int cfree __P((void *p));

/* gen/crypt.c */
int setkey __P((char *key));
int encrypt __P((char *block, int edflag));
char *crypt __P((char *pw, char *salt));

/* gen/ecvt.c */
char *ecvt __P((double arg, int ndigits, int *decpt, int *sign));
char *fcvt __P((double arg, int ndigits, int *decpt, int *sign));

/* gen/fakcu.c */
void _cleanup __P((void));

/* gen/ffs.c */
int ffs __P((register long mask));

/* gen/gcvt.c */
char *gcvt __P((double number, int ndigit, char *buf));

/* gen/getenv.c */
char *getenv __P((register char *name));

/* gen/getpass.c */
char *getpass __P((char *prompt));

/* gen/getusershell.c */
char *getusershell __P((void));
int endusershell __P((void));
int setusershell __P((void));

/* gen/insque.c */
int insque __P((register struct vaxque *e, register struct vaxque *prev));

/* gen/malloc.c */
void *malloc __P((unsigned nbytes));
void morecore __P((int bucket));
void free __P((void *cp));
void *realloc __P((void *cp, unsigned nbytes));
void mstats __P((char *s));

/* gen/mkstemp.c */
int mkstemp __P((char *as));

/* gen/mktemp.c */
char *mktemp __P((char *as));

/* gen/qsort.c */
void qsort __P((void *_base, int n, int size, int (*compar)(void *p0, void *p1)));

/* gen/random.c */
int srandom __P((unsigned x));
char *initstate __P((unsigned seed, char *arg_state, int n));
char *setstate __P((char *arg_state));
long random __P((void));

/* gen/regex.c */
char *re_comp __P((register char *sp));
int re_exec __P((register char *p1));
int backref __P((register int i, register char *lp));
int cclass __P((register char *set, int c, int af));

/* gen/remque.c */
int remque __P((register struct vaxque *e));

/* gen/swab.c */
int swab __P((register char *from, register char *to, register int n));

/* gen/timezone.c */
char *timezone __P((int zone, int dst));

/* gen/valloc.c */
void *valloc __P((int i));

#endif
