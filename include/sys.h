#ifndef _SYS_H_
#define _SYS_H_

#include <sys/acct.h>
#include <sys/dir.h>
#include <sys/exec.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/proc.h>
#include <sys/ptrace.h>
#include <sys/quota.h>
/*#include <sys/resource.h> sys/wait.h*/
#include <sys/select.h>
#include <sys/signal.h>
/*#include <sys/socket.h> sys/ioctl.h*/
#include <sys/stat.h>
/*#include <sys/time.h> sys/proc.h*/
#include <sys/uio.h>
#include <sys/wait.h>

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* sys/gethostid.c */
long gethostid __P((void));

/* sys/gethostname.c */
int gethostname __P((char *n, int l));

/* sys/getpagesize.c */
int getpagesize __P((void));

/* sys/profil.c */
void profil __P((char *b, int s, int o, int i));

/* sys/reboot.c */
void reboot __P((int h));

/* sys/sethostid.c */
int sethostid __P((long h));

/* sys/sethostname.c */
int sethostname __P((char *n, int l));

/* sys/shutdown.c */
int shutdown __P((int s, int h));

/* sys/swapon.c */
int swapon __P((char *s));

/* sys/vhangup.c */
void vhangup __P((void));

#endif
