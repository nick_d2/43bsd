#ifndef _SETJMP_H_
#define _SETJMP_H_

/*	setjmp.h	4.1	83/05/03	*/

typedef int jmp_buf[10];

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* gen/_setjmp.c */
int _setjmp __P((jmp_buf e));
void _longjmp __P((jmp_buf e, int v));

/* gen/setjmp.c */
int setjmp __P((jmp_buf e));
void longjmp __P((jmp_buf e, int v));

/* gen/setjmperr.c */
int longjmperror __P((void));

#endif
