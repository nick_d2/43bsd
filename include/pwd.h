#ifndef _PWD_H_
#define _PWD_H_

/*	pwd.h	4.1	83/05/03	*/

struct	passwd { /* see getpwent(3) */
	char	*pw_name;
	char	*pw_passwd;
	int	pw_uid;
	int	pw_gid;
	int	pw_quota;
	char	*pw_comment;
	char	*pw_gecos;
	char	*pw_dir;
	char	*pw_shell;
};

/*struct passwd *getpwent(), *getpwuid(), *getpwnam();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* gen/getpwent.c */
int setpwent __P((void));
int endpwent __P((void));
struct passwd *getpwent __P((void));
int setpwfile __P((char *file));

/* gen/getpwnamuid.c */
struct passwd *getpwnam __P((char *nam));
struct passwd *getpwuid __P((int uid));

#endif
