#ifndef _ARPA_INET_H_
#define _ARPA_INET_H_

#include <netinet/in.h>
/*#include <sys/types.h> netinet/in.h*/

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)inet.h	5.1 (Berkeley) 5/30/85
 */

/*
 * External definitions for
 * functions in inet(3N)
 */
/*unsigned long inet_addr();*/
/*char	*inet_ntoa();*/
/*struct	in_addr inet_makeaddr();*/
/*unsigned long inet_network();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* inet/inet_addr.c */
u_long inet_addr __P((register char *cp));

/* inet/inet_lnaof.c */
int inet_lnaof __P((struct in_addr in));

/* inet/inet_makeaddr.c */
struct in_addr inet_makeaddr __P((int net, int host));

/* inet/inet_netof.c */
int inet_netof __P((struct in_addr in));

/* inet/inet_network.c */
u_long inet_network __P((register char *cp));

/* inet/inet_ntoa.c */
char *inet_ntoa __P((struct in_addr in));

#endif
