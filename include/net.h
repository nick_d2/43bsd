#ifndef _NET_H_
#define _NET_H_

#include <arpa/nameser.h>
#include <netdb.h>
/*#include <stdio.h> arpa/nameser.h*/
/*#include <sys/types.h> arpa/nameser.h*/

/*
 * Functions for number representation conversion.
 */
/* formerly duplicated in netinet/in.h and netns/ns.h */
/*u_short	ntohs(), htons();*/
/*u_long	ntohl(), htonl();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* net/htonl.c */
u_long htonl __P((u_long hostlong));

/* net/htons.c */
u_short htons __P((int hostshort));

/* net/ntohl.c */
u_long ntohl __P((u_long netlong));

/* net/ntohs.c */
u_short ntohs __P((int netshort));

/* net/rcmd.c */
int rcmd __P((char **ahost, int rport, char *locuser, char *remuser, char *cmd, int *fd2p));
int rresvport __P((int *alport));
int ruserok __P((char *rhost, int superuser, char *ruser, char *luser));
int _validuser __P((FILE *hostf, char *rhost, char *luser, char *ruser, int baselen));
int _checkhost __P((char *rhost, char *lhost, int len));

/* net/rexec.c */
int rexec __P((char **ahost, int rport, char *name, char *pass, char *cmd, int *fd2p));

/* net/ruserpass.c */
void ruserpass __P((char *host, char **aname, char **apass));
void mkpwunclear __P((char *spasswd, int mch, char *sencpasswd));
void mkpwclear __P((char *sencpasswd, int mch, char *spasswd));

#endif
