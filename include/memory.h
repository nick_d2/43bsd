#ifndef _MEMORY_H_
#define _MEMORY_H_

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)memory.h	5.1 (Berkeley) 85/08/05
 */

/*
 * Definitions of the Sys5 compat memory manipulation routines
 */

/*extern char *memccpy();*/
/*extern char *memchr();*/
/*extern int memcmp();*/
/*extern char *memcpy();*/
/*extern char *memset();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* compat-sys5/memccpy.c */
char *memccpy __P((void *_t, void *_f, register c, register n));

/* compat-sys5/memchr.c */
void *memchr __P((void *_s, register c, register n));

/* compat-sys5/memcmp.c */
int memcmp __P((void *_s1, void *_s2, register n));

/* compat-sys5/memcpy.c */
void *memcpy __P((void *_t, void *_f, register n));

/* compat-sys5/memset.c */
void *memset __P((void *_s, register c, register n));

#endif
