#!/bin/sh -e

ROOT=`pwd`
STAGE0=$ROOT/stage0
STAGE1=$ROOT/stage1
OLDPATH="$PATH"

## these should always exist, but are sometimes lost due to git on empty dirs
#mkdir -p lib/libc/compat-4.1/profiled
#mkdir -p lib/libc/compat-sys5/profiled
#mkdir -p lib/libc/gen/profiled
#mkdir -p lib/libc/inet/profiled
#mkdir -p lib/libc/linux/gen/profiled
#mkdir -p lib/libc/linux/profiled
#mkdir -p lib/libc/linux/sys/profiled
#mkdir -p lib/libc/net/hosttable/profiled
#mkdir -p lib/libc/net/named/profiled
#mkdir -p lib/libc/net/profiled
#mkdir -p lib/libc/ns/profiled
#mkdir -p lib/libc/stdio/profiled
#mkdir -p lib/libc/sys/profiled
#
#rm -rf $STAGE0
#mkdir -p $STAGE0/bin
#mkdir -p $STAGE0/lib
#mkdir -p $STAGE0/usr/bin
#mkdir -p $STAGE0/usr/include
#mkdir -p $STAGE0/usr/lib
#
#(cd xify && make clean && PATH="$OLDPATH" make && make DESTDIR=$STAGE0 install)
#(cd include && make SHARED=copies DESTDIR=$STAGE0 install)
#cat <<EOF >$STAGE0/usr/include/setjmp.h
##pragma include <setjmp.h>
##pragma keyword setjmp longjmp
##pragma typedef jmp_buf
#EOF
#cat <<EOF >$STAGE0/usr/include/stdarg.h
##pragma include <stdarg.h>
##pragma keyword va_start va_arg va_end
##pragma typedef va_list
#EOF
#cat <<EOF >$STAGE0/usr/include/stdint.h
##pragma include <stdint.h>
##pragma typedef int8_t int16_t int32_t int64_t intptr_t ssize_t
##pragma typedef uint8_t uint16_t uint32_t uint64_t uintptr_t size_t
#EOF
#cat <<EOF >$STAGE0/usr/include/varargs.h
##pragma include <varargs.h>
##pragma keyword va_alist va_dcl va_start va_arg va_end
##pragma typedef va_list
#EOF
#
#export PATH="$STAGE0/bin:$STAGE0/usr/bin:$OLDPATH"
#
#(cd lib/libc && make clean && make -f Makefile.linux && make -f Makefile.linux DESTDIR=$STAGE0 install)
#(cd usr.bin/yacc && make clean && make && make DESTDIR=$STAGE0 install)
#cat - usr.bin/yacc/yaccpar <<EOF >$STAGE0/usr/lib/yaccpar
##pragma ifdef __GNUC__
##pragma pragma GCC diagnostic ignored "-Wunused-label"
##pragma endif
#EOF
#(cd usr.bin/lex && make clean && make && make DESTDIR=$STAGE0 install)
#
#rm -rf $STAGE1
#mkdir -p $STAGE1/bin
#mkdir -p $STAGE1/lib
#mkdir -p $STAGE1/usr/bin
#mkdir -p $STAGE1/usr/include
#mkdir -p $STAGE1/usr/lib
#
#(cd bin && make clean && make SUBDIR="as csh diff sed sh tp" NSTD= KMEM= && make DESTDIR=$STAGE1 SUBDIR="as csh diff sed sh tp" NSTD= KMEM= install)
#(cd usr.bin && make clean && make SUBDIR="find lex yacc" NSTD= KMEM= && make DESTDIR=$STAGE1 SUBDIR="find lex yacc" NSTD= KMEM= install)
#(cd lib && make clean && make SUBDIR="c2 cpp pcc" && make SUBDIR="c2 cpp pcc" DESTDIR=$STAGE1 install)
#(cd include && make SHARED=copies DESTDIR=$STAGE1 install)
#
export PATH="$STAGE1/bin:$STAGE1/usr/bin:$OLDPATH"

(cd lib/libc && make clean && make CPP=$STAGE1/lib/cpp && make DESTDIR=$STAGE1 install)
