#include <gen.h>
#include <stdio.h>
/*#include <strings.h> gen.h*/
/*#include <sys/dir.h> gen.h*/
/*#include <sys/exec.h> gen.h*/
#include <sys/file.h>
/*#include <sys/proc.h> gen.h*/
/*#include <sys/signal.h> gen.h*/
#include <sys/wait.h>

#ifdef X_
#include <sys/stat.h>
#endif

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)cc.c 4.13 9/18/85";
#endif
/*
 * cc - front end for C compiler
 */
/*#include <sys/param.h>*/
/*#include <stdio.h>*/
/*#include <ctype.h>*/
/*#include <signal.h>*/
/*#include <sys/dir.h>*/

#ifdef X_
char	bin[MAXPATHLEN];
char	*cpp;
char	*ccom;
char	*sccom;
char	*c2;
char	*as;
char	*ld;
char	*crt0;
#else
char	*cpp = "/lib/cpp";
char	*ccom = "/lib/ccom";
char	*sccom = "/lib/sccom";
char	*c2 = "/lib/c2";
char	*as = "/bin/as";
char	*ld = "/bin/ld";
char	*crt0 = "/lib/crt0.o";
#endif

char	tmp0[30];		/* big enough for /tmp/ctm%05.5d */
char	*tmp1, *tmp2, *tmp3, *tmp4, *tmp5;
char	*outfile;
/*char	*savestr(), *strspl(), *setsuf();*/
/*int	idexit();*/
char	**av, **clist, **llist, **plist;
int	cflag, eflag, oflag, pflag, sflag, wflag, Rflag, exflag, proflag;
int	fflag, gflag, Gflag, Mflag, debug;
char	*dflag;
int	exfail;
char	*chpass;
char	*npassname;

int	nc, nl, np, nxo, na;

#define	cunlink(s)	if (s) unlink(s)

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

void main __P((int argc, char **argv));
void idexit __P((void));
void dexit __P((void));
void error __P((char *s, char *x));
int getsuf __P((char as[]));
char *setsuf __P((char *as, int ch));
int callsys __P((char *f, char **v));
int nodup __P((char **l, char *os));
char *savestr __P((register char *cp));
char *strspl __P((char *left, char *right));

void main(argc, argv) int argc; char **argv; {
	char *t;
	char *assource;
	int i, j, c;
#ifdef X_
	char *pathptr;
	struct stat statbuf;

	for (i = strlen(argv[0]); i > 0 && argv[0][i - 1] != '/'; --i)
		;
	if (i)
		bcopy(argv[0], bin, i);
	else if ((pathptr = getenv("PATH")) != 0)
		do {
			for (; pathptr[i] && pathptr[i] != ':'; ++i)
				;
			bcopy(pathptr, bin, i);
			bin[i++] = '/';
			strcpy(bin + i, argv[0]);
			if (stat(bin, &statbuf) == 0)
				break;
			pathptr += i;
			i = 0;
		} while (pathptr[-1]);
	bin[i] = 0;
	cpp = strspl(bin, "../lib/cpp");
	ccom = strspl(bin, "../lib/ccom");
	sccom = strspl(bin, "../lib/sccom");
	c2 = strspl(bin, "../lib/c2");
	as = strspl(bin, "as");
	ld = strspl(bin, "ld");
	crt0 = strspl(bin, "../lib/crt0.o");
#endif

	/* ld currently adds upto 5 args; 10 is room to spare */
	av = (char **)calloc(argc+10, sizeof (char **));
	clist = (char **)calloc(argc, sizeof (char **));
	llist = (char **)calloc(argc, sizeof (char **));
	plist = (char **)calloc(argc, sizeof (char **));
	for (i = 1; i < argc; i++) {
		if (*argv[i] == '-') switch (argv[i][1]) {

		case 'S':
			sflag++;
			cflag++;
			continue;
		case 'o':
			if (++i < argc) {
				outfile = argv[i];
				switch (getsuf(outfile)) {

				case 'c':
					error("-o would overwrite %s",
					    outfile);
					exit(8);
				}
			}
			continue;
		case 'R':
			Rflag++;
			continue;
		case 'O':
			oflag++;
			continue;
		case 'p':
			proflag++;
#ifdef X_
			crt0 = strspl(bin, argv[i][2] == 'g' ? "../usr/lib/gcrt0.o" : "../lib/mcrt0.o");
#else
			crt0 = "/lib/mcrt0.o";
			if (argv[i][2] == 'g')
				crt0 = "/usr/lib/gcrt0.o";
#endif
			continue;
		case 'f':
			fflag++;
			continue;
		case 'g':
			if (argv[i][2] == 'o') {
			    Gflag++;	/* old format for -go */
			} else {
			    gflag++;	/* new format for -g */
			}
			continue;
		case 'w':
			wflag++;
			continue;
		case 'E':
			exflag++;
		case 'P':
			pflag++;
			if (argv[i][1]=='P')
				fprintf(stderr,
	"cc: warning: -P option obsolete; you should use -E instead\n");
			plist[np++] = argv[i];
		case 'c':
			cflag++;
			continue;
		case 'M':
			exflag++;
			pflag++;
			Mflag++;
			/* and fall through */
		case 'D':
		case 'I':
		case 'U':
		case 'C':
			plist[np++] = argv[i];
			continue;
		case 'L':
			llist[nl++] = argv[i];
			continue;
		case 't':
			if (chpass)
				error("-t overwrites earlier option", 0);
			chpass = argv[i]+2;
			if (chpass[0]==0)
				chpass = "012p";
			continue;
		case 'B':
			if (npassname)
				error("-B overwrites earlier option", 0);
			npassname = argv[i]+2;
			if (npassname[0]==0)
				npassname = "/usr/c/o";
			continue;
		case 'd':
			if (argv[i][2] == '\0') {
				debug++;
				continue;
			}
			dflag = argv[i];
			continue;
		}
		t = argv[i];
		c = getsuf(t);
		if (c=='c' || c=='s' || exflag) {
			clist[nc++] = t;
			t = setsuf(t, 'o');
		}
		if (nodup(llist, t)) {
			llist[nl++] = t;
			if (getsuf(t)=='o')
				nxo++;
		}
	}
	if (gflag || Gflag) {
		if (oflag)
			fprintf(stderr, "cc: warning: -g disables -O\n");
		oflag = 0;
	}
	if (npassname && chpass ==0)
		chpass = "012p";
	if (chpass && npassname==0)
		npassname = "/usr/new";
	if (chpass)
	for (t=chpass; *t; t++) {
		switch (*t) {

		case '0':
			if (fflag)
				sccom = strspl(npassname, "sccom");
			else
				ccom = strspl(npassname, "ccom");
			continue;
		case '2':
			c2 = strspl(npassname, "c2");
			continue;
		case 'p':
			cpp = strspl(npassname, "cpp");
			continue;
		}
	}
	if (nc==0)
		goto nocom;
	if (signal(SIGINT, SIG_IGN) != SIG_IGN)
		signal(SIGINT, (void (*) __P((int sig)))idexit);
	if (signal(SIGTERM, SIG_IGN) != SIG_IGN)
		signal(SIGTERM, (void (*) __P((int sig)))idexit);
	if (signal(SIGHUP, SIG_IGN) != SIG_IGN)
		signal(SIGHUP, (void (*) __P((int sig)))idexit);
	if (pflag==0)
		sprintf(tmp0, "/tmp/ctm%05.5d", getpid());
	tmp1 = strspl(tmp0, "1");
	tmp2 = strspl(tmp0, "2");
	tmp3 = strspl(tmp0, "3");
	if (pflag==0)
		tmp4 = strspl(tmp0, "4");
	if (oflag)
		tmp5 = strspl(tmp0, "5");
	for (i=0; i<nc; i++) {
		if (nc > 1 && !Mflag) {
			printf("%s:\n", clist[i]);
			fflush(stdout);
		}
		if (!Mflag && getsuf(clist[i]) == 's') {
			assource = clist[i];
			goto assemble;
		} else
			assource = tmp3;
		if (pflag)
			tmp4 = setsuf(clist[i], 'i');
		av[0] = "cpp"; av[1] = clist[i];
		na = 2;
		if (!exflag)
			av[na++] = tmp4;
		for (j = 0; j < np; j++)
			av[na++] = plist[j];
		av[na++] = 0;
		if (callsys(cpp, av)) {
			exfail++;
			eflag++;
		}
		if (pflag || exfail) {
			cflag++;
			continue;
		}
		if (sflag) {
			if (nc==1 && outfile)
				tmp3 = outfile;
			else
				tmp3 = setsuf(clist[i], 's');
			assource = tmp3;
		}
		av[0] = fflag ? "sccom" : "ccom";
		av[1] = tmp4; av[2] = oflag?tmp5:tmp3; na = 3;
		if (proflag)
			av[na++] = "-XP";
		if (gflag) {
			av[na++] = "-Xg";
		} else if (Gflag) {
			av[na++] = "-XG";
		}
		if (wflag)
			av[na++] = "-w";
		av[na] = 0;
		if (callsys(fflag ? sccom : ccom, av)) {
			cflag++;
			eflag++;
			continue;
		}
		if (oflag) {
			av[0] = "c2"; av[1] = tmp5; av[2] = tmp3; av[3] = 0;
			if (callsys(c2, av)) {
				unlink(tmp3);
				tmp3 = assource = tmp5;
			} else
				unlink(tmp5);
		}
		if (sflag)
			continue;
	assemble:
		cunlink(tmp1); cunlink(tmp2); cunlink(tmp4);
		av[0] = "as"; av[1] = "-o";
		if (cflag && nc==1 && outfile)
			av[2] = outfile;
		else
			av[2] = setsuf(clist[i], 'o');
		na = 3;
		if (Rflag)
			av[na++] = "-R";
		if (dflag)
			av[na++] = dflag;
		av[na++] = assource;
		av[na] = 0;
		if (callsys(as, av) > 1) {
			cflag++;
			eflag++;
			continue;
		}
	}
nocom:
	if (cflag==0 && nl!=0) {
		i = 0;
		av[0] = "ld"; av[1] = "-X"; av[2] = crt0; na = 3;
		if (outfile) {
			av[na++] = "-o";
			av[na++] = outfile;
		}
		while (i < nl)
			av[na++] = llist[i++];
		if (gflag || Gflag)
			av[na++] = "-lg";
		if (proflag)
			av[na++] = "-lc_p";
		else
			av[na++] = "-lc";
		av[na++] = 0;
		eflag |= callsys(ld, av);
		if (nc==1 && nxo==1 && eflag==0)
			unlink(setsuf(clist[0], 'o'));
	}
	dexit();
}

void idexit() {

	eflag = 100;
	dexit();
}

void dexit() {

	if (!pflag) {
		cunlink(tmp1);
		cunlink(tmp2);
		if (sflag==0)
			cunlink(tmp3);
		cunlink(tmp4);
		cunlink(tmp5);
	}
	exit(eflag);
}

void error(s, x) char *s; char *x; {
	FILE *diag = exflag ? stderr : stdout;

	fprintf(diag, "cc: ");
	fprintf(diag, s, x);
	putc('\n', diag);
	exfail++;
	cflag++;
	eflag++;
}

int getsuf(as) char as[]; {
	register int c;
	register char *s;
	register int t;

	s = as;
	c = 0;
	while (t = *s++)
		if (t=='/')
			c = 0;
		else
			c++;
	s -= 3;
	if (c <= MAXNAMLEN && c > 2 && *s++ == '.')
		return (*s);
	return (0);
}

char *setsuf(as, ch) char *as; int ch; {
	register char *s, *s1;

	s = s1 = savestr(as);
	while (*s)
		if (*s++ == '/')
			s1 = s;
	s[-1] = ch;
	return (s1);
}

int callsys(f, v) char *f; char **v; {
	int t, status;
	char **cpp;

#ifdef X_
	v[0] = f; /* we have to tell cpp and ld where they are located */
#endif
	if (debug) {
#ifndef X_
		fprintf(stderr, "%s:", f);
#endif
		for (cpp = v; *cpp != 0; cpp++)
			fprintf(stderr, " %s", *cpp);
		fprintf(stderr, "\n");
	}
	t = vfork();
	if (t == -1) {
		printf("No more processes\n");
		return (100);
	}
	if (t == 0) {
		execv(f, v);
		printf("Can't find %s\n", f);
		fflush(stdout);
		_exit(100);
	}
	while (t != wait(&status))
		;
	if ((t=(status&0377)) != 0 && t!=14) {
		if (t!=2) {
			printf("Fatal error in %s\n", f);
			eflag = 8;
		}
		dexit();
	}
	return ((status>>8) & 0377);
}

int nodup(l, os) char **l; char *os; {
	register char *t, *s;
	register int c;

	s = os;
	if (getsuf(s) != 'o')
		return (1);
	while (t = *l++) {
		while (c = *s++)
			if (c != *t++)
				break;
		if (*t==0 && c==0)
			return (0);
		s = os;
	}
	return (1);
}

#define	NSAVETAB	1024
char	*savetab;
int	saveleft;

char *savestr(cp) register char *cp; {
	register int len;

	len = strlen(cp) + 1;
	if (len > saveleft) {
		saveleft = NSAVETAB;
		if (len > saveleft)
			saveleft = len;
		savetab = (char *)malloc(saveleft);
		if (savetab == 0) {
			fprintf(stderr, "ran out of memory (savestr)\n");
			exit(1);
		}
	}
	strncpy(savetab, cp, len);
	cp = savetab;
	savetab += len;
	saveleft -= len;
	return (cp);
}

char *strspl(left, right) char *left; char *right; {
	char buf[BUFSIZ];

	strcpy(buf, left);
	strcat(buf, right);
	return (savestr(buf));
}
