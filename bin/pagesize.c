#include <stdio.h>
#include <sys.h>

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(DOCOPYRIGHT) && !defined(lint)
char copyright[] =
"@(#) Copyright (c) 1983 Regents of the University of California.\n\
 All rights reserved.\n";
#endif

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)pagesize.c	5.1 (Berkeley) 4/30/85";
#endif

void main() {

	printf("%d\n", getpagesize());
}
