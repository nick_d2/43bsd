#include <stdio.h>
#include <sys/errno.h>
#include <sys/stat.h>

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(DOCOPYRIGHT) && !defined(lint)
char copyright[] =
"@(#) Copyright (c) 1983 Regents of the University of California.\n\
 All rights reserved.\n";
#endif

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)mkdir.c	5.1 (Berkeley) 4/30/85";
#endif

/*
 * make directory
 */
/*#include <stdio.h>*/

void main(argc, argv) int argc; char *argv[]; {
	int errors = 0;

	if (argc < 2) {
		fprintf(stderr, "usage: %s directory ...\n", argv[0]);
		exit(1);
	}
	while (--argc)
		if (mkdir(*++argv, 0777) < 0) {
			fprintf(stderr, "mkdir: ");
			perror(*argv);
			errors++;
		}
	exit(errors != 0);
}
