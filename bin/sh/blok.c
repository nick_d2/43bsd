#include <gen.h>
/*#include "brkincr.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "stak.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)blok.c	4.2 8/11/83";
#endif

/*
 *	UNIX shell
 *
 *	S. R. Bourne
 *	Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

/*
 *	storage allocator
 *	(circular first fit strategy)
 */

#define BUSY 01
#define busy(x)	((intptr_t)(x)->word&BUSY)

unsigned		brkincr=BRKINCR;
struct blk		*blokp;			/*current search pointer*/
struct blk		*bloktop/*=(struct blk *)end*/;	/*top of arena (last blok)*/

void *malloc(nbytes) unsigned nbytes; {
	register unsigned		rbytes = round(nbytes+sizeof(char *),sizeof(char *));

	for(;;) {
		int		c=0;
		register struct blk	*p = blokp;
		register struct blk	*q;
		do {
			if (!busy(p)) {
				while (!busy(q = p->word)) { p->word = q->word; }
				if ((char *)q-(char *)p >= rbytes) {
					blokp = (struct blk *)((char *)p+rbytes);
					if (q > blokp) {
						blokp->word = p->word;
					}
					p->word=(struct blk *)((intptr_t)blokp|BUSY);
					return((void *)(p+1));
				}
			}
			q = p; p = (struct blk *)((intptr_t)p->word&~BUSY);
		} while (p>q || (c++)==0);
		addblok(rbytes);
	}
}

void addblok(reqd) unsigned reqd; {
	if (stakbas!=staktop) {
		register char	*rndstak;
		register struct blk	*blokstak;

		pushstak(0);
		rndstak=(char *)round((intptr_t)staktop,sizeof(char *));
		blokstak=(struct blk *)stakbas-1;
		blokstak->word=stakbsy; stakbsy=blokstak;
		bloktop->word=(struct blk *)((intptr_t)rndstak|BUSY);
		bloktop=(struct blk *)rndstak;
	}
	reqd += brkincr; reqd &= ~(brkincr-1);
	blokp=bloktop;
	bloktop=bloktop->word=(struct blk *)((char *)bloktop+reqd);
	bloktop->word=(struct blk *)((void *)end+1); {
	     
	   register char *stakadr=(char *)(bloktop+2);
	   staktop=movstr(stakbot,stakadr);
	   stakbas=stakbot=stakadr;
	}
}

void free(ap) void *ap; {
#define p ((struct blk *)ap)
	if (p && p<bloktop) {
		p[-1].word = (struct blk *)((intptr_t)p[-1].word&~BUSY);
	}
#undef p
}

#ifdef X_
/* simple realloc, because host's libc malloc() has been redirected to here */
void *realloc(ap, nbytes) void *ap; unsigned nbytes; {
#define p ((struct blk *)ap)
	void *res = malloc(nbytes);
	if (res && p && p<bloktop) {
		unsigned onbytes = (char *)p[-1].word - (char *)p;
		bcopy(p, res, onbytes < nbytes ? onbytes : nbytes);
		free(p);
	}
	return res;
#undef p
}
#endif

#ifdef DEBUG
int chkbptr(ptr) struct blk *ptr; {
	int		exf=0;
	register struct blk	*p = end;
	register struct blk	*q;
	int		us=0, un=0;

	for(;;) {
	    
	   q = (intptr_t)p->word&~BUSY;
	   if (p==ptr) { exf++; }
	   if (q<end || q>bloktop) { abort(3); }
	   if (p==bloktop) { break; }
	   if (busy(p)) {
		us += q-p;
	   }
	   else {
		un += q-p;
	   }
	   if (p>=q) { abort(4); }
	   p=q;
	}
	if (exf==0) { abort(1); }
	prn(un); prc(' '); prn(us); prc('\n');
}
#endif
