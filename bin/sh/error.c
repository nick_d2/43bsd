/*#include <setjmp.h> defs.h*/
/*#include <sys/file.h> defs.h*/
#include <sys/proc.h>
#include "defs.h"
/*#include "mode.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)error.c	4.2 8/11/83";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

/* ========	error handling	======== */

int exitset() {
	assnum(&exitadr,exitval);
}

int sigchk() {
	/* Find out if it is time to go away.
	 * `trapnote' is set to SIGSET when fault is seen and
	 * no trap has been set.
	 */
	if (trapnote&SIGSET) {
		exitsh(SIGFAIL);
	}
}

int failed(s1, s2) char *s1; char *s2; {
	prp(); prs(s1); 
	if (s2) {
		prs(colon); prs(s2);
	}
	newline(); exitsh(ERROR);
}

int error(s) char *s; {
	failed(s,NULL);
}

int exitsh(xno) int xno; {
	/* Arrive here from `FATAL' errors
	 *  a) exit command,
	 *  b) default trap,
	 *  c) fault with no trap set.
	 *
	 * Action is to return to command level or exit.
	 */
	exitval=xno;
	if ((flags & (forked|errflg|ttyflg)) != ttyflg) {
		done();
	}
	else {
		clearup();
		longjmp(errshell,1);
	}
}

int done() {
	register char	*t;
	if (t=trapcom[0]) {
		trapcom[0]=0; /*should free but not long */
		execexp(t,0);
	}
	rmtemp(0);
	/* formerly exit(), may have been an oversight as we do not use stdio */
	_exit(exitval);
}

int rmtemp(base) char *base; {
	while ((char *)iotemp>base) {
	    unlink(iotemp->ioname);
	    iotemp=iotemp->iolst;
	}
}
