#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "stak.h" defs.h*/
#include "sym.h"

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)cmd.c	4.2 8/11/83";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/
/*#include	"sym.h"*/

/*extern struct ionod	*inout();*/
/*extern void		chkword();*/
/*extern void		chksym();*/
/*extern struct trenod	*term();*/
/*extern struct trenod	*makelist();*/
/*extern struct trenod	*list();*/
/*extern struct regnod	*syncase();*/
/*extern struct trenod	*item();*/
/*extern int		skipnl();*/
/*extern void		prsym();*/
/*extern void		synbad();*/

/* ========	command line decoding	========*/

static struct trenod *makelist __P((int type, struct trenod *i, struct trenod *r));
static struct trenod *list __P((int flg));
static struct trenod *term __P((int flg));
static struct regnod *syncase __P((register int esym));
static struct trenod *item __P((int flag));
static int skipnl __P((void));
static struct ionod *inout __P((struct ionod *lastio));
static void chkword __P((void));
static void chksym __P((int sym));
static void prsym __P((int sym));
static void synbad __P((void));

struct trenod *makefork(flgs, i) int flgs; struct trenod *i; {
	register struct forknod	*t;

	t=(struct forknod *)getstak(sizeof(struct forknod));
	t->forktyp=flgs|TFORK; t->forktre=i; t->forkio=0;
	return((struct trenod *)t);
}

static struct trenod *makelist(type, i, r) int type; struct trenod *i; struct trenod *r; {
	register struct lstnod	*t;

	if (i==0 || r==0) {
		synbad();
	}
	else {
		t = (struct lstnod *)getstak(sizeof(struct lstnod));
		t->lsttyp = type;
		t->lstlef = i; t->lstrit = r;
	}
	return((struct trenod *)t);
}

/*
 * cmd
 *	empty
 *	list
 *	list & [ cmd ]
 *	list [ ; cmd ]
 */

struct trenod *cmd(sym, flg) register int sym; int flg; {
	register struct trenod	*i, *e;

	i = list(flg);

	if (wdval=='\n') {
		if (flg&NLFLG) {
			wdval=';'; chkpr('\n');
		}
	}
	else if (i==0 && (flg&MTFLG)==0) {
		synbad();
	}

	switch (wdval) {
	  

	    case '&':
		if (i) {
			i = makefork(FINT|FPRS|FAMP, i);
		}
		else {
			synbad();
		}

	    case ';':
		if (e=cmd(sym,flg|MTFLG)) {
			i=makelist(TLST, i, e);
		}
		break;

	    case EOFSYM:
		if (sym=='\n') {
			break;
		}

	    default:
		if (sym) {
			chksym(sym);
		}

	}
	return(i);
}

/*
 * list
 *	term
 *	list && term
 *	list || term
 */

static struct trenod *list(flg) int flg; {
	register struct trenod	*r;
	register int		b;

	r = term(flg);
	while (r && ((b=(wdval==ANDFSYM)) || wdval==ORFSYM)) {
		r = makelist((b ? TAND : TORF), r, term(NLFLG));
	}
	return(r);
}

/*
 * term
 *	item
 *	item |^ term
 */

static struct trenod *term(flg) int flg; {
	register struct trenod	*t;

	reserv++;
	if (flg&NLFLG) {
		skipnl();
	}
	else {
		word();
	}

	if ((t=item(-1)) && (wdval=='^' || wdval=='|')) {
		return(makelist(TFIL, makefork(FPOU,t), makefork(FPIN|FPCL,term(NLFLG))));
	}
	else {
		return(t);
	}
}

static struct regnod *syncase(esym) register int esym; {
	skipnl();
	if (wdval==esym) {
		return(0);
	}
	else {
		register struct regnod	*r=(struct regnod *)getstak(sizeof(struct regnod));
		r->regptr=0;
		for(;;) {
		     wdarg->argnxt=r->regptr;
		     r->regptr=wdarg;
		     if (wdval || ( word()!=')' && wdval!='|' )) {
			  synbad();
		     }
		     if (wdval=='|') {
			  word();
		     }
		     else {
			  break;
		     }
		}
		r->regcom=cmd(0,NLFLG|MTFLG);
		if (wdval==ECSYM) {
			r->regnxt=syncase(esym);
		}
		else {
			chksym(esym);
			r->regnxt=0;
		}
		return(r);
	}
}

/*
 * item
 *
 *	( cmd ) [ < in  ] [ > out ]
 *	word word* [ < in ] [ > out ]
 *	if ... then ... else ... fi
 *	for ... while ... do ... done
 *	case ... in ... esac
 *	begin ... end
 */

static struct trenod *item(flag) int flag; {
	struct trenod	*_t;
	register struct ionod	*io;

	if (flag) {
		io=inout((struct ionod *)0);
	}
	else {
		io=0;
	}

	switch (wdval) {
	  

	    case CASYM: {
#define t (*(struct swnod **)&_t)
		   t=(struct swnod *)getstak(sizeof(struct swnod));
		   chkword();
		   t->swarg=wdarg->argval;
		   skipnl(); chksym(INSYM|BRSYM);
		   t->swlst=syncase(wdval==INSYM?ESSYM:KTSYM);
		   t->swtyp=TSW;
		   break;
#undef t
		}

	    case IFSYM: {
#define t (*(struct ifnod **)&_t)
		   register int	w;
		   t=(struct ifnod *)getstak(sizeof(struct ifnod));
		   t->iftyp=TIF;
		   t->iftre=cmd(THSYM,NLFLG);
		   t->thtre=cmd(ELSYM|FISYM|EFSYM,NLFLG);
		   t->eltre=((w=wdval)==ELSYM ? cmd(FISYM,NLFLG) : (w==EFSYM ? (wdval=IFSYM, item(0)) : 0));
		   if (w==EFSYM) { return((struct trenod *)t); }
		   break;
#undef t
		}

	    case FORSYM: {
#define t (*(struct fornod **)&_t)
		   t=(struct fornod *)getstak(sizeof(struct fornod));
		   t->fortyp=TFOR;
		   t->forlst=0;
		   chkword();
		   t->fornam=wdarg->argval;
		   if (skipnl()==INSYM) {
			chkword();
			t->forlst=(struct comnod *)item(0);
			if (wdval!='\n' && wdval!=';') {
				synbad();
			}
			chkpr(wdval); skipnl();
		   }
		   chksym(DOSYM|BRSYM);
		   t->fortre=cmd(wdval==DOSYM?ODSYM:KTSYM,NLFLG);
		   break;
#undef t
		}

	    case WHSYM:
	    case UNSYM: {
#define t (*(struct whnod **)&_t)
		   t=(struct whnod *)getstak(sizeof(struct whnod));
		   t->whtyp=(wdval==WHSYM ? TWH : TUN);
		   t->whtre = cmd(DOSYM,NLFLG);
		   t->dotre = cmd(ODSYM,NLFLG);
		   break;
#undef t
		}

	    case BRSYM:
		_t=cmd(KTSYM,NLFLG);
		break;

	    case '(': {
		   register struct parnod	 *p;
		   p=(struct parnod *)getstak(sizeof(struct parnod));
		   p->partre=cmd(')',NLFLG);
		   p->partyp=TPAR;
		   _t=makefork(0,(struct trenod *)p);
		   break;
		}

	    default:
		if (io==0) {
			return(0);
		}

	    case 0: {
#define t (*(struct comnod **)&_t)
		   register struct argnod	*argp;
		   register struct argnod	**argtail;
		   register struct argnod	*argset=0;
		   int		keywd=1;
		   t=(struct comnod *)getstak(sizeof(struct comnod));
		   t->comio=io; /*initial io chain*/
		   argtail = &(t->comarg);
		   while (wdval==0) {
			argp = wdarg;
			if (wdset && keywd) {
				argp->argnxt=argset; argset=argp;
			}
			else {
				*argtail=argp; argtail = &(argp->argnxt); keywd=flags&keyflg;
			}
			word();
			if (flag) {
			     t->comio=inout(t->comio);
			}
		   }

		   t->comtyp=TCOM; t->comset=argset; *argtail=0;
		   return((struct trenod *)t);
#undef t
		}

	}
	reserv++; word();
	if (io=inout(io)) {
		_t=makefork(0,_t); _t->treio=io;
	}
	return(_t);
}

static int skipnl() {
	while ((reserv++, word()=='\n')) { chkpr('\n'); }
	return(wdval);
}

static struct ionod *inout(lastio) struct ionod *lastio; {
	register int		iof;
	register struct ionod	*iop;
	register char	c;

	iof=wdnum;

	switch (wdval) {
	  

	    case DOCSYM:
		iof |= IODOC; break;

	    case APPSYM:
	    case '>':
		if (wdnum==0) { iof |= 1; }
		iof |= IOPUT;
		if (wdval==APPSYM) {
			iof |= IOAPP; break;
		}

	    case '<':
		if ((c=nextc(0))=='&') {
			iof |= IOMOV;
		}
		else if (c=='>') {
			iof |= IORDW;
		}
		else {
			peekc=c|MARK;
		}
		break;

	    default:
		return(lastio);
	}

	chkword();
	iop=(struct ionod *)getstak(sizeof(struct ionod)); iop->ioname=wdarg->argval; iop->iofile=iof;
	if (iof&IODOC) {
	     iop->iolst=iopend; iopend=iop;
	}
	word(); iop->ionxt=inout(lastio);
	return(iop);
}

static void chkword() {
	if (word()) {
		synbad();
	}
}

static void chksym(sym) int sym; {
	register int		x = sym&wdval;
	if (((x&SYMFLG) ? x : sym) != wdval) {
		synbad();
	}
}

static void prsym(sym) int sym; {
	if (sym&SYMFLG) {
		register struct sysnod	*sp=reserved;
		while (sp->sysval
			&& sp->sysval!=sym) { sp++; }
		prs(sp->sysnam);
	}
	else if (sym==EOFSYM) {
		prs(endoffile);
	}
	else {
		if (sym&SYMREP) { prc(sym); }
		if (sym=='\n') {
			prs("newline");
		}
		else {
			prc(sym);
		}
	}
}

static void synbad() {
	prp(); prs(synmsg);
	if ((flags&ttyflg)==0) {
		prs(atline); prn(standin->flin);
	}
	prs(colon);
	prc('`');
	if (wdval) {
		prsym(wdval);
	}
	else {
		prs(wdarg->argval);
	}
	prc('\''); prs(unexpected);
	newline();
	exitsh(SYNBAD);
}
