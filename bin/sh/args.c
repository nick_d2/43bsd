#include <gen.h>
#include "defs.h"
/*#include "mode.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)args.c	4.4 7/31/85";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

/*static struct dolnod	*copyargs();*/
static struct dolnod	*dolh;

char	flagadr[10];

char	flagchar[] = {
	'x',	'n',	'v',	't',	's',	'i',	'e',	'r',	'k',	'u',	0
};
int	flagval[]  = {
	execpr,	noexec,	readpr,	oneflg,	stdflg,	intflg,	errflg,	rshflg,	keyflg,	setflg,	0
};

/* ========	option handling	======== */

static struct dolnod *copyargs __P((char *from[], int n));

int options(argc, argv) int argc; char **argv; {
	register char	*cp;
	register char	**argp=argv;
	register char	*flagc;
	char		*flagp;

	if (argc>1 && *argp[1]=='-') {
		cp=argp[1];
		flags &= ~(execpr|readpr);
		while (*++cp) {
			flagc=flagchar;

			while (*flagc && *flagc != *cp) { flagc++; }
			if (*cp == *flagc) {
				flags |= flagval[flagc-flagchar];
			}
			else if (*cp=='c' && argc>2 && comdiv==0) {
				comdiv=argp[2];
				argp[1]=argp[0]; argp++; argc--;
			}
			else {
				failed(argv[1],badopt);
			}
		}
		argp[1]=argp[0]; argc--;
	}

	/* set up $- */
	flagc=flagchar;
	flagp=flagadr;
	while (*flagc) {
	   if (flags&flagval[flagc-flagchar]) {
		*flagp++ = *flagc;
	   }
	   flagc++;
	}
	*flagp++=0;

	return(argc);
}

void setargs(argi) char *argi[]; {
	/* count args */
	register char	**argp=argi;
	register int		argn=0;

	while (*argp++!=NULL) { argn++; }

	/* free old ones unless on for loop chain */
	freeargs(dolh);
	dolh=copyargs(argi,argn);	/* sets dolv */
	assnum(&dolladr,dolc=argn-1);
}

struct dolnod *freeargs(blk) struct dolnod *blk; {
	register char	**argp;
	register struct dolnod	*argr=0;
	register struct dolnod	*argblk;

	if (argblk=blk) {
		argr = argblk->dolnxt;
		if ((--argblk->doluse)==0) {
			for (argp=argblk->dolarg; *argp!=NULL; argp++) { free(*argp); }
			free(argblk);
		}
	}
	return(argr);
}

static struct dolnod *copyargs(from, n) char *from[]; int n; {
	register struct dolnod *pp = (struct dolnod *)malloc(sizeof(struct dolnod)+n*sizeof(char **));
	register char **	np=pp->dolarg;
	register char **	fp=from;

	pp->doluse=1;	/* use count */
	dolv=np;

	while (n--) { *np++ = make(*fp++); }
	*np++ = NULL;
	return(pp);
}

int clearup() {
	/* force `for' $* lists to go away */
	while (argfor=freeargs(argfor));

	/* clean up io files */
	while (pop());
}

struct dolnod *useargs() {
	if (dolh) {
		dolh->doluse++;
		dolh->dolnxt=argfor;
		return(argfor=dolh);
	}
	else {
		return(0);
	}
}
