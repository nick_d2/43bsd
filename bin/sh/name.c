#include <gen.h>
/*#include <strings.h> gen.h*/
/*#include <sys/file.h> defs.h*/
/*#include <sys/proc.h> gen.h*/
/*#include "_ctype.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "name.h" defs.h*/
/*#include "stak.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)name.c	4.4 10/31/85";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

/*extern bool	chkid();*/

struct namnod	ps2nod	= {	NULL,		NULL,		ps2name},
		fngnod	= {	NULL,		NULL,		fngname},
		pathnod = {	NULL,		NULL,		pathname},
		ifsnod	= {	NULL,		NULL,		ifsname},
		ps1nod	= {	&pathnod,	&ps2nod,	ps1name},
		homenod = {	&fngnod,	&ifsnod,	homename},
		mailnod = {	&homenod,	&ps1nod,	mailname};

struct namnod	*namep = &mailnod;

/* ========	variable and string handling	======== */

static bool chkid __P((char *nam));
static void namwalk __P((register struct namnod *np));
static char *staknam __P((register struct namnod *n));

int syslook(w, syswds) char *w; struct sysnod syswds[]; {
	register char	first;
	register char	*s;
	register struct sysnod	*syscan;

	syscan=syswds; first = *w;

	while (s=syscan->sysnam) {
	    if (first == *s
		&& eq(w,s)) {
		 return(syscan->sysval);
	    }
	    syscan++;
	}
	return(0);
}

int setlist(arg, xp) register struct argnod *arg; int xp; {
	while (arg) {
	   register char	*s=mactrim(arg->argval);
	   setname(s, xp);
	   arg=arg->argnxt;
	   if (flags&execpr) {
		prs(s);
		if (arg) { blank(); } else { newline(); }
	   }
	}
}

void setname(argi, xp) char *argi; int xp; {
	register char	*argscan=argi;
	register struct namnod	*n;

	if (letter(*argscan)) {
		while (alphanum(*argscan)) { argscan++; }
		if (*argscan=='=') {
			*argscan = 0;
			n=lookup(argi);
			*argscan++ = '=';
			attrib(n, xp);
			if (xp&N_ENVNAM) {
				
				/*
				 * Importing IFS can be very dangerous
				 */
				if (!bcmp(argi, "IFS=", sizeof("IFS=") - 1)) {
				     
					int uid;
					if ((uid = getuid())!=geteuid() || !uid) {
					    
						return;
					}
				}
				n->namenv = n->namval = argscan;
			}
			else {
				assign(n, argscan);
			}
			return;
		}
	}
	failed(argi,notid);
}

int replace(a, v) register char **a; char *v; {
	free(*a); *a=make(v);
}

int dfault(n, v) struct namnod *n; char *v; {
	if (n->namval==0) {
		assign(n,v);
	}
}

int assign(n, v) struct namnod *n; char *v; {
	if (n->namflg&N_RDONLY) {
		failed(n->namid,wtfailed);
	}
	else {
		replace(&n->namval,v);
	}
}

int readvar(names) char **names; {
	struct fileblk		fb;
	register struct fileblk	*f = &fb;
	register char		c;
	register int		rc=0;
	struct namnod		*n=lookup(*names++); /* done now to avoid storage mess */
	intptr_t		rel=relstak();

	push(f); initf(dup(0));
	if (lseek(0,0L,1)==-1) {
		f->fsiz=1;
	}

	for(;;) {
		c=nextc(0);
		if ((*names && any(c, ifsnod.namval)) || eolchar(c)) {
			zerostak();
			assign(n,absstak(rel)); setstak(rel);
			if (*names) {
				n=lookup(*names++);
			}
			else {
				n=0;
			}
			if (eolchar(c)) {
				break;
			}
		}
		else {
			pushstak(c);
		}
	}
	while (n) {
	   assign(n, nullstr);
	   if (*names) { n=lookup(*names++); } else { n=0; }
	}

	if (eof) { rc=1; }
	lseek(0, (long)(f->fnxt-f->fend), 1);
	pop();
	return(rc);
}

int assnum(p, i) char **p; int i; {
	itos(i); replace(p,numbuf);
}

char *make(v) char *v; {
	register char	*p;

	if (v) {
		movstr(v,p=malloc(length(v)));
		return(p);
	}
	else {
		return(0);
	}
}

struct namnod *lookup(nam) register char *nam; {
	register struct namnod	*nscan=namep;
	register struct namnod	**prev;
	int		LR;

	if (!chkid(nam)) {
		failed(nam,notid);
	}
	while (nscan) {
		if ((LR=cf(nam,nscan->namid))==0) {
			return(nscan);
		}
		else if (LR<0) {
			prev = &(nscan->namlft);
		}
		else {
			prev = &(nscan->namrgt);
		}
		nscan = *prev;
	}

	/* add name node */
	nscan=malloc(sizeof *nscan);
	nscan->namlft=nscan->namrgt=NULL;
	nscan->namid=make(nam);
	nscan->namval=0; nscan->namflg=N_DEFAULT; nscan->namenv=0;
	return(*prev = nscan);
}

static bool chkid(nam) char *nam; {
	register char *	cp=nam;

	if (!letter(*cp)) {
		return(0);
	}
	else {
		while (*++cp) {
		   if (!alphanum(*cp)) {
			return(0);
		   }
		}
	}
	return(-1);
}

static void (*namfn) __P((struct namnod *n));
int namscan(fn) void (*fn) __P((struct namnod *n)); {
	namfn=fn;
	namwalk(namep);
}

static void namwalk(np) register struct namnod *np; {
	if (np) {
		namwalk(np->namlft);
		(*namfn)(np);
		namwalk(np->namrgt);
	}
}

void printnam(n) struct namnod *n; {
	register char	*s;

	sigchk();
	if (s=n->namval) {
		prs(n->namid);
		prc('='); prs(s);
		newline();
	}
}

static char *staknam(n) register struct namnod *n; {
	register char	*p;

	p=movstr(n->namid,staktop);
	p=movstr("=",p);
	p=movstr(n->namval,p);
	return(getstak(p+1-stakbot));
}

void exname(n) register struct namnod *n; {
	if (n->namflg&N_EXPORT) {
		free(n->namenv);
		n->namenv = make(n->namval);
	}
	else {
		free(n->namval);
		n->namval = make(n->namenv);
	}
}

void printflg(n) register struct namnod *n; {
	if (n->namflg&N_EXPORT) {
		prs(export); blank();
	}
	if (n->namflg&N_RDONLY) {
		prs(readonly); blank();
	}
	if (n->namflg&(N_EXPORT|N_RDONLY)) {
		prs(n->namid); newline();
	}
}

void setupenv() {
	register char	**e=environ;

	while (*e) { setname(*e++, N_ENVNAM); }
}

static int	namec;

void countnam(n) struct namnod *n; {
	namec++;
}

static char 	**argnam;

void pushnam(n) struct namnod *n; {
	if (n->namval) {
		*argnam++ = staknam(n);
	}
}

char **setenv() {
	register char	**er;

	namec=0;
	namscan(countnam);
	argnam = er = (char **)getstak(namec*sizeof(char *)+sizeof(char *));
	namscan(pushnam);
	*argnam++ = 0;
	return(er);
}
