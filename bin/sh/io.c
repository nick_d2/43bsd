/*#include <sys/file.h> defs.h*/
/*#include "_ctype.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "stak.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)io.c	4.3 3/19/85";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

/* ========	input output and file copying ======== */

int initf(fd) int fd; {
	register struct fileblk	*f=standin;

	f->fdes=fd; f->fsiz=((flags&(oneflg|ttyflg))==0 ? BUFSIZ : 1);
	f->fnxt=f->fend=f->fbuf; f->feval=0; f->flin=1;
	f->feof=0;
}

int estabf(s) register char *s; {
	register struct fileblk	*f;

	(f=standin)->fdes = -1;
	f->fend=length(s)+(f->fnxt=s);
	f->flin=1;
	return(f->feof=(s==0));
}

int push(af) struct fileblk *af; {
	register struct fileblk	*f;

	(f=af)->fstak=standin;
	f->feof=0; f->feval=0;
	standin=f;
}

int pop() {
	register struct fileblk	*f;

	if ((f=standin)->fstak) {
		if (f->fdes>=0) { close(f->fdes); }
		standin=f->fstak;
		return(-1);
	}
	else {
		return(0);
	}
}

int chkpipe(pv) int *pv; {
	if (pipe(pv)<0 || pv[INPIPE]<0 || pv[OTPIPE]<0) {
		error(piperr);
	}
}

int chkopen(idf) char *idf; {
	register int		rc;

	if ((rc=open(idf,0))<0) {
		failed(idf,badopen);
	}
	else {
		return(rc);
	}
}

int _rename(f1, f2) register int f1; register int f2; {
	if (f1!=f2) {
		dup2(f1, f2);
		close(f1);
		if (f2==0) { ioset|=1; }
	}
}

int create(s) char *s; {
	register int		rc;

	if ((rc=creat(s,0666))<0) {
		failed(s,badcreate);
	}
	else {
		return(rc);
	}
}

int tmpfil() {
	itos(serial++); movstr(numbuf,tmpnam);
	return(create(tmpout));
}

/* set by trim */
bool		nosubst;

int copy(ioparg) struct ionod *ioparg; {
	char		c, *ends;
	register char	*cline, *clinep;
	int		fd;
	register struct ionod	*iop;

	if (iop=ioparg) {
		copy(iop->iolst);
		ends=mactrim(iop->ioname); if (nosubst) { iop->iofile &= ~IODOC; }
		fd=tmpfil();
		iop->ioname=cpystak(tmpout);
		iop->iolst=iotemp; iotemp=iop;
		cline=locstak();

		for(;;) {
			clinep=cline; chkpr('\n');
			while ((c = (nosubst ? readc() :  nextc(*ends)),  !eolchar(c))) { *clinep++ = c; }
			*clinep=0;
			if (eof || eq(cline,ends)) { break; }
			*clinep++='\n';
			write(fd,cline,clinep-cline);
		}
		close(fd);
	}
}
