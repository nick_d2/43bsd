s/@/ATSIGN/g
s/^/@/
s/$/@/
s/[^A-Za-z0-9_]\+/@&@/g

# mac.h
s/@LOCAL@/@static@/g
s/@PROC@/@extern@/g
s/@TYPE@/@typedef@/g
s/@STRUCT@/@typedef struct@/g
s/@UNION@/@typedef union@/g
s/@REG@/@register@/g

s/@IF@/@if (NOWHITE@/g
s/@THEN@/@NOWHITE) {NEWLINE    @/g
s/@ELSE@/@}NEWLINEelse {NEWLINE    @/g
s/@ELIF@/@}NEWLINEelse if (NOWHITE@/g
s/@FI@/@NOWHITE;NEWLINE}@/g

s/@BEGIN@/@{NEWLINE     @/g
s/@END@/@}@/g
s/@SWITCH@/@switch (NOWHITE@/g
s/@IN@/@NOWHITE) {NEWLINE  @/g
s/@ENDSW@/@}@/g
s/@FOR@/@for (NOWHITE@/g
s/@WHILE@/@while (NOWHITE@/g
s/@DO@/@NOWHITE) {NEWLINE  @/g
s/@OD@/@NOWHITE;NEWLINE}@/g
s/@REP@/@do {NEWLINE   @/g
s/@PER@/@} while (NOWHITE@/g
s/@DONE@/@NOWHITE);@/g
s/@LOOP@/@for(;;) {NEWLINE    @/g
s/@POOL@/@}@/g

s/@SKIP@/@;@/g
s/@DIV@/@\/@/g
s/@REM@/@%@/g
s/@NEQ@/@^@/g
s/@ANDF@/@\&\&@/g
s/@ORF@/@||@/g

s/@TRUE@/@-1@/g
s/@FALSE@/@0@/g
s/@LOBYTE@/@0377@/g
s/@STRIP@/@0177@/g
s/@QUOTE@/@0200@/g

s/@EOF@/@0@/g
s/@NL@/@'\\n'@/g
s/@SP@/@' '@/g
s/@LQ@/@'`'@/g
s/@RQ@/@'\\''@/g
s/@MINUS@/@'-'@/g
s/@COLON@/@':'@/g

s/@MAX@/@max@/g

# mode.h
s/@CHAR@/@char@/g
s/@BOOL@/@bool@/g
s/@UFD@/@int@/g
s/@INT@/@int@/g
s/@REAL@/@float@/g
s/@ADDRESS@/@void*CHECKME@/g
s/@L_INT@/@long@/g
s/@VOID@/@void@/g
s/@POS@/@unsigned@/g
s/@STRING@/@char*CHECKME@/g
s/@MSG@/@char[]CHECKME@/g
s/@PIPE@/@int[]CHECKME@/g
s/@STKPTR@/@char*CHECKME@/g
s/@BYTPTR@/@char*CHECKME@/g

s/@STATBUF@/@struct stat@/g
s/@BLKPTR@/@struct blk*CHECKME@/g
s/@FILEBLK@/@struct fileblk@/g
s/@FILEHDR@/@struct filehdr@/g
s/@FILE@/@struct fileblk*CHECKME@/g
s/@TREPTR@/@struct trenod*CHECKME@/g
s/@FORKPTR@/@struct forknod*CHECKME@/g
s/@COMPTR@/@struct comnod*CHECKME@/g
s/@SWPTR@/@struct swnod*CHECKME@/g
s/@REGPTR@/@struct regnod*CHECKME@/g
s/@PARPTR@/@struct parnod*CHECKME@/g
s/@IFPTR@/@struct ifnod*CHECKME@/g
s/@WHPTR@/@struct whnod*CHECKME@/g
s/@FORPTR@/@struct fornod*CHECKME@/g
s/@LSTPTR@/@struct lstnod*CHECKME@/g
s/@ARGPTR@/@struct argnod*CHECKME@/g
s/@DOLPTR@/@struct dolnod*CHECKME@/g
s/@IOPTR@/@struct ionod*CHECKME@/g
s/@NAMNOD@/@struct namnod@/g
s/@NAMPTR@/@struct namnod*CHECKME@/g
s/@SYSNOD@/@struct sysnod@/g
s/@SYSPTR@/@struct sysnod*CHECKME@/g
s/@SYSTAB@/@struct sysnod[]CHECKME@/g
s/@NIL@/@NULL@/g

s/@FORKTYPE@/@sizeof(struct forknod)@/g
s/@COMTYPE@/@sizeof(struct comnod)@/g
s/@IFTYPE@/@sizeof(struct ifnod)@/g
s/@WHTYPE@/@sizeof(struct whnod)@/g
s/@FORTYPE@/@sizeof(struct fornod)@/g
s/@SWTYPE@/@sizeof(struct swnod)@/g
s/@REGTYPE@/@sizeof(struct regnod)@/g
s/@PARTYPE@/@sizeof(struct parnod)@/g
s/@LSTTYPE@/@sizeof(struct lstnod)@/g
s/@IOTYPE@/@sizeof(struct ionod)@/g

# defs.h
s/@BLK@/@(struct blk *)@/g
s/@BYT@/@(char *)@/g
s/@STK@/@(char *)@/g
s/@ADR@/@(void *)@/g

s/@//g
s/ATSIGN/@/g

s/{NEWLINE[	 ]*\([^{}]*\)NEWLINE}/{ \1 }/g
s/{NEWLINE[	 ]*\([^{}]*\)}/{ \1}/g
s/^\([	 ]*\)\([^	 {}][^{}]*\)*\([;{}]\)NEWLINE/\1\2\3\n\1/g
s/\n\([	 ]*\)\([^	 {}][^{}]*\)*\([;{}]\)NEWLINE/\n\1\2\3\n\1/g
s/\[\]CHECKME\([	 ]*\(\*[	 ]*\)*[A-Za-z_][0-9A-Za-z_]*\)\([^,]*\);/\1[]\3;/g
s/\[\]CHECKME\([	 ]*\(\*[	 ]*\)*[A-Za-z_][0-9A-Za-z_]*\)\([^,]*\)$/\1[]\3/g
s/\*CHECKME\([	 ]*\)\(\(\*[	 ]*\)*[A-Za-z_][^,]*\);/\1*\2;/g
s/\*CHECKME\([	 ]*\)\(\(\*[	 ]*\)*[A-Za-z_][^,]*\)$/\1*\2/g

# nonessential normalization of whitespace:
s/^\([	]*\)        /\1	/
s/\n\([	]*\)        /\n\1	/
s/^\([	]*\)        /\1	/
s/\n\([	]*\)        /\n\1	/
s/	 	/		/g
s/	  	/		/g
s/	   	/		/g
s/	    	/		/g
s/	     	/		/g
s/	      	/		/g
s/	       	/		/g
