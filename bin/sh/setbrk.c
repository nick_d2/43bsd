#include <sys/proc.h>
#include "defs.h"

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)setbrk.c	4.2 8/11/83";
#endif

/*
 *	UNIX shell
 *
 *	S. R. Bourne
 *	Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

void *setbrk(incr) int incr; {
	register char	*a=sbrk(incr);
	brkend=a+incr;
	return(a);
}
