#include <compat-4.1.h>
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif
/*#include <sys/file.h> defs.h*/
#include <sys/proc.h>
#include <sys/signal.h>
#include <sys/stat.h>
/*#include <sys/time.h> compat-4.1.h*/
/*#include <sys/times.h> compat-4.1.h*/
/*#include "_ctype.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "name.h" defs.h*/
/*#include "stak.h" defs.h*/
#include "sym.h"

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)xec.c	4.3 8/11/83";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/
/*#include	"sym.h"*/

static int	parent;

extern struct sysnod	commands[];

/* ========	command execution	========*/

int execute(argt, execflg, pf1, pf2) struct trenod *argt; int execflg; int *pf1; int *pf2; {
	/* `stakbot' is preserved by this routine */
	register struct trenod	*_t;
	char		*sav=savstak();

	sigchk();

	if ((_t=argt) && execbrk==0) {
		register int	treeflgs;
		int		oldexit, type;
		register char	**com;

		treeflgs = _t->tretyp; type = treeflgs&COMMSK;
		oldexit=exitval; exitval=0;

		switch (type) {
		  

		case TCOM:
#define t ((struct comnod *)_t)
			{
			char		*a1;
			int		argn, internal;
			struct argnod		*schain=gchain;
			struct ionod		*io=t->comio;
			gchain=0;
			argn = getarg(t);
			com=scan(argn);
			a1=com[1]; gchain=schain;

			if (argn==0 || (internal=syslook(com[0],commands))) {
				setlist(t->comset, 0);
			}

			if (argn && (flags&noexec)==0) {
				/* print command if execpr */
				if (flags&execpr) {
					argn=0;	prs(execpmsg);
					while (com[argn]!=NULL) { prs(com[argn++]); blank(); }
					newline();
				}

				switch (internal) {
				  

				case SYSDOT:
					if (a1) {
						register int		f;
	
						if ((f=pathopen(getpath(a1), a1)) < 0) {
						     failed(a1,notfound);
						}
						else {
						     execexp(0,f);
						}
					}
					break;
	
				case SYSTIMES: {
					struct tms tms; times(&tms);
					prt(tms.tms_cutime); blank(); prt(tms.tms_cstime); newline();
					}
					break;
	
				case SYSEXIT:
					exitsh(a1?stoi(a1):oldexit);
	
				case SYSNULL:
					io=0;
					break;
	
				case SYSCONT:
					execbrk = -loopcnt; break;
	
				case SYSBREAK:
					if ((execbrk=loopcnt) && a1) {
					     breakcnt=stoi(a1);
					}
					break;
	
				case SYSTRAP:
					if (a1) {
						bool	clear;
						if ((clear=digit(*a1))==0) {
							++com;
						}
						while (*++com) {
						   int	i;
						   if ((i=stoi(*com))>=MAXTRAP || i<MINTRAP) {
							failed(*com,badtrap);
						   }
						   else if (clear) {
							clrsig(i);
						   }
						   else {
							replace(&trapcom[i],a1);
							if (*a1) {
								getsig(i);
							}
							else {
								ignsig(i);
							}
						   }
						}
					}
					else {
						/* print out current traps */
						int		i;
	
						for (i=0; i<MAXTRAP; i++) {
						   if (trapcom[i]) {
							prn(i); prs(colon); prs(trapcom[i]); newline();
						   }
						}
					}
					break;
	
				case SYSEXEC:
					com++;
					initio(io); ioset=0; io=0;
					if (a1==0) { break; }
	
				case SYSLOGIN:
					flags |= forked;
					oldsigs(); execa(com); done();
	
				case SYSCD:
					if (flags&rshflg) {
						failed(com[0],restricted);
					}
					else if ((a1==0 && (a1=homenod.namval)==0) || chdir(a1)<0) {
						failed(a1,baddir);
					}
					break;
	
				case SYSSHFT:
					if (dolc<1) {
						error(badshift);
					}
					else {
						dolv++; dolc--;
					}
					assnum(&dolladr, dolc);
					break;
	
				case SYSWAIT:
					await(-1);
					break;
	
				case SYSREAD:
					exitval=readvar(&com[1]);
					break;

/*
				case SYSTST:
					exitval=testcmd(com);
					break;
*/

				case SYSSET:
					if (a1) {
						int	argc;
						argc = options(argn,com);
						if (argc>1) {
							setargs(com+argn-argc);
						}
					}
					else if (t->comset==0) {
						/*scan name chain and print*/
						namscan(printnam);
					}
					break;
	
				case SYSRDONLY:
					exitval=N_RDONLY;
				case SYSXPORT:
					if (exitval==0) { exitval=N_EXPORT; }
	
					if (a1) {
						while (*++com) { attrib(lookup(*com), exitval); }
					}
					else {
						namscan(printflg);
					}
					exitval=0;
					break;
	
				case SYSEVAL:
					if (a1) {
						execexp(a1,&com[2]);
					}
					break;

		                case SYSUMASK:
		                        if (a1) {
		                                int c, i;
		                                i = 0;
		                                while ((c = *a1++) >= '0' &&
		                                        c <= '7')
		                                        i = (i << 3) + c - '0';
		                                umask(i);
		                        } else {
		                                int i, j;
		                                umask(i = umask(0));
		                                prc('0');
		                                for (j = 6; j >= 0; j -= 3)
		                                        prc(((i>>j)&07) + '0');
		                                newline();
		                        }
		                        break;
	
				default:
					internal=builtin(argn,com);
	
				}

				if (internal) {
					if (io) { error(illegal); }
					chktrap();
					break;
				}
			}
			else if (t->comio==0) {
				break;
			}
			}
			/* fallthru */
	
		case TFORK:
			if (execflg && (treeflgs&(FAMP|FPOU))==0) {
				parent=0;
			}
			else {
				while ((parent=fork()) == -1) { sigchk(); alarm(10); pause(); }
			}

			if (parent) {
				/* This is the parent branch of fork;    */
				/* it may or may not wait for the child. */
				if (treeflgs&FPRS && flags&ttyflg) {
					prn(parent); newline();
				}
				if (treeflgs&FPCL) { closepipe(pf1); }
				if ((treeflgs&(FAMP|FPOU))==0) {
					await(parent);
				}
				else if ((treeflgs&FAMP)==0) {
					post(parent);
				}
				else {
					assnum(&pcsadr, parent);
				}

				chktrap();
				break;

			}
			else {
				/* this is the forked branch (child) of execute */
				flags |= forked; iotemp=0;
				postclr();
				settmp();

				/* Turn off INTR and QUIT if `FINT'  */
				/* Reset ramaining signals to parent */
				/* except for those `lost' by trap   */
				oldsigs();
				if (treeflgs&FINT) {
					signal(INTR,SIG_IGN); signal(QUIT,SIG_IGN);
				}

				/* pipe in or out */
				if (treeflgs&FPIN) {
					_rename(pf1[INPIPE],0);
					close(pf1[OTPIPE]);
				}
				if (treeflgs&FPOU) {
					_rename(pf2[OTPIPE],1);
					close(pf2[INPIPE]);
				}

				/* default std input for & */
				if (treeflgs&FINT && ioset==0) {
					_rename(chkopen(devnull),0);
				}

				/* io redirection */
				initio(t->comio);
				if (type!=TCOM) {
#if defined(__STDC__) || defined(lint)
					execute(((struct forknod *)t)->forktre,1,0,0);
#else
					execute(((struct forknod *)t)->forktre,1);
#endif
				}
				else if (com[0]!=NULL) {
					setlist(t->comset,N_EXPORT);
					execa(com);
				}
				done();
			}
#undef t

		case TPAR:
#define t ((struct parnod *)_t)
			_rename(dup(2),output);
#if defined(__STDC__) || defined(lint)
			execute(t->partre,execflg,0,0);
#else
			execute(t->partre,execflg);
#endif
			done();
#undef t

		case TFIL:
#define t ((struct lstnod *)_t)
			{
			   int pv[2]; chkpipe(pv);
			   if (execute(t->lstlef, 0, pf1, pv)==0) {
				execute(t->lstrit, execflg, pv, pf2);
			   }
			   else {
				closepipe(pv);
			   }
			}
			break;
#undef t

		case TLST:
#define t ((struct lstnod *)_t)
#if defined(__STDC__) || defined(lint)
			execute(t->lstlef,0,0,0);
			execute(t->lstrit,execflg,0,0);
#else
			execute(t->lstlef,0);
			execute(t->lstrit,execflg);
#endif
			break;
#undef t

		case TAND:
#define t ((struct lstnod *)_t)
#if defined(__STDC__) || defined(lint)
			if (execute(t->lstlef,0,0,0)==0) {
				execute(t->lstrit,execflg,0,0);
			}
#else
			if (execute(t->lstlef,0)==0) {
				execute(t->lstrit,execflg);
			}
#endif
			break;
#undef t

		case TORF:
#define t ((struct lstnod *)_t)
#if defined(__STDC__) || defined(lint)
			if (execute(t->lstlef,0,0,0)!=0) {
				execute(t->lstrit,execflg,0,0);
			}
#else
			if (execute(t->lstlef,0)!=0) {
				execute(t->lstrit,execflg);
			}
#endif
			break;
#undef t

		case TFOR:
#define t ((struct fornod *)_t)
			{
			   struct namnod	*n = lookup(t->fornam);
			   char	**args;
			   struct dolnod	*argsav=0;

			   if (t->forlst==0) {
				   args=dolv+1;
				   argsav=useargs();
			   }
			   else {
				   struct argnod	*schain=gchain;
				   gchain=0;
				   trim((args=scan(getarg(t->forlst)))[0]);
				   gchain=schain;
			   }
			   loopcnt++;
			   while (*args!=NULL && execbrk==0) {
				assign(n,*args++);
#if defined(__STDC__) || defined(lint)
				execute(t->fortre,0,0,0);
#else
				execute(t->fortre,0);
#endif
				if (execbrk<0) { execbrk=0; }
			   }
			   if (breakcnt) { breakcnt--; }
			   execbrk=breakcnt; loopcnt--;
			   argfor=freeargs(argsav);
			}
			break;
#undef t

		case TWH:
		case TUN:
#define t ((struct whnod *)_t)
			{
			   int		i=0;

			   loopcnt++;
#if defined(__STDC__) || defined(lint)
			   while (execbrk==0 && (execute(t->whtre,0,0,0)==0)==(type==TWH)) {
			      i=execute(t->dotre,0,0,0);
			      if (execbrk<0) { execbrk=0; }
			   }
#else
			   while (execbrk==0 && (execute(t->whtre,0)==0)==(type==TWH)) {
			      i=execute(t->dotre,0);
			      if (execbrk<0) { execbrk=0; }
			   }
#endif
			   if (breakcnt) { breakcnt--; }
			   execbrk=breakcnt; loopcnt--; exitval=i;
			}
			break;
#undef t

		case TIF:
#define t ((struct ifnod *)_t)
#if defined(__STDC__) || defined(lint)
			if (execute(t->iftre,0,0,0)==0) {
				execute(t->thtre,execflg,0,0);
			}
			else {
				execute(t->eltre,execflg,0,0);
			}
#else
			if (execute(t->iftre,0)==0) {
				execute(t->thtre,execflg);
			}
			else {
				execute(t->eltre,execflg);
			}
#endif
			break;
#undef t

		case TSW:
#define t ((struct swnod *)_t)
			{
			   register char	*r = mactrim(t->swarg);
			   struct regnod	*u = t->swlst;
			   while (u) {
				struct argnod		*rex=u->regptr;
				while (rex) {
					register char	*s;
					if (gmatch(r,s=macro(rex->argval)) || (trim(s), eq(r,s))) {
#if defined(__STDC__) || defined(lint)
						execute(u->regcom,0,0,0);
#else
						execute(u->regcom,0);
#endif
						u=0; break;
					}
					else {
						rex=rex->argnxt;
					}
				}
				if (u) { u=u->regnxt; }
			   }
			}
			break;
#undef t
		}
		exitset();
	}

	sigchk();
	tdystak(sav);
	return(exitval);
}

#ifdef __STDC__
int execexp(char *s, ...)
#else
int execexp(s, va_alist) char *s; va_dcl
#endif
{
	struct fileblk		fb;
	va_list			argp;

	push(&fb);
	_va_start(argp, s);
	if (s) {
		estabf(s); fb.feval=va_arg(argp, char **);
	}
	else {
		int f=va_arg(argp, int);
		if (f>=0) { initf(f); }
	}
	va_end(argp);
#if defined(__STDC__) || defined(lint)
	execute(cmd('\n', NLFLG|MTFLG),0,0,0);
#else
	execute(cmd('\n', NLFLG|MTFLG),0);
#endif
	pop();
}
