/*#include <setjmp.h> defs.h*/
/*#include <sys/errno.h> defs.h*/
#include <sys/exec.h>
/*#include <sys/file.h> defs.h*/
#include <sys/ioctl.h>
#include <sys/wait.h>
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "stak.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)service.c	4.4 3/19/85";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

/*extern void	gsort();*/

#define ARGMK	01

extern int		errno;
extern char		*sysmsg[];
extern int		num_sysmsg;

/* fault handling */
#define ENOMEM	12
#define ENOEXEC 8
#define E2BIG	7
#define ENOENT	2
#define ETXTBSY 26

/* service routines for `execute' */

static char *execs __P((char *ap, register char *t[]));
static void gsort __P((char *from[], char *to[]));
static int split __P((register char *s));

void initio(iop) struct ionod *iop; {
	register char	*ion;
	register int		iof, fd;

	if (iop) {
		iof=iop->iofile;
		ion=mactrim(iop->ioname);
		if (*ion && (flags&noexec)==0) {
			if (iof&IODOC) {
				subst(chkopen(ion),(fd=tmpfil()));
				close(fd); fd=chkopen(tmpout); unlink(tmpout);
			}
			else if (iof&IOMOV) {
				if (eq(minus,ion)) {
					fd = -1;
					close(iof&IOUFD);
				}
				else if ((fd=stoi(ion))>=USERIO) {
					failed(ion,badfile);
				}
				else {
					fd=dup(fd);
				}
			}
			else if ((iof&IOPUT)==0) {
				fd=chkopen(ion);
			}
			else if (flags&rshflg) {
				failed(ion,restricted);
			}
			else if (iof&IOAPP && (fd=open(ion,1))>=0) {
				lseek(fd, 0L, 2);
			}
			else {
				fd=create(ion);
			}
			if (fd>=0) {
				_rename(fd,iof&IOUFD);
			}
		}
		initio(iop->ionxt);
	}
}

char *getpath(s) char *s; {
	register char	*path;
	if (any('/',s)) {
		if (flags&rshflg) {
			failed(s, restricted);
		}
		else {
			return(nullstr);
		}
	}
	else if ((path = pathnod.namval)==0) {
		return(defpath);
	}
	else {
		return(cpystak(path));
	}
}

int pathopen(path, name) register char *path; register char *name; {
	register int		f;

	do {
	    path=catpath(path,name);
	} while ((f=open(curstak(),0))<0 && path);
	return(f);
}

char *catpath(path, name) register char *path; char *name; {
	/* leaves result on top of stack */
	register char	*scanp = path,
			*argp = locstak();

	while (*scanp && *scanp!=':') { *argp++ = *scanp++; }
	if (scanp!=path) { *argp++='/'; }
	if (*scanp==':') { scanp++; }
	path=(*scanp ? scanp : 0); scanp=name;
	while ((*argp++ = *scanp++));
	return(path);
}

static char	*xecmsg;
static char	**xecenv;

void execa(at) char *at[]; {
	register char	*path;
	register char	**t = at;

	if ((flags&noexec)==0) {
		xecmsg=notfound; path=getpath(*t);
		namscan(exname);
		xecenv=setenv();
		while (path=execs(path,t));
		failed(*t,xecmsg);
	}
}

static char *execs(ap, t) char *ap; register char *t[]; {
	register char	*p, *prefix;

	prefix=catpath(ap,t[0]);
	trim(p=curstak());

	sigchk();
	execve(p, &t[0] ,xecenv);
	switch (errno) {
	  

	    case ENOEXEC:
		flags=0;
		comdiv=0; ioset=0;
		clearup(); /* remove open files and for loop junk */
		if (input) { close(input); }
		close(output); output=2;
		input=chkopen(p);

		/* band aid to get csh... 2/26/79 */ {
			char c;
			if (!isatty(input)) {
				read(input, &c, 1);
				if (c == '#')
					gocsh(t, p, xecenv);
				lseek(input, (long) 0, 0);
			}
		}

		/* set up new args */
		setargs(t);
		longjmp(subshell,1);

	    case ENOMEM:
		failed(p,toobig);

	    case E2BIG:
		failed(p,arglist);

	    case ETXTBSY:
		failed(p,txtbsy);

	    default:
		xecmsg=badexec;
	    case ENOENT:
		return(prefix);
	}
}

int gocsh(t, cp, xecenv) register char **t; register char *cp; register char **xecenv; {
	char *newt[1000];
	register char **p;
	register int i;

	for (i = 0; t[i]; i++)
		newt[i+1] = t[i];
	newt[i+1] = 0;
	newt[0] = "/bin/csh";
	newt[1] = cp;
	execve("/bin/csh", newt, xecenv);
}

/* for processes to be waited for */
#define MAXP 20
static int	pwlist[MAXP];
static int	pwc;

int postclr() {
	register int		*pw = pwlist;

	while (pw <= &pwlist[pwc]) { *pw++ = 0; }
	pwc=0;
}

void post(pcsid) int pcsid; {
	register int		*pw = pwlist;

	if (pcsid) {
		while (*pw) { pw++; }
		if (pwc >= MAXP-1) {
			pw--;
		}
		else {
			pwc++;
		}
		*pw = pcsid;
	}
}

void await(i) int i; {
	int		rc=0, wx=0;
	int		w;
	int		ipwc = pwc;

	post(i);
	while (pwc) {
		register int		p;
		register int		sig;
		int		w_hi;
 {
		     
		   register int	*pw=pwlist;
 		   if (setjmp(INTbuf) == 0) {
 			trapjmp[INTR] = 1; p=wait(&w);
 		   }
 		   else {
 			p = -1;
 		   }
 		   trapjmp[INTR] = 0;
		   while (pw <= &pwlist[ipwc]) {
		      if (*pw==p) {
			   *pw=0; pwc--;
		      }
		      else {
			   pw++;
		      }
		   }
		}

		if (p == -1) { continue; }

		w_hi = (w>>8)&0377;

		if (sig = w&0177) {
			if (sig == 0177	/* ptrace! return */) {
				prs("ptrace: ");
				sig = w_hi;
			}
			if (sig < num_sysmsg && sysmsg[sig]) {
				if (i!=p || (flags&prompt)==0) {
				     prp(); prn(p); blank();
				}
				prs(sysmsg[sig]);
				if (w&0200) { prs(coredump); }
			}
			newline();
		}

		if (rc==0) {
			rc = (sig ? sig|SIGFLG : w_hi);
		}
		wx |= w;
	}

	if (wx && flags&errflg) {
		exitsh(rc);
	}
	exitval=rc; exitset();
}

bool		nosubst;

int trim(at) char *at; {
	register char	*p;
	register char	c;
	register char	q=0;

	if (p=at) {
		while (c = *p) { *p++=c&0177; q |= c; }
	}
	nosubst=q&0200;
}

char *mactrim(s) char *s; {
	register char	*t=macro(s);
	trim(t);
	return(t);
}

char **scan(argn) int argn; {
	register struct argnod	*argp = (struct argnod *)((intptr_t)gchain&~ARGMK);
	register char	**comargn, **comargm;

	comargn=(char **)getstak(sizeof(char *)*argn+sizeof(char *)); comargm = comargn += argn; *comargn = NULL;

	while (argp) {
		*--comargn = argp->argval;
		if (argp = argp->argnxt) {
		     trim(*comargn);
		}
		if (argp==0 || (intptr_t)argp&ARGMK) {
			gsort(comargn,comargm);
			comargm = comargn;
		}
		argp = (struct argnod *)((intptr_t)argp&~ARGMK);
	}
	return(comargn);
}

static void gsort(from, to) char *from[]; char *to[]; {
	int		k, m, n;
	register int		i, j;

	if ((n=to-from)<=1) { return; }

	for (j=1; j<=n; j*=2);

	for (m=2*j-1; m/=2;) {
	    k=n-m;
	    for (j=0; j<k; j++) {
		for (i=j; i>=0; i-=m) {
		    register char **fromi; fromi = &from[i];
		    if (cf(fromi[m],fromi[0])>0) {
			 break;
		    }
		    else {
			 char *s; s=fromi[m]; fromi[m]=fromi[0]; fromi[0]=s;
		    }
		}
	    }
	}
}

/* Argument list generation */

int getarg(ac) struct comnod *ac; {
	register struct argnod	*argp;
	register int		count=0;
	register struct comnod	*c;

	if (c=ac) {
		argp=c->comarg;
		while (argp) {
			count += split(macro(argp->argval));
			argp=argp->argnxt;
		}
	}
	return(count);
}

static int split(s) register char *s; {
	register char	*argp;
	register int		c;
	int		count=0;

	for(;;) {
		sigchk(); argp=locstak()+sizeof(char *);
		while ((c = *s++, !any(c,ifsnod.namval) && c)) { *argp++ = c; }
		if (argp==staktop+sizeof(char *)) {
			if (c) {
				continue;
			}
			else {
				return(count);
			}
		}
		else if (c==0) {
			s--;
		}
		if (c=expand(((struct argnod *)(argp=endstak(argp)))->argval,0)) {
			count += c;
		}
		else {
			/* assign(&fngnod, argp->argval); */
			makearg((struct argnod *)argp); count++;
		}
		gchain = (struct argnod *)((intptr_t)gchain|ARGMK);
	}
}
