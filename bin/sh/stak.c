#include <gen.h>
/*#include "brkincr.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "stak.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)stak.c	4.2 8/11/83";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

/* moved here from stak.h, which now just declares them extern */
struct blk	*stakbsy;
char		*stakbas;
char		*brkend;
char		*staktop;

char		*stakbot=nullstr;

/* ========	storage allocation	======== */

char *getstak(asize) int asize; {	/* allocate requested stack */
	register char	*oldstak;
	register int		size;

	size=round(asize,sizeof(char *));
	oldstak=stakbot;
	staktop = stakbot += size;
	return(oldstak);
}

char *locstak() {	/* set up stack for local use
	 * should be followed by `endstak'
	 */
	if (brkend-stakbot<BRKINCR) {
		setbrk(brkincr);
		if (brkincr < BRKMAX) {
			brkincr += 256;
		}
	}
	return(stakbot);
}

char *savstak() {
	assert(staktop==stakbot);
	return(stakbot);
}

char *endstak(argp) register char *argp; {	/* tidy up after `locstak' */
	register char	*oldstak;
	*argp++=0;
	oldstak=stakbot; stakbot=staktop=(char *)round((intptr_t)argp,sizeof(char *));
	return(oldstak);
}

void tdystak(x) register char *x; {
	/* try to bring stack back to x */
	while ((char *)stakbsy>x) {
	   free(stakbsy);
	   stakbsy = stakbsy->word;
	}
	staktop=stakbot=max(x,stakbas);
	rmtemp(x);
}

int stakchk() {
	if ((brkend-stakbas)>BRKINCR+BRKINCR) {
		setbrk(-BRKINCR);
	}
}

char *cpystak(x) char *x; {
	return(endstak(movstr(x,locstak())));
}
