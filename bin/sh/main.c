/*#include <setjmp.h> defs.h*/
/*#include <sys/file.h> defs.h*/
#include <sys/ioctl.h>
#include <sys/proc.h>
#include <sys/stat.h>
/*#include "brkincr.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "stak.h" defs.h*/
#include "sym.h"

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)main.c	4.3 3/19/85";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/
/*#include	"sym.h"*/
/*#include	"timeout.h"*/
/*#include	<sys/types.h>*/
/*#include	<sys/stat.h>*/
/*#include	<sgtty.h>*/
/*#include	<signal.h>*/

/* moved here from defs.h, which now just declares them extern */
/* temp files and io */
int		output;
int		ioset;
struct ionod	*iotemp;		/* files to be deleted sometime */
struct ionod	*iopend;		/* documents waiting to be read at '\n' */

/* substitution */
int		dolc;
char		**dolv;
struct dolnod	*argfor;
struct argnod	*gchain;

/* name tree and words */
int		wdval;
int		wdnum;
struct argnod	*wdarg;
int		wdset;
bool		reserv;

/* special names */
char		*cmdadr;
char		*exitadr;
char		*dolladr;
char		*pcsadr;
char		*pidadr;

/* transput */
char		*tmpnam;
int		serial;
int		peekc;
char		*comdiv;

/* flags */
int		flags;

/* error exits from various parts of shell */
jmp_buf		subshell;
jmp_buf		errshell;
jmp_buf		INTbuf;

/* fault handling */
unsigned	brkincr;

bool		trapnote;

/* execflgs */
int		exitval;
bool		execbrk;
int		loopcnt;
int		breakcnt;

/* end of data segment, formerly done by linker using dummy array "end" */
struct blk	*end;

int		output = 2;
static bool	beenhere = 0;
char		tmpout[20] = "/tmp/sh-";
struct fileblk	stdfile;
struct fileblk	*standin = &stdfile;

/*extern void	exfile();*/

static void exfile __P((int prof));

int main(c, v) int c; char *v[]; {
	register int		rflag=ttyflg;

	/* initialise storage allocation */
	stdsigs();
	end = (struct blk *)setbrk(BRKINCR);
	bloktop = end;
	addblok((unsigned)0);

	/* set names from userenv */
	setupenv();

	/* look for restricted */
/*	if (c>0 && any('r', *v)) { rflag=0; } */

	/* look for options */
	dolc=options(c,v);
	if (dolc<2) { flags |= stdflg; }
	if ((flags&stdflg)==0) {
		dolc--;
	}
	dolv=v+c-dolc; dolc--;

	/* return here for shell file execution */
	setjmp(subshell);

	/* number of positional parameters */
	assnum(&dolladr,dolc);
	cmdadr=dolv[0];

	/* set pidname */
	assnum(&pidadr, getpid());

	/* set up temp file names */
	settmp();

	/* default ifs */
	dfault(&ifsnod, sptbnl);

	if ((beenhere++)==0) {
		/* ? profile */
		if (*cmdadr=='-'
		    && (input=pathopen(nullstr, profile))>=0) {
			exfile(rflag); flags &= ~ttyflg;
		}
		if (rflag==0) { flags |= rshflg; }

		/* open input file if specified */
		if (comdiv) {
			estabf(comdiv); input = -1;
		}
		else {
			input=((flags&stdflg) ? 0 : chkopen(cmdadr));
			comdiv--;
		}
#ifdef stupid
	}
	else {
		*execargs=dolv;	/* for `ps' cmd */
#endif
	}

	exfile(0);
	done();
}

static void exfile(prof) int prof; {
	register long	mailtime = 0;
	register int		userid;
	union {
		struct stat	stat;
		struct sgttyb	sgttyb;
	} b;

	/* move input */
	if (input>0) {
		Ldup(input,INIO);
		input=INIO;
	}

	/* move output to safe place */
	if (output==2) {
		Ldup(dup(2),OTIO);
		output=OTIO;
	}

	userid=getuid();

	/* decide whether interactive */
	if ((flags&intflg) || ((flags&oneflg)==0 && gtty(output,&b.sgttyb)==0 && gtty(input,&b.sgttyb)==0)) {
		dfault(&ps1nod, (userid?stdprompt:supprompt));
		dfault(&ps2nod, readmsg);
		flags |= ttyflg|prompt; ignsig(KILL);
/* {
	#include <signal.h>
		signal(SIGTTIN, SIG_IGN);
		signal(SIGTTOU, SIG_IGN);
		signal(SIGTSTP, SIG_IGN);
		}
*/
	}
	else {
		flags |= prof; flags &= ~prompt;
	}

	if (setjmp(errshell) && prof) {
		close(input); return;
	}

	/* error return here */
	loopcnt=breakcnt=peekc=0; iopend=0;
	if (input>=0) { initf(input); }

	/* command loop */
	for(;;) {
		tdystak(0);
		stakchk(); /* may reduce sbrk */
		exitset();
		if ((flags&prompt) && standin->fstak==0 && !eof) {
			if (mailnod.namval
			    && stat(mailnod.namval,&b.stat)>=0 && b.stat.st_size
			    && (b.stat.st_mtime != mailtime)
			    && mailtime) {
				prs(mailmsg);
			}
			mailtime=b.stat.st_mtime;
			prs(ps1nod.namval);
		}

		trapnote=0; peekc=readc();
		if (eof) {
			return;
		}
#if defined(__STDC__) || defined(lint)
		execute(cmd('\n',MTFLG),0,0,0);
#else
		execute(cmd('\n',MTFLG),0);
#endif
		eof |= (flags&oneflg);
	}
}

int chkpr(eor) int eor; {
	if ((flags&prompt) && standin->fstak==0 && eor=='\n') {
		prs(ps2nod.namval);
	}
}

int settmp() {
	itos(getpid()); serial=0;
	tmpnam=movstr(numbuf,&tmpout[TMPNAM]);
}

int Ldup(fa, fb) register int fa; register int fb; {
	dup2(fa, fb);
	close(fa);
	ioctl(fb, FIOCLEX, 0);
}
