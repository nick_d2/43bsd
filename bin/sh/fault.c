#include <gen.h>
/*#include <setjmp.h> defs.h*/
/*#include <sys/signal.h> gen.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)fault.c	4.3 8/11/83";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

char		*trapcom[MAXTRAP];
bool		trapflg[MAXTRAP];
bool		trapjmp[MAXTRAP];

/* ========	fault handling routines	   ======== */

void fault(sig) register int sig; {
	register int		flag;

	if (sig==MEMF) {
		if (setbrk(brkincr) == (void *)-1) {
			error(nospace);
		}
	}
	else if (sig==ALARM) {
		if (flags&waiting) {
			done();
		}
	}
	else {
		flag = (trapcom[sig] ? TRAPSET : SIGSET);
		trapnote |= flag;
		trapflg[sig] |= flag;
	}
	if (trapjmp[sig] && sig==INTR) {
	    
		trapjmp[sig] = 0;
		longjmp(INTbuf, 1);
	}
}

int stdsigs() {
	ignsig(QUIT);
	getsig(INTR);
	getsig(MEMF);
	getsig(ALARM);
}

void (*ignsig(n)) __P((int sig)) int n; {
	register void	(*s) __P((int sig));
	register int	i;

	if ((s=signal(i=n,SIG_IGN))!=SIG_IGN) {
		trapflg[i] |= SIGMOD;
	}
	return(s);
}

int getsig(n) int n; {
	register int		i;

	if (trapflg[i=n]&SIGMOD || ignsig(i)==SIG_DFL) {
		signal(i,(void (*) __P((int sig)))fault);
	}
}

int oldsigs() {
	register int		i;
	register char	*t;

	i=MAXTRAP;
	while (i--) {
	    t=trapcom[i];
	    if (t==0 || *t) {
		 clrsig(i);
	    }
	    trapflg[i]=0;
	}
	trapnote=0;
}

int clrsig(i) int i; {
	free(trapcom[i]); trapcom[i]=0;
	if (trapflg[i]&SIGMOD) {
		signal(i,(void (*) __P((int sig)))fault);
		trapflg[i] &= ~SIGMOD;
	}
}

int chktrap() {
	/* check for traps */
	register int		i=MAXTRAP;
	register char	*t;

	trapnote &= ~TRAPSET;
	while (--i) {
	   if (trapflg[i]&TRAPSET) {
		trapflg[i] &= ~TRAPSET;
		if (t=trapcom[i]) {
			int	savxit=exitval;
			execexp(t,0);
			exitval=savxit; exitset();
		}
	   }
	}
}
