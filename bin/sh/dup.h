#ifndef _DUP_H_
#define _DUP_H_

/*	dup.h	4.1	82/05/07	*/

/*
 *	UNIX shell
 *
 *	S. R. Bourne
 *	Bell Telephone Laboratories
 *
 */

#define DUPFLG 0100

#endif
