#!/bin/sh
for i in *.c *.h
do
  sed -f demac.sed $i |\
  sed -e 's/@/ATSIGN/g' |\
  tr '\n' '@' |\
  sed -e 's/[	 @]*NOWHITE[	 @]*//g' |\
  tr '@' '\n' |\
  sed -e 's/ATSIGN/@/g; s/;; }/; }/; s/;;$/;/; s/};/}/' >a
  mv a $i
done
