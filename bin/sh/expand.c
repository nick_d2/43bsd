/*#include <setjmp.h> defs.h*/
#include <sys/dir.h>
#include <sys/stat.h>
/*#include "_ctype.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "stak.h" defs.h*/

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)expand.c	4.5 8/11/83";
#endif

/*
 *	UNIX shell
 *
 *	S. R. Bourne
 *	Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/
/*#include	<sys/param.h>*/
/*#include	<sys/stat.h>*/
/*#include	<sys/dir.h>*/

/* globals (file name generation)
 *
 * "*" in params matches r.e ".*"
 * "?" in params matches r.e. "."
 * "[...]" in params matches character class
 * "[...a-z...]" in params matches a through z.
 *
 */

/*extern void	addg();*/

static void addg __P((char *as1, char *as2, char *as3));

int expand(as, rflg) char *as; int rflg; {
	int		count;
	DIR		*dirf;
	bool		dir=0;
	char		*rescan = 0;
	register char	*s, *cs;
	struct argnod		*schain = gchain;
	struct direct	*dp;
	struct stat		statb;

	if (trapnote&SIGSET) { return(0); }

	s=cs=as;

	/* check for meta chars */ {
	     
	   register bool slash; slash=0;
	   while (!fngchar(*cs)) {
		if (*cs++==0) {
			if (rflg && slash) { break; } else { return(0); }
		}
		else if (*cs=='/') {
			slash++;
		}
	   }
	}

	for(;;) {
		if (cs==s) {
			s=nullstr;
			break;
		}
		else if (*--cs == '/') {
			*cs=0;
			if (s==cs) { s="/"; }
			break;
		}
	}
	if (stat(s == nullstr ? "." : s,&statb)>=0
	    && (statb.st_mode&S_IFMT)==S_IFDIR
	    && (dirf=opendir(s == nullstr ? "." : s)) != NULL) {
		dir++;
	}
	count=0;
	if (*cs==0) { *cs++=0200; }
	if (dir) {
		/* check for rescan */
		register char *rs; rs=cs;

		do {
			if (*rs=='/') { rescan=rs; *rs=0; gchain=0; }
		} while (*rs++);

		if (setjmp(INTbuf) == 0) { trapjmp[INTR] = 1; }
		while ((trapnote&SIGSET) == 0 && (dp = readdir(dirf)) != NULL) {
			if ((*dp->d_name=='.' && *cs!='.')) {
				continue;
			}
			if (gmatch(dp->d_name, cs)) {
				addg(s,dp->d_name,rescan); count++;
			}
		}
		closedir(dirf); trapjmp[INTR] = 0;

		if (rescan) {
			register struct argnod	*rchain;
			rchain=gchain; gchain=schain;
			if (count) {
				count=0;
				while (rchain) {
					count += expand(rchain->argval,1);
					rchain=rchain->argnxt;
				}
			}
			*rescan='/';
		}
	}
 {
	     
	   register char	c;
	   s=as;
	   while (c = *s) { *s++=(c&0177?c:'/'); }
	}
	return(count);
}

int gmatch(s, p) register char *s; register char *p; {
	register int		scc;
	char		c;

	if (scc = *s++) {
		if ((scc &= 0177)==0) {
			scc=0200;
		}
	}
	switch (c = *p++) {
	  

	    case '[':
		{bool ok; int lc;
		ok=0; lc=077777;
		while (c = *p++) {
			if (c==']') {
				return(ok?gmatch(s,p):0);
			}
			else if (c=='-') {
				if (lc<=scc && scc<=(*p++)) { ok++; }
			}
			else {
				if (scc==(lc=(c&0177))) { ok++; }
			}
		}
		return(0);
		}

	    default:
		if ((c&0177)!=scc) { return(0); }

	    case '?':
		return(scc?gmatch(s,p):0);

	    case '*':
		if (*p==0) { return(1); }
		--s;
		while (*s) {
		    if (gmatch(s++,p)) { return(1); }
		}
		return(0);

	    case 0:
		return(scc==0);
	}
}

static void addg(as1, as2, as3) char *as1; char *as2; char *as3; {
	register char	*s1, *s2;
	register int		c;

	s2 = locstak()+sizeof(char *);

	s1=as1;
	while (c = *s1++) {
		if ((c &= 0177)==0) {
			*s2++='/';
			break;
		}
		*s2++=c;
	}
	s1=as2;
	while (*s2 = *s1++) { s2++; }
	if (s1=as3) {
		*s2++='/';
		while (*s2++ = *++s1);
	}
	makearg((struct argnod *)endstak(s2));
}

int makearg(args) register struct argnod *args; {
	args->argnxt=gchain;
	gchain=args;
}
