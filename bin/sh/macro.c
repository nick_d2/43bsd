/*#include <sys/file.h> defs.h*/
/*#include "_ctype.h" defs.h*/
#include "defs.h"
/*#include "mode.h" defs.h*/
/*#include "name.h" defs.h*/
/*#include "stak.h" defs.h*/
#include "sym.h"

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)macro.c	4.3 8/11/83";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/
/*#include	"sym.h"*/

static char	quote;	/* used locally */
static char	quoted;	/* used locally */

static char *copyto __P((int endch));
static skipto __P((int endch));
static getch __P((int endch));
static comsubst __P((void));
static flush __P((int ot));

static char *copyto(endch) int endch; {
	register char	c;

	while ((c=getch(endch))!=endch && c) { pushstak(c|quote); }
	zerostak();
	if (c!=endch) { error(badsub); }
}

static skipto(endch) int endch; {
	/* skip chars up to } */
	register char	c;
	while ((c=readc()) && c!=endch) {
		switch (c) {
		  

		case '`':	skipto('`'); break;

		case '"':	skipto('"'); break;

		case '$':	if (readc()=='{') { skipto('}');
				}
		}
	}
	if (c!=endch) { error(badsub); }
}

static getch(endch) int endch; {
	register char	d;

retry:
	d=readc();
	if (!subchar(d)) {
		return(d);
	}
	if (d=='$') {
		register int	c;
		if ((c=readc(), dolchar(c))) {
			struct namnod		*n=NULL;
			int		dolg=0;
			bool		bra;
			intptr_t	rel;
			register char	*argp, *v;
			char		idb[2];
			char		*id=idb;

			if (bra=(c=='{')) { c=readc(); }
			if (letter(c)) {
				rel=relstak();
				while (alphanum(c)) { pushstak(c); c=readc(); }
				zerostak();
				n=lookup(absstak(rel)); setstak(rel);
				v = n->namval; id = n->namid;
				peekc = c|MARK;
			}
			else if (digchar(c)) {
				*id=c; idb[1]=0;
				if (astchar(c)) {
					dolg=1; c='1';
				}
				c -= '0';
				/*v=((c==0) ? cmdadr : (c<=dolc) ? dolv[c] : (dolg=0));*/
				if (c == 0)
					v = cmdadr;
				else if (c <= dolc)
					v = dolv[c];
				else {
					dolg = 0;
					v = 0;
				}
			}
			else if (c=='$') {
				v=pidadr;
			}
			else if (c=='!') {
				v=pcsadr;
			}
			else if (c=='#') {
				v=dolladr;
			}
			else if (c=='?') {
				v=exitadr;
			}
			else if (c=='-') {
				v=flagadr;
			}
			else if (bra) {
			     error(badsub);
			}
			else {
				goto retry;
			}
			c = readc();
			if (!defchar(c) && bra) {
				error(badsub);
			}
			argp=0;
			if (bra) { if (c!='}') {
					rel=relstak();
					if ((v==0)^(setchar(c))) { copyto('}');
					}
					else { skipto('}');
					}
					argp=absstak(rel);
				}
			}
			else {
				peekc = c|MARK; c = 0;
			}
			if (v) {
				if (c!='+') {
					for(;;) {
					     while (c = *v++) { pushstak(c|quote); }
					     if (dolg==0 || (++dolg>dolc)) {
						  break;
					     }
					     else {
						  v=dolv[dolg]; pushstak(' '|(*id=='*' ? quote : 0));
					     }
					}
				}
			}
			else if (argp) {
				if (c=='?') {
					failed(id,*argp?argp:badparam);
				}
				else if (c=='=') {
					if (n) {
						assign(n,argp);
					}
					else {
						error(badsub);
					}
				}
			}
			else if (flags&setflg) {
				failed(id,badparam);
			}
			goto retry;
		}
		else {
			peekc=c|MARK;
		}
	}
	else if (d==endch) {
		return(d);
	}
	else if (d=='`') {
		comsubst(); goto retry;
	}
	else if (d=='"') {
		quoted++; quote^=0200; goto retry;
	}
	return(d);
}

char *macro(as) char *as; {
	/* Strip "" and do $ substitution
	 * Leaves result on top of stack
	 */
	register bool	savqu =quoted;
	register char	savq = quote;
	/*struct filehdr		fb;*/
	char		fb[sizeof(struct fileblk)-BUFSIZ];

	push(/*&fb*/(struct fileblk *)fb); estabf(as);
	usestak();
	quote=0; quoted=0;
	copyto(0);
	pop();
	if (quoted && (stakbot==staktop)) { pushstak(0200); }
	quote=savq; quoted=savqu;
	return(fixstak());
}

static comsubst() {
	/* command substn */
	struct fileblk		cb;
	register char	d;
	register char	*savptr = fixstak();

	usestak();
	while ((d=readc())!='`' && d) { pushstak(d); }
 {
	     
	   register char	*argc;
	   trim(argc=fixstak());
	   push(&cb); estabf(argc);
	} {
	     
	   register struct trenod	*t = makefork(FPOU,cmd(EOFSYM,MTFLG|NLFLG));
	   int		pv[2];

	   /* this is done like this so that the pipe
	    * is open only when needed
	    */
	   chkpipe(pv);
	   initf(pv[INPIPE]);
	   execute(t, 0, 0, pv);
	   close(pv[OTPIPE]);
	}
	tdystak(savptr); staktop=movstr(savptr,stakbot);
	while (d=readc()) { locstak(); pushstak(d|quote); }
	await(0);
	while (stakbot!=staktop) {
		if ((*--staktop&0177)!='\n') {
			++staktop; break;
		}
	}
	pop();
}

#define CPYSIZ	512

int subst(in, ot) int in; int ot; {
	register char	c;
	struct fileblk		fb;
	register int		count=CPYSIZ;

	push(&fb); initf(in);
	/* '"' used to stop it from quoting */
	while (c=(getch('"')&0177)) {
	   pushstak(c);
	   if (--count == 0) {
		flush(ot); count=CPYSIZ;
	   }
	}
	flush(ot);
	pop();
}

static flush(ot) int ot; {
	write(ot,stakbot,staktop-stakbot);
	if (flags&execpr) { write(output,stakbot,staktop-stakbot); }
	staktop=stakbot;
}
