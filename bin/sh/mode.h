#ifndef _MODE_H_
#define _MODE_H_

/*	mode.h	4.1	82/05/07	*/

/*
 *	UNIX shell
 */

#ifdef __STDC__
#include <stdint.h>
#else
typedef int intptr_t;
#endif

typedef char bool;

#ifndef NULL
#define NULL ((void *)0)
#endif

/* heap storage */
struct blk {
	struct blk	*word;
};

#define	BUFSIZ	64
struct fileblk {
	int	fdes;
	unsigned	flin;
	bool	feof;
	char	fsiz;
	char	*fnxt;
	char	*fend;
	char	**feval;
	struct fileblk	*fstak;
	char	fbuf[BUFSIZ];
};

/* for files not used with file descriptors */
/*struct filehdr {*/
/*	int	fdes;*/
/*	unsigned	flin;*/
/*	bool	feof;*/
/*	char	fsiz;*/
/*	char	*fnxt;*/
/*	char	*fend;*/
/*	char	**feval;*/
/*	struct fileblk	*fstak;*/
/*	char	_fbuf[1];*/
/*};*/

struct sysnod {
	char	*sysnam;
	int	sysval;
};

/* this node is a proforma for those that follow */
struct trenod {
	int	tretyp;
	struct ionod	*treio;
};

/* dummy for access only */
struct argnod {
	struct argnod	*argnxt;
	char	argval[1];
};

struct dolnod {
	struct dolnod	*dolnxt;
	int	doluse;
	char	*dolarg[1];
};

struct forknod {
	int	forktyp;
	struct ionod	*forkio;
	struct trenod	*forktre;
};

struct comnod {
	int	comtyp;
	struct ionod	*comio;
	struct argnod	*comarg;
	struct argnod	*comset;
};

struct ifnod {
	int	iftyp;
	struct trenod	*iftre;
	struct trenod	*thtre;
	struct trenod	*eltre;
};

struct whnod {
	int	whtyp;
	struct trenod	*whtre;
	struct trenod	*dotre;
};

struct fornod {
	int	fortyp;
	struct trenod	*fortre;
	char	*fornam;
	struct comnod	*forlst;
};

struct swnod {
	int	swtyp;
	char	*swarg;
	struct regnod	*swlst;
};

struct regnod {
	struct argnod	*regptr;
	struct trenod	*regcom;
	struct regnod	*regnxt;
};

struct parnod {
	int	partyp;
	struct trenod	*partre;
};

struct lstnod {
	int	lsttyp;
	struct trenod	*lstlef;
	struct trenod	*lstrit;
};

struct ionod {
	int	iofile;
	char	*ioname;
	struct ionod	*ionxt;
	struct ionod	*iolst;
};

#endif
