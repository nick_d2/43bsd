#ifndef _DEFS_H_
#define _DEFS_H_

#include <setjmp.h>
#include <sys/file.h>
#include "mode.h"
#include "name.h"

/*	defs.h	4.4	85/03/19	*/

/*
 *	UNIX shell
 */

/* error exits from various parts of shell */
#define ERROR	1
#define SYNBAD	2
#define SIGFAIL 3
#define SIGFLG	0200

/* command tree */
#define FPRS	020
#define FINT	040
#define FAMP	0100
#define FPIN	0400
#define FPOU	01000
#define FPCL	02000
#define FCMD	04000
#define COMMSK	017

#define TCOM	0
#define TPAR	1
#define TFIL	2
#define TLST	3
#define TIF	4
#define TWH	5
#define TUN	6
#define TSW	7
#define TAND	8
#define TORF	9
#define TFORK	10
#define TFOR	11

/* execute table */
#define SYSSET	1
#define SYSCD	2
#define SYSEXEC	3
#define SYSLOGIN 4
#define SYSTRAP	5
#define SYSEXIT	6
#define SYSSHFT 7
#define SYSWAIT	8
#define SYSCONT 9
#define SYSBREAK 10
#define SYSEVAL 11
#define SYSDOT	12
#define SYSRDONLY 13
#define SYSTIMES 14
#define SYSXPORT 15
#define SYSNULL 16
#define SYSREAD 17
#define SYSTST	18
#define	SYSUMASK	19

/* used for input and output of shell */
#define INIO 10
#define OTIO 11

/*io nodes*/
#define USERIO	10
#define IOUFD	15
#define IODOC	16
#define IOPUT	32
#define IOAPP	64
#define IOMOV	128
#define IORDW	256
#define INPIPE	0
#define OTPIPE	1

/*#include	"mode.h"*/
/*#include	"name.h"*/

/* result type declarations */
/*void		*malloc();*/
/*void		addblok();*/
/*char		*make();*/
/*char		*movstr();*/
/*struct trenod	*cmd();*/
/*struct trenod	*makefork();*/
/*struct namnod	*lookup();*/
/*void		setname();*/
/*void		setargs();*/
/*struct dolnod	*useargs();*/
/*float		expr();*/
/*char		*catpath();*/
/*char		*getpath();*/
/*char		**scan();*/
/*char		*mactrim();*/
/*char		*macro();*/
/*char		*execs();*/
/*void		await();*/
/*void		post();*/
/*char		*copyto();*/
/*void		exname();*/
/*char		*staknam();*/
/*void		printnam();*/
/*void		printflg();*/
/*void		prs();*/
/*void		prc();*/
/*void		setupenv();*/
/*char		**setenv();*/

#define attrib(n,f)	(n->namflg |= f)
#define round(a,b)	(((a)+((b)-1))&~((b)-1))
#define closepipe(x)	(close(x[INPIPE]), close(x[OTPIPE]))
#define eq(a,b)		(cf(a,b)==0)
#define max(a,b)	((a)>(b)?(a):(b))
#define assert(x)	;

/* temp files and io */
extern int		output;
extern int		ioset;
extern struct ionod	*iotemp;		/* files to be deleted sometime */
extern struct ionod	*iopend;		/* documents waiting to be read at '\n' */

/* substitution */
extern int		dolc;
extern char		**dolv;
extern struct dolnod	*argfor;
extern struct argnod	*gchain;

/* stak stuff */
#include	"stak.h"

/* string constants */
extern char		atline[];
extern char		readmsg[];
extern char		colon[];
extern char		minus[];
extern char		nullstr[];
extern char		sptbnl[];
extern char		unexpected[];
extern char		endoffile[];
extern char		synmsg[];

/* name tree and words */
extern struct sysnod	reserved[];
extern int		wdval;
extern int		wdnum;
extern struct argnod	*wdarg;
extern int		wdset;
extern bool		reserv;

/* prompting */
extern char		stdprompt[];
extern char		supprompt[];
extern char		profile[];

/* built in names */
extern struct namnod	fngnod;
extern struct namnod	ifsnod;
extern struct namnod	homenod;
extern struct namnod	mailnod;
extern struct namnod	pathnod;
extern struct namnod	ps1nod;
extern struct namnod	ps2nod;

/* special names */
extern char		flagadr[];
extern char		*cmdadr;
extern char		*exitadr;
extern char		*dolladr;
extern char		*pcsadr;
extern char		*pidadr;

extern char		defpath[];

/* names always present */
extern char		mailname[];
extern char		homename[];
extern char		pathname[];
extern char		fngname[];
extern char		ifsname[];
extern char		ps1name[];
extern char		ps2name[];

/* transput */
extern char		tmpout[];
extern char		*tmpnam;
extern int		serial;
#define		TMPNAM 7
extern struct fileblk	*standin;
#define input	(standin->fdes)
#define eof	(standin->feof)
extern int		peekc;
extern char		*comdiv;
extern char		devnull[];

/* flags */
#define		noexec	01
#define		intflg	02
#define		prompt	04
#define		setflg	010
#define		errflg	020
#define		ttyflg	040
#define		forked	0100
#define		oneflg	0200
#define		rshflg	0400
#define		waiting	01000
#define		stdflg	02000
#define		execpr	04000
#define		readpr	010000
#define		keyflg	020000
#define		batchflg	040000
extern int		flags;

/* error exits from various parts of shell */
/*#include	<setjmp.h>*/
extern jmp_buf		subshell;
extern jmp_buf		errshell;
extern jmp_buf		INTbuf;

/* fault handling */
#include	"brkincr.h"
extern unsigned		brkincr;
extern struct blk	*blokp;			/*current search pointer*/
extern struct blk	*bloktop;		/*top of arena (last blok)*/

#define MINTRAP	0
#define MAXTRAP	32

#define INTR	2
#define QUIT	3
#define MEMF	11
#define ALARM	14
#define KILL	15
#define TRAPSET	2
#define SIGSET	4
#define SIGMOD	8

/*void		fault();*/
extern bool		trapnote;
extern char		*trapcom[];
extern bool		trapflg[];
extern bool		trapjmp[];

/* name tree and words */
extern char		**environ;
extern char		numbuf[];
extern char		export[];
extern char		readonly[];

/* execflgs */
extern int		exitval;
extern bool		execbrk;
extern int		loopcnt;
extern int		breakcnt;

/* messages */
extern char		mailmsg[];
extern char		coredump[];
extern char		badopt[];
extern char		badparam[];
extern char		badsub[];
extern char		nospace[];
extern char		notfound[];
extern char		badtrap[];
extern char		baddir[];
extern char		badshift[];
extern char		illegal[];
extern char		restricted[];
extern char		execpmsg[];
extern char		notid[];
extern char		wtfailed[];
extern char		badcreate[];
extern char		piperr[];
extern char		badopen[];
extern char		badnum[];
extern char		arglist[];
extern char		txtbsy[];
extern char		toobig[];
extern char		badexec[];
extern char		notfound[];
extern char		badfile[];

/* end of data segment, formerly done by linker using dummy array "end" */
extern struct blk	*end;

#include	"_ctype.h"

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* args.c */
int options __P((int argc, char **argv));
void setargs __P((char *argi[]));
struct dolnod *freeargs __P((struct dolnod *blk));
int clearup __P((void));
struct dolnod *useargs __P((void));

/* blok.c */
void *malloc __P((unsigned nbytes));
void addblok __P((unsigned reqd));
void free __P((void *ap));
#ifdef X_
/* simple realloc, because host's libc malloc() has been redirected to here */
void *realloc __P((void *ap, unsigned nbytes));
#endif
int chkbptr __P((struct blk *ptr));

/* builtin.c */
int builtin __P((int argn, char **com));

/* cmd.c */
struct trenod *makefork __P((int flgs, struct trenod *i));
struct trenod *cmd __P((register int sym, int flg));

/* error.c */
int exitset __P((void));
int sigchk __P((void));
int failed __P((char *s1, char *s2));
int error __P((char *s));
int exitsh __P((int xno));
int done __P((void));
int rmtemp __P((char *base));

/* expand.c */
int expand __P((char *as, int rflg));
int gmatch __P((register char *s, register char *p));
int makearg __P((register struct argnod *args));

/* fault.c */
void fault __P((register int sig));
int stdsigs __P((void));
void (*ignsig __P((int n))) __P((int sig));
int getsig __P((int n));
int oldsigs __P((void));
int clrsig __P((int i));
int chktrap __P((void));

/* io.c */
int initf __P((int fd));
int estabf __P((register char *s));
int push __P((struct fileblk *af));
int pop __P((void));
int chkpipe __P((int *pv));
int chkopen __P((char *idf));
int _rename __P((register int f1, register int f2));
int create __P((char *s));
int tmpfil __P((void));
int copy __P((struct ionod *ioparg));

/* macro.c */
char *macro __P((char *as));
int subst __P((int in, int ot));

/* main.c */
int main __P((int c, char *v[]));
int chkpr __P((int eor));
int settmp __P((void));
int Ldup __P((register int fa, register int fb));

/* name.c */
int syslook __P((char *w, struct sysnod syswds[]));
int setlist __P((register struct argnod *arg, int xp));
void setname __P((char *argi, int xp));
int replace __P((register char **a, char *v));
int dfault __P((struct namnod *n, char *v));
int assign __P((struct namnod *n, char *v));
int readvar __P((char **names));
int assnum __P((char **p, int i));
char *make __P((char *v));
struct namnod *lookup __P((register char *nam));
int namscan __P((void (*fn)(struct namnod *n)));
void printnam __P((struct namnod *n));
void exname __P((register struct namnod *n));
void printflg __P((register struct namnod *n));
void setupenv __P((void));
void countnam __P((struct namnod *n));
void pushnam __P((struct namnod *n));
char **setenv __P((void));

/* print.c */
int newline __P((void));
int blank __P((void));
int prp __P((void));
void prs __P((char *as));
void prc __P((int c));
int prt __P((long t));
int prn __P((int n));
int itos __P((int n));
int stoi __P((char *icp));

/* service.c */
void initio __P((struct ionod *iop));
char *getpath __P((char *s));
int pathopen __P((register char *path, register char *name));
char *catpath __P((register char *path, char *name));
void execa __P((char *at[]));
int gocsh __P((register char **t, register char *cp, register char **xecenv));
int postclr __P((void));
void post __P((int pcsid));
void await __P((int i));
int trim __P((char *at));
char *mactrim __P((char *s));
char **scan __P((int argn));
int getarg __P((struct comnod *ac));

/* setbrk.c */
void *setbrk __P((int incr));

/* string.c */
char *movstr __P((register char *a, register char *b));
int any __P((int c, char *s));
int cf __P((register char *s1, register char *s2));
int length __P((char *as));

/* word.c */
int word __P((void));
int nextc __P((int quote));
int readc __P((void));

/* xec.c */
int execute __P((struct trenod *argt, int execflg, int *pf1, int *pf2));
int execexp __P((char *s, ...));

#endif
