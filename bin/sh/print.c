/*#include <sys/file.h> defs.h*/
/*#include "_ctype.h" defs.h*/
#include "defs.h"

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)print.c	4.2 8/11/83";
#endif

/*
 * UNIX shell
 *
 * S. R. Bourne
 * Bell Telephone Laboratories
 *
 */

/*#include	"defs.h"*/

char		numbuf[6];

/* printing and io conversion */

int newline() {	prc('\n');
}

int blank() {	prc(' ');
}

int prp() {
	if ((flags&prompt)==0 && cmdadr) {
		prs(cmdadr); prs(colon);
	}
}

void prs(as) char *as; {
	register char	*s;

	if (s=as) {
		write(output,s,length(s)-1);
	}
}

void prc(c) int c; {
	if (c) {
		write(output,&c,1);
	}
}

int prt(t) long t; {
	register int	hr, min, sec;

	t += 30; t /= 60;
	sec=t%60; t /= 60;
	min=t%60;
	if (hr=t/60) {
		prn(hr); prc('h');
	}
	prn(min); prc('m');
	prn(sec); prc('s');
}

int prn(n) int n; {
	itos(n); prs(numbuf);
}

int itos(n) int n; {
	register char *abuf; register unsigned a, i; int pr, d;
	abuf=numbuf; pr=0; a=n;
	for (i=10000; i!=1; i/=10) {
		if ((pr |= (d=a/i))) { *abuf++=d+'0'; }
		a %= i;
	}
	*abuf++=a+'0';
	*abuf++=0;
}

int stoi(icp) char *icp; {
	register char	*cp = icp;
	register int		r = 0;
	register char	c;

	while ((c = *cp, digit(c)) && c && r>=0) { r = r*10 + c - '0'; cp++; }
	if (r<0 || cp==icp) {
		failed(icp,badnum);
	}
	else {
		return(r);
	}
}
