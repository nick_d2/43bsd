s/@/ATSIGN/g
s/^/@/
s/$/@/
s/[^A-Za-z0-9_]\+/@&@/g

# defs.h
s/@alloc@/@malloc@/g
s/@rename@/@_rename@/g

# mode.h
s/@BYTESPERWORD@/@sizeof(char *)@/g

# sym.h
s/@DQUOTE@/@'"'@/g
s/@SQUOTE@/@'`'@/g
s/@LITERAL@/@'\\''@/g
s/@DOLLAR@/@'$'@/g
s/@ESCAPE@/@'\\\\'@/g
s/@BRACE@/@'{'@/g

s/@//g
s/ATSIGN/@/g
