#include <a.out.h>
#include <ctype.h>
#include <strings.h>
#include "defs.h"
#include "head.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)expr.c	4.8 8/11/83";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include "defs.h"*/

extern char		BADSYM[];
extern char		BADVAR[];
extern char		BADKET[];
extern char		BADSYN[];
extern char		NOCFN[];
extern char		NOADR[];
extern char		BADLOC[];

intptr_t		lastframe;
intptr_t		savlastf;
intptr_t		savframe;
intptr_t		savpc;
intptr_t		callpc;

extern char		*lp;
extern short		radix;
extern char		*errflg;
long		localval;
char		isymbol[1024];

extern char		lastc,peekc;

extern long		dot;
extern long		ditto;
extern short		dotinc;
extern long		var[];
extern long		expv;

int expr(a) int a; {	/* term | term dyadic expr |  */
	short		rc;
	long		lhs;

	rdc(); lp--; rc=term(a);

	while (rc) {
	    lhs = expv;

	    switch ((int)readchar()) {

		    case '+':
			term(a|1); expv += lhs; break;

		    case '-':
			term(a|1); expv = lhs - expv; break;

		    case '#':
			term(a|1); expv = round(lhs,expv); break;

		    case '*':
			term(a|1); expv *= lhs; break;

		    case '%':
			term(a|1); expv = lhs/expv; break;

		    case '&':
			term(a|1); expv &= lhs; break;

		    case '|':
			term(a|1); expv |= lhs; break;

		    case ')':
			if ((a&2)==0) { error(BADKET); }

		    default:
			lp--;
			return(rc);
	    }
	}
	return(rc);
}

int term(a) int a; {	/* item | monadic item | (expr) | */

	switch ((int)readchar()) {

		    case '*':
			term(a|1); expv=chkget(expv,DSP); return(1);

		    case '@':
			term(a|1); expv=chkget(expv,_ISP); return(1);

		    case '-':
			term(a|1); expv = -expv; return(1);

		    case '~':
			term(a|1); expv = ~expv; return(1);

		    case '#':
			term(a|1); expv = !expv; return(1);

		    case '(':
			expr(2);
			if (*lp!=')') {
				error(BADSYN);
			}
			else {
				lp++; return(1);
			}

		    default:
			lp--;
			return(item(a));
	}
}

int item(a) int a; {	/* name [ . local ] | number | . | ^ | <var | <register | 'x | | */
	short		base, d;
	char		savc;
	long		frame;
	register struct nlist *symp;
	int regptr;

	readchar();
	if (symchar(0)) {
		readsym();
		if (lastc=='.') {
			frame= *(intptr_t *)(((intptr_t)&u)+_FP); lastframe=0;
			callpc= *(intptr_t *)(((intptr_t)&u)+_PC);
			while (errflg==0) {
			    savpc=callpc;
				findsym(callpc,ISYM);
			    if (eqsym(cursym->n_un.n_name,isymbol,'~')) {
				 break;
			    }
				callpc=get(frame+16, DSP);
			    lastframe=frame;
			    frame=get(frame+12,DSP)&-2;
			    if (frame==0) {
				 error(NOCFN);
			    }
			}
			savlastf=lastframe; savframe=frame;
			readchar();
			if (symchar(0)) {
				chkloc(expv=frame);
			}
		}
		else if ((symp=lookup(isymbol))==0) {
		     error(BADSYM);
		}
		else {
		     expv = symp->n_value;
		}
		lp--;

	}
	else if (getnum(readchar)) {
	     ;
	}
	else if (lastc=='.') {
		readchar();
		if (symchar(0)) {
			lastframe=savlastf; callpc=savpc;
			chkloc(savframe);
		}
		else {
			expv=dot;
		}
		lp--;

	}
	else if (lastc=='"') {
		expv=ditto;

	}
	else if (lastc=='+') {
		expv=inkdot(dotinc);

	}
	else if (lastc=='^') {
		expv=inkdot(-dotinc);

	}
	else if (lastc=='<') {
		savc=rdc();
		if (regptr=getreg(savc)) {
			if (kcore) {
			     expv = *(int *)regptr;
			}
			else { expv= * (intptr_t *)(((intptr_t)&u)+regptr); }
		}
		else if ((base=varchk(savc)) != -1) {
			expv=var[base];
		}
		else {
			error(BADVAR);
		}

	}
	else if (lastc=='\'') {
		d=4; expv=0;
		while (quotchar()) {
		    if (d--) {
			 expv = (expv << 8) | lastc;
		    }
		    else {
			 error(BADSYN);
		    }
		}

	}
	else if (a) {
		error(NOADR);
	}
	else {
		lp--; return(0);
	}
	return(1);
}

/* service routines for expression reading */
int getnum(rdf) int (*rdf)(); {
	short base,d,frpt;
	bool hex;
	union{float r; long i;} real;
	if (isdigit(lastc) || (hex=-1, lastc=='#' && isxdigit((*rdf)()))) {
		expv = 0;
		base = (hex ? 16 : radix);
		while ((base>10 ? isxdigit(lastc) : isdigit(lastc))) {
		    expv = (base==16 ? expv<<4 : expv*base);
		    if ((d=convdig(lastc))>=base) { error(BADSYN); }
		    expv += d; (*rdf)();
		    if (expv==0) {
			 if ((lastc=='x' || lastc=='X')) {
				      hex=-1; base=16; (*rdf)();
				 }
				 else if ((lastc=='t' || lastc=='T')) {
				  hex=0; base=10; (*rdf)();
			 }
			 else if ((lastc=='o' || lastc=='O')) {
			      hex=0; base=8; (*rdf)();
				 }
		    }
		}
		if (lastc=='.' && (base==10 || expv==0) && !hex) {
			real.r=expv; frpt=0; base=10;
			while (isdigit((*rdf)())) {
				real.r *= base; frpt++;
				real.r += lastc-'0';
			}
			while (frpt--) { real.r /= base; }
			expv = real.i;
		}
		peekc=lastc;
/*		lp--; */
		return(1);
	}
	else {
	     return(0);
	}
}

void readsym() {
	register char	*p;

	p = isymbol;
	do {
	    if (p < &isymbol[sizeof(isymbol)-1]) {
		 *p++ = lastc;
	    }
	    readchar();
	} while (symchar(1));
	*p++ = 0;
}

int convdig(c) int c; {
	if (isdigit(c)) {
		return(c-'0');
	}
	else if (isxdigit(c)) {
		return(c-'a'+10);
	}
	else {
		return(17);
	}
}

int symchar(dig) int dig; {
	if (lastc=='\\') { readchar(); return(-1); }
	return( isalpha(lastc) || lastc=='_' || dig && isdigit(lastc) );
}

int varchk(name) int name; {
	if (isdigit(name)) { return(name-'0'); }
	if (isalpha(name)) { return((name&037)-1+10); }
	return(-1);
}

void chkloc(frame) long frame; {
	readsym();
	do {
/* Nick added ",frame" below */
/* it is supposed to be the pointer to the arguments of the current frame */
/* our caller has taken frame from _FP just prior to calling us */
/* possibly it could have got argp from _AP in the same manner and sent here */
/* but it doesn't seem completely clear, see calls vs callg stuff in print.c */
/* we will treat it as the callg case where argp is set to frame as fallback */
	    if (localsym(frame,frame)==0) { error(BADLOC); }
	    expv=localval;
	} while (!eqsym(cursym->n_un.n_name,isymbol,'~'));
}

int eqsym(s1, s2, c) register char *s1; register char *s2; int c; {

	if (!strcmp(s1,s2))
		return (1);
	if (*s1 == c && !strcmp(s1+1, s2))
		return (1);
	return (0);
}
