#ifndef _DEFS_H_
#define _DEFS_H_

#include <sys/ptrace.h>
#include <vax/machparam.h>
#include <vax/pcb.h>

/*	defs.h	4.6	86/03/26	*/

/*
 * adb - vax string table version; common definitions
 */
/*#include <machine/psl.h>*/
/*#include <machine/pte.h>*/

/*#include <sys/param.h>*/
/*#include <sys/dir.h>*/
/*#include <sys/user.h>*/

/*#include <ctype.h>*/
/*#include <a.out.h>*/
/*#include <sys/ptrace.h>*/

/*#include "mode.h"*/
/*#include "head.h"*/

/* access modes */
#define RD	0
#define WT	1

#define NSP	0
#define	_ISP	1
#define	DSP	2
#define STAR	4
#define STARCOM 0200

/*
 * Symbol types, used internally in calls to findsym routine.
 * One the VAX this all degenerates since I & D symbols are indistinct.
 * Basically we get NSYM==0 for `=' command, ISYM==DSYM otherwise.
 */
#define NSYM	0
#define DSYM	1		/* Data space symbol */
#define ISYM	DSYM		/* Instruction space symbol == DSYM on VAX */

#define BKPTSET	1
#define BKPTEXEC 2

#define USERPS	_PSL
#define USERPC	_PC
#define BPT	03
#define TBIT	020
#define FD	0200

/* puns from <sys/ptrace.h> */
#define	CONTIN	PT_CONTINUE
#define SINGLE	PT_STEP

/* the quantities involving ctob() are located in the kernel stack. */
/* the others are in the pcb. */
#define _KSP	0
#define _ESP	4
#define _SSP	8
#define _USP	(ctob(UPAGES)-5*sizeof(int))
#define _R0	(ctob(UPAGES)-18*sizeof(int))
#define _R1	(ctob(UPAGES)-17*sizeof(int))
#define _R2	(ctob(UPAGES)-16*sizeof(int))
#define _R3	(ctob(UPAGES)-15*sizeof(int))
#define _R4	(ctob(UPAGES)-14*sizeof(int))
#define _R5	(ctob(UPAGES)-13*sizeof(int))
#define _R6	(ctob(UPAGES)-12*sizeof(int))
#define _R7	(ctob(UPAGES)-11*sizeof(int))
#define _R8	(ctob(UPAGES)-10*sizeof(int))
#define _R9	(ctob(UPAGES)-9*sizeof(int))
#define _R10	(ctob(UPAGES)-8*sizeof(int))
#define _R11	(ctob(UPAGES)-7*sizeof(int))
#define _AP	(ctob(UPAGES)-21*sizeof(int))
#define _FP	(ctob(UPAGES)-20*sizeof(int))
#define _PC	(ctob(UPAGES)-2*sizeof(int))
#define _PSL	(ctob(UPAGES)-1*sizeof(int))
#define _P0BR	80
#define _P0LR	84
#define _P1BR	88
#define _P1LR	92

#define MAXOFF	255
#define MAXPOS	80
#define MAXLIN	128

/* result type declarations */
/*long		inkdot();*/
/*unsigned	get();*/
/*unsigned	chkget();*/
/*char		*exform();*/
/*long		round();*/
/*struct bkpt	*scanbkpt();*/
/*void		fault();*/

extern struct	pcb	pcb;
extern int	kernel;
extern int	kcore;
extern struct	pte *sbr;
extern int	slr;
extern int	masterpcbb;

#endif
