/*#include <sys/ptrace.h> defs.h*/
#include "defs.h"
#include "head.h"
#include "machine.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)command.c	4.6 10/13/84";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include "defs.h"*/

extern char		BADEQ[];
extern char		NOMATCH[];
extern char		BADVAR[];
extern char		BADCOM[];

extern struct map	txtmap;
extern struct map	datmap;
short		executing;
char		*lp;
extern short		fcor;
short		fsym;
short		mkfault;
extern char		*errflg;

extern char		lastc;
char		eqformat[512] = "z";
char		stformat[512] = "X\"= \"^i";

extern long		dot;
long		ditto;
short		dotinc;
short		lastcom = '=';
extern long		var[];
long		locval;
long		locmsk;
extern short		pid;
long		expv;
extern long		adrval;
extern short		adrflg;
extern long		cntval;
extern short		cntflg;

/* command decoding */

int command(buf, defcom) char *buf; int defcom; {
	short		itype, ptype, modifier, regptr;
	bool		longpr, eqcom;
	char		wformat[1];
	char		savc;
	long		w, savdot;
	char		*savlp=lp;
	if (buf) {
	     if (*buf=='\n') {
		  return(0);
	     }
	     else {
		  lp=buf;
	     }
	}

	do {
	   
	if (adrflg=expr(0)) {
	     dot=expv; ditto=dot;
	}
	adrval=dot;
	if (rdc()==',' && expr(0)) {
	     cntflg=-1; cntval=expv;
	}
	else {
	     cntflg=0; cntval=1; lp--;
	}

	if (!eol(rdc())) {
	     lastcom=lastc;
	}
	else {
	     if (adrflg==0) { dot=inkdot(dotinc); }
	     lp--; lastcom=defcom;
	}

	switch(lastcom&0x7f) {

	    case '/':
		itype=DSP; ptype=DSYM;
		goto trystar;

	    case '=':
		itype=NSP; ptype=0;
		goto trypr;

	    case '?':
		itype=_ISP; ptype=ISYM;
		goto trystar;

	    trystar:
		if (rdc()=='*') { lastcom |= 0x80; } else { lp--; }
		if (lastcom&0x80) {
		     itype |= STAR; ptype = (DSYM+ISYM)-ptype;
		}

	    trypr:
		longpr=0; eqcom=lastcom=='=';
		switch (rdc()) {

			case 'm':
			    {/*reset map data*/
			    short		fcount;
			    struct map		*smap;
			    union{struct map *m; long *mp;}amap;

			    if (eqcom) { error(BADEQ); }
			    smap=(itype&DSP?&datmap:&txtmap);
			    amap.m=smap; fcount=3;
			    if (itype&STAR) {
				 amap.mp += 3;
			    }
			    while (fcount-- && expr(0)) { *(amap.mp)++ = expv; }
			    if (rdc()=='?') {
				 smap->ufd=fsym;
			    }
			    else if (lastc == '/') {
				 smap->ufd=fcor;
			    }
			    else {
				 lp--;
			    }
			    }
			    break;

			case 'L':
			    longpr=-1;
			case 'l':
			    /*search for exp*/
			    if (eqcom) { error(BADEQ); }
			    dotinc=(longpr?4:2); savdot=dot;
			    expr(1); locval=expv;
			    if (expr(0)) { locmsk=expv; } else { locmsk = -1L; }
				if (!longpr) { locmsk &= 0xFFFF; locval &= 0xFFFF; }
			    for(;;) {
				 w=get(dot,itype);
				 if (errflg || mkfault || (w&locmsk)==locval) { break; }
				 dot=inkdot(dotinc);
			    }
			    if (errflg) {
				 dot=savdot; errflg=NOMATCH;
			    }
			    psymoff(dot,ptype,"");
			    break;

			case 'W':
			    longpr=-1;
			case 'w':
			    if (eqcom) { error(BADEQ); }
			    wformat[0]=lastc; expr(1);
			    do {
				 savdot=dot; psymoff(dot,ptype,":%16t"); exform(1,wformat,itype,ptype);
				 errflg=0; dot=savdot;
				 if (longpr) {
				      put(dot,itype,expv);
				 }
				 else {
				      put(dot,itype,((get(dot+2,itype)&0xFFFFL)<<16)|(expv&0xFFFFL));
				 }
				 savdot=dot;
				 _printf("=%8t"); exform(1,wformat,itype,ptype);
				 newline();
			    } while (expr(0) && errflg==0);
			    dot=savdot;
			    chkerr();
			    break;

			default:
			    lp--;
			    getformat(eqcom ? eqformat : stformat);
			    if (!eqcom) {
				 psymoff(dot,ptype,":%16t");
			    }
			    scanform(cntval,(eqcom?eqformat:stformat),itype,ptype);
		}
		break;

	    case '>':
		lastcom=0; savc=rdc();
		if (regptr=getreg(savc)) {
		     if (kcore) {
			 
			*(int *)regptr = dot;
		     }
		     else {
			 
			*(intptr_t *)(((intptr_t)&u)+regptr) = dot;
			ptrace(PT_WRITE_U, pid, regptr,
			     *(intptr_t *)(((intptr_t)&u)+regptr));
		     }
		}
		else if ((modifier=varchk(savc)) != -1) {
			var[modifier]=dot;
		}
		else {
			error(BADVAR);
		}
		break;

	    case '!':
		lastcom=0;
		shell(); break;

	    case '$':
		lastcom=0;
		printtrace(nextchar()); break;

	    case ':':
		if (!executing) {
		     executing=-1;
		     subpcs(nextchar());
		     executing=0;
		     lastcom=0;
		}
		break;

	    case 0:
		prints(DBNAME);
		break;

	    default: error(BADCOM);
	}

	flushbuf();
	} while (rdc()==';');
	if (buf) { lp=savlp; } else { lp--; }
	return(adrflg && dot!=0);
}
