#include <a.out.h>
#include <strings.h>
#include <sys/file.h>
#include <sys/signal.h>
/*#include <vax/machparam.h> defs.h*/
/*#include <vax/pcb.h> defs.h*/
/*#include <vax/pte.h> sys/file.h*/
#include "defs.h"
#include "head.h"
#include "machine.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)print.c 4.11 5/2/85";
#endif
/*
 *
 *	UNIX debugger
 *
 */
/*#include "defs.h"*/

extern char		LONGFIL[];
extern char		NOTOPEN[];
extern char		A68BAD[];
extern char		A68LNK[];
extern char		BADMOD[];

extern struct map	txtmap;
extern struct map	datmap;

extern intptr_t		lastframe;
extern intptr_t		callpc;

extern short		infile;
extern short		outfile;
extern char		*lp;
extern long		maxoff;
extern long		maxpos;
extern short		radix;

/* symbol management */
extern long		localval;

/* breakpoints */
extern struct bkpt		*bkpthead;

struct reglist reglist [] = {
	{"p1lr",	_P1LR,	&pcb.pcb_p1lr},
	{"p1br",	_P1BR,	&pcb.pcb_p1br},
	{"p0lr",	_P0LR,	&pcb.pcb_p0lr},
	{"p0br",	_P0BR,	&pcb.pcb_p0br},
	{"ksp",	_KSP,	&pcb.pcb_ksp},
	{"esp",	_ESP,	&pcb.pcb_esp},
	{"ssp",	_SSP,	&pcb.pcb_ssp},
	{"psl",	_PSL,	&pcb.pcb_psl},
	{"pc",	_PC,	&pcb.pcb_pc},
	{"usp",	_USP,	&pcb.pcb_usp},
	{"fp",	_FP,	&pcb.pcb_fp},
	{"ap",	_AP,	&pcb.pcb_ap},
	{"r11",	_R11,	&pcb.pcb_r11},
	{"r10",	_R10,	&pcb.pcb_r10},
	{"r9",	_R9,	&pcb.pcb_r9},
	{"r8",	_R8,	&pcb.pcb_r8},
	{"r7",	_R7,	&pcb.pcb_r7},
	{"r6",	_R6,	&pcb.pcb_r6},
	{"r5",	_R5,	&pcb.pcb_r5},
	{"r4",	_R4,	&pcb.pcb_r4},
	{"r3",	_R3,	&pcb.pcb_r3},
	{"r2",	_R2,	&pcb.pcb_r2},
	{"r1",	_R1,	&pcb.pcb_r1},
	{"r0",	_R0,	&pcb.pcb_r0}
};

char		lastc;

short		fcor;
char		*errflg;
short		signo;
short		sigcode;

long		dot;
long		var[];
char		*symfil;
char		*corfil;
short		pid;
long		adrval;
short		adrflg;
long		cntval;
short		cntflg;

char		*signals[] = {
		"",
		"hangup",
		"interrupt",
		"quit",
		"illegal instruction",
		"trace/BPT",
		"IOT",
		"EMT",
		"floating exception",
		"killed",
		"bus error",
		"memory fault",
		"bad system call",
		"broken pipe",
		"alarm call",
		"terminated",
		"signal 16",
		"stop (signal)",
		"stop (tty)",
		"continue (signal)",
		"child termination",
		"stop (tty input)",
		"stop (tty output)",
		"input available (signal)",
		"cpu timelimit",
		"file sizelimit",
		"signal 26",
		"signal 27",
		"signal 28",
		"signal 29",
		"signal 30",
		"signal 31",
};

/* general printing routines ($) */

void printtrace(modif) int modif; {
	short		narg, i;
	register struct bkpt	*bkptr;
	intptr_t		word;
	char		*comptr;
	intptr_t		argp, frame;
	register struct nlist *sp;
	short		stack;
	short		ntramp;

	if (cntflg==0) { cntval = -1; }

	switch (modif) {

	    case '<':
		if (cntval == 0) {
			while (readchar() != '\n') { }
			lp--;
			break;
		}
		if (rdc() == '<') {
			stack = 1;
		}
		else {
			stack = 0; lp--;
		}
		/* fall thru... */

	    case '>':
		{char		file[64];
		char		Ifile[128];
		extern char	*Ipath;
		short		index;

		index=0;
		if (rdc()!='\n') {
			do {
			    file[index++]=lastc;
			    if (index>=63) { error(LONGFIL); }
			} while (readchar()!='\n');
			file[index]=0;
			if (modif=='<') {
				if (Ipath) {
				    
					strcpy(Ifile, Ipath);
					strcat(Ifile, "/");
					strcat(Ifile, file);
				}
				if (strcmp(file, "-")!=0) {
					iclose(stack, 0);
					infile=open(file,0);
					if (infile<0) {
						infile=open(Ifile,0);
					}
				}
				else {
					lseek(infile, 0L, 0);
				}
				if (infile<0) {
					infile=0; error(NOTOPEN);
				}
				else {
					if (cntflg) {
						var[9] = cntval;
					}
					else {
						var[9] = 1;
					}
				}
			}
			else {
				oclose();
				outfile=open(file,1);
				if (outfile<0) {
					outfile=creat(file,0644);
#ifndef EDDT
				}
				else {
					lseek(outfile,0L,2);
#endif
				}
			}

		}
		else {
			if (modif == '<') {
				iclose(-1, 0);
			}
			else {
				oclose();
			}
		}
		lp--;
		}
		break;

	    case 'p':
		if (kernel == 0) {
			_printf("not debugging kernel\n");
		}
		else {
			if (adrflg) {
				int pte = _access(RD, dot, DSP, 0);
				masterpcbb = (pte&PG_PFNUM)*512;
			}
			getpcb();
		}
		break;

	    case 'd':
		if (adrflg) {
			if (adrval < 2 || adrval > 16) {
				_printf("must have 2 <= radix <= 16");
				break;
			}
			_printf("radix=%d base ten",radix=adrval);
		}
		break;

	    case 'q': case 'Q': case '%':
		done();

	    case 'w': case 'W':
		maxpos=(adrflg?adrval:MAXPOS);
		break;

	    case 's': case 'S':
		maxoff=(adrflg?adrval:MAXOFF);
		break;

	    case 'v': case 'V':
		prints("variables\n");
		for (i=0;i<=35;i++) {
			if (var[i]) {
			     printc((i<=9 ? '0' : 'a'-10) + i);
				_printf(" = %x\n",var[i]);
			}
		}
		break;

	    case 'm': case 'M':
		printmap("? map",&txtmap);
		printmap("/ map",&datmap);
		break;

	    case 0: case '?':
		if (pid) {
		     _printf("pcs id = %d\n",pid);
		}
		else {
		     prints("no process\n");
		}
		sigprint(); flushbuf();

	    case 'r': case 'R':
		printregs();
		return;

	    case 'c': case 'C':
		if (adrflg) {
			frame=adrval;
			word=get(adrval+6,DSP)&0xFFFF;
			if (word&0x2000) {
				/* 'calls', can figure out argp */
				argp=adrval+20+((word>>14)&3); word &= 0xFFF;
				while (word) {
					if (word&1) {
						argp+=4;
					}
					word>>=1;
				}
			}
			else {
				/* 'callg', can't tell where argp is */
				argp=frame;
			}
			callpc=get(frame+16,DSP);
		}
		else if (kcore) {
			argp = pcb.pcb_ap;
			frame = pcb.pcb_fp;
			callpc = pcb.pcb_pc;
		}
		else {
			argp= *(intptr_t *)(((intptr_t)&u)+_AP);
			frame= *(intptr_t *)(((intptr_t)&u)+_FP);
			callpc= *(intptr_t *)(((intptr_t)&u)+_PC);
		}
		lastframe=0;
		ntramp = 0;
		while (cntval--) {
			char *name;
			chkerr();
			/* if in extended pcb must be signal trampoline code */
			if (KERNOFF - ctob(UPAGES) < callpc &&
			    (unsigned)callpc < KERNOFF) {
				name = "sigtramp";
				ntramp++;
			}
			else {
				ntramp = 0;
				findsym(callpc,ISYM);
				if (cursym &&
				    !strcmp(cursym->n_un.n_name, "start")) {
				     break;
				}
				if (cursym) {
				     name = cursym->n_un.n_name;
				}
				else {
				     name = "?";
				}
			}
			_printf("%s(", name);
			narg = get(argp,DSP); if (narg&~0xFF) { narg=0; }
			for(;;) {
				if (narg==0) { break; }
				_printf("%R", get(argp += 4, DSP));
				if (--narg!=0) { printc(','); }
			}
			if (ntramp == 1) {
			     callpc=get(frame+92, DSP);
			}
			else {
			     callpc=get(frame+16, DSP);
			}
			if (callpc != 0) {
				prints(") from ");
				psymoff(callpc, ISYM, "\n");
			}
			else {
				prints(")\n");
			}

			if (modif=='C') {
				while (localsym(frame,argp)) {
					word=get(localval,DSP);
					_printf("%8t%s:%10t",
					    cursym->n_un.n_name);
					if (errflg) {
					     prints("?\n"); errflg=0;
					}
					else {
					     _printf("%R\n",word);
					}
				}
			}

			argp=get(frame+8, DSP);
			lastframe=frame;
			frame=get(frame+12, DSP)&-2;
			if (frame==0) { break; }
			if (!adrflg && !INSTACK(frame)) {
				if (!kcore || !kstackaddr(frame)) {
				     break;
				}
			}
		}
		break;

	    /*print externals*/
	    case 'e': case 'E':
		for (sp = symtab; sp < esymtab; sp++) {
			if (sp->n_type == (N_DATA|N_EXT) ||
			   sp->n_type == (N_BSS|N_EXT)) {
			     _printf("%s:%12t%R\n", sp->n_un.n_name,
				    get(sp->n_value,DSP));
			}
		}
		break;

	    case 'a': case 'A':
		error("No algol 68 on VAX");
		/*NOTREACHED*/

	    /*print breakpoints*/
	    case 'b': case 'B':
		_printf("breakpoints\ncount%8tbkpt%24tcommand\n");
		for (bkptr=bkpthead; bkptr; bkptr=bkptr->nxtbkpt) {
			if (bkptr->flag) {
				_printf("%-8.8d",bkptr->count);
				psymoff((long)(unsigned)bkptr->loc,ISYM,"%24t");
				comptr=bkptr->comm;
				while (*comptr) { printc(*comptr++); }
			}
		}
		break;

	    default:
		error(BADMOD);
	}

}

void printmap(s, amap) char *s; struct map *amap; {
	int file;
	file=amap->ufd;
	_printf("%s%12t`%s'\n", s,
	    (file<0 ? "-" : (file==fcor ? corfil : symfil)));
	_printf("b1 = %-16R",amap->b1);
	_printf("e1 = %-16R",amap->e1);
	_printf("f1 = %-16R",amap->f1);
	_printf("\nb2 = %-16R",amap->b2);
	_printf("e2 = %-16R",amap->e2);
	_printf("f2 = %-16R",amap->f2);
	printc('\n');
}

void printregs() {
	register struct reglist	*p;
	long		v;

	for (p=reglist; p < &reglist[24]; p++) {
		v = kcore ? *p->rkern : *(intptr_t *)(((intptr_t)&u)+p->roffs);
		_printf("%s%6t%R %16t", p->rname, v);
		valpr(v,(p->roffs==_PC?ISYM:DSYM));
		printc('\n');
	}
	printpc();
}

int getreg(regnam) int regnam; {
	register struct reglist	*p;
	register char	*regptr;
	char	*olp;
	char		regnxt;

	olp=lp;
	for (p=reglist; p < &reglist[24]; p++) {
		regptr=p->rname;
		if ((regnam == *regptr++)) {
		    
			while (*regptr) {
			   if ((regnxt=readchar()) != *regptr++) {
				     --regptr; break;
				}
			}
			if (*regptr) {
			     lp=olp;
			}
			else {
			    
				int i = kcore ? (int)p->rkern : p->roffs;
				return (i);
			}
		}
	}
	lp=olp;
	return(0);
}

void printpc() {
	dot= *(intptr_t *)(((intptr_t)&u)+_PC);
	psymoff(dot,ISYM,":%16t"); printins(0,_ISP,chkget(dot,_ISP));
	printc('\n');
}

char	*illinames[] = {
	"reserved addressing fault",
	"priviliged instruction fault",
	"reserved operand fault"
};
char	*fpenames[] = {
	0,
	"integer overflow trap",
	"integer divide by zero trap",
	"floating overflow trap",
	"floating/decimal divide by zero trap",
	"floating underflow trap",
	"decimal overflow trap",
	"subscript out of range trap",
	"floating overflow fault",
	"floating divide by zero fault",
	"floating undeflow fault"
};

void sigprint() {
	if ((signo>=0) && (signo<sizeof signals/sizeof signals[0])) { prints(signals[signo]); }
	switch (signo) {

	case SIGFPE:
		if ((sigcode > 0 &&
		    sigcode < sizeof fpenames / sizeof fpenames[0])) {
			
			prints(" ("); prints(fpenames[sigcode]); prints(")");
		}
		break;

	case SIGILL:
		if ((sigcode >= 0 &&
		    sigcode < sizeof illinames / sizeof illinames[0])) {
			
			prints(" ("); prints(illinames[sigcode]); prints(")");
		}
		break;
	}
}
