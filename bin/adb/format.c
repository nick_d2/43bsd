#include <gen.h>
/*#include <sys/exec.h> gen.h*/
/*#include <sys/proc.h> gen.h*/
/*#include <sys/signal.h> gen.h*/
#include <sys/wait.h>
#include "defs.h"
#include "head.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)format.c	4.3 2/27/86";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include "defs.h"*/

extern char		BADMOD[];
extern char		NOFORK[];
extern char		ADWRAP[];

extern short		mkfault;
extern char		*lp;
extern long		maxoff;
extern void		(*sigint) __P((int sig));
extern void		(*sigqit) __P((int sig));
extern char		*errflg;
extern char		lastc,peekc;
extern long		dot;
extern short		dotinc;
extern long		expv;
extern long		var[];

char		*fphack;
int rdfp() {
	return(lastc= *fphack++);
}

void scanform(icount, ifp, itype, ptype) long icount; char *ifp; int itype; int ptype; {
	char		*fp;
	char		modifier;
	short		fcount, init=1;
	long		savdot;
	bool exact;

	while (icount) {
	    fp=ifp;
	    savdot=dot; init=0;

	    if (init==0 && (exact=(findsym(dot,ptype)==0)) && maxoff) {
		 _printf("\n%s:%16t",cursym->n_un.n_name);
	    }

	    /*now loop over format*/
	    while (*fp && errflg==0) {
		if (digit(modifier = *fp)) {
		     fcount = 0;
			while (digit(modifier = *fp++)) {
			   fcount *= 10;
			   fcount += modifier-'0';
			}
			fp--;
			if (fcount==0) { fcount = 1; }
		}
		else {
		     fcount = 1;
		}

		if (*fp==0) { break; }
		if (exact && dot==savdot && itype==_ISP && cursym->n_un.n_name[0]=='_' && *fp=='i') {
		     exform(1,"x",itype,ptype); fp++; printc('\n'); /* entry mask */
		}
		else {
		     fp=exform(fcount,fp,itype,ptype);
		}
	    }
	    dotinc=dot-savdot;
	    dot=savdot;

	    if (errflg) {
		 if (icount<0) {
		      errflg=0; break;
		 }
		 else {
		      error(errflg);
		 }
	    }
	    if (--icount) {
		 dot=inkdot(dotinc);
	    }
	    if (mkfault) { error(0); }
	}
}

char *exform(fcount, ifp, itype, ptype) int fcount; char *ifp; int itype; int ptype; {
	/* execute single format item `fcount' times
	 * sets `dotinc' and moves `dot'
	 * returns address of next format item
	 */
	unsigned		w;
	long		savdot, wx;
	char		*fp;
	char		c, modifier, longpr;
	union {
		double		fw;
		struct {
			long	sa;
			short	sb,sc;
		} fw_st;
	} fw_un;

	while (fcount>0) {
		fp = ifp; c = *fp;
		longpr=(c>='A')&(c<='Z')|(c=='f')|(c=='4')|(c=='p');
		if (itype==NSP || *fp=='a') {
		     wx=dot; w=dot;
		}
		else {
		     w=get(dot,itype);
		     if (longpr) {
			  wx=((get(inkdot(2),itype)&0xFFFFL)<<16)|(w&0xFFFFL);
		     }
		     else {
			  wx=w;
		     }
		}
		if (c=='F') {
		     fw_un.fw_st.sb=get(inkdot(4),itype);
		     fw_un.fw_st.sc=get(inkdot(6),itype);
		}
		if (errflg) { return(fp); }
		if (mkfault) { error(0); }
		var[0]=wx;
		modifier = *fp++;
		dotinc=(longpr?4:2);;

		if (charpos()==0 && modifier!='a') { _printf("%16m"); }

		switch(modifier) {

		    case ' ': case '\t':
			break;

		    case 't': case 'T':
			_printf("%T",fcount); return(fp);

		    case 'r': case 'R':
			_printf("%M",fcount); return(fp);

		    case 'a':
			psymoff(dot,ptype,":%16t"); dotinc=0; break;

		    case 'p':
			psymoff(var[0],ptype,"%16t"); break;

		    case 'u':
			_printf("%-8u",w); break;

		    case 'U':
			_printf("%-16U",wx); break;

		    case 'c': case 'C':
			if (modifier=='C') {
			     printesc(w&0xff);
			}
			else {
			     printc(w&0xff);
			}
			dotinc=1; break;

		    case 'b': case 'B':
			_printf("%-8o", w&0xff); dotinc=1; break;

			case '1':
			_printf("%-8r", w&0xff); dotinc=1; break;

			case '2':
		    case 'w':
			_printf("%-8r", w); break;

			case '4':
		    case 'W':
			_printf("%-16R", wx); break;

		    case 's': case 'S':
			savdot=dot; dotinc=1;
			while ((c=get(dot,itype)&0xff) && errflg==0) {
			   dot=inkdot(1);
			   if (modifier == 'S') {
				printesc(c);
			   }
			   else {
				printc(c);
			   }
			   endline();
			}
			dotinc=dot-savdot+1; dot=savdot; break;

		    case 'x':
			_printf("%-8x",w); break;

		    case 'X':
			_printf("%-16X", wx); break;

		    case 'Y':
			_printf("%-24Y", wx); break;

		    case 'q':
			_printf("%-8q", w); break;

		    case 'Q':
			_printf("%-16Q", wx); break;

		    case 'o':
			_printf("%-8o", w); break;

		    case 'O':
			_printf("%-16O", wx); break;

		    case 'i':
			printins(0,itype,w); printc('\n'); break;

		    case 'd':
			_printf("%-8d", w); break;

		    case 'D':
			_printf("%-16D", wx); break;

		    case 'f':
			fw_un.fw = 0;
			fw_un.fw_st.sa = wx;
			if ((wx & ~0xFFFF00FF) == 0x8000) {
			     _printf("(reserved oprnd)");
			}
			else {
			     _printf("%-16.9f", fw_un.fw);
			}
			dotinc=4; break;

		    case 'F':
			fw_un.fw_st.sa = wx;
			if ((wx & ~0xFFFF00FF) == 0x8000) {
			     _printf("%-32s", "(reserved oprnd)");
			}
			else {
			     _printf("%-32.18F", fw_un.fw);
			}
			dotinc=8; break;

		    case 'n': case 'N':
			printc('\n'); dotinc=0; break;

		    case '"':
			dotinc=0;
			while (*fp != '"' && *fp) { printc(*fp++); }
			if (*fp) { fp++; }
			break;

		    case '^':
			dot=inkdot(-dotinc*fcount); return(fp);

		    case '+':
			dot=inkdot(fcount); return(fp);

		    case '-':
			dot=inkdot(-fcount); return(fp);

		    default: error(BADMOD);
		}
		if (itype!=NSP) {
			dot=inkdot(dotinc);
		}
		fcount--; endline();
	}

	return(fp);
}

void shell() {
#ifndef EDDT
	/*short*/int		rc, status, unixpid;
	char		*argp = lp;
	char		*getenv(), *shell = getenv("SHELL");
#ifdef VFORK
	char		oldstlp;
#endif

	if (shell == 0)
		shell = "/bin/sh";
	while (lastc!='\n') { rdc(); }
#ifndef VFORK
	if ((unixpid=fork())==0)
#else
	oldstlp = *lp;
	if ((unixpid=vfork())==0)
#endif
	{
		signal(SIGINT,sigint); signal(SIGQUIT,sigqit);
		*lp=0; execl(shell, "sh", "-c", argp, 0);
		_exit(16);
	}
#ifndef VFORK
	else if (unixpid == -1)
#else
	else if (*lp = oldstlp, unixpid == -1)
#endif
	{
		error(NOFORK);
	}
	else {
		signal(SIGINT,SIG_IGN);
		while ((rc = wait(&status)) != unixpid && rc != -1);
		signal(SIGINT,sigint);
		prints("!"); lp--;
	}
#endif
}

void printesc(c) int c; {
	c &= 0x7f;
	if (c==0177) {
	     _printf("^?");
	}
	else if (c<' ') {
	     _printf("^%c", c + '@');
	}
	else {
	     printc(c);
	}
}

long inkdot(incr) int incr; {
	long		newdot;

	newdot=dot+incr;
	if ((dot ^ newdot) >> 24) { error(ADWRAP); }
	return(newdot);
}

int digit(c) int c; {
	return c >= '0' && c <= '9';
}
