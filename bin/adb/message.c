#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)message.c	4.3 8/11/83";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include	"mode.h"*/

char		VERSION[] =	"\nVERSION VM/VAX4.3	DATE 8/11/83\n";

char		BADMOD[] =	"bad modifier";
char		BADCOM[] =	"bad command";
char		BADSYM[] =	"symbol not found";
char		BADLOC[] =	"automatic variable not found";
char		NOCFN[] =	"c routine not found";
char		NOMATCH[] =	"cannot locate value";
char		NOBKPT[] =	"no breakpoint set";
char		BADKET[] =	"unexpected ')'";
char		NOADR[] =	"address expected";
char		NOPCS[] =	"no process";
char		BADVAR[] =	"bad variable";
char		EXBKPT[] =	"too many breakpoints";
char		A68BAD[] =	"bad a68 frame";
char		A68LNK[] =	"bad a68 link";
char		ADWRAP[] =	"address wrap around";
char		BADEQ[] =	"unexpected `='";
char		BADWAIT[] =	"wait error: process disappeared!";
char		ENDPCS[] =	"process terminated";
char		NOFORK[] =	"try again";
char		BADSYN[] =	"syntax error";
char		NOEOR[] =	"newline expected";
char		SZBKPT[] =	"bkpt: command too long";
char		BADFIL[] =	"bad file format";
char		BADNAM[] =	"not enough space for symbols";
char		LONGFIL[] =	"filename too long";
char		NOTOPEN[] =	"cannot open";
char		BADMAG[] =	"bad core magic number";
char		TOODEEP[] =	"$<< nesting too deep";
