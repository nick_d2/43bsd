#include <stdio.h>
/*#include <sys/errno.h> sys/file.h*/
#include <sys/exec.h>
#include <sys/file.h>
#include <sys/proc.h>
/*#include <sys/ptrace.h> defs.h*/
/*#include <sys/signal.h> sys/user.h*/
#include <sys/user.h>
#include <sys/wait.h>
#include "defs.h"
#include "head.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)runpcs.c	4.6 4/25/85";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include "defs.h"*/

extern struct map	txtmap;

extern char		NOFORK[];
extern char		ENDPCS[];
extern char		BADWAIT[];

extern char		*lp;
extern void		(*sigint) __P((int sig));
extern void		(*sigqit) __P((int sig));

/* breakpoints */
extern struct bkpt	*bkpthead;

extern struct reglist		reglist[];

extern char		lastc;

extern short		fcor;
extern short		fsym;
extern char		*errflg;
extern int		errno;
extern short		signo;
extern short		sigcode;

extern long		dot;
extern char		*symfil;
extern short		wtflag;
extern long		pid;
extern long		expv;
extern short		adrflg;
extern long		loopcnt;

/* service routines for sub process control */

int getsig(sig) int sig; {	return(expr(0) ? expv : sig);
}

intptr_t userpc = 1;

int runpcs(runmode, execsig) int runmode; int execsig; {
	short		rc;
	register struct bkpt	*bkpt;
	if (adrflg) { userpc=dot; }
	_printf("%s: running\n", symfil);

	while (--loopcnt>=0) {
	  
#ifdef DEBUG
		_printf("\ncontinue %x %d\n",userpc,execsig);
#endif
		if (runmode==SINGLE) {
		     delbp(); /* hardware handles single-stepping */
		}
		else {
		     /* continuing from a breakpoint is hard */
			if (bkpt=scanbkpt(userpc)) {
			     execbkpt(bkpt,execsig); execsig=0;
			}
			setbp();
		}
		ptrace(runmode,pid,userpc,execsig);
		bpwait(); chkerr(); execsig=0; delbp(); readregs();

		if ((signo==0) && (bkpt=scanbkpt(userpc))) {
		     /* stopped by BPT instruction */
#ifdef DEBUG
			_printf("\n BPT code; '%s'%o'%o'%d",
				bkpt->comm,bkpt->comm[0],'\n',bkpt->flag);
#endif
			dot=bkpt->loc;
			if (bkpt->flag==BKPTEXEC
			|| ((bkpt->flag=BKPTEXEC)
				&& bkpt->comm[0]!='\n'
				&& command(bkpt->comm,':')
				&& --bkpt->count)) {
			     execbkpt(bkpt,execsig); execsig=0; loopcnt++;
			}
			else {
			     bkpt->count=bkpt->initcnt; rc=1;
			}
		}
		else {
		     execsig=signo; rc=0;
		}
	}
	return(rc);
}

#define BPOUT 0
#define BPIN 1
short bpstate = BPOUT;

void endpcs() {
	register struct bkpt	*bkptr;
	if (pid) {
	     ptrace(PT_KILL,pid,0,0); pid=0; userpc=1;
	     for (bkptr=bkpthead; bkptr; bkptr=bkptr->nxtbkpt) {
		if (bkptr->flag) {
		     bkptr->flag=BKPTSET;
		}
	     }
	}
	bpstate=BPOUT;
}

#ifdef VFORK
void nullsig() {

}
#endif

void setup() {
	close(fsym); fsym = -1;
#ifndef VFORK
	if ((pid = fork()) == 0)
#else
	if ((pid = vfork()) == 0)
#endif
	{
	     ptrace(PT_TRACE_ME,0,0,0);
#ifdef VFORK
	     signal(SIGTRAP,(int (*) __P((int sig)))nullsig);
#endif
	     signal(SIGINT,sigint); signal(SIGQUIT,sigqit);
	     doexec(); exit(0);
	}
	else if (pid == -1) {
	     error(NOFORK);
	}
	else {
	     bpwait(); readregs(); lp[0]='\n'; lp[1]=0;
	     fsym=open(symfil,wtflag);
	     if (errflg) {
		  _printf("%s: cannot execute\n",symfil);
		  endpcs(); error(0);
	     }
	}
	bpstate=BPOUT;
}

void execbkpt(bkptr, execsig) struct bkpt *bkptr; int execsig; {
#ifdef DEBUG
	_printf("exbkpt: %d\n",bkptr->count);
#endif
	delbp();
	ptrace(PT_STEP,pid,bkptr->loc,execsig);
	bkptr->flag=BKPTSET;
	bpwait(); chkerr(); readregs();
}

void doexec() {
	char		*argl[MAXARG];
	char		args[LINSIZ];
	char		*p, **ap, *filnam;
	extern char *environ;
	ap=argl; p=args;
	*ap++=symfil;
	do {
		if (rdc()=='\n') { break; }
		*ap = p;
		/*
		 * First thing is to look for direction characters
		 * and get filename.  Do not use up the args for filenames.
		 * Then get rid of spaces before next args.
		 */
		if (lastc=='<') {
			do { readchar(); } while (lastc==' ' || lastc=='\t');
			filnam = p;
			while (lastc!='\n' && lastc!=' ' && lastc!='\t' && lastc!='>') { *p++=lastc; readchar(); }
			*p = 0;
			close(0);
			if (open(filnam,0)<0) {
				_printf("%s: cannot open\n",filnam); _exit(0);
			}
			p = *ap;
		}
		else if (lastc=='>') {
			do { readchar(); } while (lastc==' ' || lastc=='\t');
			filnam = p;
			while (lastc!='\n' && lastc!=' ' && lastc!='\t' && lastc!='<') { *p++=lastc; readchar(); }
			*p = '\0';
			close(1);
			if (creat(filnam,0666)<0) {
				_printf("%s: cannot create\n",filnam); _exit(0);
			}
			p = *ap;
		}
		else {
			
			while (lastc!='\n' && lastc!=' ' && lastc!='\t' && lastc!='>' && lastc!='<') { *p++=lastc; readchar(); }
			*p++ = '\0';
			ap++;
		}
	} while (lastc!='\n');
	*ap++=0;
	exect(symfil, argl, environ);
	perror(symfil);
}

struct bkpt *scanbkpt(adr) intptr_t adr; {
	register struct bkpt	*bkptr;
	for (bkptr=bkpthead; bkptr; bkptr=bkptr->nxtbkpt) {
	   if (bkptr->flag && bkptr->loc==adr) {
		break;
	   }
	}
	return(bkptr);
}

void delbp() {
	register intptr_t	a;
	register struct bkpt	*bkptr;
	if (bpstate!=BPOUT) {
	    
		for (bkptr=bkpthead; bkptr; bkptr=bkptr->nxtbkpt) {
			if (bkptr->flag) {
			     a=bkptr->loc;
				if (a < txtmap.e1) {
				    
					ptrace(PT_WRITE_I,pid,a,bkptr->ins);
				}
				else {
				    
					ptrace(PT_WRITE_D,pid,a,bkptr->ins);
				}
			}
		}
		bpstate=BPOUT;
	}
}

#ifdef vax
#define	SETBP(ins)	(BPT | ((ins) &~ 0xFF))
#endif

void setbp() {
	register intptr_t		a;
	register struct bkpt	*bkptr;

	if (bpstate!=BPIN) {
	    
		for (bkptr=bkpthead; bkptr; bkptr=bkptr->nxtbkpt) {
		   if (bkptr->flag) {
			a = bkptr->loc;
			if (a < txtmap.e1) {
			    
				bkptr->ins = ptrace(PT_READ_I, pid, a, 0);
				ptrace(PT_WRITE_I, pid, a, SETBP(bkptr->ins));
			}
			else {
			    
				bkptr->ins = ptrace(PT_READ_D, pid, a, 0);
				ptrace(PT_WRITE_D, pid, a, SETBP(bkptr->ins));
			}
			if (errno) {
			     prints("cannot set breakpoint: ");
			     psymoff(bkptr->loc,ISYM,"\n");
			}
		   }
		}
		bpstate=BPIN;
	}
}

void bpwait() {
	register int/*ptr_t*/ w;
	int/*ptr_t*/ stat;

	signal(SIGINT, SIG_IGN);
	while ((w = wait(&stat))!=pid && w != -1);
	signal(SIGINT,sigint);
	if (w == -1) {
	     pid=0;
	     errflg=BADWAIT;
	}
	else if ((stat & 0177) != 0177) {
	     sigcode = 0;
	     if (signo = stat&0177) {
		  sigprint();
	     }
	     if (stat&0200) {
		  prints(" - core dumped");
		  close(fcor);
		  setcor();
	     }
	     pid=0;
	     errflg=ENDPCS;
	}
	else {
	     signo = stat>>8;
	     sigcode = ptrace(PT_READ_U, pid, &((struct user *)0)->u_code, 0);
	     if (signo!=SIGTRAP) {
		  sigprint();
	     }
	     else {
		  signo=0;
	     }
	     flushbuf();
	}
}

void readregs() {
	/*get register values from pcs*/
	register i;
	for (i=24; --i>=0;) {
	   *(intptr_t *)(((intptr_t)&u)+reglist[i].roffs) =
		    ptrace(PT_READ_U, pid, reglist[i].roffs, 0);
	}
 	userpc= *(intptr_t *)(((intptr_t)&u)+_PC);
}
