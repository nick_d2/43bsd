#include <sys/proc.h>
#include "defs.h"
#include "head.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)pcs.c	4.2 8/11/83";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include "defs.h"*/

extern char		NOBKPT[];
extern char		SZBKPT[];
extern char		EXBKPT[];
extern char		NOPCS[];
extern char		BADMOD[];

/* breakpoints */
struct bkpt		*bkpthead;

extern char		*lp;
extern char		lastc;

extern short		signo;
extern long		dot;
extern short		pid;
extern long		cntval;
long		loopcnt;

long		entrypt;
extern short		adrflg;

/* sub process control */

void subpcs(modif) int modif; {
	register short		check;
	short		execsig,runmode;
	register struct bkpt	*bkptr;
	char		*comptr;
	execsig=0; loopcnt=cntval;

	switch (modif) {

	    /* delete breakpoint */
	    case 'd': case 'D':
		if ((bkptr=scanbkpt(dot))) {
		     bkptr->flag=0; return;
		}
		else {
		     error(NOBKPT);
		}

	    /* set breakpoint */
	    case 'b': case 'B':
		if ((bkptr=scanbkpt(dot))) {
		     bkptr->flag=0;
		}
		for (bkptr=bkpthead; bkptr; bkptr=bkptr->nxtbkpt) {
		   if (bkptr->flag == 0) {
			break;
		   }
		}
		if (bkptr==0) {
		     if ((bkptr=sbrk(sizeof *bkptr)) == -1) {
			  error(SZBKPT);
		     }
		     else {
			  bkptr->nxtbkpt=bkpthead;
			  bkpthead=bkptr;
		     }
		}
		bkptr->loc = dot;
		bkptr->initcnt = bkptr->count = cntval;
		bkptr->flag = BKPTSET;
		check=MAXCOM-1; comptr=bkptr->comm; rdc(); lp--;
		do {
		    *comptr++ = readchar();
		} while (check-- && lastc!='\n');
		*comptr=0; lp--;
		if (check) {
		     return;
		}
		else {
		     error(EXBKPT);
		}

	    /* exit */
	    case 'k' :case 'K':
		if (pid) {
		     _printf("%d: killed", pid); endpcs(); return;
		}
		error(NOPCS);

	    /* run program */
	    case 'r': case 'R':
		endpcs();
		setup(); runmode=CONTIN;
		if (adrflg) {
		     if (!scanbkpt(dot)) { loopcnt++; }
		}
		else {
		     if (!scanbkpt(entrypt+2)) { loopcnt++; }
		}
		break;

	    /* single step */
	    case 's': case 'S':
		if (pid) {
		    
			runmode=SINGLE; execsig=getsig(signo);
		}
		else {
		     setup(); loopcnt--;
		}
		break;

	    /* continue with optional signal */
	    case 'c': case 'C': case 0:
		if (pid==0) { error(NOPCS); }
		runmode=CONTIN; execsig=getsig(signo);
		break;

	    default: error(BADMOD);
	}

	if (loopcnt>0 && runpcs(runmode,execsig)) {
	     _printf("breakpoint%16t");
	}
	else {
	     _printf("stopped at%16t");
	}
	delbp();
	printpc();
}
