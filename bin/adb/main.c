#include <a.out.h>
#include <setjmp.h>
#include <stdio.h>
/*#include <sys/exec.h> a.out.h*/
#include <sys/file.h>
#include <sys/signal.h>
/*#include <vax/pcb.h> defs.h*/
/*#include <vax/pte.h> sys/file.h*/
#include "defs.h"
#include "head.h"
#include "machine.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)main.c 4.3 4/1/82";
#endif
/*
 * adb - main command loop and error/interrupt handling
 */
/*#include <setjmp.h>*/
/*#include "defs.h"*/

extern char		NOEOR[];

extern short		mkfault;
extern short		executing;
extern short		infile;
extern char		*lp;
extern long		maxoff;
extern long		maxpos;
void		(*sigint) __P((int sig));
void		(*sigqit) __P((int sig));
extern short		wtflag;
extern long		maxfile;
extern char		*errflg;
long		exitflg;

extern char		lastc;
extern short		eof;

extern short		lastcom;

long	maxoff = MAXOFF;
long	maxpos = MAXPOS;
char	*Ipath = "/usr/lib/adb";

/* from defs.h */
struct	pcb	pcb;
int	kernel;
int	kcore;
struct	pte *sbr;
int	slr;
int	masterpcbb;

/* from head.h */
struct	nlist *symtab, *esymtab;
struct	nlist *cursym;
struct	exec filhdr;
int	xargc;
union	udot_un udot;

/* use this instead of original setexit() / reset() */
jmp_buf reset_buf;

int main(argc, argv) int argc; register char **argv; {

	mkioptab();
another:
	if (argc>1) {
		if (eqstr("-w", argv[1])) {
			wtflag = 2;		/* suitable for open() */
			argc--, argv++;
			goto another;
		}
		if (eqstr("-k", argv[1])) {
			kernel = 1;
			argc--, argv++;
			goto another;
		}
		if (argv[1][0] == '-' && argv[1][1] == 'I') {
			Ipath = argv[1]+2;
			argc--, argv++;
		}
	}
	if (argc > 1)
		symfil = argv[1];
	if (argc > 2)
		corfil = argv[2];
	xargc = argc;
	setsym(); setcor(); setvar();

	if ((sigint=signal(SIGINT,SIG_IGN)) != SIG_IGN) {
		sigint = fault;
		signal(SIGINT, fault);
	}
	sigqit = signal(SIGQUIT, SIG_IGN);
	/*setexit();*/ setjmp(reset_buf);
	if (executing)
		delbp();
	executing = 0;
	for (;;) {
		flushbuf();
		if (errflg) {
			_printf("%s\n", errflg);
			exitflg = errflg;
			errflg = 0;
		}
		if (mkfault) {
			mkfault=0;
			printc('\n');
			prints(DBNAME);
		}
		lp=0; rdc(); lp--;
		if (eof) {
			if (infile) {
				iclose(-1, 0); eof=0; /*reset();*/ longjmp(reset_buf, 1);
			} else
				done();
		} else
			exitflg = 0;
		command(0, lastcom);
		if (lp && lastc!='\n')
			error(NOEOR);
	}
}

int done() {
	endpcs();
	exit(exitflg);
}

long round(a, b) register long a; register long b; {
	register long w;
	w = (a/b)*b;
	if (a!=w) { w += b; }
	return(w);
}

/*
 * If there has been an error or a fault, take the error.
 */
void chkerr() {
	if (errflg || mkfault)
		error(errflg);
}

/*
 * An error occurred; save the message for later printing,
 * close open files, and reset to main command loop.
 */
void error(n) char *n; {
	errflg = n;
	iclose(0, 1); oclose();
	/*reset();*/ longjmp(reset_buf, 1);
}

/*
 * An interrupt occurred; reset the interrupt
 * catch, seek to the end of the current file
 * and remember that there was a fault.
 */
void fault(a) int a; {
	signal(a, fault);
	lseek(infile, 0L, 2);
	mkfault++;
}
