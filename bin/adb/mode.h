#ifndef _MODE_H_
#define _MODE_H_

/*	mode.h	4.2	81/05/14	*/

/*#include "machine.h"*/
/*
 * sdb/adb - common definitions for old srb style code
 */

#define MAXCOM	64
#define MAXARG	32
#define LINSIZ	512

#ifdef __STDC__
#include <stdint.h>
#else
typedef long intptr_t;
#endif
typedef char bool;

/* file address maps */
struct map {
	long	b1;
	long	e1;
	long	f1;
	long	b2;
	long	e2;
	long	f2;
	short	ufd;
};

struct bkpt {
	intptr_t	loc;
	intptr_t	ins;
	short	count;
	short	initcnt;
	short	flag;
	char	comm[MAXCOM];
	struct bkpt	*nxtbkpt;
};

struct reglist {
	char	*rname;
	short	roffs;
	int	*rkern;
};

#endif
