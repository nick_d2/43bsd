#ifndef _HEAD_H_
#define _HEAD_H_

#include <a.out.h>
/*#include <sys/exec.h> a.out.h*/
/*#include <sys/types.h> sys/user.h*/
#include <sys/user.h>
#include <vax/frame.h>
/*#include <vax/machparam.h> sys/user.h*/
#include "mode.h"

/*	head.h	4.1	81/05/14	*/

extern long	maxoff;
extern long	localval;

extern struct	nlist *symtab, *esymtab;
extern struct	nlist *cursym;
/*extern struct	nlist *lookup();*/

extern struct	exec filhdr;

extern long	var[36];

extern int	xargc;

extern struct map	txtmap;
extern struct map	datmap;
extern short	wtflag;
extern short	fcor;
extern short	fsym;
extern long	maxfile;
extern long	maxstor;
extern short	signo;

extern union udot_un {
	struct	user U;
	char	UU[ctob(UPAGES)];
} udot;
#define	u	udot.U

extern char	*corfil, *symfil;

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* access.c */
void put __P((off_t addr, int space, int value));
u_int get __P((off_t addr, int space));
u_int chkget __P((off_t addr, int space));
u_int bchkget __P((off_t addr, int space));
int _access __P((int mode, off_t addr, int space, int value));
int vtophys __P((off_t addr));
void rwerr __P((int space));
int physrw __P((int file, off_t addr, int *aw, int rd));
int chkmap __P((register long *addr, int space));
int within __P((u_int addr, u_int lbd, u_int ubd));
int longseek __P((int f, off_t a));

/* command.c */
int command __P((char *buf, int defcom));

/* expr.c */
int expr __P((int a));
int term __P((int a));
int item __P((int a));
int getnum __P((int (*rdf)(void)));
void readsym __P((void));
int convdig __P((int c));
int symchar __P((int dig));
int varchk __P((int name));
void chkloc __P((long frame));
int eqsym __P((register char *s1, register char *s2, int c));

/* format.c */
int rdfp __P((void));
void scanform __P((long icount, char *ifp, int itype, int ptype));
char *exform __P((int fcount, char *ifp, int itype, int ptype));
void shell __P((void));
void printesc __P((int c));
long inkdot __P((int incr));
int digit __P((int c));

/* input.c */
int eol __P((int c));
int rdc __P((void));
int readchar __P((void));
int nextchar __P((void));
int quotchar __P((void));
void getformat __P((char *deformat));

/* main.c */
int main __P((int argc, register char **argv));
int done __P((void));
long round __P((register long a, register long b));
void chkerr __P((void));
void error __P((char *n));
void fault __P((int a));

/* opset.c */
int mapescbyte __P((int byte));
void mkioptab __P((void));
void printins __P((int fmt, int Idsp, int ins));
void casebody __P((long base, long limit));
long inkdot __P((int incr));
void printc __P((int c));
void psymoff __P((long v, int regnumber, char *fmt));
int prdiff __P((int diff));

/* output.c */
int eqstr __P((register char *s1, register char *s2));
int length __P((register char *s));
void printc __P((int c));
int charpos __P((void));
void flushbuf __P((void));
void _printf __P((char *fmat, ...));
void printdate __P((long tvec));
void prints __P((char *s));
void newline __P((void));
int convert __P((register char **cp));
void printnum __P((int n, int fmat, int base));
void printoct __P((long o, int s));
#ifndef vax
void printdbl __P((short lx, short ly, char fmat, int base));
#else
void printdbl __P((long lxy, char fmat, int base));
#endif
void iclose __P((int stack, int err));
void oclose __P((void));
void endline __P((void));

/* pcs.c */
void subpcs __P((int modif));

/* print.c */
void printtrace __P((int modif));
void printmap __P((char *s, struct map *amap));
void printregs __P((void));
int getreg __P((int regnam));
void printpc __P((void));
void sigprint __P((void));

/* runpcs.c */
int getsig __P((int sig));
int runpcs __P((int runmode, int execsig));
void endpcs __P((void));
int nullsig __P((void));
void setup __P((void));
void execbkpt __P((struct bkpt *bkptr, int execsig));
void doexec __P((void));
struct bkpt *scanbkpt __P((intptr_t adr));
void delbp __P((void));
void setbp __P((void));
void bpwait __P((void));
void readregs __P((void));

/* setup.c */
void setsym __P((void));
void setcor __P((void));
void getpcb __P((void));
void findstackframe __P((void));
struct frame *checkintstack __P((int fcor));
struct frame *getframe __P((int fcor, caddr_t fp));
int checkframe __P((register struct frame *fp));
int kstackaddr __P((caddr_t addr));
int create __P((char *f));
int getfile __P((char *filnam, int cnt));
void setvar __P((void));

/* sym.c */
struct nlist *lookup __P((char *symstr));
int findsym __P((long val, int type));
int localsym __P((intptr_t cframe, intptr_t cargp));
void psymoff __P((long v, int type, char *s));
void valpr __P((long v, int idsp));

#endif
