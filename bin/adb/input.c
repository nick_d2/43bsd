#include <sys/file.h>
#include "head.h"
#include "mode.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)input.c	4.2 8/11/83";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include "defs.h"*/

extern short		mkfault;
char		line[LINSIZ];
short		infile;
extern char		*lp;
char		peekc,lastc = '\n';
short		eof;

/* input routines */

int eol(c) int c; {
	return(c=='\n' || c==';');
}

int rdc() {
	do {
		readchar();
	} while (lastc==' ' || lastc=='\t');
	return(lastc);
}

int readchar() {
	if (eof) {
		lastc=0;
	}
	else {
		if (lp==0) {
			lp=line;
			do {
			    eof = read(infile,lp,1)==0;
			    if (mkfault) { error(0); }
			} while (eof==0 && *lp++!='\n');
			*lp=0; lp=line;
		}
		if (lastc = peekc) {
		     peekc=0;
		}
		else if (lastc = *lp) {
		     lp++;
		}
	}
	return(lastc);
}

int nextchar() {
	if (eol(rdc())) {
	     lp--; return(0);
	}
	else {
	     return(lastc);
	}
}

int quotchar() {
	if (readchar()=='\\') {
		return(readchar());
	}
	else if (lastc=='\'') {
		return(0);
	}
	else {
		return(lastc);
	}
}

void getformat(deformat) char *deformat; {
	register char	*fptr;
	register bool	quote;
	fptr=deformat; quote=0;
	while ((quote ? readchar()!='\n' : !eol(readchar()))) {
	    if ((*fptr++ = lastc)=='"') {
		 quote = ~quote;
	    }
	}
	lp--;
	if (fptr!=deformat) { *fptr++ = '\0'; }
}
