s/@/ATSIGN/g
s/^/@/
s/$/@/
s/[^A-Za-z0-9_]\+/@&@/g

# mac.h
s/@TYPE@/@typedef@/g
s/@STRUCT@/@struct@/g
s/@UNION@/@union@/g
s/@REG@/@register@/g

s/@BEGIN@/@{NEWLINE     @/g
s/@END@/@}@/g

s/@IF@/@if (NOWHITE@/g
s/@THEN@/@NOWHITE) {NEWLINE    @/g
s/@ELSE@/@}NEWLINEelse {NEWLINE    @/g
s/@ELIF@/@}NEWLINEelse if (NOWHITE@/g
s/@FI@/@}@/g

s/@FOR@/@for (NOWHITE@/g
s/@WHILE@/@while (NOWHITE@/g
s/@DO@/@NOWHITE) {NEWLINE  @/g
s/@OD@/@}@/g
s/@REP@/@do {NEWLINE   @/g
s/@PER@/@} while (NOWHITE@/g
s/@DONE@/@NOWHITE);@/g
s/@LOOP@/@for(;;) {NEWLINE    @/g
s/@POOL@/@}@/g

s/@SKIP@/@;@/g
s/@DIV@/@\/@/g
s/@REM@/@%@/g
s/@NEQ@/@^@/g
s/@ANDF@/@\&\&@/g
s/@ORF@/@||@/g

s/@TRUE@/@-1@/g
s/@FALSE@/@0@/g
s/@LOBYTE@/@0377@/g
s/@HIBYTE@/@0177400@/g
s/@STRIP@/@0177@/g
s/@HEXMSK@/@017@/g

s/@SP@/@' '@/g
s/@TB@/@'\\t'@/g
s/@NL@/@'\\n'@/g

# mode.h
s/@ADDR@/@intptr_t@/g
s/@INT@/@short@/g
s/@VOID@/@void@/g
s/@L_INT@/@long@/g
s/@REAL@/@float@/g
s/@L_REAL@/@double@/g
s/@POS@/@unsigned@/g
s/@BOOL@/@bool@/g
s/@CHAR@/@char@/g
s/@STRING@/@char*CHECKME@/g
s/@MSG@/@char[]CHECKME@/g
s/@MAP@/@struct map@/g
s/@MAPPTR@/@struct map*CHECKME@/g
s/@BKPT@/@struct bkpt@/g
s/@BKPTR@/@struct bkpt*CHECKME@/g

s/@REGLIST@/@struct reglist@/g
s/@REGPTR@/@struct reglist*CHECKME@/g

# defs.h
s/@EOR@/@'\\n'@/g
s/@SP@/@' '@/g
s/@TB@/@'\\t'@/g
s/@QUOTE@/@0200@/g
s/@STRIP@/@0177@/g
s/@LOBYTE@/@0377@/g
s/@EVEN@/@-2@/g

s/@//g
s/ATSIGN/@/g

s/{NEWLINE[	 ]*\([^{}]*}\)/{ \1/g
s/^\([	 ]*\)\([^	 {}][^{}]*\)\?\([{}]\)NEWLINE/\1\2\3\n\1/g
s/\n\([	 ]*\)\([^	 {}][^{}]*\)\?\([{}]\)NEWLINE/\n\1\2\3\n\1/g
s/\[\]CHECKME\([	 ]*[A-Za-z_][0-9A-Za-z_]*\)\([	 ]*;\)/\1[]\2/g
s/\*CHECKME\([	 ]*\)\([A-Za-z_][0-9A-Za-z_]*[	 ]*;\)/\1*\2/g

# nonessential normalization of whitespace:
s/^\([	]*\)        /\1	/
s/\n\([	]*\)        /\n\1	/
s/^\([	]*\)        /\1	/
s/\n\([	]*\)        /\n\1	/
s/	 	/		/g
s/	  	/		/g
s/	   	/		/g
s/	    	/		/g
s/	     	/		/g
s/	      	/		/g
s/	       	/		/g
