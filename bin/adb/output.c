#include <gen.h>
#include <stdio.h>
#include <sys/file.h>
/*#include <time.h> gen.h*/
#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif
#include "defs.h"
#include "head.h"

#if defined(DOSCCS) && !defined(lint)
static	char sccsid[] = "@(#)output.c	4.4 4/26/85";
#endif
/*
 *
 *	UNIX debugger
 *
 */

/*#include "defs.h"*/
/*#include <stdio.h>*/
/*#include <varargs.h>*/

extern short		mkfault;
extern short		infile;
short		outfile = 1;
extern long		maxpos;
extern long		maxoff;
short		radix = 16;

char		printbuf[MAXLIN];
char		*printptr = printbuf;
char		*digitptr;
extern char		TOODEEP[];

int eqstr(s1, s2) register char *s1; register char *s2; {
	while (*s1++ == *s2) {
	   if (*s2++ == 0) {
		return(1);
	   }
	}
	return(0);
}

int length(s) register char *s; {
	short		n = 0;
	while (*s++) { n++; }
	return(n);
}

void printc(c) int c; {
	char		d;
	char		*q;
	short		posn, tabs, p;

	if (mkfault) {
		return;
	}
	else if ((*printptr=c)=='\n') {
	     tabs=0; posn=0; q=printbuf;
	     for (p=0; p<printptr-printbuf; p++) {
		d=printbuf[p];
		if ((p&7)==0 && posn) {
		     tabs++; posn=0;
		}
		if (d==' ') {
		     posn++;
		}
		else {
		     while (tabs>0) { *q++='\t'; tabs--; }
		     while (posn>0) { *q++=' '; posn--; }
		     *q++=d;
		}
	     }
	     *q++='\n';
#ifdef EDDT
		printptr=printbuf; do putchar(*printptr++); while (printptr<q);
#else
	     write(outfile,printbuf,q-printbuf);
#endif
	     printptr=printbuf;
	}
	else if (c=='\t') {
	     *printptr++=' ';
	     while ((printptr-printbuf)&7) { *printptr++=' '; }
	}
	else if (c) {
	     printptr++;
	}
	if (printptr >= &printbuf[MAXLIN-9]) {
	    
		write(outfile, printbuf, printptr - printbuf);
		printptr = printbuf;
	}
}

int charpos() {	return(printptr-printbuf);
}

void flushbuf() {	if (printptr!=printbuf) {
	     printc('\n');
	}
}

#ifdef __STDC__
void _printf(char *fmat, ...)
#else
void _printf(fmat, va_alist) char *fmat; va_dcl
#endif
{
	char		*fptr, *s;
	va_list		argp;
	short		width, prec;
	char		c, adj;
#ifndef vax
	short		decpt;
#endif
	short		n;
	char		digits[64];

	fptr = fmat; _va_start(argp, fmat);

	while (c = *fptr++) {
	    if (c!='%') {
		 printc(c);
	    }
	    else {
		 if (*fptr=='-') { adj='l'; fptr++; } else { adj='r'; }
		 width=convert(&fptr);
		 if (*fptr=='.') { fptr++; prec=convert(&fptr); } else { prec = -1; }
		 digitptr=digits;
		 s=0;
		 /* it seems this is converted from 16-bit code, and */
		 /* is concerned about automatic promotion to signed */
		 /* int, of unsigned shorts that appear to be signed */
		 switch (c = *fptr++) {

		    case 'd':
		    case 'u':
			printnum((short)va_arg(argp, int),c,10); break;
		    case 'o':
			printoct((long)(unsigned short)va_arg(argp, int),0); break;
		    case 'q':
			printoct((long)(short)va_arg(argp, int),-1); break;
		    case 'x':
			printdbl((long)(unsigned short)va_arg(argp, int),c,16); break;
		    case 'r':
			printdbl((long)(short)va_arg(argp, int),c,radix); break;
		    case 'R':
			printdbl(va_arg(argp, long),c,radix); break;
		    case 'Y':
			printdate(va_arg(argp, long)); break;
		    case 'D':
		    case 'U':
			printdbl(va_arg(argp, long),c,10); break;
		    case 'O':
			printoct(va_arg(argp, long),0); break;
		    case 'Q':
			printoct(va_arg(argp, long),-1); break;
		    case 'X':
			printdbl(va_arg(argp, long),'x',16); break;
		    case 'c':
			printc((short)va_arg(argp, int)); break;
		    case 's':
			s=va_arg(argp, char *); break;
#ifndef EDDT
		    case 'f':
		    case 'F':
#ifdef vax
			sprintf(s=digits,"%+.16e",va_arg(argp, double)); prec= -1; break;
#else
			s=ecvt(va_arg(argp, double), prec, &decpt, &n);
			*digitptr++=(n?'-':'+');
			*digitptr++ = (decpt<=0 ? '0' : *s++);
			if (decpt>0) { decpt--; }
			*digitptr++ = '.';
			while (*s && prec--) { *digitptr++ = *s++; }
			while (*--digitptr=='0');
			digitptr += (digitptr-digits>=3 ? 1 : 2);
			if (decpt) {
			     *digitptr++ = 'e'; printnum(decpt,'d',10);
			}
			s=0; prec = -1; break;
#endif
#endif
		    case 'm':
			break;
		    case 'M':
			width=(short)va_arg(argp, int); break;
		    case 'T':
		    case 't':
			if (c=='T') {
			     width=(short)va_arg(argp, int);
			}
			if (width) {
			     width -= charpos()%width;
			}
			break;
		    default:
			printc(c);
			break;
		}

		if (s==0) {
		     *digitptr=0; s=digits;
		}
		n=length(s);
		n=(prec<n && prec>=0 ? prec : n);
		width -= n;
		if (adj=='r') {
		     while (width-- > 0) { printc(' '); }
		}
		while (n--) { printc(*s++); }
		while (width-- > 0) { printc(' '); }
		digitptr=digits;
	    }
	}
}

void printdate(tvec) long tvec; {
	register short		i;
	register char	*timeptr;
#ifndef EDDT
	timeptr = ctime(&tvec);
#else
	timeptr="????????????????????????";
#endif
	for (i=20; i<24; i++) { *digitptr++ = *(timeptr+i); }
	for (i=3; i<19; i++) { *digitptr++ = *(timeptr+i); }
} /*printdate*/

void prints(s) char *s; {	_printf("%s",s);
}

void newline() {
	printc('\n');
}

int convert(cp) register char **cp; {
	register char	c;
	short		n;
	n=0;
	while (((c = *(*cp)++)>='0') && (c<='9')) { n=n*10+c-'0'; }
	(*cp)--;
	return(n);
}

void printnum(n, fmat, base) int n; int fmat; int base; {
	register char	k;
	register short		*dptr;
	short		digs[15];
	dptr=digs;
	if (n<0 && fmat=='d') { n = -n; *digitptr++ = '-'; }
	n &= 0xffff;
	while (n) {
	    *dptr++ = ((unsigned)(n&0xffff))%base;
	    n=((unsigned)(n&0xffff))/base;
	}
	if (dptr==digs) { *dptr++=0; }
	while (dptr!=digs) {
	    k = *--dptr;
	    *digitptr++ = (k+(k<=9 ? '0' : 'a'-10));
	}
}

void printoct(o, s) long o; int s; {
	short		i;
	long		po = o;
	char		digs[12];

	if (s) {
	     if (po<0) {
		  po = -po; *digitptr++='-';
	     }
	     else {
		  if (s>0) { *digitptr++='+'; }
	     }
	}
	for (i=0;i<=11;i++) { digs[i] = po&7; po >>= 3; }
	digs[10] &= 03; digs[11]=0;
	for (i=11;i>=0;i--) {
	   if (digs[i]) { break; } }
	for (i++;i>=0;i--) { *digitptr++=digs[i]+'0'; }
}

#ifndef vax
void printdbl(lx, ly, fmat, base) short lx, ly; char fmat; int base;
#else
void printdbl(lxy, fmat, base) long lxy; char fmat; int base;
#endif
{
	int digs[20];
#ifndef MULD2
	register char *cp1;
	cp1=digs; if ((lxy&0xFFFF0000L)==0xFFFF0000L) {*cp1++='-'; lxy= -lxy;}
	sprintf(cp1,base==16 ? "%x" : "%D",lxy);
	cp1=digs; while (*digitptr++= *cp1++); --digitptr;
#else
	int *dptr; char k;
	double f ,g; long q;
#ifdef vax
	short lx,ly;
	ly=lxy; lx=(lxy>>16)&0xFFFF;
#endif
	dptr=digs;
	if (fmat=='D' || fmat=='r') {
		f=((lx&0xFFFFL)<<16)|(ly&0xFFFFL);
		if (f<0) { *digitptr++='-'; f = -f; }
	}
	else {
	    
		if (lx==-1) {
		     *digitptr++='-'; f=(long)(unsigned)-ly;
		}
		else {
		     f=((lx&0xFFFFL)<<16)|(ly&0xFFFFL);
		}
		if (fmat=='x') { *digitptr++='#'; }
	}
	while (f) {
	    q=f/base; g=q;
	    *dptr++ = f-g*base;
	    f=q;
	}
	if (dptr==digs || dptr[-1]>9) { *dptr++=0; }
	while (dptr!=digs) {
	    k = *--dptr;
	    *digitptr++ = (k+(k<=9 ? '0' : 'a'-10));
	}
#endif
}

#define	MAXIFD	5
struct {
	int	fd;
	int	r9;
} istack[MAXIFD];
int	ifiledepth;

void iclose(stack, err) int stack; int err; {
	if (err) {
		if (infile) {
			close(infile); infile=0;
		}
		while (--ifiledepth >= 0) {
			if (istack[ifiledepth].fd) {
				close(istack[ifiledepth].fd);
			}
		}
		ifiledepth = 0;
	}
	else if (stack == 0) {
		if (infile) {
			close(infile); infile=0;
		}
	}
	else if (stack > 0) {
		if (ifiledepth >= MAXIFD) {
			error(TOODEEP);
		}
		istack[ifiledepth].fd = infile;
		istack[ifiledepth].r9 = var[9];
		ifiledepth++;
		infile = 0;
	}
	else {
		if (infile) {
			close(infile); infile=0;
		}
		if (ifiledepth > 0) {
			infile = istack[--ifiledepth].fd;
			var[9] = istack[ifiledepth].r9;
		}
	}
}

void oclose() {
	if (outfile!=1) {
		flushbuf(); close(outfile); outfile=1;
	}
}

void endline() {

	if (maxpos <= charpos())
		_printf("\n");
}
