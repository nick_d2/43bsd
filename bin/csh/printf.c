#ifdef __STDC__
#include <stdarg.h>
#define _va_start(argp, arg) va_start(argp, arg)
#else
#include <varargs.h>
#define _va_start(argp, arg) va_start(argp)
#endif
#include "sh.h"

/*
 * Copyright (c) 1987 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)printf.c	5.4 (Berkeley) 6/27/88";
#endif

/*#include <stdio.h>*/
/*#include <varargs.h>*/

#ifdef __STDC__
int printf(char *fmt, ...)
#else
int printf(fmt, va_alist) char *fmt; va_dcl
#endif
{
	va_list argp;
	int len;

	_va_start(argp, fmt);
	len = _doprnt(fmt, argp/*, stdout*/);
	va_end(argp);
	return(/*ferror(stdout) ? EOF :*/ len);
}
