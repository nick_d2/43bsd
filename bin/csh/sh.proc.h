#ifndef _SH_PROC_H_
#define _SH_PROC_H_

/*#include <sys/resource.h> sh.h*/
/*#include <sys/time.h> sh.h*/
#include "sh.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley Software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)sh.proc.h	5.2 (Berkeley) 6/6/85
 */

/*
 * C shell - process structure declarations
 */

/*
 * Structure for each process the shell knows about:
 *	allocated and filled by pcreate.
 *	flushed by pflush; freeing always happens at top level
 *	    so the interrupt level has less to worry about.
 *	processes are related to "friends" when in a pipeline;
 *	    p_friends links makes a circular list of such jobs
 */
struct process	{
	struct	process *p_next;	/* next in global "proclist" */
	struct	process	*p_friends;	/* next in job list (or self) */
	struct	directory *p_cwd;	/* cwd of the job (only in head) */
	unsigned short p_flags;		/* various job status flags */
	char	p_reason;		/* reason for entering this state */
	char	p_index;		/* shorthand job index */
	int	p_pid;
	int	p_jobid;		/* pid of job leader */
	/* if a job is stopped/background p_jobid gives its pgrp */
	struct	timeval p_btime;	/* begin time */
	struct	timeval p_etime;	/* end time */
	struct	rusage p_rusage;
	char	*p_command;		/* first PMAXLEN chars of command */
};

/* flag values for p_flags */
#define	PRUNNING	(1<<0)		/* running */
#define	PSTOPPED	(1<<1)		/* stopped */
#define	PNEXITED	(1<<2)		/* normally exited */
#define	PAEXITED	(1<<3)		/* abnormally exited */
#define	PSIGNALED	(1<<4)		/* terminated by a signal != SIGINT */

#define	PALLSTATES	(PRUNNING|PSTOPPED|PNEXITED|PAEXITED|PSIGNALED|PINTERRUPTED)
#define	PNOTIFY		(1<<5)		/* notify async when done */
#define	PTIME		(1<<6)		/* job times should be printed */
#define	PAWAITED	(1<<7)		/* top level is waiting for it */
#define	PFOREGND	(1<<8)		/* started in shells pgrp */
#define	PDUMPED		(1<<9)		/* process dumped core */
#define	PDIAG		(1<<10)		/* diagnostic output also piped out */
#define	PPOU		(1<<11)		/* piped output */
#define	PREPORTED	(1<<12)		/* status has been reported */
#define	PINTERRUPTED	(1<<13)		/* job stopped via interrupt signal */
#define	PPTIME		(1<<14)		/* time individual process */
#define	PNEEDNOTE	(1<<15)		/* notify as soon as practical */

#define	PNULL		(struct process *)0
#define	PMAXLEN		80

/* defines for arguments to pprint */
#define	NUMBER		01
#define	NAME		02
#define	REASON		04
#define	AMPERSAND	010
#define	FANCY		020
#define	SHELLDIR	040		/* print shell's dir if not the same */
#define	JOBDIR		0100		/* print job's dir if not the same */
#define	AREASON		0200

struct	process	proclist;		/* list head of all processes */
bool	pnoprocesses;			/* pchild found nothing to wait for */

struct	process *pholdjob;		/* one level stack of current jobs */

struct	process *pcurrjob;		/* current job */
struct	process	*pcurrent;		/* current job in table */
struct	process *pprevious;		/* previous job in table */

short	pmaxindex;			/* current maximum job index */

/*int	psigint();*/
/*struct	process	*pgetcurr();*/
/*struct	process	*plookup();*/
/*struct	process *pfind();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* sh.proc.c */
void pchild __P((void));
void pnote __P((void));
void pwait __P((void));
void pjwait __P((register struct process *pp));
void dowait __P((char **v, struct command *kp));
void pflushall __P((void));
void pflush __P((register struct process *pp));
void pclrcurr __P((register struct process *pp));
void palloc __P((int pid, register struct command *t));
void padd __P((register struct command *t));
void pads __P((char *cp));
void psavejob __P((void));
void prestjob __P((void));
void pendjob __P((void));
int pprint __P((register struct process *pp, int flag));
void ptprint __P((register struct process *tp));
void dojobs __P((char **v, struct command *kp));
void dofg __P((char **v, struct command *kp));
void dofg1 __P((char **v, struct command *kp));
void dobg __P((char **v, struct command *kp));
void dobg1 __P((char **v, struct command *kp));
void dostop __P((char **v, struct command *kp));
void dokill __P((char **v, struct command *kp));
void pkill __P((char **v, int signum));
void pstart __P((register struct process *pp, int foregnd));
void panystop __P((int neednl));
struct process *pfind __P((char *cp));
struct process *pgetcurr __P((register struct process *pp));
void donotify __P((char **v, struct command *kp));
int pfork __P((struct command *t, int wanttty));
void okpcntl __P((void));

/* sh.sem.c */
void execute __P((register struct command *t, int wanttty, int *pipein, int *pipeout));
void vffree __P((void));
void doio __P((register struct command *t, int *pipein, int *pipeout));
void mypipe __P((register int *pv));
void chkclob __P((register char *cp));

#endif
