#ifndef _SH_DIR_H_
#define _SH_DIR_H_

#include "sh.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley Software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)sh.dir.h	5.2 (Berkeley) 6/6/85
 */

/*
 * Structure for entries in directory stack.
 */
struct	directory	{
	struct	directory *di_next;	/* next in loop */
	struct	directory *di_prev;	/* prev in loop */
	unsigned short *di_count;	/* refcount of processes */
	char	*di_name;		/* actual name */
};
struct directory *dcwd;		/* the one we are in now */

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* sh.dir.c */
void dinit __P((char *hp));
void dodirs __P((char **v, struct command *kp));
void dtildepr __P((register char *home, register char *dir));
void dochngd __P((char **v, struct command *kp));
char *dfollow __P((register char *cp));
void dopushd __P((char **v, struct command *kp));
struct directory *dfind __P((register char *cp));
void dopopd __P((char **v, struct command *kp));
void dfree __P((register struct directory *dp));
char *dcanon __P((register char *cp, register char *p));
void dnewcwd __P((register struct directory *dp));

#endif
