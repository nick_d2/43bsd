#ifndef _SH_H_
#define _SH_H_

#include <gen.h>
/*#include <setjmp.h> gen.h*/
/*#include <strings.h> gen.h*/
/*#include <sys/param.h> gen.h*/
#include <sys/resource.h>
#include <sys/stat.h>
/*#include <sys/time.h> gen.h*/
/*#include <sys/types.h> gen.h*/
#ifdef __STDC__
#include <stdarg.h>
#endif
#include "sh.local.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley Software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)sh.h	5.3 (Berkeley) 3/29/86
 */

/*#include <sys/time.h>*/
/*#include <sys/resource.h>*/
/*#include <sys/param.h>*/
/*#include <sys/stat.h>*/
/*#include <sys/signal.h>*/
/*#include <errno.h>*/
/*#include <setjmp.h>*/
/*#include "sh.local.h"*/
/*#include "sh.char.h"*/

/*
 * C shell
 *
 * Bill Joy, UC Berkeley
 * October, 1978; May 1980
 *
 * Jim Kulp, IIASA, Laxenburg Austria
 * April, 1980
 */

#define	isdir(d)	((d.st_mode & S_IFMT) == S_IFDIR)

typedef	char	bool;

#define	eq(a, b)	(strcmp(a, b) == 0)

/*
 * Global flags
 */
extern bool	chkstop;		/* Warned of stopped jobs... allow exit */
extern bool	didfds;			/* Have setup i/o fd's for child */
extern bool	doneinp;		/* EOF indicator after reset from readc */
extern bool	exiterr;		/* Exit if error or non-zero exit status */
extern bool	child;			/* Child shell ... errors cause exit */
extern bool	haderr;			/* Reset was because of an error */
extern bool	intty;			/* Input is a tty */
extern bool	intact;			/* We are interactive... therefore prompt */
extern bool	justpr;			/* Just print because of :p hist mod */
extern bool	loginsh;		/* We are a loginsh -> .login/.logout */
extern bool	neednote;		/* Need to pnotify() */
extern bool	noexec;			/* Don't execute, just syntax check */
extern bool	pjobs;			/* want to print jobs if interrupted */
extern bool	setintr;		/* Set interrupts on/off -> Wait intr... */
extern bool	timflg;			/* Time the next waited for command */
extern bool	havhash;		/* path hashing is available */
#ifdef FILEC
extern bool	filec;			/* doing filename expansion */
#endif

/*
 * Global i/o info
 */
extern char	*arginp;		/* Argument input for sh -c and internal `xx` */
extern int	onelflg;		/* 2 -> need line for -t, 1 -> exit on read */
extern char	*file;			/* Name of shell file for $0 */

extern char	*err;			/* Error message from scanner/parser */
extern int	errno;			/* Error from C library routines */
extern char	*shtemp;		/* Temp name for << shell files in /tmp */
extern struct	timeval time0;		/* Time at which the shell started */
extern struct	rusage ru0;

/*
 * Miscellany
 */
extern char	*doldol;		/* Character pid for $$ */
extern int	uid;			/* Invokers uid */
extern time_t	chktim;			/* Time mail last checked */
extern int	shpgrp;			/* Pgrp of shell */
extern int	tpgrp;			/* Terminal process group */
/* If tpgrp is -1, leave tty alone! */
extern int	opgrp;			/* Initial pgrp and tty pgrp */
extern int	oldisc;			/* Initial line discipline or -1 */

/*
 * These are declared here because they want to be
 * initialized in sh.init.c (to allow them to be made readonly)
 */

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

struct command; /* forward declaration for bfunct below */

extern struct	biltins {
	char	*bname;
	void	(*bfunct) __P((char **v, struct command *kp));
	short	minargs, maxargs;
} bfunc[];
extern int nbfunc;

extern struct srch {
	char	*s_name;
	short	s_value;
} srchn[];
extern int nsrchn;

/*
 * To be able to redirect i/o for builtins easily, the shell moves the i/o
 * descriptors it uses away from 0,1,2.
 * Ideally these should be in units which are closed across exec's
 * (this saves work) but for version 6, this is not usually possible.
 * The desired initial values for these descriptors are defined in
 * sh.local.h.
 */
extern short	SHIN;			/* Current shell input (script) */
extern short	SHOUT;			/* Shell output */
extern short	SHDIAG;			/* Diagnostic output... shell errs go here */
extern short	OLDSTD;			/* Old standard input (def for cmds) */

/*
 * Error control
 *
 * Errors in scanning and parsing set up an error message to be printed
 * at the end and complete.  Other errors always cause a reset.
 * Because of source commands and .cshrc we need nested error catches.
 */

extern jmp_buf	reslab;

#define	setexit()	((void) setjmp(reslab))
#define	reset()		longjmp(reslab, 0)
	/* Should use structure assignment here */
#define	getexit(a)	bcopy((char *)reslab, (char *)(a), sizeof reslab)
#define	resexit(a)	bcopy((char *)(a), (char *)reslab, sizeof reslab)

extern char	*gointr;		/* Label for an onintr transfer */
void	(*parintr) __P((int sig));		/* Parents interrupt catch */
void	(*parterm) __P((int sig));		/* Parents terminate catch */

/*
 * Lexical definitions.
 *
 * All lexical space is allocated dynamically.
 * The eighth bit of characters is used to prevent recognition,
 * and eventually stripped.
 */
#define	QUOTE 	0200		/* Eighth char bit used internally for 'ing */
#define	TRIM	0177		/* Mask to strip quote bit */

/*
 * Each level of input has a buffered input structure.
 * There are one or more blocks of buffered input for each level,
 * exactly one if the input is seekable and tell is available.
 * In other cases, the shell buffers enough blocks to keep all loops
 * in the buffer.
 */
extern struct	Bin {
	off_t	Bfseekp;		/* Seek pointer */
	off_t	Bfbobp;			/* Seekp of beginning of buffers */
	off_t	Bfeobp;			/* Seekp of end of buffers */
	short	Bfblocks;		/* Number of buffer blocks */
	char	**Bfbuf;		/* The array of buffer blocks */
} B;

#define	fseekp	B.Bfseekp
#define	fbobp	B.Bfbobp
#define	feobp	B.Bfeobp
#define	fblocks	B.Bfblocks
#define	fbuf	B.Bfbuf

#define btell()	fseekp

/*
 * The shell finds commands in loops by reseeking the input
 * For whiles, in particular, it reseeks to the beginning of the
 * line the while was on; hence the while placement restrictions.
 */
extern off_t	lineloc;

#ifdef	TELL
extern bool	cantell;			/* Is current source tellable ? */
#endif

/*
 * Input lines are parsed into doubly linked circular
 * lists of words of the following form.
 */
struct	wordent {
	char	*word;
	struct	wordent *prev;
	struct	wordent *next;
};

/*
 * During word building, both in the initial lexical phase and
 * when expanding $ variable substitutions, expansion by `!' and `$'
 * must be inhibited when reading ahead in routines which are themselves
 * processing `!' and `$' expansion or after characters such as `\' or in
 * quotations.  The following flags are passed to the getC routines
 * telling them which of these substitutions are appropriate for the
 * next character to be returned.
 */
#define	DODOL	1
#define	DOEXCL	2
#define	DOALL	DODOL|DOEXCL

/*
 * Labuf implements a general buffer for lookahead during lexical operations.
 * Text which is to be placed in the input stream can be stuck here.
 * We stick parsed ahead $ constructs during initial input,
 * process id's from `$$', and modified variable values (from qualifiers
 * during expansion in sh.dol.c) here.
 */
extern char	labuf[BUFSIZ];

extern char	*lap;

/*
 * Parser structure
 *
 * Each command is parsed to a tree of command structures and
 * flags are set bottom up during this process, to be propagated down
 * as needed during the semantics/exeuction pass (sh.sem.c).
 */
struct	command {
	short	t_dtyp;				/* Type of node */
	short	t_dflg;				/* Flags, e.g. FAND|... */
	union {
		char	*T_dlef;		/* Input redirect word */
		struct	command *T_dcar;	/* Left part of list/pipe */
	} L;
	union {
		char	*T_drit;		/* Output redirect word */
		struct	command *T_dcdr;	/* Right part of list/pipe */
	} R;
#define	t_dlef	L.T_dlef
#define	t_dcar	L.T_dcar
#define	t_drit	R.T_drit
#define	t_dcdr	R.T_dcdr
	char	**t_dcom;			/* Command/argument vector */
	struct	command *t_dspr;		/* Pointer to ()'d subtree */
	short	t_nice;
};

#define	TCOM	1		/* t_dcom <t_dlef >t_drit	*/
#define	TPAR	2		/* ( t_dspr ) <t_dlef >t_drit	*/
#define	TFIL	3		/* t_dlef | t_drit		*/
#define	TLST	4		/* t_dlef ; t_drit		*/
#define	TOR	5		/* t_dlef || t_drit		*/
#define	TAND	6		/* t_dlef && t_drit		*/

#define	FSAVE	(FNICE|FTIME|FNOHUP)	/* save these when re-doing */

#define	FAND	(1<<0)		/* executes in background	*/
#define	FCAT	(1<<1)		/* output is redirected >>	*/
#define	FPIN	(1<<2)		/* input is a pipe		*/
#define	FPOU	(1<<3)		/* output is a pipe		*/
#define	FPAR	(1<<4)		/* don't fork, last ()ized cmd	*/
#define	FINT	(1<<5)		/* should be immune from intr's */
/* spare */
#define	FDIAG	(1<<7)		/* redirect unit 2 with unit 1	*/
#define	FANY	(1<<8)		/* output was !			*/
#define	FHERE	(1<<9)		/* input redirection is <<	*/
#define	FREDO	(1<<10)		/* reexec aft if, repeat,...	*/
#define	FNICE	(1<<11)		/* t_nice is meaningful */
#define	FNOHUP	(1<<12)		/* nohup this command */
#define	FTIME	(1<<13)		/* time this command */

/*
 * The keywords for the parser
 */
#define	ZBREAK		0
#define	ZBRKSW		1
#define	ZCASE		2
#define	ZDEFAULT 	3
#define	ZELSE		4
#define	ZEND		5
#define	ZENDIF		6
#define	ZENDSW		7
#define	ZEXIT		8
#define	ZFOREACH	9
#define	ZGOTO		10
#define	ZIF		11
#define	ZLABEL		12
#define	ZLET		13
#define	ZSET		14
#define	ZSWITCH		15
#define	ZTEST		16
#define	ZTHEN		17
#define	ZWHILE		18

/*
 * Structure defining the existing while/foreach loops at this
 * source level.  Loops are implemented by seeking back in the
 * input.  For foreach (fe), the word list is attached here.
 */
extern struct	whyle {
	off_t	w_start;		/* Point to restart loop */
	off_t	w_end;			/* End of loop (0 if unknown) */
	char	**w_fe, **w_fe0;	/* Current/initial wordlist for fe */
	char	*w_fename;		/* Name for fe */
	struct	whyle *w_next;		/* Next (more outer) loop */
} *whyles;

/*
 * Variable structure
 *
 * Aliases and variables are stored in AVL balanced binary trees.
 */
extern struct	varent {
	char	**vec;		/* Array of words which is the value */
	char	*v_name;	/* Name of variable/alias */
	struct	varent *v_link[3];	/* The links, see below */
	int	v_bal;		/* Balance factor */
} shvhed, aliases;
#define v_left		v_link[0]
#define v_right		v_link[1]
#define v_parent	v_link[2]

/*struct varent *adrof1();*/
#define adrof(v)	adrof1(v, &shvhed)
#define value(v)	value1(v, &shvhed)

/*
 * The following are for interfacing redo substitution in
 * aliases to the lexical routines.
 */
extern struct	wordent *alhistp;		/* Argument list (first) */
extern struct	wordent *alhistt;		/* Node after last in arg list */
extern char	**alvec;			/* The (remnants of) alias vector */

/*
 * Filename/command name expansion variables
 */
extern short	gflag;				/* After tglob -> is globbing needed? */

/*
 * A reasonable limit on number of arguments would seem to be
 * the maximum number of characters in an arg list / 6.
 */
#define	GAVSIZ	NCARGS / 6

/*
 * Variables for filename expansion
 */
extern char	**gargv;			/* Pointer to the (stack) arglist */
extern short	gargc;				/* Number args in gargv */
extern short	gnleft;

/*
 * Variables for command expansion.
 */
extern char	**pargv;			/* Pointer to the argv list space */
extern char	*pargs;				/* Pointer to start current word */
extern short	pargc;				/* Count of arguments in pargv */
extern short	pnleft;				/* Number of chars left in pargs */
extern char	*pargcp;			/* Current index into pargs */

/*
 * History list
 *
 * Each history list entry contains an embedded wordlist
 * from the scanner, a number for the event, and a reference count
 * to aid in discarding old entries.
 *
 * Essentially "invisible" entries are put on the history list
 * when history substitution includes modifiers, and thrown away
 * at the next discarding since their event numbers are very negative.
 */
extern struct	Hist {
	struct	wordent Hlex;
	int	Hnum;
	int	Href;
	struct	Hist *Hnext;
} Histlist;

extern struct	wordent	paraml;			/* Current lexical word list */
extern int	eventno;			/* Next events number */
extern int	lastev;				/* Last event reference (default) */

extern char	HIST;				/* history invocation character */
extern char	HISTSUB;			/* auto-substitute character */

/*
 * In lines for frequently called functions
 */
#define XFREE(cp) { \
	char stack; \
	if ((cp) >= end && (cp) < &stack) \
		free(cp); \
}
extern void	*alloctmp;
#define xalloc(i) ((alloctmp = malloc(i)) ? alloctmp : nomem(i))

/*char	*Dfix1();*/
/*char	**blkcat();*/
/*char	**blkcpy();*/
/*char	**blkend();*/
/*char	**blkspl();*/
/*char	*calloc();*/
/*char	*malloc();*/
/*char	*cname();*/
/*char	**copyblk();*/
/*char	**dobackp();*/
/*char	*domod();*/
/*struct	wordent *dosub();*/
/*char	*exp3();*/
/*char	*exp3a();*/
/*char	*exp4();*/
/*char	*exp5();*/
/*char	*exp6();*/
/*struct	Hist *enthist();*/
/*struct	Hist *findev();*/
/*struct	wordent *freenod();*/
/*char	*getenv();*/
/*char	*getinx();*/
/*struct	varent *getvx();*/
/*struct	passwd *getpwnam();*/
/*struct	wordent *gethent();*/
/*struct	wordent *getsub();*/
/*char	*getwd();*/
/*char	**glob();*/
/*char	*globone();*/
/*char	*index();*/
/*struct	biltins *isbfunc();*/
/*off_t	lseek();*/
/*char	*operate();*/
/*int	phup();*/
/*int	pintr();*/
/*int	pchild();*/
/*char	*putn();*/
/*char	*rindex();*/
/*char	**saveblk();*/
/*char	*savestr();*/
/*char	*strcat();*/
/*char	*strcpy();*/
/*char	*strend();*/
/*char	*strings();*/
/*char	*strip();*/
/*char	*strspl();*/
/*char	*subword();*/
/*struct	command *syntax();*/
/*struct	command *syn0();*/
/*struct	command *syn1();*/
/*struct	command *syn1a();*/
/*struct	command *syn1b();*/
/*struct	command *syn2();*/
/*struct	command *syn3();*/
/*char	*value1();*/
/*char	*xhome();*/
/*char	*xname();*/
/*char	*xset();*/

#define	NOSTR	((char *) 0)

/*
 * setname is a macro to save space (see sh.err.c)
 */
extern char	*bname;
#define	setname(a)	(bname = (a))

#ifdef VFORK
extern char	*Vsav;
extern char	**Vav;
extern char	*Vdp;
#endif

extern char	**evalvec;
extern char	*evalp;

extern struct	mesg {
	char	*iname;		/* name from /usr/include */
	char	*pname;		/* print name */
} mesg[];

/* end of data segment, formerly done by linker using dummy array "end" */
extern char	*end;

#ifndef NORETURN
#ifdef __GNUC__
#define NORETURN __attribute__ ((__noreturn__))
#else
#define NORETURN
#endif
#endif

/* doprnt.c */
int _doprnt __P((char *fmt, va_list argp));

/* malloc.c */
void *malloc __P((unsigned nbytes));
void morecore __P((int bucket));
void free __P((void *cp));
void *realloc __P((void *cp, unsigned nbytes));
void showall __P((char **s, struct command *kp));

/* printf.c */
int printf __P((char *fmt, ...));

/* sh.c */
void main __P((int c, char **av));
void untty __P((void));
void importpath __P((char *cp));
void srccat __P((char *cp, char *dp));
void srcunit __P((register int unit, int onlyown, int hflg));
void rechist __P((void));
void goodbye __P((void));
void exitstat __P((void));
void phup __P((void));
void pintr __P((void));
void pintr1 __P((int wantnl));
void process __P((int catch));
void dosource __P((char **v, struct command *kp));
void mailchk __P((void));
int gethdir __P((char *home));
void initdesc __P((void));
int exit __P((int i)) NORETURN;
void printprompt __P((void));

/* sh.dol.c */
void Dfix __P((register struct command *t));
char *Dfix1 __P((register char *cp));
void Dfix2 __P((char **v));
int Dword __P((void));
int DgetC __P((register int flag));
void Dgetdol __P((void));
void setDolp __P((register char *cp));
void unDredc __P((int c));
int Dredc __P((void));
void Dtestq __P((register int c));
void heredoc __P((char *term));

/* sh.err.c */
int error __P((char *s, ...)) NORETURN;
void Perror __P((char *s));
void bferr __P((char *cp)) NORETURN;
void seterr __P((char *s));
void seterr2 __P((char *cp, char *dp));
void seterrc __P((char *cp, int d));

/* sh.exec.c */
void doexec __P((register struct command *t));
void pexerr __P((void));
void texec __P((char *f, register char **t));
void execash __P((char **t, register struct command *kp));
void xechoit __P((char **t));
void dohash __P((char **t, struct command *kp));
void dounhash __P((char **v, struct command *kp));
void hashstat __P((char **v, struct command *kp));
int hashname __P((register char *cp));

/* sh.exp.c */
int exp __P((register char ***vp));
int exp0 __P((register char ***vp, int ignore));
int exp1 __P((register char ***vp, int ignore));
int exp2 __P((register char ***vp, int ignore));
int exp2a __P((register char ***vp, int ignore));
int exp2b __P((register char ***vp, int ignore));
int exp2c __P((register char ***vp, int ignore));
char *exp3 __P((register char ***vp, int ignore));
char *exp3a __P((register char ***vp, int ignore));
char *exp4 __P((register char ***vp, int ignore));
char *exp5 __P((register char ***vp, int ignore));
char *exp6 __P((register char ***vp, int ignore));
void evalav __P((register char **v));
int isa __P((register char *cp, register int what));
int egetn __P((register char *cp));
int etraci __P((char *str, int i, char ***vp));
int etracc __P((char *str, char *cp, char ***vp));

/* sh.file.c */
int tenex __P((char *inputline, int inputline_size));

/* sh.func.c */
struct biltins *isbfunc __P((struct command *t));
void func __P((register struct command *t, register struct biltins *bp));
void dolabel __P((char **v, struct command *kp));
void doonintr __P((char **v, struct command *kp));
void donohup __P((char **v, struct command *kp));
void dozip __P((char **v, struct command *kp));
void prvars __P((void));
void doalias __P((char **v, struct command *kp));
void unalias __P((char **v, struct command *kp));
void dologout __P((char **v, struct command *kp));
void dologin __P((char **v, struct command *kp));
int donewgrp __P((char **v));
void islogin __P((void));
void doif __P((char **v, struct command *kp));
void reexecute __P((register struct command *kp));
void doelse __P((char **v, struct command *kp));
void dogoto __P((char **v, struct command *kp));
void doswitch __P((char **v, struct command *kp));
void dobreak __P((char **v, struct command *kp));
void doexit __P((char **v, struct command *kp));
void doforeach __P((char **v, struct command *kp));
void dowhile __P((char **v, struct command *kp));
void preread __P((void));
void doend __P((char **v, struct command *kp));
void docontin __P((char **v, struct command *kp));
void doagain __P((void));
void dorepeat __P((char **v, struct command *kp));
void doswbrk __P((char **v, struct command *kp));
int srchx __P((register char *cp));
void search __P((int type, register int level, char *goal));
int getword __P((register char *wp));
void toend __P((void));
void wfree __P((void));
void doecho __P((char **v, struct command *kp));
void doglob __P((char **v, struct command *kp));
void echo __P((int sep, register char **v));
void dosetenv __P((char **v, struct command *kp));
void dounsetenv __P((char **v, struct command *kp));
void setenv __P((char *name, char *val));
void unsetenv __P((char *name));
void doumask __P((char **v, struct command *kp));
struct limits *findlim __P((char *cp));
void dolimit __P((char **v, struct command *kp));
int getval __P((register struct limits *lp, char **v));
void limtail __P((char *cp, char *str0));
void plim __P((register struct limits *lp, int hard));
void dounlimit __P((char **v, struct command *kp));
int setlim __P((register struct limits *lp, int hard, int limit));
void dosuspend __P((char **v, struct command *kp));
void doeval __P((char **v, struct command *kp));

/* sh.glob.c */
char **glob __P((register char **v));
void ginit __P((char **agargv));
void collect __P((register char *as));
void acollect __P((register char *as));
int sortscmp __P((char **a1, char **a2));
void expand __P((char *as));
void matchdir __P((char *pattern));
int execbrc __P((char *p, char *s));
int match __P((char *s, char *p));
int amatch __P((register char *s, register char *p));
int Gmatch __P((register char *s, register char *p));
void Gcat __P((char *s1, char *s2));
void addpath __P((int c));
void rscan __P((register char **t, void (*f)(register int c)));
void trim __P((register char **t));
void tglob __P((register char **t));
char *globone __P((register char *str));
char **dobackp __P((char *cp, int literal));
void backeval __P((char *cp, int literal));
void psave __P((int c));
void pword __P((void));

/* sh.hist.c */
void savehist __P((struct wordent *sp));
struct Hist *enthist __P((int event, register struct wordent *lp, int docopy));
void hfree __P((register struct Hist *hp));
void dohist __P((char **vp, struct command *kp));
void dohist1 __P((struct Hist *hp, int *np, int rflg, int hflg));
void phist __P((register struct Hist *hp, int hflg));

/* sh.lex.c */
int lex __P((register struct wordent *hp));
void prlex __P((struct wordent *sp0));
void copylex __P((register struct wordent *hp, register struct wordent *fp));
void freelex __P((register struct wordent *vp));
char *word __P((void));
int getC1 __P((register int flag));
void getdol __P((void));
void addla __P((char *cp));
void getexcl __P((int sc));
struct wordent *getsub __P((struct wordent *en));
struct wordent *dosub __P((int sc, struct wordent *en, int global));
char *subword __P((char *cp, int type, bool *adid));
char *domod __P((char *cp, int type));
int matchs __P((register char *str, register char *pat));
int getsel __P((register int *al, register int *ar, int dol));
struct wordent *gethent __P((int sc));
struct Hist *findev __P((char *cp, int anyarg));
void noev __P((char *cp));
void setexclp __P((register char *cp));
void unreadc __P((int c));
int readc __P((int wanteof));
int bgetc __P((void));
void bfree __P((void));
void bseek __P((off_t l));
void btoeof __P((void));
void settell __P((void));

/* sh.misc.c */
int any __P((register int c, register char *s));
int onlyread __P((char *cp));
void xfree __P((char *cp));
char *savestr __P((register char *s));
void *calloc __P((register unsigned i, unsigned j));
void *nomem __P((unsigned i)) NORETURN;
char **blkend __P((register char **up));
void blkpr __P((register char **av));
int blklen __P((register char **av));
char **blkcpy __P((char **oav, register char **bv));
char **blkcat __P((char **up, char **vp));
void blkfree __P((char **av0));
char **saveblk __P((register char **v));
char *strspl __P((char *cp, char *dp));
char **blkspl __P((register char **up, register char **vp));
int lastchr __P((register char *cp));
void closem __P((void));
void donefds __P((void));
int dmove __P((register int i, register int j));
int dcopy __P((register int i, register int j));
int renum __P((register int i, register int j));
void lshift __P((register char **v, register int c));
int number __P((char *cp));
char **copyblk __P((register char **v));
char *strend __P((register char *cp));
char *strip __P((char *cp));
void udvar __P((char *name));
int prefix __P((register char *sub, register char *str));

/* sh.parse.c */
void alias __P((register struct wordent *lex));
void asyntax __P((register struct wordent *p1, register struct wordent *p2));
void asyn0 __P((struct wordent *p1, register struct wordent *p2));
void asyn3 __P((struct wordent *p1, register struct wordent *p2));
struct wordent *freenod __P((register struct wordent *p1, register struct wordent *p2));
struct command *syntax __P((register struct wordent *p1, register struct wordent *p2, int flags));
struct command *syn0 __P((struct wordent *p1, struct wordent *p2, int flags));
struct command *syn1 __P((struct wordent *p1, struct wordent *p2, int flags));
struct command *syn1a __P((struct wordent *p1, struct wordent *p2, int flags));
struct command *syn1b __P((struct wordent *p1, struct wordent *p2, int flags));
struct command *syn2 __P((struct wordent *p1, struct wordent *p2, int flags));
struct command *syn3 __P((struct wordent *p1, struct wordent *p2, int flags));
void freesyn __P((register struct command *t));

/* sh.print.c */
void psecs __P((long l));
void p2dig __P((register int i));
void putchar __P((register int c));
void draino __P((void));
void flush __P((void));

/* sh.set.c */
void doset __P((char **v, struct command *kp));
char *getinx __P((register char *cp, register int *ip));
void asx __P((char *vp, int subscr, char *p));
struct varent *getvx __P((char *vp, int subscr));
void dolet __P((char **v, struct command *kp));
char *xset __P((char *cp, char ***vp));
char *operate __P((int op, char *vp, char *p));
char *putn __P((register int n));
void putn1 __P((register int n));
int getn __P((register char *cp));
char *value1 __P((char *var, struct varent *head));
struct varent *madrof __P((char *pat, register struct varent *vp));
struct varent *adrof1 __P((register char *name, register struct varent *v));
void set __P((char *var, char *val));
void set1 __P((char *var, char **vec, struct varent *head));
void setq __P((char *name, char **vec, register struct varent *p));
void unset __P((char **v, struct command *kp));
void unset1 __P((register char **v, struct varent *head));
void unsetv __P((char *var));
void unsetv1 __P((register struct varent *p));
void setNS __P((char *cp));
void shift __P((char **v, struct command *kp));
void exportpath __P((char **val));
void balance __P((register struct varent *p, register f, int d));
void plist __P((register struct varent *p));

/* sh.time.c */
void settimes __P((void));
void dotime __P((char **v, struct command *kp));
void donice __P((char **v, struct command *kp));
void ruadd __P((register struct rusage *ru, register struct rusage *ru2));
void prusage __P((register struct rusage *r0, register struct rusage *r1, struct timeval *e, struct timeval *b));
void pdeltat __P((struct timeval *t1, struct timeval *t0));
void tvadd __P((struct timeval *tsum, struct timeval *t0));
void tvsub __P((struct timeval *tdiff, struct timeval *t1, struct timeval *t0));

/* vprintf.c */
int vprintf __P((char *fmt, va_list argp));

#endif
