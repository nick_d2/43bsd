#ifndef _TP_H_
#define _TP_H_

/*	@(#)tp.h	4.2	6/28/84	*/

/*	c-version of tp?.s
 *
 *	M. Ferentz
 *	August 1976
 *
 *	revised July 1977 BTL
 */

#define	MDIRENT	496		/* must be zero mod 8 */
#define DIRSZ	sizeof(struct dent)
#define MAPSIZE 4096
#define MAPMASK 07777
#define NAMELEN 32
#define BSIZE   512
#define	TCSIZ	578
#define TCDIRS	192
#define	MTSIZ	32767
#define TPB	(BSIZE/sizeof(struct tent))
#define	OK	0100000
#define	BRKINCR	512

#define	tapeblk	&tpentry[0]
#define tapeb	&tpentry[0]

struct 	tent	{	/* Structure of a tape directory block */
	char	pathnam[NAMELEN];
	short	mode;
	char	uid;
	char	gid;
	char	spare;
	char	size0;
	unsigned short	size1;
	long	time;
	unsigned short	tapea;	/* tape address */
	short	unused[8];
	short	cksum;
}	tpentry[TPB];

struct	dent {	/* in core version of tent with "unused" removed
		 * and pathname replaced by pointer to same in a
		 * packed area (nameblock).
		 */
	char	*d_namep;
	int	d_mode;
	int	d_uid;
	int	d_gid;
	long	d_size;
	long	d_time;
	int	d_tapea;
}  dir[MDIRENT];

extern char	map[MAPSIZE];
extern char	name[NAMELEN];
extern char	name1[NAMELEN];
extern char	mt[];
extern char	tc[];
extern char	*tname;
extern char	mheader[];
extern char	theader[];

extern int	narg, rnarg;
extern char	**parg;
extern int	wseeka,rseeka;
extern int	tapsiz;
extern int	fio;
extern short	ndirent, ndentb;
extern struct	dent *edir;
extern struct	dent *lastd;		/* for improvement */
/*char *sbrk();*/
/*char *strcpy();*/
/*long lseek();*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

extern void (*command) __P((void));

extern	int	flags;
#define	flc	0001
#define	fli	0004
#define	flm	0010
#define	flu	0020
#define	flv	0040
#define	flw	0100
#define fls	0200

/* tp1.c */
void main __P((int argc, char **argv));
void optap __P((void));
void setcom __P((void (*newcom)(void)));
void useerr __P((void));
void cmd __P((void));
void cmr __P((void));
void cmt __P((void));
void cmx __P((void));
void check __P((void));
void done __P((void));
void encode __P((char *pname, struct dent *dptr));
void decode __P((char *pname, struct dent *dptr));

/* tp2.c */
void clrdir __P((void));
void clrent __P((struct dent *ptr));
void rddir __P((void));
void wrdir __P((void));
void tread __P((void));
void twrite __P((void));
void rseek __P((int blk));
void wseek __P((int blk));
void seekerr __P((void));
int verify __P((int key));
void getfiles __P((void));
void expand __P((void));
void fserr __P((void));
void callout __P((void));
void swabdir __P((register struct tent *tp));

/* tp3.c */
void gettape __P((void (*how)(register struct dent *d)));
void delete __P((struct dent *dd));
void update __P((void));
void update1 __P((void));
void phserr __P((void));
void bitmap __P((void));
void setmap __P((register struct dent *d));
void maperr __P((void));
void usage __P((void));
void taboc __P((struct dent *dd));
void extract __P((register struct dent *d));

#endif
