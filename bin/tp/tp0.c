#include "tp.h"

#if defined(DOSCCS) && !defined(lint)
static char sccsid[] = "@(#)tp0.c	4.1 12/18/82";
#endif

/*#include "tp.h"*/

int	flags	= flu;
char	mt[]	= "/dev/rmt0";
char	tc[]	= "/dev/tapx";
char	mheader[] = "/usr/mdec/mboot";
char	theader[] = "/usr/mdec/tboot";

char	map[MAPSIZE];
char	name[NAMELEN];
char	name1[NAMELEN];
char	*tname;

int	narg, rnarg;
char	**parg;
int	wseeka,rseeka;
int	tapsiz;
int	fio;
short	ndirent, ndentb;
struct	dent *edir;
struct	dent *lastd;		/* for improvement */

void (*command) __P((void));
