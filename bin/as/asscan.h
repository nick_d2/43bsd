#ifndef _ASSCAN_H_
#define _ASSCAN_H_

/*#include <sys/types.h> asnumber.h*/
#include "asnumber.h"

#ifdef __STDC__
#include <stdint.h>
#else
typedef int intptr_t;
#endif

/*
 * Copyright (c) 1982 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)asscan.h	5.1 (Berkeley) 4/30/85
 */

/*
 *	The character scanner is called to fill up one token buffer
 *
 *	However, once the tokens are filled up by the
 *	character scanner, they are used in both the first and the second
 *	pass.  Holes created by .stab removal are replaced
 *	with 'skip' tokens that direct the second pass to ignore the
 *	following tokens.
 */

#define TOKBUFLG		4096
#define MAXVAX			32		
#define SAFETY			16

#define AVAILTOKS		TOKBUFLG -\
		sizeof(int) -\
		sizeof (struct tokbufdesc *) -\
		MAXVAX - SAFETY

struct tokbufdesc{
	int		tok_count;		/*absolute byte length*/
	struct		tokbufdesc *tok_next;
	char		toks[AVAILTOKS];
	char		bufovf[MAXVAX + SAFETY];
};
/*
 *	Definitions for handling tokens in the intermediate file
 *	buffers.
 *
 *	We want to have the compiler produce the efficient auto increment
 *	instruction for stepping through the buffer of tokens.  We must
 *	fool the type checker into thinking that a pointer can point
 *	to various size things.
 */

typedef int inttoktype;
typedef char bytetoktype;
typedef u_short lgtype;			/*for storing length of strings or skiping*/
/*
 *	defintions for putting various typed values
 *	into the intermediate buffers
 *	ptr will ALWAYS be of type void *
 */

#define pchar(ptr,val)		(*(*(char **)&(ptr))++ = (val))
#define puchar(ptr,val)		(*(*(char **)&(ptr))++ = (val))

#define pshort(ptr,val)		(*(*(short **)&(ptr))++ = (val))
#define plgtype(ptr,val)	(*(*(lgtype **)&(ptr))++ = (val))
#define pushort(ptr,val)	(*(*(u_short **)&(ptr))++ = (val))
#define pint(ptr,val)		(*(*(int **)&(ptr))++ = (val))
#define puint(ptr,val)		(*(*(u_int **)&(ptr))++ = (val))
#define plong(ptr,val)		(*(*(long **)&(ptr))++ = (val))
#define pulong(ptr,val)		(*(*(u_long **)&(ptr))++ = (val))
#define pnumber(ptr,val)	(*(*(Bignum **)&(ptr))++ = (val))
#define popcode(ptr,val)	(*(*(struct Opcode **)&(ptr))++ = (val))

#define pptr(ptr,val)		(*(*(intptr_t **)&(ptr))++ = (val))
#define ptoken(ptr,val)		(*(*(char **)&(ptr))++ = (val))
#define pskiplg(ptr,val)	(*(*(lgtype **)&(ptr))++ = (val))

#define gchar(val, ptr)		((val) = *(*(char **)&(ptr))++)
#define guchar(val, ptr)	((val) = *(*(u_char **)&(ptr))++)

#define gshort(val, ptr)	((val) = *(*(short **)&(ptr))++ )
#define glgtype(val, ptr)	((val) = *(*(lgtype **)&(ptr))++ )
#define gushort(val, ptr)	((val) = *(*(u_short **)&(ptr))++ )
#define gint(val, ptr)		((val) = *(*(int **)&(ptr))++)
#define guint(val, ptr)		((val) = *(*(u_int **)&(ptr))++)
#define glong(val, ptr)		((val) = *(*(long **)&(ptr))++)
#define gulong(val, ptr)	((val) = *(*(u_int **)&(ptr))++)
#define gnumber(val, ptr)	((val) = *(*(Bignum **)&(ptr))++)
#define gopcode(val, ptr)	((val) = *(*(struct Opcode **)&(ptr))++)

#define gptr(val, ptr)		((val) = *(*(intptr_t **)&(ptr))++)
#define gtoken(val, ptr)	((val) = *(*(char **)&(ptr))++)
#define gskiplg(val, ptr)	((val) = *(*(lgtype **)&(ptr))++)

extern	void  *tokptr;	/*the next token to consume, call by copy*/
extern	void  *tokub;	/*current upper bound in the current buffer*/

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* asscan1.c */
void inittokfile __P((void));
void closetokfile __P((void));
inttoktype yylex __P((void));
void buildskip __P((void *from, void *to));
void new_dot_s __P((char *namep));
int min __P((int a, int b));

/* asscan2.c */
void fillinbuffer __P((void));
void scan_dot_s __P((struct tokbufdesc *bufferbox));

/* asscan4.c */
int number __P((register int ch));
Bignum floatnumber __P((int fltradix));
int scanint __P((int signOK, char **dstcpp));

#endif
