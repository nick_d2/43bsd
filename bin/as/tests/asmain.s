.data
.data
.globl	_copyright
_copyright:.long	0x29232840
.long	0x706f4320
.long	0x67697279
.long	0x28207468
.long	0x31202963
.long	0x20323839
.long	0x65676552
.long	0x2073746e
.long	0x7420666f
.long	0x55206568
.long	0x6576696e
.long	0x74697372
.long	0x666f2079
.long	0x6c614320
.long	0x726f6669
.long	0x2e61696e
.long	0x6c41200a
.long	0x6972206c
.long	0x73746867
.long	0x73657220
.long	0x65767265
.long	0xa2e64
_sccsid:.long	0x29232840
.long	0x616d7361
.long	0x632e6e69
.long	0x322e3509
.long	0x65422820
.long	0x6c656b72
.long	0x20297965
.long	0x322f3031
.long	0x35382f31
.long	0x0
.comm	_rusefile,32
.comm	_relfil,4
.comm	_dotsname,4
.comm	_lineno,4
.comm	_innames,4
.comm	_ninfiles,4
.data
.align	2
.globl	_silent
_silent:.long	0
.align	2
.globl	_savelabels
_savelabels:.long	0
.align	2
.globl	_d124
_d124:.long	4
.align	2
.globl	_maxalign
_maxalign:.long	2
.align	2
.globl	_anyerrs
_anyerrs:.long	0
.align	2
.globl	_anywarnings
_anywarnings:.long	0
.align	2
.globl	_orgwarn
_orgwarn:.long	0
.align	2
.globl	_passno
_passno:.long	1
.align	2
.globl	_jxxxJUMP
_jxxxJUMP:.long	0
.align	2
.globl	_readonlydata
_readonlydata:.long	0
.align	2
.globl	_nGHnumbers
_nGHnumbers:.long	0
.align	2
.globl	_nGHopcodes
_nGHopcodes:.long	0
.align	2
.globl	_nnewopcodes
_nnewopcodes:.long	0
.align	2
.globl	_useVM
_useVM:.long	0
.comm	_endcore,4
.comm	_hdr,32
.comm	_tsize,4
.comm	_dsize,4
.comm	_datbase,4
.comm	_trsize,4
.comm	_drsize,4
.comm	_usedot,224
.comm	_dotp,4
.comm	_tokfile,4
.comm	_tokfilename,256
.comm	_strfile,4
.comm	_strfilename,256
.align	2
.globl	_strfilepos
_strfilepos:.long	0
.align	2
.globl	_outfile
_outfile:.data	2
L138:.ascii	"a.out\0"
.data
.long	L138
.comm	_a_out_file,4
.comm	_a_out_off,4
.comm	_usefile,32
.comm	_txtfil,4
.comm	_rusefile,32
.comm	_relfil,4
.comm	_relocfile,4
.align	2
.globl	_tmpdirprefix
_tmpdirprefix:.data	2
L143:.ascii	"/tmp/\0"
.data
.long	L143
.text
LL0:.align	1
.globl	_main
.data	1
L176:.ascii	"Caution\72 absolute origins.\12\0"
.text
.data	1
L179:.ascii	"Caution\72 G or H format floating point numbers\0"
.text
.data	1
L181:.ascii	"Caution\72 G or H format floating point operators\0"
.text
.data	1
L183:.ascii	"Caution\72 New Opcodes\0"
.text
.data	1
L185:.ascii	"These are not defined for all implementations of the VAX archite"
.ascii	"cture.\12\0"
.text
.set	L146,0x0
.data
.text
_main:.word	L146
clrb	_tokfilename
clrb	_strfilename
pushl	$0
calls	$1,_sbrk
movl	r0,_endcore
pushl	8(ap)
pushl	4(ap)
calls	$2,_argprocess
tstl	_anyerrs
jeql	L152
pushl	$1
calls	$1,_exit
L152:calls	$0,_initialize
calls	$0,_zeroorigins
calls	$0,_zerolocals
calls	$0,_i_pass1
calls	$0,_pass1
calls	$0,_testlocals
tstl	_anyerrs
jeql	L160
calls	$0,_delexit
L160:calls	$0,_pass1_5
tstl	_anyerrs
jeql	L162
calls	$0,_delexit
L162:calls	$0,_open_a_out
calls	$0,_roundsegments
calls	$0,_build_hdr
calls	$0,_i_pass2
calls	$0,_pass2
tstl	_anyerrs
jeql	L168
calls	$0,_delexit
L168:calls	$0,_fillsegments
calls	$0,_reloc_syms
calls	$0,_delete
calls	$0,_bflush
calls	$0,_fix_a_out
tstl	_anyerrs
jneq	L174
tstl	_orgwarn
jeql	L174
pushal	L176
calls	$1,_yyerror
L174:tstl	_nGHnumbers
jeql	L177
pushal	L179
calls	$1,_yywarning
L177:tstl	_nGHopcodes
jeql	L180
pushal	L181
calls	$1,_yywarning
L180:tstl	_nnewopcodes
jeql	L182
pushal	L183
calls	$1,_yywarning
L182:tstl	_nGHnumbers
jneq	L99999
tstl	_nGHopcodes
jneq	L99999
tstl	_nnewopcodes
jeql	L184
L99999:pushal	L185
calls	$1,_yywarning
L184:tstl	_anyerrs
jeql	L99998
movl	$1,r0
jbr	L99997
L99998:clrl	r0
L99997:pushl	r0
calls	$1,_exit
ret
.align	1
.globl	_argprocess
.data	1
L190:.ascii	"<argv error>\0"
.text
.data	1
L200:.ascii	"Unknown flag\72 %c\0"
.text
.data	1
L205:.ascii	"-d[124] only\0"
.text
.data	1
L212:.ascii	"-a\72 0<=align<=16\0"
.text
.data	1
L215:.ascii	"-o what???\0"
.text
.data	1
L220:.ascii	"-t what???\0"
.text
.set	L186,0x800
.data
.text
_argprocess:.word	L186
clrl	_ninfiles
clrl	_silent
pushl	$4
addl3	$1,4(ap),-(sp)
calls	$2,_ClearCalloc
movl	r0,_innames
moval	L190,_dotsname
jbr	L191
L2000000:movl	8(ap),r0
ashl	$2,_ninfiles,r1
addl2	_innames,r1
movl	4(r0),(r1)
incl	_ninfiles
L194:decl	4(ap)
addl2	$4,8(ap)
L191:cmpl	4(ap),$1
jleq	L192
movl	8(ap),r0
cvtbl	*4(r0),r0
cmpl	r0,$45
jneq	L2000000
movl	8(ap),r0
addl3	$1,4(r0),r11
jbr	L195
L2000005:jgtr	L228
cmpl	r0,$74
jeql	L224
jbr	L199
L206:addl3	$1,r11,-(sp)
calls	$1,_atoi
movl	r0,_maxalign
L2000001:incl	r11
cvtbl	(r11),r0
jbs	$2,__ctype_+1[r0],L2000001
cmpl	_maxalign,$16
jgtr	L99996
tstl	_maxalign
jgeq	L195
L99996:pushal	L212
jbr	L2000010
L218:cmpl	4(ap),$3
jgeq	L219
pushal	L220
calls	$1,_yyerror
pushl	$1
calls	$1,_exit
L219:movl	8(ap),r0
movl	8(r0),_tmpdirprefix
jbr	L216
L221:movl	$1,_useVM
jbr	L195
L224:movl	$1,_jxxxJUMP
jbr	L195
L225:movl	$1,_readonlydata
jbr	L195
L228:cmpl	r0,$82
jeql	L225
jbr	L199
L227:cmpl	r0,$97
jeql	L206
jgtr	L199
cmpl	r0,$87
jneq	L199
movl	$1,_silent
jbr	L195
L2000012:cmpl	r0,$86
jeql	L221
jgtr	L227
cmpl	r0,$76
jneq	L2000005
movl	$1,_savelabels
L195:tstb	(r11)
jeql	L194
cvtbl	(r11)+,r0
cmpl	r0,$100
jeql	L203
jleq	L2000012
cmpl	r0,$116
jeql	L218
jgtr	L229
cmpl	r0,$111
jneq	L199
cmpl	4(ap),$3
jgeq	L214
pushal	L215
calls	$1,_yyerror
pushl	$1
calls	$1,_exit
L214:movl	8(ap),r0
movl	8(r0),_outfile
L216:subl2	$2,4(ap)
addl2	$8,8(ap)
jbr	L191
L229:cmpl	r0,$118
jeql	L201
L199:cvtbl	-(r11),-(sp)
pushal	L200
calls	$2,_yyerror
incl	r11
jbr	L195
L201:pushal	__iob+20
calls	$1,_selfwhat
pushl	$1
calls	$1,_exit
L203:cvtbl	(r11)+,r0
subl3	$48,r0,_d124
cmpl	_d124,$1
jeql	L195
cmpl	_d124,$2
jeql	L195
cmpl	_d124,$4
jeql	L195
pushal	L205
L2000010:calls	$1,_yyerror
pushl	$1
calls	$1,_exit
jbr	L195
L192:ret
.align	1
.globl	_selfwhat
.set	L230,0xe00
.data
.text
_selfwhat:.word	L230
moval	_environ,r10
pushl	$0
calls	$1,_sbrk
movl	r0,r11
jbr	L237
L2000016:cmpb	(r10),$64
jneq	L235
cmpb	1(r10),$40
jneq	L235
cmpb	2(r10),$35
jneq	L235
cmpb	3(r10),$41
jneq	L235
pushl	4(ap)
pushl	$9
calls	$2,_fputc
addl2	$4,r10
jbr	L245
L2000014:tstb	(r10)
jeql	L244
cmpb	(r10),$62
jeql	L244
cmpb	(r10),$10
jeql	L244
pushl	4(ap)
cvtbl	(r10),-(sp)
calls	$2,_fputc
incl	r10
L245:cmpl	r10,r11
jlss	L2000014
L244:pushl	4(ap)
pushl	$10
calls	$2,_fputc
L235:incl	r10
L237:cmpl	r10,r11
jlss	L2000016
ret
.align	1
.globl	_initialize
.set	L249,0x0
.data
.text
_initialize:.word	L249
pushl	$1
pushl	$2
calls	$2,_signal
cmpl	r0,$1
jeql	L253
pushal	_delexit
pushl	$2
calls	$2,_signal
L253:calls	$0,_symtabinit
calls	$0,_syminstall
calls	$0,_buildtokensets
ret
.align	1
.globl	_zeroorigins
.set	L257,0x800
.data
.text
_zeroorigins:.word	L257
clrl	r11
L2000018:mull3	$28,r11,r0
cvtlb	$4,_usedot+20[r0]
addl3	$4,r11,r0
mull2	$28,r0
cvtlb	$6,_usedot+20[r0]
mull3	$28,r11,r0
clrl	_usedot(r0)
addl3	$4,r11,r0
mull2	$28,r0
clrl	_usedot(r0)
incl	r11
cmpl	r11,$4
jlss	L2000018
ret
.align	1
.globl	_zerolocals
.set	L264,0x800
.data
.text
_zerolocals:.word	L264
clrl	r11
L2000020:movl	$1,_lgensym[r11]
clrb	_genref[r11]
aobleq	$9,r11,L2000020
ret
.align	1
.globl	_i_pass1
.data	1
L277:.ascii	"T\0"
.text
.data	1
L278:.ascii	"S\0"
.text
.set	L271,0x0
.data
.text
_i_pass1:.word	L271
tstl	_useVM
jneq	L276
pushal	L277
pushal	_tokfilename
calls	$2,_tempopen
movl	r0,_tokfile
L276:pushal	L278
pushal	_strfilename
calls	$2,_tempopen
movl	r0,_strfile
movl	$4,_strfilepos
pushl	r0
pushl	$1
pushl	$4
pushal	_strfilepos
calls	$4,_fwrite
calls	$0,_inittokfile
calls	$0,_initijxxx
ret
.align	1
.globl	_tempopen
.data	1
L286:.ascii	"%s%sas%s%05d\0"
.text
.data	1
L288:.ascii	"/\0"
.text
.data	1
L289:.ascii	"\0"
.text
.data	1
L291:.ascii	"w\0"
.text
.data	1
L293:.ascii	"Bad pass 1 temporary file for writing %s\0"
.text
.set	L282,0x0
.data
.text
_tempopen:.word	L282
subl2	$4,sp
calls	$0,_getpid
pushl	r0
pushl	8(ap)
pushl	_tmpdirprefix
calls	$1,_strlen
decl	r0
movl	_tmpdirprefix,r1
cmpb	(r1)[r0],$47
jeql	L99995
moval	L288,r0
jbr	L99994
L99995:moval	L289,r0
L99994:pushl	r0
pushl	_tmpdirprefix
pushal	L286
pushl	4(ap)
calls	$6,_sprintf
pushal	L291
pushl	4(ap)
calls	$2,_fopen
movl	r0,-4(fp)
tstl	r0
jneq	L292
pushl	4(ap)
pushal	L293
calls	$2,_yyerror
calls	$0,_delexit
L292:movl	-4(fp),r0
ret
.align	1
.globl	_pass1
.data	1
L299:.ascii	"<stdin>\0"
.text
.data	1
L306:.ascii	"r\0"
.text
.data	1
L308:.ascii	"Can't open source file %s\12\0"
.text
.set	L294,0x800
.data
.text
_pass1:.word	L294
movl	$1,_passno
moval	_usedot,_dotp
clrl	_txtfil
clrl	_relfil
tstl	_ninfiles
jneq	L298
movl	$1,_lineno
moval	L299,_dotsname
calls	$0,_yyparse
L301:calls	$0,_closetokfile
ret
L298:clrl	r11
L304:cmpl	r11,_ninfiles
jgeq	L301
ashl	$2,r11,r0
addl2	_innames,r0
pushl	(r0)
calls	$1,_new_dot_s
pushal	__iob
pushal	L306
ashl	$2,r11,r0
addl2	_innames,r0
pushl	(r0)
calls	$3,_freopen
tstl	r0
jneq	L307
ashl	$2,r11,r0
addl2	_innames,r0
pushl	(r0)
pushal	L308
calls	$2,_yyerror
pushl	$2
calls	$1,_exit
L307:calls	$0,_yyparse
incl	r11
jbr	L304
.align	1
.globl	_testlocals
.data	1
L318:.ascii	"Reference to undefined local label %df\0"
.text
.set	L310,0x800
.data
.text
_testlocals:.word	L310
clrl	r11
L2000022:tstb	_genref[r11]
jeql	L317
pushl	r11
pushal	L318
calls	$2,_yyerror
L317:movl	$1,_lgensym[r11]
clrb	_genref[r11]
acbl	$9,$1,r11,L2000022
ret
.align	1
.globl	_pass1_5
.set	L319,0x0
.data
.text
_pass1_5:.word	L319
calls	$0,_sortsymtab
calls	$0,_jxxxfix
ret
.align	1
.globl	_open_a_out
.data	1
L329:.ascii	"w\0"
.text
.data	1
L331:.ascii	"Cannot create %s\0"
.text
.set	L325,0x0
.data
.text
_open_a_out:.word	L325
movab	-64(sp),sp
pushal	L329
pushl	_outfile
calls	$2,_fopen
movl	r0,_a_out_file
tstl	r0
jneq	L330
pushl	_outfile
pushal	L331
calls	$2,_yyerror
calls	$0,_delexit
L330:movl	_a_out_file,r0
cvtbl	18(r0),_biofd
pushal	-64(fp)
pushl	_biofd
calls	$2,_fstat
movl	-16(fp),_biobufsize
clrl	_a_out_off
ret
.align	1
.globl	_roundsegments
.set	L333,0xc00
.data
.text
_roundsegments:.word	L333
clrl	_tsize
clrl	r11
L2000024:mull3	$28,r11,r0
addl3	$3,_usedot(r0),r0
bicl3	$3,r0,r10
mull3	$28,r11,r0
movl	_tsize,_usedot(r0)
tstl	r11
jeql	L99993
tstl	r10
jeql	L340
L99993:pushl	$20
pushl	$1
calls	$2,_Calloc
movl	r0,_usefile[r11]
pushl	_a_out_off
pushl	_usefile[r11]
calls	$2,_bopen
tstl	r11
jneq	L343
movl	$32,_a_out_off
jbr	L343
L340:mnegl	$1,_usefile[r11]
L343:addl2	r10,_tsize
addl2	r10,_a_out_off
incl	r11
cmpl	r11,$4
jlss	L2000024
addl3	$3,_tsize,r0
bicl3	$3,r0,_datbase
clrl	r11
L2000026:addl3	$4,r11,r0
mull2	$28,r0
addl3	$3,_usedot(r0),r0
bicl3	$3,r0,r10
addl3	_dsize,_datbase,r0
addl3	$4,r11,r1
mull2	$28,r1
movl	r0,_usedot(r1)
tstl	r10
jeql	L347
pushl	$20
pushl	$1
calls	$2,_Calloc
addl3	$4,r11,r1
movl	r0,_usefile[r1]
pushl	_a_out_off
addl3	$4,r11,r0
pushl	_usefile[r0]
calls	$2,_bopen
jbr	L348
L347:addl3	$4,r11,r0
mnegl	$1,_usefile[r0]
L348:addl2	r10,_dsize
addl2	r10,_a_out_off
incl	r11
cmpl	r11,$4
jlss	L2000026
movl	_dsize,_hdr+12
calls	$0,_freezesymtab
calls	$0,_stabfix
clrl	r11
L2000028:clrl	_rusefile[r11]
aoblss	$8,r11,L2000028
ret
.align	1
.globl	_build_hdr
.set	L354,0x0
.data
.text
_build_hdr:.word	L354
cvtwl	$263,_hdr
movl	_tsize,_hdr+4
movl	_dsize,_hdr+8
subl2	_dsize,_hdr+12
calls	$0,_sizesymtab
movl	r0,_hdr+16
clrl	_hdr+20
clrl	_hdr+24
clrl	_hdr+28
pushl	_usefile
pushl	$32
pushal	_hdr
calls	$3,_bwrite
ret
.align	1
.globl	_i_pass2
.data	1
L366:.ascii	"r\0"
.text
.data	1
L368:.ascii	"Bad pass 2 temporary file for reading %s\0"
.text
.data	1
L369:.ascii	"r\0"
.text
.set	L360,0x0
.data
.text
_i_pass2:.word	L360
tstl	_useVM
jneq	L364
pushl	_tokfile
calls	$1,_fclose
pushal	L366
pushal	_tokfilename
calls	$2,_fopen
movl	r0,_tokfile
tstl	r0
jneq	L364
pushal	_tokfilename
pushal	L368
calls	$2,_yyerror
calls	$0,_delexit
L364:pushl	_strfile
calls	$1,_fclose
pushal	L369
pushal	_strfilename
calls	$2,_fopen
movl	r0,_strfile
ret
.align	1
.globl	_pass2
.set	L370,0x0
.data
.text
_pass2:.word	L370
movl	$2,_passno
movl	$1,_lineno
moval	_usedot,_dotp
movl	_usefile,_txtfil
clrl	_relfil
calls	$0,_initoutrel
calls	$0,_inittokfile
calls	$0,_yyparse
calls	$0,_closetokfile
ret
.align	1
.globl	_fillsegments
.set	L375,0x0
.data
.text
_fillsegments:.word	L375
subl2	$4,sp
clrl	-4(fp)
L2000034:movl	-4(fp),r0
tstl	_usefile[r0]
jeql	L379
movl	_usefile[r0],_txtfil
mull3	$28,r0,r0
movab	_usedot[r0],_dotp
jbr	L383
L2000030:decw	*_txtfil
movl	_txtfil,r0
movl	4(r0),r1
incl	4(r0)
clrb	(r1)
jbr	L383
L2000032:incl	*_dotp
cmpl	_passno,$2
jneq	L383
tstw	*_txtfil
jneq	L2000030
pushl	$0
pushl	_txtfil
calls	$2,_bflushc
L383:mull3	$28,-4(fp),r0
bitl	$3,_usedot(r0)
jneq	L2000032
L379:incl	-4(fp)
cmpl	-4(fp),$8
jlss	L2000034
ret
.align	1
.globl	_reloc_syms
.set	L387,0x0
.data
.text
_reloc_syms:.word	L387
pushl	$20
pushl	$1
calls	$2,_Calloc
movl	r0,_relocfile
pushl	_a_out_off
pushl	r0
calls	$2,_bopen
pushl	_relocfile
calls	$1,_closeoutrel
addl2	r0,_a_out_off
movl	_trsize,_hdr+24
movl	_drsize,_hdr+28
tstl	_readonlydata
jeql	L393
addl2	_hdr+8,_hdr+4
clrl	_hdr+8
addl2	_hdr+28,_hdr+24
clrl	_hdr+28
L393:pushl	$0
pushl	$0
pushl	_strfile
calls	$3,_fseek
pushl	_relocfile
calls	$1,_symwrite
ret
.align	1
.globl	_fix_a_out
.data	1
L402:.ascii	"Reposition for header rewrite fails\0"
.text
.data	1
L405:.ascii	"Rewrite of header fails\0"
.text
.set	L396,0x0
.data
.text
_fix_a_out:.word	L396
pushl	$0
pushl	$0
movl	_a_out_file,r0
cvtbl	18(r0),-(sp)
calls	$3,_lseek
tstl	r0
jgeq	L401
pushal	L402
calls	$1,_yyerror
L401:pushl	$32
pushal	_hdr
movl	_a_out_file,r0
cvtbl	18(r0),-(sp)
calls	$3,_write
tstl	r0
jgeq	L404
pushal	L405
calls	$1,_yyerror
L404:ret
.align	1
.globl	_delexit
.set	L406,0x0
.data
.text
_delexit:.word	L406
calls	$0,_delete
cmpl	_passno,$2
jneq	L410
pushl	_outfile
calls	$1,_unlink
L410:pushl	$1
calls	$1,_exit
ret
.align	1
.globl	_delete
.set	L412,0x0
.data
.text
_delete:.word	L412
tstl	_useVM
jeql	L99990
tstb	_tokfilename
jeql	L416
L99990:pushal	_tokfilename
calls	$1,_unlink
L416:tstb	_strfilename
jeql	L417
pushal	_strfilename
calls	$1,_unlink
L417:ret
.align	1
.globl	_sawabort
.set	L419,0x0
.data
.text
_sawabort:.word	L419
L2000035:calls	$0,_fillinbuffer
tstl	r0
jneq	L2000035
calls	$0,_delete
pushl	$1
calls	$1,_exit
ret
.align	1
.globl	_panic
.data	1
L431:.ascii	"Assembler panic\72 bad internal data structure.\0"
.text
.set	L427,0x0
.data
.text
_panic:.word	L427
pushal	L431
calls	$1,_yyerror
pushl	20(ap)
pushl	16(ap)
pushl	12(ap)
pushl	8(ap)
pushl	4(ap)
calls	$5,_yyerror
calls	$0,_delete
calls	$0,_abort
ret

