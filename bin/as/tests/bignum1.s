.data
.data
_sccsid:.long	0x29232840
.long	0x6e676962
.long	0x2e316d75
.long	0x2e350963
.long	0x42282031
.long	0x656b7265
.long	0x2979656c
.long	0x332f3420
.long	0x35382f30
.long	0x0
.comm	_rusefile,32
.comm	_relfil,4
.text
LL0:.align	1
.globl	_as_atof
.data	1
L121:.ascii	"Floating conversion over/underflowed\12\0"
.text
.lcomm	L122,20
.set	L105,0x0
.data
.text
_as_atof:.word	L105
subl2	$20,sp
movc3	$20,_Znumber,-20(fp)
clrl	_errno
movl	8(ap),r0
casel	r0,$5,$3
L2000000:
.word	L114-L2000000
.word	L114-L2000000
.word	L116-L2000000
.word	L116-L2000000
L111:cmpl	_errno,$34
jneq	L119
cmpl	_passno,$2
jneq	L119
pushal	L121
calls	$1,_yywarning
L119:moval	-20(fp),r0
movab	L122,r1
movc3	$20,(r0),(r1)
movab	L122,r0
ret
L114:cvtlb	$6,-4(fp)
clrl	*12(ap)
pushl	4(ap)
calls	$1,_atof
movd	r0,-20(fp)
jbr	L111
L116:pushl	8(ap)
pushl	4(ap)
calls	$2,_bigatof
movc3	$20,(r0),-20(fp)
jbr	L111
.align	1
.globl	_as_atoi
.data
L180:.long	0x2020100
.long	0x3030303
.long	0x4040404
.long	0x4040404
.long	0x3030201
.long	0x4040404
.space	8
.long	0x0
.space	12
.long	0x4040302
.space	12
.text
.data	1
L184:.ascii	"%s%s\12\0"
.text
.data	1
L185:.ascii	"n_n.num_tag != 0\0"
.text
.data	1
L186:.ascii	"Botch width computation\0"
.text
.lcomm	L187,20
.set	L123,0xe00
.data
.text
_as_atoi:.word	L123
subl2	$56,sp
movl	4(ap),r11
clrl	-56(fp)
clrl	-52(fp)
L129:tstb	(r11)
jeql	L128
cvtbl	(r11),r0
cmpl	r0,$43
jeql	L127
cmpl	r0,$45
jeql	L134
cmpl	r0,$48
jeql	L127
L128:movc3	$20,_Znumber,-28(fp)
movc3	$20,_Znumber,-48(fp)
moval	-28(fp),r10
pushl	r10
calls	$1,_numclear
moval	-48(fp),-4(fp)
pushl	-4(fp)
calls	$1,_numclear
L137:tstb	(r11)
jeql	L143
cvtbl	(r11),r0
casel	r0,$48,$54
L2000008:
.word	L151-L2000008
.word	L151-L2000008
.word	L151-L2000008
.word	L151-L2000008
.word	L151-L2000008
.word	L151-L2000008
.word	L151-L2000008
.word	L151-L2000008
.word	L141-L2000008
.word	L141-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L157-L2000008
.word	L157-L2000008
.word	L157-L2000008
.word	L157-L2000008
.word	L157-L2000008
.word	L157-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L143-L2000008
.word	L164-L2000008
.word	L164-L2000008
.word	L164-L2000008
.word	L164-L2000008
.word	L164-L2000008
.word	L164-L2000008
L143:pushl	r10
calls	$1,_posovf
bisl2	r0,-56(fp)
tstl	-52(fp)
jeql	L173
jbc	$3,-56(fp),L174
bicl2	$12,-56(fp)
jbr	L173
L134:xorl2	$1,-52(fp)
L127:incl	r11
jbr	L129
L141:cmpl	8(ap),$10
jlss	L143
L151:cvtbl	(r11),r0
subl3	$48,r0,-8(fp)
jbr	L138
L2000002:cmpl	r0,$10
jeql	L172
cmpl	r0,$16
jeql	L171
L168:pushl	-8(fp)
pushl	r10
pushl	r10
calls	$3,_numaddd
bisl2	r0,-56(fp)
incl	r11
jbr	L137
L157:cmpl	8(ap),$16
jlss	L143
cvtbl	(r11),r0
subl3	$55,r0,-8(fp)
jbr	L138
L164:cmpl	8(ap),$16
jlss	L143
cvtbl	(r11),r0
subl3	$87,r0,-8(fp)
L138:movl	8(ap),r0
cmpl	r0,$8
jneq	L2000002
pushl	r10
pushl	r10
pushl	$3
L2000006:calls	$3,_numshift
L2000007:bisl2	r0,-56(fp)
jbr	L168
L171:pushl	r10
pushl	r10
pushl	$4
jbr	L2000006
L172:pushl	r10
pushl	-4(fp)
pushl	$1
calls	$3,_numshift
bisl2	r0,-56(fp)
pushl	r10
pushl	r10
pushl	$3
calls	$3,_numshift
bisl2	r0,-56(fp)
pushl	r10
pushl	-4(fp)
pushl	r10
calls	$3,_numaddv
jbr	L2000007
L174:pushl	r10
pushl	r10
calls	$2,_numnegate
bisl2	r0,-56(fp)
L173:tstl	-52(fp)
jeql	L99999
mnegl	$1,r0
jbr	L99998
L99999:clrl	r0
L99998:movl	r0,-8(fp)
clrl	r9
L2000004:cmpl	(r10)[r9],-8(fp)
jeql	L177
aoblss	$4,r9,L2000004
L177:sobgeq	r9,L181
clrl	r9
L181:movb	L180+48[r9],-12(fp)
jneq	L182
pushal	L186
pushal	L185
pushal	L184
calls	$3,_panic
L182:movl	-56(fp),*12(ap)
moval	-28(fp),r0
movab	L187,r1
movc3	$20,(r0),(r1)
movab	L187,r0
ret
.align	1
.globl	_posovf
.set	L188,0xc00
.data
.text
_posovf:.word	L188
subl2	$4,sp
movl	4(ap),r11
clrl	-4(fp)
jbc	$31,12(r11),L192
movl	$4,-4(fp)
L192:cmpl	12(r11),$-2147483648
jneq	L2000011
movl	$2,r10
jbr	L196
L2000010:tstl	(r11)[r10]
jeql	L194
L2000011:movl	-4(fp),r0
ret
L194:decl	r10
L196:tstl	r10
jgeq	L2000010
bisl2	$8,-4(fp)
jbr	L2000011
.align	1
.globl	_isclear
.set	L199,0x800
.data
.text
_isclear:.word	L199
movl	4(ap),r11
pushal	_Znumber
pushl	r11
calls	$2,_isunequal
tstl	r0
jneq	L99997
movl	$1,r0
jbr	L99996
L99997:clrl	r0
L99996:ret
.align	1
.globl	_isunequal
.set	L204,0xe00
.data
.text
_isunequal:.word	L204
movl	4(ap),r11
movl	8(ap),r10
movl	$4,r9
L210:cmpl	(r11)+,(r10)+
jeql	L209
movl	r9,r0
ret
L209:decl	r9
jneq	L210
clrl	r0
ret
.align	1
.globl	_numclear
.set	L212,0xc00
.data
.text
_numclear:.word	L212
movl	4(ap),r11
movl	$4,r10
L218:clrl	(r11)+
decl	r10
jneq	L218
clrl	r0
ret
.align	1
.globl	_numshift
.set	L219,0xfc0
.data
.text
_numshift:.word	L219
subl2	$4,sp
movl	8(ap),r11
movl	12(ap),r10
movl	$4,r9
tstl	4(ap)
jneq	L223
L226:movl	(r10)+,(r11)+
decl	r9
jneq	L226
clrl	r0
ret
L223:clrl	r8
ashl	4(ap),$1,r0
subl3	$1,r0,r6
tstl	4(ap)
jleq	L227
L230:movl	(r10)+,-4(fp)
subl3	4(ap),$32,r0
movl	-4(fp),r1
subl3	r0,$32,r2
extzv	r0,r2,r1,r1
mcoml	r6,r0
bicl3	r0,r1,r7
ashl	4(ap),-4(fp),-4(fp)
bicl2	r6,-4(fp)
bisl3	r8,-4(fp),(r11)+
movl	r7,r8
decl	r9
jneq	L230
tstl	r8
jeql	L99995
movl	$2,r0
jbr	L99994
L99995:clrl	r0
L99994:ret
L227:mnegl	4(ap),4(ap)
addl2	$16,r10
addl2	$16,r11
L233:movl	-(r10),-4(fp)
mcoml	r6,r0
bicl3	r0,-4(fp),r7
movl	-4(fp),r0
subl3	4(ap),$32,r1
extzv	4(ap),r1,r0,-4(fp)
subl3	4(ap),$32,r0
ashl	r0,$1,r0
decl	r0
mcoml	r0,r0
bicl2	r0,-4(fp)
bisl3	r8,-4(fp),-(r11)
subl3	4(ap),$32,r0
ashl	r0,r7,r8
decl	r9
jneq	L233
tstl	r8
jeql	L99993
movl	$2,r0
jbr	L99992
L99993:clrl	r0
L99992:ret
.align	1
.globl	_numaddd
.lcomm	L238,20
.set	L234,0x0
.data
.text
_numaddd:.word	L234
cvtlb	12(ap),L238
pushal	L238
pushl	8(ap)
pushl	4(ap)
calls	$3,_numaddv
ret
.align	1
.globl	_numaddv
.set	L239,0xfc0
.data
.text
_numaddv:.word	L239
subl2	$8,sp
movl	4(ap),r11
movl	8(ap),r10
movl	12(ap),r9
clrl	r7
movl	$4,r8
L245:movl	(r10)+,r6
movl	(r9)+,-4(fp)
addl3	r6,-4(fp),r0
addl3	r7,r0,-8(fp)
movl	-8(fp),(r11)+
clrl	r7
cmpl	-8(fp),r6
jlssu	L99991
cmpl	-8(fp),-4(fp)
jgequ	L244
L99991:movl	$1,r7
L244:decl	r8
jneq	L245
tstl	r7
jeql	L99990
movl	$1,r0
jbr	L99989
L99990:clrl	r0
L99989:ret
.align	1
.globl	_numnegate
.set	L247,0x0
.data
.text
_numnegate:.word	L247
subl2	$4,sp
pushl	8(ap)
pushl	4(ap)
calls	$2,_num1comp
movl	r0,-4(fp)
pushl	$1
pushl	4(ap)
pushl	4(ap)
calls	$3,_numaddd
bisl2	r0,-4(fp)
movl	-4(fp),r0
ret
.align	1
.globl	_num1comp
.set	L251,0xe00
.data
.text
_num1comp:.word	L251
movl	4(ap),r11
movl	8(ap),r10
movl	$4,r9
L257:mcoml	(r10)+,(r11)+
decl	r9
jneq	L257
clrl	r0
ret
.align	1
.globl	_slitflt
.data	1
L265:.ascii	"%s%s\12\0"
.text
.data	1
L266:.ascii	"ovf == 0\0"
.text
.data	1
L267:.ascii	"overflow in unpacking floating #!?\0"
.text
.set	L259,0x800
.data
.text
_slitflt:.word	L259
subl2	$28,sp
clrl	*28(ap)
movl	24(ap),r0
tstl	_ty_float[r0]
jneq	L263
L2000014:clrl	r0
ret
L263:pushal	-28(fp)
subl2	$20,sp
movc3	$20,4(ap),(sp)
calls	$6,_bignumunpack
movc3	$20,(r0),-24(fp)
tstl	-28(fp)
jeql	L264
pushal	L267
pushal	L266
pushal	L265
calls	$3,_panic
L264:tstb	-7(fp)
jneq	L2000014
tstw	-6(fp)
jlss	L2000014
cmpw	-6(fp),$7
jgtr	L2000014
clrl	r11
L2000013:tstl	-24(fp)[r11]
jneq	L2000014
aoblss	$3,r11,L2000013
bitl	$536870911,-12(fp)
jneq	L2000014
extzv	$0,$3,-6(fp),r0
ashl	$3,r0,*28(ap)
movl	-12(fp),r0
extzv	$29,$3,r0,-4(fp)
extzv	$0,$3,-4(fp),-4(fp)
bisl2	-4(fp),*28(ap)
extzv	$0,$6,*28(ap),*28(ap)
movl	$1,r0
ret
.align	1
.globl	_bignumwrite
.set	L277,0x800
.data
.text
_bignumwrite:.word	L277
cmpl	_passno,$2
jeql	L281
ret
L281:addl3	$4,ap,r11
cvtbl	20(ap),r0
casel	r0,$0,$4
L2000016:
.word	L288-L2000016
.word	L288-L2000016
.word	L288-L2000016
.word	L288-L2000016
.word	L288-L2000016
pushl	24(ap)
subl2	$20,sp
movc3	$20,4(ap),(sp)
calls	$6,_floatconvert
L2000015:movc3	$20,(r0),4(ap)
pushl	_txtfil
movl	24(ap),r0
pushl	_ty_nbyte[r0]
pushl	r11
calls	$3,_bwrite
ret
L288:pushl	24(ap)
subl2	$20,sp
movc3	$20,4(ap),(sp)
calls	$6,_intconvert
jbr	L2000015

