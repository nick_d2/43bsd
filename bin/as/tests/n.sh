#!/bin/sh

for i in asscan1.s asscan2.s asscan3.s asscan4.s bignum1.s bignum2.s natof.s floattab.s asparse.s asexpr.s asmain.s assyms.s asjxxx.s ascode.s assizetab.s asio.s
do
  o=`basename $i .s`.o
  ../as -o $o $i
  diff -q $o $o.ok
done
../as -o aspseudo.o -R aspseudo.s
diff -q aspseudo.o aspseudo.o.ok

cp crt0.o.ok crt0.o
cp libc.a.ok libc.a
touch --date="01 Jan 1980 12:00:00" libc.a

../../ld -X crt0.o -o as asscan1.o asscan2.o asscan3.o asscan4.o bignum1.o bignum2.o natof.o floattab.o asparse.o asexpr.o asmain.o assyms.o asjxxx.o ascode.o aspseudo.o assizetab.o asio.o -L. -lc
diff -q as as.ok
