.data
.data
_sccsid:.long	0x29232840
.long	0x6f697361
.long	0x3509632e
.long	0x2820312e
.long	0x6b726542
.long	0x79656c65
.long	0x2f342029
.long	0x382f3033
.long	0x35
.comm	_rusefile,32
.comm	_relfil,4
.comm	_biofd,4
.comm	_biobufsize,4
.comm	_boffset,4
.comm	_biobufs,4
.text
LL0:.align	1
.globl	_Flushfield
.set	L106,0x800
.data
.text
_Flushfield:.word	L106
movl	4(ap),r11
jbr	L110
L2000001:incl	*_dotp
cmpl	_passno,$2
jneq	L112
tstw	*_txtfil
jeql	L99999
decw	*_txtfil
movl	_txtfil,r0
movl	4(r0),r1
incl	4(r0)
cvtlb	_bitfield,(r1)
jbr	L112
L99999:pushl	_bitfield
pushl	_txtfil
calls	$2,_bflushc
L112:ashl	$-8,_bitfield,_bitfield
subl2	$8,r11
L110:tstl	r11
jgtr	L2000001
clrl	_bitoff
clrl	_bitfield
ret
.align	1
.globl	_bopen
.set	L115,0x0
.data
.comm	_bwrerror,4
.text
_bopen:.word	L115
pushl	_biobufsize
pushl	$1
calls	$2,_Calloc
movl	4(ap),r1
movl	r0,8(r1)
movl	4(ap),r0
movl	8(r1),4(r0)
divl3	_biobufsize,8(ap),r0
mull2	_biobufsize,r0
subl3	r0,8(ap),r0
subl3	r0,_biobufsize,r0
cvtlw	r0,*4(ap)
movl	4(ap),r0
movl	8(ap),12(r0)
movl	4(ap),r0
movl	_biobufs,16(r0)
movl	4(ap),_biobufs
ret
.align	1
.globl	_bwrite
.data	1
L137:.ascii	"Output write error\0"
.text
.set	L121,0xf80
.data
.text
_bwrite:.word	L121
movl	4(ap),r11
movl	8(ap),r10
movl	12(ap),r9
jbr	L125
L2000003:cvtwl	(r9),r8
cmpl	r8,r10
jleq	L128
movl	r10,r8
L128:cvtwl	(r9),r0
subl2	r8,r0
cvtlw	r0,(r9)
movl	4(r9),r7
movc3	r8,(r11),(r7)
addl2	r8,4(r9)
L2000005:addl2	r8,r11
subl2	r8,r10
jbr	L125
L2000007:cmpl	4(r9),8(r9)
jeql	L130
pushl	r9
calls	$1,_bflush1
L130:divl3	_biobufsize,r10,r0
mull2	_biobufsize,r0
subl3	r0,r10,r0
subl3	r0,r10,r8
cmpl	_boffset,12(r9)
jeql	L132
pushl	$0
pushl	12(r9)
pushl	_biofd
calls	$3,_lseek
L132:pushl	r8
pushl	r11
pushl	_biofd
calls	$3,_write
cmpl	r0,r8
jeql	L135
movl	$1,_bwrerror
pushal	L137
calls	$1,_yyerror
calls	$0,_delexit
L135:addl2	r8,12(r9)
movl	12(r9),_boffset
jbr	L2000005
L126:tstw	(r9)
jneq	L2000003
cmpl	r10,_biobufsize
jgeq	L2000007
pushl	r9
calls	$1,_bflush1
L125:tstl	r10
jneq	L126
ret
.align	1
.globl	_bflush
.set	L141,0x800
.data
.text
_bflush:.word	L141
tstl	_bwrerror
jeql	L145
ret
L145:movl	_biobufs,r11
jbr	L148
L2000009:pushl	r11
calls	$1,_bflush1
movl	16(r11),r11
L148:tstl	r11
jneq	L2000009
ret
.align	1
.globl	_bflush1
.data	1
L156:.ascii	"Output write error\0"
.text
.set	L149,0xc00
.data
.text
_bflush1:.word	L149
movl	4(ap),r11
subl3	8(r11),4(r11),r0
movl	r0,r10
jneq	L153
ret
L153:cmpl	_boffset,12(r11)
jeql	L154
pushl	$0
pushl	12(r11)
pushl	_biofd
calls	$3,_lseek
L154:pushl	r10
pushl	8(r11)
pushl	_biofd
calls	$3,_write
cmpl	r0,r10
jeql	L155
movl	$1,_bwrerror
pushal	L156
calls	$1,_yyerror
calls	$0,_delexit
L155:addl2	r10,12(r11)
movl	12(r11),_boffset
movl	8(r11),4(r11)
cvtlw	_biobufsize,(r11)
ret
.align	1
.globl	_bflushc
.set	L158,0x800
.data
.text
_bflushc:.word	L158
movl	4(ap),r11
pushl	r11
calls	$1,_bflush1
tstw	(r11)
jeql	L99997
decw	(r11)
movl	4(r11),r0
incl	4(r11)
movb	8(ap),(r0)
cvtbl	(r0),r0
jbr	L99996
L99997:cvtbl	8(ap),-(sp)
pushl	r11
calls	$2,_bflushc
L99996:ret

