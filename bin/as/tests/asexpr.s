.data
.data
_sccsid:.long	0x29232840
.long	0x78657361
.long	0x632e7270
.long	0x322e3509
.long	0x65422820
.long	0x6c656b72
.long	0x20297965
.long	0x39312f36
.long	0x35382f
.comm	_rusefile,32
.comm	_relfil,4
.globl	_pltab
_pltab:.long	0x0
.long	0x2000000
.long	0x1080604
.long	0xffff0400
.long	0x600ffff
.long	0xffffffff
.long	0xffff0800
.long	0x100ffff
.long	0xffffffff
.globl	_mintab
_mintab:.long	0x0
.long	0x2000000
.long	0xffffffff
.long	0xff020400
.long	0x600ffff
.long	0xffff02ff
.long	0xffff0800
.long	0x100ff02
.long	0xffffffff
.globl	_othtab
_othtab:.long	0x0
.long	0x2000000
.long	0xffffffff
.long	0xffffff00
.long	0xff00ffff
.long	0xffffffff
.long	0xffffff00
.long	0xff00ffff
.long	0xffffffff
.text
LL0:.align	1
.globl	_combine
.data	1
L123:.ascii	"The assembler can only do arithmetic on 1,2, or 4 byte integers\0"
.text
.data	1
L156:.ascii	"Divide check\0"
.text
.data	1
L160:.ascii	"Divide check (modulo)\0"
.text
.data	1
L163:.ascii	"Internal error\72 unknown operator\0"
.text
.data	1
L167:.ascii	"Relocation error\0"
.text
.set	L119,0xf80
.data
.text
_combine:.word	L119
subl2	$4,sp
movl	8(ap),r11
movl	12(ap),r10
moval	L123,-4(fp)
clrl	_lastnam
cvtbl	20(r11),r0
bicl3	$-31,r0,r9
cvtbl	20(r10),r0
bicl3	$-31,r0,r8
cmpb	20(r11),$1
jneq	L124
movl	$10,r9
L124:cmpb	20(r10),$1
jneq	L125
movl	$10,r8
L125:cmpl	_passno,$1
jneq	L126
cmpb	21(r11),21(r10)
jeql	L126
cmpl	r9,r8
jneq	L126
movl	$10,r8
movl	r8,r9
L126:ashl	$-1,r9,r9
ashl	$-1,r8,r8
cvtbl	16(r11),r0
cmpl	r0,$0
jeql	L128
cmpl	r0,$1
jeql	L128
cmpl	r0,$2
jeql	L128
L2000002:pushl	-4(fp)
L2000007:calls	$1,_yyerror
L2000003:movl	r11,r0
ret
L128:cvtbl	16(r10),r0
cmpl	r0,$0
jeql	L135
cmpl	r0,$1
jeql	L135
cmpl	r0,$2
jneq	L2000002
L135:movl	4(ap),r0
casel	r0,$45,$26
L2000010:
.word	L153-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L158-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L150-L2000010
.word	L151-L2000010
.word	L144-L2000010
.word	L162-L2000010
.word	L147-L2000010
.word	L154-L2000010
.word	L162-L2000010
.word	L162-L2000010
.word	L143-L2000010
.word	L145-L2000010
.word	L148-L2000010
.word	L149-L2000010
.word	L149-L2000010
L162:pushal	L163
calls	$1,_yyerror
L141:cmpl	r8,$5
jneq	L165
movl	24(r10),24(r11)
L165:cvtbl	20(r11),r0
cvtbl	20(r10),r1
bisl2	r1,r0
bicl2	$-34,r0
bisl2	r7,r0
cvtlb	r0,20(r11)
cmpl	r7,$-1
jneq	L2000003
pushal	L167
jbr	L2000007
L143:addl2	(r10),(r11)
mull3	$6,r9,r0
addl2	$_pltab,r0
L2000004:cvtbl	(r0)[r8],r7
jbr	L141
L144:subl2	(r10),(r11)
mull3	$6,r9,r0
addl2	$_mintab,r0
jbr	L2000004
L145:bisl2	(r10),(r11)
L146:mull3	$6,r9,r0
addl2	$_othtab,r0
jbr	L2000004
L147:xorl2	(r10),(r11)
jbr	L146
L148:mcoml	(r10),r0
bicl2	r0,(r11)
jbr	L146
L149:mcoml	(r10),r0
bisl2	r0,(r11)
jbr	L146
L150:ashl	(r10),(r11),(r11)
jbr	L146
L151:mnegl	(r10),r0
ashl	r0,(r11),(r11)
jbr	L146
L153:mull2	(r10),(r11)
jbr	L146
L154:tstl	(r10)
jneq	L155
pushal	L156
jbr	L2000006
L2000009:pushal	L160
L2000006:calls	$1,_yyerror
jbr	L146
L155:divl2	(r10),(r11)
jbr	L146
L158:tstl	(r10)
jeql	L2000009
movl	(r11),r0
divl3	(r10),r0,r1
mull2	(r10),r1
subl3	r1,r0,(r11)
jbr	L146
.align	1
.globl	_buildtokensets
.set	L169,0x0
.data
.lcomm	_val,4
.text
_buildtokensets:.word	L169
bisb2	$1,_tokensets+65
bisb2	$1,_tokensets+50
bisb2	$1,_tokensets+39
bisb2	$5,_tokensets+41
bisb2	$4,_tokensets+33
bisb2	$4,_tokensets+32
bisb2	$4,_tokensets+44
bisb2	$4,_tokensets+34
bisb2	$8,_tokensets+39
bisb2	$8,_tokensets+40
bisb2	$16,_tokensets+67
bisb2	$18,_tokensets+61
bisb2	$2,_tokensets+47
bisb2	$32,_tokensets+68
bisb2	$32,_tokensets+63
bisb2	$32,_tokensets+69
bisb2	$32,_tokensets+71
bisb2	$66,_tokensets+70
bisb2	$64,_tokensets+59
bisb2	$64,_tokensets+60
bisb2	$64,_tokensets+45
bisb2	$64,_tokensets+64
bisb2	$64,_tokensets+49
ret
.align	1
.globl	_exprparse
.set	L175,0x800
.data
.text
_exprparse:.word	L175
subl2	$4,sp
movl	4(ap),_val
calls	$0,_boolterm
L2000011:movl	r0,r11
movl	_val,r0
jbs	$4,_tokensets[r0],L2000013
movl	r11,*8(ap)
movl	_val,r0
ret
L2000013:movl	_val,-4(fp)
calls	$0,_yylex
movl	r0,_val
calls	$0,_boolterm
pushl	r0
pushl	r11
pushl	-4(fp)
calls	$3,_combine
jbr	L2000011
.align	1
.globl	_boolterm
.set	L181,0x800
.data
.text
_boolterm:.word	L181
subl2	$4,sp
calls	$0,_term
L2000014:movl	r0,r11
movl	_val,r0
jbs	$5,_tokensets[r0],L2000016
movl	r11,r0
ret
L2000016:movl	_val,-4(fp)
calls	$0,_yylex
movl	r0,_val
calls	$0,_term
pushl	r0
pushl	r11
pushl	-4(fp)
calls	$3,_combine
jbr	L2000014
.align	1
.globl	_term
.set	L187,0x800
.data
.text
_term:.word	L187
subl2	$4,sp
calls	$0,_factor
L2000017:movl	r0,r11
movl	_val,r0
jbs	$6,_tokensets[r0],L2000019
movl	r11,r0
ret
L2000019:movl	_val,-4(fp)
calls	$0,_yylex
movl	r0,_val
calls	$0,_factor
pushl	r0
pushl	r11
pushl	-4(fp)
calls	$3,_combine
jbr	L2000017
.align	1
.globl	_factor
.data	1
L202:.ascii	"right parenthesis expected\0"
.text
.data	1
L211:.ascii	"Bad expression syntax\0"
.text
.set	L193,0x0
.data
.text
_factor:.word	L193
subl2	$8,sp
cmpl	_val,$47
jeql	L99999
tstl	_droppedLP
jeql	L198
L99999:tstl	_droppedLP
jeql	L199
clrl	_droppedLP
jbr	L200
L199:calls	$0,_yylex
movl	r0,_val
L200:pushal	-4(fp)
pushl	_val
calls	$2,_exprparse
movl	r0,_val
cmpl	r0,$75
jeql	L201
pushal	L202
calls	$1,_yyerror
jbr	L204
L198:movl	_val,r0
jbc	$2,_tokensets[r0],L205
pushl	_yylval
pushl	r0
calls	$2,_yukkyexpr
movl	r0,-4(fp)
jbr	L201
L2000023:movl	_yylval,-4(fp)
L201:calls	$0,_yylex
movl	r0,_val
jbr	L204
L205:movl	_val,r0
jbs	$3,_tokensets[r0],L2000023
cmpl	r0,$70
jeql	L99998
cmpl	r0,$61
jneq	L209
L99998:movl	_val,-8(fp)
calls	$0,_yylex
movl	r0,_val
movl	_xp,-4(fp)
addl2	$28,_xp
movl	-4(fp),r0
cvtlb	$2,20(r0)
moval	_Znumber,r0
movl	-4(fp),r1
movc3	$20,(r0),(r1)
movl	-4(fp),r0
cvtlb	$2,16(r0)
calls	$0,_factor
pushl	r0
pushl	-4(fp)
pushl	-8(fp)
calls	$3,_combine
movl	r0,-4(fp)
jbr	L204
L209:pushal	L211
calls	$1,_yyerror
movl	_xp,-4(fp)
addl2	$28,_xp
movl	-4(fp),r0
cvtlb	$2,20(r0)
moval	_Znumber,r0
movl	-4(fp),r1
movc3	$20,(r0),(r1)
movl	-4(fp),r0
cvtlb	$2,16(r0)
L204:movl	-4(fp),r0
ret
.align	1
.globl	_yukkyexpr
.data	1
L222:.ascii	"Reference to undefined local label %db\0"
.text
.data	1
L224:.ascii	"L%d\1%d\0"
.text
.data	1
L230:.ascii	"Internal Error in yukkyexpr\0"
.text
.set	L212,0xc00
.data
.data
_pdirect:.long	0x65726964
.long	0x76697463
.long	0x65
_pinstr:.long	0x74736e69
.long	0x74637572
.long	0x6e6f69
_phunk:.long	0x6578656c
.long	0x656d
_psmall:.long	0x6c616d73
.long	0x7973206c
.long	0x6c6f626d
.long	0x0
_pcntrl:.long	0x746e6f63
.long	0x206c6f72
.long	0x656b6f74
.long	0x6e
.comm	_tok_name,308
.align	2
.globl	_tok_desc
_tok_desc:.long	0
.long	_pdirect
.data	2
L242:.ascii	"first token\0"
.data
.long	L242
.long	2
.long	_pdirect
.data	2
L243:.ascii	".byte\0"
.data
.long	L243
.long	3
.long	_pdirect
.data	2
L244:.ascii	".word\0"
.data
.long	L244
.long	4
.long	_pdirect
.data	2
L245:.ascii	".int\0"
.data
.long	L245
.long	5
.long	_pdirect
.data	2
L246:.ascii	".long\0"
.data
.long	L246
.long	6
.long	_pdirect
.data	2
L247:.ascii	".quad\0"
.data
.long	L247
.long	7
.long	_pdirect
.data	2
L248:.ascii	".octa\0"
.data
.long	L248
.long	14
.long	_pdirect
.data	2
L249:.ascii	".ffloat\0"
.data
.long	L249
.long	15
.long	_pdirect
.data	2
L250:.ascii	".dfloat\0"
.data
.long	L250
.long	16
.long	_pdirect
.data	2
L251:.ascii	".gfloat\0"
.data
.long	L251
.long	17
.long	_pdirect
.data	2
L252:.ascii	".hfloat\0"
.data
.long	L252
.long	19
.long	_pdirect
.data	2
L253:.ascii	".ascii\0"
.data
.long	L253
.long	20
.long	_pdirect
.data	2
L254:.ascii	".asciz\0"
.data
.long	L254
.long	25
.long	_pdirect
.data	2
L255:.ascii	".fill\0"
.data
.long	L255
.long	1
.long	_pdirect
.data	2
L256:.ascii	".space\0"
.data
.long	L256
.long	8
.long	_pdirect
.data	2
L257:.ascii	".data\0"
.data
.long	L257
.long	11
.long	_pdirect
.data	2
L258:.ascii	".text\0"
.data
.long	L258
.long	9
.long	_pdirect
.data	2
L259:.ascii	".global\0"
.data
.long	L259
.long	31
.long	_pdirect
.data	2
L260:.ascii	".align\0"
.data
.long	L260
.long	10
.long	_pdirect
.data	2
L261:.ascii	".set\0"
.data
.long	L261
.long	12
.long	_pdirect
.data	2
L262:.ascii	".comm\0"
.data
.long	L262
.long	13
.long	_pdirect
.data	2
L263:.ascii	".lcomm\0"
.data
.long	L263
.long	18
.long	_pdirect
.data	2
L264:.ascii	".org\0"
.data
.long	L264
.long	21
.long	_pdirect
.data	2
L265:.ascii	".lsym\0"
.data
.long	L265
.long	26
.long	_pdirect
.data	2
L266:.ascii	".stab\0"
.data
.long	L266
.long	27
.long	_pdirect
.data	2
L267:.ascii	".stabstr\0"
.data
.long	L267
.long	28
.long	_pdirect
.data	2
L268:.ascii	".stabnone\0"
.data
.long	L268
.long	29
.long	_pdirect
.data	2
L269:.ascii	".stabdot\0"
.data
.long	L269
.long	22
.long	_pdirect
.data	2
L270:.ascii	".file\0"
.data
.long	L270
.long	23
.long	_pdirect
.data	2
L271:.ascii	".lineno\0"
.data
.long	L271
.long	24
.long	_pdirect
.data	2
L272:.ascii	".abort\0"
.data
.long	L272
.long	30
.long	_pinstr
.data	2
L273:.ascii	"jump pseudo\0"
.data
.long	L273
.long	32
.long	_pinstr
.data	2
L274:.ascii	""
.ascii	"0 argument inst\0"
.data
.long	L274
.long	33
.long	_pinstr
.data	2
L275:.ascii	"n argument inst\0"
.data
.long	L275
.long	35
.long	_pcntrl
.data	2
L276:.ascii	"parse end of file\0"
.data
.long	L276
.long	36
.long	_pcntrl
.data	2
L277:.ascii	"skip lines\0"
.data
.long	L277
.long	37
.long	_pcntrl
.data	2
L278:.ascii	"void\0"
.data
.long	L278
.long	38
.long	_pcntrl
.data	2
L279:.ascii	"skip\0"
.data
.long	L279
.long	50
.long	_pcntrl
.data	2
L280:.ascii	"new line\0"
.data
.long	L280
.long	51
.long	_pcntrl
.data	2
L281:.ascii	"scanner end of file\0"
.data
.long	L281
.long	52
.long	_pcntrl
.data	2
L282:.ascii	"bad character\0"
.data
.long	L282
.long	58
.long	_pcntrl
.data	2
L283:.ascii	"comment, #\0"
.data
.long	L283
.long	39
.long	_phunk
.data	2
L284:.ascii	"int\0"
.data
.long	L284
.long	34
.long	_phunk
.data	2
L285:.ascii	"local label\0"
.data
.long	L285
.long	40
.long	_phunk
.data	2
L286:.ascii	"big number\0"
.data
.long	L286
.long	41
.long	_phunk
.data	2
L287:.ascii	"name\0"
.data
.long	L287
.long	42
.long	_phunk
.data	2
L288:.ascii	"string\0"
.data
.long	L288
.long	44
.long	_phunk
.data	2
L289:.ascii	"register specifier\0"
.data
.long	L289
.long	43
.long	_psmall
.data	2
L290:.ascii	"size specifier, [BWLbwl]\0"
.data
.long	L290
.long	62
.long	_psmall
.data	2
L291:.ascii	"sizequote, [^']\0"
.data
.long	L291
.long	46
.long	_psmall
.data	2
L292:.ascii	"litop\0"
.data
.long	L292
.long	48
.long	_psmall
.data	2
L293:.ascii	"minus parenthesis, -(\0"
.data
.long	L293
.long	49
.long	_psmall
.data	2
L294:.ascii	"register operator, %\0"
.data
.long	L294
.long	53
.long	_psmall
.data	2
L295:.ascii	"space\0"
.data
.long	L295
.long	54
.long	_psmall
.data	2
L296:.ascii	"alphabetic character, [A-Za-z_]\0"
.data
.long	L296
.long	55
.long	_psmall
.data	2
L297:.ascii	"digit character, [A-Fa-f0-9]\0"
.data
.long	L297
.long	56
.long	_psmall
.data	2
L298:.ascii	"single quote, '\0"
.data
.long	L298
.long	57
.long	_psmall
.data	2
L299:.ascii	"double quote, \"\0"
.data
.long	L299
.long	59
.long	_psmall
.data	2
L300:.ascii	"arithmetic left shift, <\0"
.data
.long	L300
.long	60
.long	_psmall
.data	2
L301:.ascii	"arithmetic right shift, >\0"
.data
.long	L301
.long	63
.long	_psmall
.data	2
L302:.ascii	"exclusive or, ^\0"
.data
.long	L302
.long	67
.long	_psmall
.data	2
L303:.ascii	"plus, +\0"
.data
.long	L303
.long	61
.long	_psmall
.data	2
L304:.ascii	"minus, -\0"
.data
.long	L304
.long	45
.long	_psmall
.data	2
L305:.ascii	"multiply, *\0"
.data
.long	L305
.long	64
.long	_psmall
.data	2
L306:.ascii	"divide, /\0"
.data
.long	L306
.long	65
.long	_psmall
.data	2
L307:.ascii	"semi colon, ;\0"
.data
.long	L307
.long	66
.long	_psmall
.data	2
L308:.ascii	"colon, \72\0"
.data
.long	L308
.long	68
.long	_psmall
.data	2
L309:.ascii	"inclusive or, |\0"
.data
.long	L309
.long	69
.long	_psmall
.data	2
L310:.ascii	"and, &\0"
.data
.long	L310
.long	70
.long	_psmall
.data	2
L311:.ascii	"one's complement, ~\0"
.data
.long	L311
.long	71
.long	_psmall
.data	2
L312:.ascii	"ornot, !\0"
.data
.long	L312
.long	72
.long	_psmall
.data	2
L313:.ascii	"comma\0"
.data
.long	L313
.long	73
.long	_psmall
.data	2
L314:.ascii	"left bracket, [\0"
.data
.long	L314
.long	74
.long	_psmall
.data	2
L315:.ascii	"right bracket, ]\0"
.data
.long	L315
.long	47
.long	_psmall
.data	2
L316:.ascii	"left parenthesis, (\0"
.data
.long	L316
.long	75
.long	_psmall
.data	2
L317:.ascii	"right parentheis, )\0"
.data
.long	L317
.long	76
.long	_psmall
.data	2
L318:.ascii	"last token\0"
.data
.long	L318
.text
_yukkyexpr:.word	L212
subl2	$4,sp
movl	8(ap),r11
clrl	-4(fp)
clrl	_exprisname
movl	_xp,r10
addl2	$28,_xp
movc3	$20,_Znumber,(r10)
cvtlb	$2,16(r10)
movl	4(ap),r0
casel	r0,$32,$12
L2000025:
.word	L233-L2000025
.word	L233-L2000025
.word	L219-L2000025
.word	L229-L2000025
.word	L229-L2000025
.word	L229-L2000025
.word	L229-L2000025
.word	L229-L2000025
.word	L229-L2000025
.word	L225-L2000025
.word	L229-L2000025
.word	L229-L2000025
.word	L233-L2000025
L229:pushal	L230
calls	$1,_yyerror
L233:cvtlb	$2,20(r10)
movzbl	r11,(r10)
clrb	21(r10)
L2000024:clrl	24(r10)
L217:movl	r10,r0
ret
L219:movl	(r11),_yylval
jgeq	L220
mnegl	_yylval,_yylval
decl	_yylval
mnegl	$1,-4(fp)
movl	_yylval,r0
cmpl	_lgensym[r0],$1
jneq	L223
pushl	r0
pushal	L222
calls	$2,_yyerror
jbr	L223
L220:decl	_yylval
movl	_yylval,r0
cvtlb	$1,_genref[r0]
L223:movl	_yylval,r0
addl3	-4(fp),_lgensym[r0],-(sp)
pushl	_yylval
pushal	L224
pushal	_yytext
calls	$4,_sprintf
cmpl	_passno,$1
jneq	L99997
movl	$1,r0
jbr	L99996
L99997:clrl	r0
L99996:pushl	r0
calls	$1,_lookup
movl	(r0),r11
movl	r11,_yylval
movl	r11,_lastnam
L225:incl	_exprisname
movb	4(r11),20(r10)
bitb	$30,4(r11)
jneq	L226
movl	r11,24(r10)
clrl	(r10)
cmpl	_passno,$1
jneq	L217
bisb2	$32,4(r11)
jbr	L217
L226:movl	8(r11),(r10)
jbr	L2000024
.align	1
.globl	_tok_to_name
.data
.align	2
L324:.long	0
.lcomm	L325,64
.align	2
L326:.long	0
.long	0
.data	1
L327:.ascii	"NOT ASSIGNED\0"
.data
.long	L327
.text
.data	1
L336:.ascii	"%s %s\0"
.text
.data	1
L338:.ascii	"Unknown token number, %d\12\0"
.text
.set	L320,0x0
.data
.text
_tok_to_name:.word	L320
subl2	$8,sp
tstl	L324
jneq	L328
clrl	-4(fp)
L2000027:movl	-4(fp),r0
moval	L326,_tok_name[r0]
aobleq	$76,-4(fp),L2000027
clrl	-4(fp)
L2000029:mull3	$12,-4(fp),r0
addl2	$_tok_desc,r0
mull3	$12,-4(fp),r1
movl	_tok_desc(r1),r1
movl	r0,_tok_name[r1]
aobleq	$77,-4(fp),L2000029
movl	$1,L324
L328:tstl	4(ap)
jlss	L335
cmpl	4(ap),$76
jgtr	L335
movl	4(ap),r0
movl	_tok_name[r0],r0
pushl	8(r0)
movl	4(ap),r0
movl	_tok_name[r0],r0
pushl	4(r0)
pushal	L336
pushal	L325
calls	$4,_sprintf
moval	L325,r0
ret
L335:pushl	4(ap)
pushal	L338
calls	$2,_panic
ret

