.data
.data
_sccsid:.long	0x29232840
.long	0x6f74616e
.long	0x9632e66
.long	0x20312e35
.long	0x72654228
.long	0x656c656b
.long	0x34202979
.long	0x2f30332f
.long	0x3538
.comm	_rusefile,32
.comm	_relfil,4
.text
LL0:.align	1
.globl	_bigatof
.lcomm	L110,20
.data	1
L126:.ascii	"%s%s\12\0"
.text
.data	1
L127:.ascii	"((acc[HOC] & SIGNBIT) == 0)\0"
.text
.data	1
L128:.ascii	"Negative HOC\0"
.text
.data	1
L131:.ascii	"%s%s\12\0"
.text
.data	1
L132:.ascii	"ovf == 0\0"
.text
.data	1
L133:.ascii	"Overflow building mantissa\0"
.text
.data	1
L170:.ascii	"%s%s\12\0"
.text
.data	1
L171:.ascii	"((acc[HOC] & SIGNBIT) == 0)\0"
.text
.data	1
L172:.ascii	"Negative HOC\0"
.text
.data	1
L179:.ascii	"%s%s\12\0"
.text
.data	1
L180:.ascii	"ovf == 0\0"
.text
.data	1
L181:.ascii	"Carry out of left rounding up by 2\0"
.text
.data	1
L186:.ascii	"%s%s\12\0"
.text
.data	1
L187:.ascii	"dividend >= 0\0"
.text
.data	1
L188:.ascii	"dividend < 0\0"
.text
.data	1
L190:.ascii	"%s%s\12\0"
.text
.data	1
L191:.ascii	"dividend >= 0\0"
.text
.data	1
L192:.ascii	"dividend < 0\0"
.text
.data	1
L200:.ascii	"%s%s\12\0"
.text
.data	1
L201:.ascii	"ovf == 0\0"
.text
.data	1
L202:.ascii	"Scaling * 10 of manitissa\0"
.text
.data	1
L205:.ascii	"%s%s\12\0"
.text
.data	1
L206:.ascii	"Acc.num_sign == 0\0"
.text
.data	1
L207:.ascii	"unpacked integer is < 0\0"
.text
.lcomm	L209,20
.set	L106,0xfc0
.data
.text
_bigatof:.word	L106
movab	-80(sp),sp
movl	4(ap),r11
clrl	-4(fp)
clrl	-8(fp)
clrl	-12(fp)
clrl	-28(fp)
movc3	$20,L110,-52(fp)
cvtlb	8(ap),-36(fp)
moval	-52(fp),r7
moval	-72(fp),r10
L115:cvtbl	(r11)+,-24(fp)
movl	-24(fp),r0
jbs	$3,__ctype_+1[r0],L115
cmpl	r0,$43
jeql	L119
cmpl	r0,$45
jeql	L118
jbr	L123
L2000021:movl	-24(fp),r0
cmpl	r0,$100
jeql	L147
jgtr	L160
cmpl	r0,$71
jeql	L147
jgtr	L161
cmpl	r0,$68
jeql	L147
jgtr	L162
cmpl	r0,$46
jneq	L136
tstl	-12(fp)
jneq	L136
incl	-12(fp)
jbr	L119
L2000001:pushl	r7
pushl	r10
pushl	$3
calls	$3,_numshift
movl	r0,-76(fp)
pushl	r7
pushl	r7
pushl	$1
calls	$3,_numshift
bisl2	r0,-76(fp)
pushl	r7
pushl	r10
pushl	r7
calls	$3,_numaddv
bisl2	r0,-76(fp)
subl3	$48,-24(fp),-(sp)
pushl	r7
pushl	r7
calls	$3,_numaddd
bisl2	r0,-76(fp)
jeql	L134
pushal	L133
pushal	L132
pushal	L131
calls	$3,_panic
jbr	L134
L118:mnegl	$1,-4(fp)
L119:cvtbl	(r11)+,-24(fp)
L123:movl	-24(fp),r0
jbc	$2,__ctype_+1[r0],L2000021
jbc	$31,12(r7),L124
pushal	L128
pushal	L127
pushal	L126
calls	$3,_panic
L124:cmpl	12(r7),$214748364
jlssu	L2000001
incl	-28(fp)
L134:tstl	-12(fp)
jeql	L119
decl	-28(fp)
jbr	L119
L2000005:cmpl	-80(fp),$214748364
jgequ	L151
mull2	$10,-80(fp)
subl3	$48,-24(fp),r0
addl2	r0,-80(fp)
jbr	L151
L157:addl2	-80(fp),-28(fp)
jbr	L136
L162:cmpl	r0,$69
jneq	L136
L147:clrl	-80(fp)
cvtbl	(r11)+,-24(fp)
clrl	-8(fp)
movl	-24(fp),r0
cmpl	r0,$43
jeql	L151
cmpl	r0,$45
jneq	L154
movl	$1,-8(fp)
L151:cvtbl	(r11)+,-24(fp)
L154:movl	-24(fp),r0
jbs	$2,__ctype_+1[r0],L2000005
tstl	-8(fp)
jeql	L157
subl2	-80(fp),-28(fp)
jbr	L136
L161:cmpl	r0,$72
jneq	L136
jbr	L147
L160:cmpl	r0,$103
jeql	L147
jgtr	L163
cmpl	r0,$101
jneq	L136
jbr	L147
L163:cmpl	r0,$104
jeql	L147
L136:pushl	r7
calls	$1,_isclear
tstl	r0
jneq	L2000007
clrl	-32(fp)
jbr	L168
L2000009:pushl	r7
pushl	r7
pushl	-16(fp)
calls	$3,_numshift
bisl2	r0,-76(fp)
subl2	-16(fp),-32(fp)
L176:mcoml	r9,r0
bicl3	r0,12(r7),r0
jeql	L2000009
decl	-80(fp)
cmpl	-80(fp),$1
jgequ	L2000011
pushl	$2
pushl	r7
pushl	r7
calls	$3,_numaddd
movl	r0,-76(fp)
tstl	r0
jeql	L178
pushal	L181
pushal	L180
pushal	L179
calls	$3,_panic
L178:clrl	r8
movl	r7,-20(fp)
movl	$7,-16(fp)
L2000013:ashl	$16,r8,r0
movl	-16(fp),r1
movzwl	*-20(fp)[r1],r1
bisl3	r1,r0,r6
jgeq	L185
pushal	L188
pushal	L187
pushal	L186
calls	$3,_panic
L185:divl3	$5,r6,r9
mull3	$5,r9,r0
subl3	r0,r6,r8
movl	-16(fp),r0
cvtlw	r9,*-20(fp)[r0]
movl	r8,r8
decl	-16(fp)
cmpl	-16(fp),$1
jgequ	L2000013
ashl	$16,r8,r0
movzwl	*-20(fp),r1
bisl3	r1,r0,r6
jgeq	L189
pushal	L192
pushal	L191
pushal	L190
calls	$3,_panic
L189:divl3	$10,r6,r9
mull3	$10,r9,r0
subl3	r0,r6,r8
addl3	r9,r9,r0
cvtlw	r0,*-20(fp)
cmpl	r8,$5
jlssu	L193
pushl	$1
pushl	r7
pushl	r7
calls	$3,_numaddd
movl	r0,-76(fp)
L193:decl	-32(fp)
incl	-28(fp)
L168:tstl	-28(fp)
jgeq	L196
jbc	$31,12(r7),L169
pushal	L172
pushal	L171
pushal	L170
calls	$3,_panic
L169:clrl	-76(fp)
movl	$5,-80(fp)
L2000011:subl3	$1,-80(fp),r0
ashl	r0,$1,-16(fp)
ashl	-16(fp),$1,r0
subl3	$1,r0,r9
subl3	-16(fp),$31,r0
ashl	r0,r9,r9
jbr	L176
L2000017:pushl	r7
pushl	r7
pushl	$-1
calls	$3,_numshift
incl	-32(fp)
L197:cmpl	12(r7),$429496729
jgtru	L2000017
pushl	r7
pushl	r10
pushl	$2
calls	$3,_numshift
movl	r0,-76(fp)
pushl	r10
pushl	r7
pushl	r7
calls	$3,_numaddv
bisl2	r0,-76(fp)
jeql	L199
pushal	L202
pushal	L201
pushal	L200
calls	$3,_panic
L199:incl	-32(fp)
decl	-28(fp)
L196:tstl	-28(fp)
jgtr	L197
cvtlb	$4,-36(fp)
pushal	-76(fp)
subl2	$20,sp
movc3	$20,-52(fp),(sp)
calls	$6,_bignumunpack
movc3	$20,(r0),-52(fp)
tstl	-76(fp)
jeql	L203
movl	$34,_errno
L203:cvtwl	-34(fp),r0
addl2	-32(fp),r0
cvtlw	r0,-34(fp)
tstb	-35(fp)
jeql	L204
pushal	L207
pushal	L206
pushal	L205
calls	$3,_panic
L204:cvtlb	-4(fp),-35(fp)
clrl	-76(fp)
pushal	-76(fp)
pushl	8(ap)
subl2	$20,sp
movc3	$20,-52(fp),(sp)
calls	$7,_bignumpack
movc3	$20,(r0),-52(fp)
tstl	-76(fp)
jeql	L2000007
movl	$34,_errno
L2000007:moval	-52(fp),r0
movab	L209,r1
movc3	$20,(r0),(r1)
movab	L209,r0
ret

