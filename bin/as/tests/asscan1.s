.data
.data
_sccsid:.long	0x29232840
.long	0x63737361
.long	0x2e316e61
.long	0x2e350963
.long	0x42282031
.long	0x656b7265
.long	0x2979656c
.long	0x332f3420
.long	0x35382f30
.long	0x0
.comm	_rusefile,32
.comm	_relfil,4
.comm	_bufstart,4
.comm	_buftail,4
.comm	_emptybuf,4
.comm	_tok_free,4
.comm	_tok_temp,4
.comm	_bufno,4
.comm	_tokbuf,8192
.comm	_tokptr,4
.comm	_tokub,4
.comm	_yylval,4
.comm	_yybignum,20
.comm	_yyopcode,2
.comm	_newfflag,4
.comm	_newfname,4
.comm	_scanlineno,4
.comm	_charsets,0
.comm	_type,0
.text
LL0:.align	1
.globl	_inittokfile
.set	L125,0x0
.data
.text
_inittokfile:.word	L125
cmpl	_passno,$1
jneq	L129
tstl	_useVM
jeql	L130
moval	_tokbuf,_bufstart
moval	_tokbuf+4096,_buftail
movl	_bufstart,r0
movl	_buftail,4(r0)
movl	_buftail,r0
clrl	4(r0)
L130:mnegl	$1,_tokbuf
mnegl	$1,_tokbuf+4096
L129:clrl	_tok_temp
clrl	_tok_free
clrl	_bufno
ashl	$12,_bufno,r0
addl2	$_tokbuf,r0
movl	r0,_emptybuf
clrl	_tokptr
clrl	_tokub
ret
.align	1
.globl	_closetokfile
.data	1
L144:.ascii	"Unexpected end of file writing the interpass tmp file\0"
.text
.set	L132,0x0
.data
.text
_closetokfile:.word	L132
cmpl	_passno,$1
jneq	L136
tstl	_useVM
jeql	L137
movl	*_emptybuf,r0
incl	*_emptybuf
addl3	$8,_emptybuf,r1
cvtlb	$35,(r1)[r0]
L136:ret
L137:xorl3	$1,_bufno,r0
ashl	$12,r0,r0
tstl	_tokbuf(r0)
jlss	L139
pushl	$4096
xorl3	$1,_bufno,r0
ashl	$12,r0,r0
pushab	_tokbuf[r0]
movl	_tokfile,r0
cvtbl	18(r0),-(sp)
calls	$3,_write
cmpl	r0,$4096
jeql	L139
L142:pushal	L144
calls	$1,_yyerror
pushl	$2
calls	$1,_exit
L139:ashl	$12,_bufno,r0
movl	_tokbuf(r0),r1
incl	_tokbuf(r0)
ashl	$12,_bufno,r0
addl2	$_tokbuf+8,r0
cvtlb	$35,(r0)[r1]
pushl	$4096
ashl	$12,_bufno,r0
pushab	_tokbuf[r0]
movl	_tokfile,r0
cvtbl	18(r0),-(sp)
calls	$3,_write
cmpl	r0,$4096
jneq	L142
jbr	L136
.align	1
.globl	_yylex
.lcomm	L152,4
.data	1
L161:.ascii	"Too many expressions; try simplyfing\0"
.text
.data	1
L166:.ascii	"Too many expressions; try simplyfing\0"
.text
.data	1
L201:.ascii	"Unexpected end of file writing the interpass tmp file\0"
.text
.data	1
L205:.ascii	"Unexpected end of file while reading the interpass tmp file\0"
.text
.set	L148,0xe00
.data
.text
_yylex:.word	L148
subl2	$8,sp
movl	_tokptr,r11
L153:cmpl	r11,_tokub
jgeq	L154
cvtbl	(r11)+,r10
movl	r10,_yylval
movl	r10,r0
casel	r0,$26,$18
L2000006:
.word	L182-L2000006
.word	L182-L2000006
.word	L182-L2000006
.word	L182-L2000006
.word	L173-L2000006
.word	L182-L2000006
.word	L172-L2000006
.word	L172-L2000006
.word	L159-L2000006
.word	L157-L2000006
.word	L174-L2000006
.word	L153-L2000006
.word	L175-L2000006
.word	L159-L2000006
.word	L164-L2000006
.word	L168-L2000006
.word	L182-L2000006
.word	L170-L2000006
.word	L170-L2000006
L155:movl	r11,_tokptr
movl	r10,r0
ret
L157:movl	$35,r10
movl	r10,_yylval
jbr	L155
L159:cmpl	_xp,$_explist+560
jlss	L160
pushal	L161
calls	$1,_yyerror
jbr	L162
L160:movl	_xp,r9
addl2	$28,_xp
L162:movc3	$20,_Znumber,(r9)
cvtlb	$2,16(r9)
movl	(r11),(r9)
addl2	$4,r11
L163:cvtlb	$2,20(r9)
clrb	21(r9)
clrl	24(r9)
movl	r9,_yylval
jbr	L155
L164:cmpl	_xp,$_explist+560
jlss	L165
pushal	L166
calls	$1,_yyerror
jbr	L167
L165:movl	_xp,r9
addl2	$28,_xp
L167:movc3	$20,(r11),(r9)
addl2	$20,r11
jbr	L163
L168:movl	(r11),_yylval
addl2	$4,r11
movl	_yylval,_lastnam
jbr	L155
L170:cvtbl	(r11)+,_yylval
jbr	L155
L172:movw	(r11),_yyopcode
addl2	$2,r11
jbr	L155
L173:movw	(r11),_yyopcode
addl2	$2,r11
movl	(r11),L152
addl2	$4,r11
movl	L152,_lastjxxx
jbr	L155
L174:movl	(r11),_yylval
addl2	$4,r11
addl2	_yylval,_lineno
jbr	L153
L175:movzwl	(r11),r0
addl2	$2,r0
addl2	r0,r11
jbr	L153
L182:movl	(r11),_yylval
addl2	$4,r11
jbr	L155
L154:tstl	_useVM
jeql	L186
cmpl	_passno,$2
jneq	L187
movl	_emptybuf,r0
movl	4(r0),_tok_temp
movl	_tok_free,4(r0)
movl	_emptybuf,_tok_free
movl	_tok_temp,_emptybuf
jbr	L188
L187:movl	_emptybuf,r0
movl	4(r0),_emptybuf
L188:incl	_bufno
tstl	_emptybuf
jneq	L189
cmpl	_passno,$2
jeql	L191
pushl	$4096
pushl	$8
calls	$2,_Calloc
movl	r0,-4(fp)
movl	r0,_emptybuf
clrl	-8(fp)
L2000001:movl	_buftail,r0
movl	-4(fp),4(r0)
movl	-4(fp),_buftail
addl2	$4096,-4(fp)
aoblss	$8,-8(fp),L2000001
movl	_buftail,r0
clrl	4(r0)
L189:addl3	$8,_emptybuf,r11
cmpl	_passno,$1
jeql	L2000003
jbr	L197
L2000005:tstl	*_emptybuf
jlss	L2000003
pushl	$4096
pushl	_emptybuf
movl	_tokfile,r0
cvtbl	18(r0),-(sp)
calls	$3,_write
cmpl	r0,$4096
jeql	L2000003
pushal	L201
calls	$1,_yyerror
pushl	$2
calls	$1,_exit
L2000003:pushl	_emptybuf
calls	$1,_scan_dot_s
jbr	L197
L186:xorl2	$1,_bufno
ashl	$12,_bufno,r0
movab	_tokbuf[r0],_emptybuf
addl3	$8,_emptybuf,r11
cmpl	_passno,$1
jeql	L2000005
pushl	$4096
pushl	_emptybuf
movl	_tokfile,r0
cvtbl	18(r0),-(sp)
calls	$3,_read
cmpl	r0,$4096
jeql	L197
L191:pushal	L205
calls	$1,_yyerror
pushl	$1
calls	$1,_exit
L197:addl3	*_emptybuf,r11,_tokub
jbr	L153
.align	1
.globl	_buildskip
.data	1
L213:.ascii	"Internal error\72 bad skip construction\0"
.text
.set	L207,0xe00
.data
.text
_buildskip:.word	L207
subl2	$4,sp
movl	4(ap),r11
movl	8(ap),r10
tstl	_useVM
jeql	L99999
movl	_emptybuf,r0
jbr	L99998
L99999:moval	_tokbuf+4096,r0
L99998:movl	r0,r9
cmpl	r11,r9
jleq	L99997
movl	$1,r0
jbr	L99996
L99997:clrl	r0
L99996:cmpl	r10,r9
jleq	L99995
movl	$1,r1
jbr	L99994
L99995:clrl	r1
L99994:xorl2	r1,r0
jeql	L211
cvtlb	$38,(r11)+
movw	$4097,(r11)
addl2	$4099,r11
addl3	$8,_emptybuf,r11
L211:cmpl	r11,r10
jleq	L212
pushal	L213
calls	$1,_yyerror
jbr	L214
L2000008:cvtlb	$38,(r11)+
subl3	r11,r10,r0
subl2	$2,r0
cvtlw	r0,(r11)
addl3	$2,r10,r0
subl2	r11,r0
addl2	r0,r11
L214:ret
L212:subl3	r11,r10,r0
movl	r0,-4(fp)
cmpl	r0,$4
jgeq	L2000008
L219:tstl	-4(fp)
jleq	L214
cvtlb	$37,(r11)+
decl	-4(fp)
jbr	L219
.align	1
.globl	_movestr
.set	L221,0x0
.data
.text
_movestr:.word	L221
tstl	12(ap)
jgtr	L225
ret
L225:movc3	12(ap),*8(ap),*4(ap)
ret
.align	1
.globl	_new_dot_s
.set	L227,0x0
.data
.text
_new_dot_s:.word	L227
movl	$1,_newfflag
movl	4(ap),_newfname
movl	4(ap),_dotsname
movl	$1,_lineno
movl	$1,_scanlineno
ret
.align	1
.globl	_min
.set	L232,0x0
.data
.text
_min:.word	L232
cmpl	4(ap),8(ap)
jgeq	L99993
movl	4(ap),r0
jbr	L99992
L99993:movl	8(ap),r0
L99992:ret

