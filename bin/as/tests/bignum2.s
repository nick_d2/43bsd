.data
.data
_sccsid:.long	0x29232840
.long	0x6e676962
.long	0x2e326d75
.long	0x2e350963
.long	0x42282031
.long	0x656b7265
.long	0x2979656c
.long	0x332f3420
.long	0x35382f30
.long	0x0
.comm	_rusefile,32
.comm	_relfil,4
.comm	_Znumber,20
.text
LL0:.align	1
.globl	_intconvert
.data	1
L112:.ascii	"Conversion between %s and %s looses significance\0"
.text
.lcomm	L116,20
.set	L105,0x800
.data
.text
_intconvert:.word	L105
cvtbl	20(ap),r0
cmpl	r0,24(ap)
jneq	L109
L2000002:addl3	$4,ap,r0
movab	L116,r1
movc3	$20,(r0),(r1)
movab	L116,r0
ret
L109:cvtbl	20(ap),r0
movl	24(ap),r1
cmpl	_ty_nbyte[r0],_ty_nbyte[r1]
jleq	L110
cmpl	_passno,$2
jneq	L110
movl	r1,r0
pushl	_ty_string[r0]
cvtbl	20(ap),r0
pushl	_ty_string[r0]
pushal	L112
calls	$3,_yywarning
L110:movl	24(ap),r0
movl	_ty_nbyte[r0],r11
jbr	L115
L2000001:clrb	4(ap)[r11]
incl	r11
L115:cmpl	r11,_ty_nbyte+16
jlss	L2000001
jbr	L2000002
.align	1
.globl	_floatconvert
.data	1
L138:.ascii	"Bad floating point conversion?\0"
.text
.data	1
L145:.ascii	"Converting from %s to %s\72 %s \0"
.text
.data	1
L146:.ascii	"gains significance\0"
.text
.data	1
L147:.ascii	"looses significance\0"
.text
.data	1
L148:.ascii	"mixs exponent formats\0"
.text
.data	1
L151:.ascii	"Floating conversion over/underflowed\12\0"
.text
.lcomm	L153,20
.set	L117,0x800
.data
.text
_floatconvert:.word	L117
subl2	$16,sp
clrl	-4(fp)
clrl	-8(fp)
clrl	-12(fp)
cvtbl	20(ap),r0
cmpl	r0,24(ap)
jneq	L121
L2000007:addl3	$4,ap,r0
movab	L153,r1
movc3	$20,(r0),(r1)
movab	L153,r0
ret
L121:addl3	$4,ap,r11
cvtbl	20(ap),r0
ashl	$4,r0,r0
addl2	24(ap),r0
cmpl	r0,$118
jeql	L125
jgtr	L139
cmpl	r0,$101
jeql	L127
jgtr	L140
cmpl	r0,$87
jeql	L125
jgtr	L141
cmpl	r0,$86
jneq	L136
cvtfd	(r11), (r11)
L122:tstl	-8(fp)
jneq	L99999
tstl	-12(fp)
jneq	L99999
tstl	-4(fp)
jeql	L144
L99999:cmpl	_passno,$2
jneq	L144
tstl	-8(fp)
jeql	L99998
moval	L146,r0
jbr	L99997
L127:cvtdf	(r11), (r11)
jbr	L122
L141:cmpl	r0,$88
jeql	L125
jbr	L136
L2000004:cmpl	r0,$103
jneq	L136
L125:incl	-12(fp)
jbr	L122
L2000006:cmpl	r0,$133
jeql	L125
jgtr	L136
cmpl	r0,$120
jeql	L125
L136:pushal	L138
calls	$1,_panic
jbr	L122
L140:cmpl	r0,$104
jeql	L125
jleq	L2000004
cmpl	r0,$117
jneq	L136
jbr	L125
L139:cmpl	r0,$134
jeql	L125
jleq	L2000006
cmpl	r0,$135
jneq	L136
jbr	L125
L99998:tstl	-4(fp)
jeql	L99996
moval	L147,r0
jbr	L99997
L99996:moval	L148,r0
L99997:pushl	r0
movl	24(ap),r0
pushl	_ty_string[r0]
cvtbl	20(ap),r0
pushl	_ty_string[r0]
pushal	L145
calls	$4,_yywarning
L144:tstl	-12(fp)
jeql	L149
pushal	-16(fp)
pushl	24(ap)
subl2	$20,sp
movc3	$20,4(ap),(sp)
calls	$7,_bignumconvert
movc3	$20,(r0),4(ap)
tstl	-16(fp)
jeql	L2000007
cmpl	_passno,$2
jneq	L2000007
pushal	L151
calls	$1,_yywarning
jbr	L2000007
L149:cvtlb	24(ap),20(ap)
jbr	L2000007
.align	1
.globl	_bignumconvert
.lcomm	L161,20
.set	L154,0x0
.data
.text
_bignumconvert:.word	L154
subl2	$4,sp
clrl	*28(ap)
cvtbl	20(ap),-4(fp)
cmpl	-4(fp),24(ap)
jneq	L158
addl3	$4,ap,r0
jbr	L155
L2000009:pushl	28(ap)
pushl	24(ap)
subl2	$20,sp
movc3	$20,4(ap),(sp)
L2000012:calls	$7,_bignumpack
jbr	L155
L2000011:pushl	28(ap)
subl2	$20,sp
movc3	$20,4(ap),(sp)
calls	$6,_bignumunpack
L155:movab	L161,r1
movc3	$20,(r0),(r1)
movab	L161,r0
ret
L158:cmpl	-4(fp),$9
jeql	L2000009
cmpl	24(ap),$9
jeql	L2000011
pushl	28(ap)
pushl	24(ap)
pushl	28(ap)
subl2	$20,sp
movc3	$20,4(ap),(sp)
calls	$6,_bignumunpack
subl2	$20,sp
movc3	$20,(r0),(sp)
jbr	L2000012
.align	1
.globl	_bignumunpack
.data	1
L185:.ascii	"%s%s\12\0"
.text
.data	1
L186:.ascii	"mantissa[HOC] & SIGNBIT\0"
.text
.data	1
L187:.ascii	"integer <<ing\0"
.text
.lcomm	L190,20
.set	L162,0xf80
.data
.text
_bignumunpack:.word	L162
subl2	$60,sp
cvtbl	20(ap),r0
mull2	$44,r0
movab	_ty_bigdesc[r0],r10
clrl	*24(ap)
movc3	$20,_Znumber,-20(fp)
clrl	-56(fp)
clrw	-50(fp)
moval	-20(fp),r8
moval	-40(fp),r7
addl3	$4,ap,r9
pushl	r9
calls	$1,_isclear
tstl	r0
jeql	L167
cvtlb	$9,-4(fp)
movw	$-32768,-2(fp)
L2000017:moval	-20(fp),r0
movab	L190,r1
movc3	$20,(r0),(r1)
movab	L190,r0
ret
L167:pushl	r10
pushl	$16
pushl	r9
pushl	r8
calls	$4,_mapnumber
pushl	r8
pushl	r8
cvtbl	36(r10),-(sp)
calls	$3,_numshift
cvtbl	20(ap),r0
casel	r0,$0,$4
L2000018:
.word	L175-L2000018
.word	L175-L2000018
.word	L175-L2000018
.word	L175-L2000018
.word	L175-L2000018
movc3	$20,_Znumber,-40(fp)
addl3	$32,r10,-(sp)
pushl	$2
pushl	r9
pushl	r7
calls	$4,_mapnumber
extzv	$0,$15,-40(fp),r0
cvtlw	r0,-50(fp)
cvtbl	37(r10),r0
movzwl	-50(fp),r1
subl3	r0,$32,r2
extzv	r0,r2,r1,r1
cvtlw	r1,-50(fp)
ashl	40(r10),$1,r0
decl	r0
mcoml	r0,r0
movzwl	-50(fp),r1
bicl2	r0,r1
cvtlw	r1,-50(fp)
subw2	42(r10),-50(fp)
bitw	$-32768,-40(fp)
jeql	L99994
mnegl	$1,r0
jbr	L99993
L175:clrl	-56(fp)
movw	42(r10),-50(fp)
jbc	$31,12(r8),L176
mnegl	$1,-56(fp)
pushl	r8
pushl	r8
calls	$2,_numnegate
bisl2	r0,*24(ap)
L176:clrl	-48(fp)
movl	$4,-44(fp)
jbr	L179
L2000016:ashl	-44(fp),$1,r11
jbr	L180
L2000014:ashl	r11,$1,r0
decl	r0
subl3	r11,$32,r1
ashl	r1,r0,-60(fp)
mcoml	-60(fp),r0
bicl3	r0,12(r8),r0
jneq	L177
pushl	r8
pushl	r8
pushl	r11
calls	$3,_numshift
addl2	r11,-48(fp)
movzwl	-50(fp),r0
subl2	r11,r0
cvtlw	r0,-50(fp)
L180:movl	-48(fp),r0
cvtwl	38(r10),r1
cmpl	r0,r1
jlss	L2000014
L177:decl	-44(fp)
L179:tstl	-44(fp)
jgeq	L2000016
jbs	$31,12(r8),L184
pushal	L187
pushal	L186
pushal	L185
calls	$3,_panic
L184:pushl	r8
pushl	r8
pushl	$1
calls	$3,_numshift
L169:cvtlb	$9,-4(fp)
cvtlb	-56(fp),-3(fp)
movw	-50(fp),-2(fp)
jbr	L2000017
L99994:clrl	r0
L99993:movl	r0,-56(fp)
jbr	L169
.align	1
.globl	_bignumpack
.data	1
L196:.ascii	"Argument to bignumpack is not unpacked\0"
.text
.lcomm	L225,20
.set	L191,0xf80
.data
.text
_bignumpack:.word	L191
movab	-72(sp),sp
cmpb	20(ap),$9
jeql	L195
pushal	L196
calls	$1,_panic
L195:clrl	*28(ap)
movc3	$20,_Znumber,-20(fp)
movc3	$20,_Znumber,-60(fp)
cvtlb	24(ap),-4(fp)
moval	-20(fp),r10
moval	-60(fp),r8
moval	-40(fp),r9
addl3	$4,ap,r7
mull3	$44,24(ap),r0
movab	_ty_bigdesc[r0],r11
movw	22(ap),-62(fp)
movb	21(ap),-63(fp)
cmpw	-62(fp),$-32768
jneq	L197
L2000025:moval	-20(fp),r0
movab	L225,r1
movc3	$20,(r0),(r1)
movab	L225,r0
ret
L197:movl	24(ap),r0
casel	r0,$0,$4
L2000032:
.word	L204-L2000032
.word	L204-L2000032
.word	L204-L2000032
.word	L204-L2000032
.word	L204-L2000032
pushl	r7
pushl	r8
cvtbl	36(r11),r0
mnegl	r0,-(sp)
calls	$3,_numshift
cvtwl	-62(fp),-(sp)
pushl	r11
pushal	-60(fp)
calls	$3,_upround
cvtlw	r0,-62(fp)
cvtwl	-62(fp),r0
cvtwl	42(r11),r1
addl2	r1,r0
cvtwl	r0,r0
jneq	L217
jbr	L218
L204:pushl	r7
pushl	r8
pushl	$-1
calls	$3,_numshift
jbss	$31,12(r8),L2000024
L2000024:cmpw	-62(fp),42(r11)
jleq	L205
pushl	r8
calls	$1,_numclear
pushl	r8
pushl	r8
calls	$2,_num1comp
jbcc	$31,12(r8),L2000023
L2000023:movb	-63(fp),-63(fp)
jbss	$9,*28(ap),L206
L206:tstb	-63(fp)
jeql	L214
pushl	r8
pushl	r8
calls	$2,_numnegate
bisl2	r0,*28(ap)
L214:addl3	$16,r11,-(sp)
pushl	$16
pushl	r8
pushl	r10
calls	$4,_mapnumber
jbr	L2000025
L205:tstw	-62(fp)
jgtr	L207
pushl	r8
calls	$1,_numclear
clrb	-63(fp)
jbss	$10,*28(ap),L206
jbr	L206
L207:movl	$4,-72(fp)
L211:tstl	-72(fp)
jlss	L206
ashl	-72(fp),$1,-68(fp)
jbr	L212
L2000027:pushl	r8
pushl	r8
mnegl	-68(fp),-(sp)
calls	$3,_numshift
cvtwl	-62(fp),r0
addl2	-68(fp),r0
cvtlw	r0,-62(fp)
L212:cvtwl	-62(fp),r0
addl2	-68(fp),r0
cvtwl	42(r11),r1
cmpl	r0,r1
jleq	L2000027
decl	-72(fp)
jbr	L211
L217:cvtwl	-62(fp),r0
cvtwl	42(r11),r1
addl2	r1,r0
cvtwl	r0,r0
jgeq	L219
L218:pushl	r8
calls	$1,_numclear
clrw	-62(fp)
clrb	-63(fp)
jbss	$10,*28(ap),L220
jbr	L220
L219:cvtwl	-62(fp),r0
cvtwl	42(r11),r1
addl2	r1,r0
ashl	40(r11),$1,r1
cmpl	r0,r1
jlssu	L221
pushl	r8
calls	$1,_numclear
pushl	r8
pushl	r8
calls	$2,_num1comp
ashl	40(r11),$1,r0
decl	r0
cvtlw	r0,-62(fp)
movb	-63(fp),-63(fp)
jbss	$9,*28(ap),L220
jbr	L220
L221:addw2	42(r11),-62(fp)
L220:cvtbl	37(r11),r0
cvtwl	-62(fp),r1
ashl	r0,r1,r1
cvtlw	r1,-62(fp)
bicw2	$-32768,-62(fp)
tstb	-63(fp)
jeql	L223
bisw2	$-32768,-62(fp)
L223:movw	-62(fp),-40(fp)
addl3	$34,r11,-(sp)
pushl	$2
pushl	r9
pushl	r10
calls	$4,_mapnumber
jbr	L214
.align	1
.globl	_mapnumber
.set	L226,0xe00
.data
.text
_mapnumber:.word	L226
movl	4(ap),r10
movl	8(ap),r9
clrl	r11
jbr	L232
L2000034:cvtbl	*16(ap)[r11],r0
incl	r0
jeql	L230
movzbl	(r9)[r11],r0
cvtbl	*16(ap)[r11],r1
movzbl	(r10)[r1],r2
bisl2	r0,r2
cvtlb	r2,(r10)[r1]
L230:incl	r11
L232:cmpl	r11,12(ap)
jlss	L2000034
ret
.align	1
.globl	_upround
.lcomm	L241,20
.data	1
L243:.ascii	"%s%s\12\0"
.text
.data	1
L244:.ascii	"(nbytes % 8) == 0\0"
.text
.data	1
L245:.ascii	"mantissa sig bits\0"
.text
.data	1
L247:.ascii	"%s%s\12\0"
.text
.data	1
L248:.ascii	"byteindex >= 0\0"
.text
.data	1
L249:.ascii	"ulp in outer space\0"
.text
.data	1
L251:.ascii	"%s%s\12\0"
.text
.data	1
L252:.ascii	"ovfbitindex >= 0\0"
.text
.data	1
L253:.ascii	"Shifted byte 15 into byte 14\0"
.text
.set	L237,0xf80
.data
.text
_upround:.word	L237
subl2	$16,sp
movl	4(ap),r11
movl	8(ap),r10
movl	r11,r7
movl	r11,r9
cvtwl	38(r10),r0
decl	r0
cvtbl	36(r10),r1
addl3	r1,r0,-4(fp)
divl3	$8,-4(fp),r0
ashl	$3,r0,r0
subl3	r0,-4(fp),r0
jeql	L242
pushal	L245
pushal	L244
pushal	L243
calls	$3,_panic
L242:divl2	$8,-4(fp)
subl3	-4(fp),$15,-8(fp)
jgeq	L246
pushal	L249
pushal	L248
pushal	L247
calls	$3,_panic
L246:pushl	r7
pushl	r7
pushl	$-2
calls	$3,_numshift
cvtbl	36(r10),r0
addl2	$2,r0
subl3	r0,$8,r8
jgeq	L250
pushal	L253
pushal	L252
pushal	L251
calls	$3,_panic
L250:ashl	r8,$1,-12(fp)
ashl	r8,$2,-16(fp)
movzbl	15(r9),r0
bisl2	-12(fp),r0
cvtlb	r0,15(r9)
movl	-8(fp),r0
bisb2	$32,L241[r0]
pushal	L241
pushl	r7
pushl	r7
calls	$3,_numaddv
movl	-8(fp),r0
bicb2	$32,L241[r0]
mcoml	-16(fp),r0
movzbl	15(r9),r1
bicl2	r0,r1
jeql	L254
incl	12(ap)
pushl	r7
pushl	r7
pushl	$1
jbr	L2000035
L254:pushl	r7
pushl	r7
pushl	$2
L2000035:calls	$3,_numshift
movl	$8,r0
cvtbl	36(r10),r1
subl2	r1,r0
ashl	r0,$1,r0
decl	r0
mcoml	r0,r0
movzbl	15(r9),r1
bicl2	r0,r1
cvtlb	r1,15(r9)
movl	12(ap),r0
ret

