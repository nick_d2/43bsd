s/@/ATSIGN/g
s/^/@/
s/$/@/
s/[^A-Za-z0-9_]\+/@&@/g

s/@movestr@/@memcpy@/g
s/@readonly@[ 	]*/@/g
s/@reg@/@register@/g
s/@register@[	 ]*@ptrall@/@ptrall@/g
s/@ptrall@/@void *CHECKME@/g
s/@shift@;/val = yylex()@;/g

s/@//g
s/ATSIGN/@/g

s/\[\]CHECKME\([	 ]*[A-Za-z_][0-9A-Za-z_]*\)\([	 ]*;\)/\1[]\2/g
s/\*CHECKME\([	 ]*\)\([A-Za-z_][0-9A-Za-z_]*[	 ]*;\)/\1*\2/g
s/CHECKME[	 ]*)/)/g
s/CHECKME[	 ]*$//g

# specific to this file because I'm lazy and don't want a 2 step process:
s/\*CHECKME	from, to;/	*from, *to;/
