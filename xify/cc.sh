#!/bin/sh

BIN="$0"
if ! echo $BIN |grep -q /
then
  BIN=`which $BIN`
fi
BIN=`dirname $BIN`

hostcpp=cpp
include="$BIN/../usr/include"
xify="$BIN/../lib/xify"
hostcc=gcc
ld="$BIN/ld"
rm=rm

export PATH=/bin:/usr/bin

clist=
llist=
plist=
cflag=
oflag=
pflag=
sflag=
Rflag=
exflag=
proflag=
fflag=
gflag=
Gflag=
Mflag=
debug=
dflag=
xflag=
nc=0
nxo=0
crt0="$BIN/../lib/crt0.o"

while test $# -ge 1
do
  case "$1" in
  -S)
    sflag=1
    cflag=1
    ;;
  -o)
    if test -z "$2"
    then
      echo "$0: error: -o where?" >&2
      exit 1
    fi
    outfile=$2
    shift
    if echo $outfile |grep -q '\.c$'
    then
      echo "$0: error: -o would overwrite $outfile" >&2
      exit 1
    fi
    ;;
  -R)
    Rflag=1
    ;;
  -O)
    oflag=1
    ;;
  -p)
    proflag=1
    crt0="$BIN/../lib/mcrt0.o"
    ;;
  -pg)
    proflag=1
    crt0="$BIN/../usr/lib/gcrt0.o"
    ;;
  -f)
    fflag=1
    ;;
  -go)
    Gflag=1
    ;;
  -g)
    gflag=1
    ;;
  -w)
    wflag=1
    ;;
  -E)
    exflag=1
    pflag=1
    plist="$plist $1" # this is ignored by 4.3bsd cpp
    ;;
  -P)
    pflag=1
    echo "$0: warning: -P option obsolete; you should use -E instead"
    plist="$plist $1"
    ;;
  -c)
    cflag=1
    ;;
  -M*)
    exflag=1
    pflag=1
    Mflag=1
    plist="$plist $1"
    ;;
  -[DIU]*)
    plist="$plist $1"
    ;;
  -C)
    plist="$plist $1"
    ;;
  -L*)
    llist="$llist $1"
    ;;
  # chpass and passname stuff not implemented
  -d)
    debug=1
    ;;
  -d*)
    dflag=$1
    ;;
  -X)
    xflag=1
    ;;
  -*)
    echo "$0: error: bad flag $1" >&2
    exit 1
    ;;
  *)
    t=$1
    if echo $t |grep -q '\.[cs]$' || test -n "$exflag"
    then
      clist="$clist $t"
      nc=`expr $nc + 1`
      t=`echo $t |sed -e 's/.$/o/'`
    fi

    dup=
    for i in $llist
    do
      if test "$i" = "$t"
      then
        dup=1
        break
      fi
    done
    if test -z "$dup"
    then
      llist="$llist $t"
      if echo $t |grep -q '\.o$'
      then
        nxo=`expr $nxo + 1`
      fi
    fi
    ;;
  esac
  shift
done

#echo "clist=$clist"
#echo "llist=$llist"
#echo "plist=$plist"
#echo "cflag=$cflag"
#echo "oflag=$oflag"
#echo "pflag=$pflag"
#echo "sflag=$sflag"
#echo "Rflag=$Rflag"
#echo "exflag=$exflag"
#echo "proflag=$proflag"
#echo "fflag=$fflag"
#echo "gflag=$gflag"
#echo "Gflag=$Gflag"
#echo "Mflag=$Mflag"
#echo "debug=$debug"
#echo "dflag=$dflag"
#echo "xflag=$xflag"
#echo "nc=$nc"
#echo "nxo=$nxo"
#echo "crt0=$crt0"

if test \( -n "$gflag" -o -n "$Gflag" \) -a -n "$oflag"
then
  echo "$0: warning: -g disables -O" >&2
  oflag=
fi

eflag=

if test -n "$clist"
then
  # signal stuff not implemented

  if test -z "$pflag"
  then
    tmp=`mktemp /tmp/ctmXXXXXX`
    rm $tmp # we only want $tmp.i, $tmp.s and so on
  else
    tmp=
  fi

  for i in $clist
  do
    if test $nc -gt 1 -a -z "$Mflag"
    then
      echo "$i:"
    fi

    if test -z "$Mflag" && echo $i |grep -q '\.s$'
    then
      tmp_s=
      assource=$i
    else
      if test -n "$pflag"
      then
        tmp_i=`echo $i |sed -e 's/.$/i/'`
      else
        tmp_i="$tmp.i"
      fi

      av="$hostcpp $i"
      if test -z "$exflag"
      then
        av="$av $tmp_i"
      fi
#      av="$av$plist"
      av="$av -nostdinc -DX_ -Dunix -Dvax$plist -I$include"
      if test -n "$debug"
      then
        echo $av
      fi
      if ! $av
      then
        cflag=1
        eflag=1
        continue
      fi
      if test -n "$pflag"
      then
        cflag=1
        continue
      fi

      av="$xify $tmp_i"
# a bit horrible because we cannot redirect sed output to $tmp.c conditionally
#      pattern='s/va_arg(x_argp, x_\(unsigned_\)\?\(short\|int\|long\))/_va_arg_\1\2(x_argp)/g'
#      if test -z "$xflag"
#      then
#        if test -n "$debug"
#        then
#          echo $av "|"sed -e "'$pattern'" ">"$tmp.c
#        fi
#        if ! $av |sed -e "$pattern" >$tmp.c
#        then
#          cflag=1
#          eflag=1
#          continue
#        fi
#      else
#        if test -n "$debug"
#        then
#          echo $av "|"sed -e "'$pattern'"
#        fi
#        if ! $av |sed -e "$pattern"
#        then
#          cflag=1
#          eflag=1
#          continue
#        fi
#        cflag=1
#        continue
#      fi
      if test -z "$xflag"
      then
        av="$av $tmp.c"
      fi
      if test -n "$debug"
      then
        echo $av
      fi
      if ! $av
      then
        cflag=1
        eflag=1
        continue
      fi
      $rm -f $tmp_i
      if test -n "$xflag"
      then
        cflag=1
        continue
      fi

      if test -n "$sflag"
      then
        if test $nc -eq 1 -a -n "$outfile"
        then
          tmp_s="$outfile"
        else
          tmp_s=`echo $i |sed -e 's/.$/s/'`
        fi
      else
# optimization: don't call compiler and assembler separately unless have -S
        tmp_s="$tmp.c"
        assource="$tmp_s"
# since we can't "goto" out of enclosing if, duplicate all code until "done"
    #    av="$as -o"
        av="$hostcc -o"
        if test -n "$cflag" -a $nc -eq 1 -a -n "$outfile"
        then
          av="$av $outfile"
        else
          av="$av `echo $i |sed -e 's/.$/o/'`"
        fi
    #    if test -n "$Rflag"
    #    then
    #      av="$av -R"
    #    fi
    #    if test -n "$dflag"
    #    then
    #      av="$av $dflag"
    #    fi
        av="$av -fno-strict-aliasing"
        if test -n "$proflag"
        then
          av="$av -pg"
        fi
        if test -n "$gflag" -o -n "$Gflag"
        then
          av="$av -g"
        fi
        if test -z "$wflag"
        then
          av="$av -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-maybe-uninitialized -Wno-parentheses -Wno-strict-aliasing -Wno-unused-result"
        fi
        av="$av -c $assource"
        if test -n "$debug"
        then
          echo $av
        fi
    #    if ! $av && $? -gt 1
        if ! $av
        then
          cflag=1
          eflag=1
          continue
        fi
        if test -n "$tmp_s"
        then
          $rm -f $tmp_s
        fi
        continue
# else
#        tmp_s="$tmp.s"
# end
      fi

#      if test -n "$fflag"
#        av="$sccom"
#      else
#        av="$ccom"
#      fi
#      if test -n "$oflag"
#      then
#        av="$av $tmp.c2s"
#      else
#        av="$av $tmp_s"
#      fi
#      if test -n "$proflag"
#      then
#        av="$av -XP"
#      fi
#      if test -n "$gflag"
#      then
#        av="$av -Xg"
#      elif test -n "$Gflag"
#      then
#        av="$av -XG"
#      fi
#      if test -n "$wflag"
#      then
#        av="$av -w"
#      fi
#      if test -n "$debug"
#      then
#        echo $av
#      fi
#      if ! $av
#      then
#        cflag=1
#        eflag=1
#        continue
#      fi
#      $rm -f $tmp_i
#
#      if test -n "$oflag"
#      then
#        av="$c2 $tmp.c2s $tmp_s"
#        if test -n "$debug"
#        then
#          echo $av
#        fi
#        if ! $av
#        then
#          $rm -f $tmp_s
#          tmp_s="$tmp.c2s"
#        else
#          $rm -f $tmp.c2s
#        fi
#      fi
      av="$hostcc -o $tmp_s -fno-strict-aliasing"
      if test -n "$proflag"
      then
        av="$av -pg"
      fi
      if test -n "$gflag" -o -n "$Gflag"
      then
        av="$av -g"
      fi
      if test -z "$wflag"
      then
        av="$av -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-maybe-uninitialized -Wno-parentheses -Wno-strict-aliasing -Wno-unused-result"
      fi
      av="$av -S $tmp.c"
      if test -n "$debug"
      then
        echo $av
      fi
      if ! $av
      then
        cflag=1
        eflag=1
        continue
      fi

#      if test -n "$sflag"
#      then
        continue
#      fi
#
#      assource="$tmp_s"
    fi

#    av="$as -o"
    av="$hostcc -o"
    if test -n "$cflag" -a $nc -eq 1 -a -n "$outfile"
    then
      av="$av $outfile"
    else
      av="$av `echo $i |sed -e 's/.$/o/'`"
    fi
#    if test -n "$Rflag"
#    then
#      av="$av -R"
#    fi
#    if test -n "$dflag"
#    then
#      av="$av $dflag"
#    fi
    av="$av -fno-strict-aliasing"
    if test -n "$proflag"
    then
      av="$av -pg"
    fi
    if test -n "$gflag" -o -n "$Gflag"
    then
      av="$av -g"
    fi
    if test -z "$wflag"
    then
      av="$av -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-maybe-uninitialized -Wno-parentheses -Wno-strict-aliasing -Wno-unused-result"
    fi
    av="$av -c $assource"
    if test -n "$debug"
    then
      echo $av
    fi
#    if ! $av && $? -gt 1
    if ! $av
    then
      cflag=1
      eflag=1
      continue
    fi
    if test -n "$tmp_s"
    then
      $rm -f $tmp_s
    fi
  done
fi

if test -z "$cflag" -a -n "$llist"
then
  av="$ld -X $crt0"
  if test -n "$outfile"
  then
    av="$av -o $outfile"
  fi
  if test -n "$gflag" -o -n "$Gflag"
  then
    av="$av -lg"
  fi
  av="$av$llist"
  if test -n "$proflag"
  then
    av="$av -lc_p"
  else
    av="$av -lc"
  fi
  if test -n "$debug"
  then
    av="$av -t"
    echo $av
  fi
  if ! $av
  then
    eflag=1
  elif test $nc -eq 1 -a $nxo -eq 1 -a -z "$eflag"
  then
    set $clist
    $rm -f `echo $1 |sed -e 's/.$/o/'`
  fi
fi

#echo "eflag=$eflag"
exit $eflag
