#!/bin/sh

BIN="$0"
if ! echo $BIN |grep -q /
then
  BIN=`which $BIN`
fi
BIN=`dirname $BIN`

hostcc=gcc
hostld=ld
mkdir=mkdir
ln=ln
rm=rm

export PATH=/bin:/usr/bin

outfile=
dirs=
files=
Mflag=
xflag=
Xflag=
Sflag=
rflag=
arflag=
sflag=
nflag=
Nflag=
zflag=
dflag=
trace=
ytab=
objs=

while test $# -ge 1
do
  case "$1" in
  -o)
    if test -z "$2"
    then
      echo "$0: error: -o where?" >&2
      exit 1
    fi
    outfile=$2
    shift
    ;;
  -M)
    Mflag=1
    ;;
  -x)
    xflag=1
    ;;
  -X)
    Xflag=1
    ;;
  -S)
    Sflag=1
    ;;
  -r)
    rflag=1
    arflag=1
    ;;
  -s)
    sflag=1
    xflag=1
    ;;
  -n)
    nflag=1
    Nflag=
    zflag=
    ;;
  -N)
    Nflag=1
    nflag=
    zflag=
    ;;
  -d)
    dflag=1
    ;;
  -i)
    echo "$0: warning: -i ignored" >&2
    ;;
  -t)
    trace=1
    ;;
  -y*)
    if test "$1" = "-y"
    then
      echo "$0: warning: -y: symbol name missing"
    else
      ytab="$ytab `echo $1 |sed -e 's/^-y//'`"
    fi
    ;;
  -z)
    zflag=1
    Nflag=
    nflag=
    ;;
  -L*)
    if test "$1" = "-L"
    then
      echo "$0: warning: -L: pathname missing"
    else
      dirs="$dirs `echo $1 |sed -e 's/^-L//'`"
    fi
    ;;
  -l*)
    if test "$1" = "-l"
    then
      echo "$0: warning: -l: library name missing"
    else
      files="$files $1"
    fi
    ;;
  -*)
    echo "$0: error: bad flag $1" >&2
    exit 1
    ;;
  *)
    files="$files $1"
    ;;
  esac
  shift
done

dirs="$dirs $BIN/../lib $BIN/../usr/lib $BIN/../usr/local/lib"

if test -z "$rflag" -a -z "$Nflag" -a -z "$nflag"
then
  zflag=1
fi

#echo "outfile=$outfile"
#echo "Mflag=$Mflag"
#echo "xflag=$xflag"
#echo "Xflag=$Xflag"
#echo "Sflag=$Sflag"
#echo "rflag=$rflag"
#echo "arflag=$arflag"
#echo "sflag=$sflag"
#echo "nflag=$nflag"
#echo "Nflag=$Nflag"
#echo "zflag=$zflag"
#echo "dflag=$dflag"
#echo "trace=$trace"
#echo "ytab=$ytab"
#echo "dirs=$dirs"
#echo "files=$files"

if test -n "$rflag"
then
  av="$hostld"
else
  av="$hostcc"
fi
if test -n "$outfile"
then
  av="$av -o $outfile"
fi
for i in $dirs
do
  av="$av -L$i/.xify"
done
if test -n "$rflag"
then
  av="$av -r"
fi
for i in $files
do
  case "$i" in
  -lnox_*)
    av="$av `echo $i |sed -e 's/^-lnox_/-l/'`"
    ;;
  -l*)
    j=`echo $i |sed -e 's/^-l//'`
    for k in $dirs
    do
      if test -f $k/lib$j.a
      then
        if ! test -f $k/.xify/libx_$j.a
        then
          if test -n "$trace"
          then
            echo $mkdir -p $k/.xify
          fi
          $mkdir -p $k/.xify
          if test -n "$trace"
          then
            echo $ln -s ../lib$j.a $k/.xify/libx_$j.a
          fi
          $ln -s ../lib$j.a $k/.xify/libx_$j.a
        fi
        break
      fi
      if test -n "$trace"
      then
        echo $rm -f $k/.xify/libx_$j.a
      fi
      $rm -f $k/.xify/libx_$j.a
    done
    av="$av -lx_$j"
    ;;
  *)
    av="$av $i"
    ;;
  esac
done
if test -n "$trace"
then
  echo $av
fi
$av
exit $?
