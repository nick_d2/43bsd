#!/bin/sh

if test $# -lt 1
then
  set Makefile
fi

while test $# -ge 1
do
  sed -e 's/	\(-\)\?cc /	\1${CC} /; s/	\(-\)\?ld /	\1${LD} /; s/	\(-\)\?yacc /	\1${YACC} /; s/	\(-\)\?lex /	\1${LEX} /; s/	\(-\)\?\/lib\/cpp /	\1${CPP} /; s/\([	 ]\)cc -M /\1${CC} -M /g; /^# DO NOT DELETE THIS LINE -- make depend uses it$/{N; q}' -i $1
  if grep -q '\${LD}' $1 && ! grep -q '^LD[	 ]*=' $1
  then
    sed -e '1iLD=ld' -i $1
  fi
  if grep -q '\${YACC}' $1 && ! grep -q '^YACC[	 ]*=' $1
  then
    sed -e '1iYACC=yacc' -i $1
  fi
  if grep -q '\${LEX}' $1 && ! grep -q '^LEX[	 ]*=' $1
  then
    sed -e '1iLEX=lex' -i $1
  fi
  if grep -q '\${CPP}' $1 && ! grep -q '^CPP[	 ]*=' $1
  then
    sed -e '1iCPP=/lib/cpp' -i $1
  fi
  shift
done
