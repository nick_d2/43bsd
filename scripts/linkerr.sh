#!/bin/sh
sed -ne "s/^x_\\(.*\\):([^()]*): undefined reference to \`x_\\(.*\\)'/\\2 \\1/p" |sort |sed -e 's/\(.*\) \(.*\)/\2 \1/' |uniq --skip-fields=1 |sed -e "s/\\(.*\\) \\(.*\\)/sed -e 's\\/^extern \\\\(.*[^A-Za-z0-9_]\\2[^A-Za-z0-9_].*\\\\)$\\/\\\\1\\/' -i \\1/"
