#!/bin/sh

ROOT=`dirname $0`/..
ROOT=`cd $ROOT && pwd`
SCRIPTS=$ROOT/scripts

$SCRIPTS/errfunc.sh |grep "‘return’ with a value, in function returning void" |sed -e 's/:.*//' |sort |uniq |sed -e 's/.*/s\/^\\(static \\)\\?void &\\( __P(\\)\\?(\/\\1int &\\2(\//'
