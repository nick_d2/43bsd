#!/bin/sh
ROOT=`dirname $0`/..
ROOT=`cd $ROOT && pwd`
STAGE0=$ROOT/stage0
STAGE1=$ROOT/stage1
PATH=$STAGE0/bin:$STAGE0/usr/bin:$PATH make DESTDIR=$STAGE1 "$@"
