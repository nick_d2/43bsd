#include <stdio.h>
#include <stdlib.h>

int main() {
  int c, d;

  while ((c = getchar()) != EOF) {
    if (c == '\'' || c == '"') {
      do {
        while ((d = getchar()) == '\\') {
          d = getchar();
          if (d == EOF)
            exit(1);
        }
        if (d == EOF)
          exit(1);
      } while (d != c);
    }
    else
      putchar(c);
  }
  return 0;
}
