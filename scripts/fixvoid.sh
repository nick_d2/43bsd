#!/bin/sh

ROOT=`dirname $0`/..
ROOT=`cd $ROOT && pwd`
SCRIPTS=$ROOT/scripts

if test $# -lt 1
then
  set *.c
fi

all_h=`echo *.h`
if test "$all_h" = "*.h"
then
  all_h=
fi

while test $# -ge 1
do
  o=`basename $1 .c`.o

  rm -f $o
  $SCRIPTS/make.sh $o 2>x
  $SCRIPTS/nonvoid.sh <x >y
  sed -f y -i $1 $all_h

  rm -f $o
  $SCRIPTS/make.sh $o 2>x
  $SCRIPTS/isvoid.sh <x >y
  sed -f y -i $1 $all_h

  shift
done
