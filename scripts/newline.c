#include <stdio.h>
#include <stdlib.h>

int main() {
  int c;

  while ((c = getchar()) == '\n')
    ;
  while (c != EOF) {
  loop:
    putchar(c);
    c = getchar();
    if (c == '\n') {
      putchar(c);
      c = getchar();
      if (c == '\n') {
        while ((c = getchar()) == '\n')
          ;
        if (c == EOF)
          break;
        putchar('\n');
        goto loop;
      }
    }
  }
  return 0;
}
