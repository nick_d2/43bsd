#!/bin/sh
ROOT=`dirname $0`/..
ROOT=`cd $ROOT && pwd`
INCLUDE=$ROOT/stage0/usr/include
grep -H "^$INCLUDE/$1$" *.uses |sed -e "s:\.uses\:.*: $1:" >>conflicts.txt
LC_ALL=C sort <conflicts.txt |uniq >a
mv a conflicts.txt
