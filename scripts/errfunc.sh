#!/bin/sh
prefix=
while read i
do
  func=`echo $i |sed -ne 's/.*: In function ‘x_\(.*\)’:$/\1/p'`
  if test -n "$func"
  then
    prefix="$func: "
  else
    echo "$prefix$i"
  fi
done
