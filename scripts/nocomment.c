#include <stdio.h>
#include <stdlib.h>

int main() {
  int c;

loop:
  while ((c = getchar()) != EOF) {
    while (c == '/') {
      c = getchar();
      if (c == '*') {
        while ((c = getchar()) != EOF) {
          while (c == '*') {
            c = getchar();
            if (c == '/')
              goto loop;
            if (c == EOF)
              exit(1);
          }
        }
      }
      else
        putchar('/');
      if (c == EOF)
        exit(0);
    }
    putchar(c);
  }
  return 0;
}
