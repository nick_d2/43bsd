#!/bin/sh

ROOT=`dirname $0`/..
ROOT=`cd $ROOT && pwd`
SCRIPTS=$ROOT/scripts

$SCRIPTS/errfunc.sh |grep "control reaches end of non-void function\|‘return’ with no value, in function returning non-void\|no return statement in function returning non-void" |sed -e 's/:.*//' |sort |uniq |sed -e 's/.*/s\/^\\(static \\)\\?\\(int \\)\\?&\\( __P(\\)\\?(\/\\1void &\\3(\//'
