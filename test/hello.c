#include <errno.h>
#include <stdio.h>

int main() {
	FILE *fp;

	printf("hello, %s %.*s %5.2f\n", "world", 2, "fun", 3.14159);
	fp = fopen("hello.txt", "w");
	if (fp == NULL) {
		perror("hello.txt");
		exit(1);
	}
	fprintf(fp, "hello, world\n");
	fclose(fp);
	return 0;
}
